<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Status_To_Complete</fullName>
        <field>Status__c</field>
        <literalValue>Complete</literalValue>
        <name>Update Status To Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Status Completed</fullName>
        <active>true</active>
        <formula>ISPICKVAL(Status__c , &apos;Active&apos;) &amp;&amp;  Vote_End__c &gt;   NOW()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Status_To_Complete</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Vote__c.Vote_End__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
