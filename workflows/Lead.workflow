<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Membership_Interest</fullName>
        <description>New Membership Interest</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Emails/Membership_Interest_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_notification_to_Qualify_User</fullName>
        <ccEmails>keinhaus@virtualinc.com</ccEmails>
        <description>Send notification to Qualify User</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Automated_Emails/LeadsFormassemblyQualifyResponse</template>
    </alerts>
    <fieldUpdates>
        <fullName>Conga_Trigger_Update_Field_Status</fullName>
        <field>Status</field>
        <literalValue>Waiting to be approved</literalValue>
        <name>Conga Trigger - Update Field Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Conga_Trigger_Counter_Sign_Outbound</fullName>
        <apiVersion>47.0</apiVersion>
        <endpointUrl>https://workflow.congamerge.com/OBMListener.ashx</endpointUrl>
        <fields>Conga_Trigger_Lead_Counter_Sign__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>keinhaus2@virtualinc.com</integrationUser>
        <name>Conga Trigger-Counter Sign Outbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Conga Trigger - Lead Counter Sign</fullName>
        <actions>
            <name>Conga_Trigger_Update_Field_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Conga_Trigger_Counter_Sign_Outbound</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Document Signed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
