<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Welcome_Flow_Field</fullName>
        <field>Welcome_Flow_Enabled__c</field>
        <literalValue>1</literalValue>
        <name>Update Welcome Flow Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Welcome Flow Field 120</fullName>
        <active>true</active>
        <formula>AND( 
Welcome_Flow_Enabled__c=FALSE, 
TEXT( Contact.Account.Association__r.Login_Flow_Renewal__c)=&apos;120&apos;
    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Welcome_Flow_Field</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>120</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Welcome Flow Field 30</fullName>
        <active>true</active>
        <formula>AND( 
Welcome_Flow_Enabled__c=FALSE, 
TEXT( Contact.Account.Association__r.Login_Flow_Renewal__c)=&apos;30&apos;
    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Welcome_Flow_Field</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Welcome Flow Field 365</fullName>
        <active>true</active>
        <formula>AND( 
Welcome_Flow_Enabled__c=FALSE, 
TEXT( Contact.Account.Association__r.Login_Flow_Renewal__c)=&apos;365&apos;
    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Welcome_Flow_Field</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>365</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Welcome Flow Field 60</fullName>
        <active>true</active>
        <formula>AND( 
Welcome_Flow_Enabled__c=FALSE, 
TEXT( Contact.Account.Association__r.Login_Flow_Renewal__c)=&apos;60&apos;
    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Welcome_Flow_Field</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Welcome Flow Field 90</fullName>
        <active>true</active>
        <formula>AND( 
Welcome_Flow_Enabled__c=FALSE, 
TEXT( Contact.Account.Association__r.Login_Flow_Renewal__c)=&apos;90&apos;
    )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Welcome_Flow_Field</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
