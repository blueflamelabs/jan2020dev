/*********************************************************************************************************************************************************
*   Version  CreateDate   CreatedBy               ModifiedDate   ModifiedBy  Description
*   1.0      241219       VennScience_BFL_Monali                             This is the Apex Trigger on ContentDocumentLink to update the visiblity . 
*	  
**********************************************************************************************************************************************************/
trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert) {
    
    // Check if before insert event 
    if(Trigger.isbefore && Trigger.isInsert) {
        ContentDocumentLinkHandler.filterContentDocumentLinkRecords(trigger.new);
    }

}