/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy  Description
*     1.0        041119         VennScience_BFL_Amruta                             This is the apex trigger for Vote object.                               
**********************************************************************************************************************************************************/
trigger VoteTrigger on Vote__c (after update) {
    // T-000776 - 041119 - VennScience_BFL_Amruta - Added before update criteria check
     if(Trigger.isUpdate && Trigger.isAfter) {
        // T-000776 - 041119 - VennScience_BFL_Amruta - Call handler method in order to filter the Vote records and create new Ballot records
        if(!VoteTriggerHandler.isRecursiveCall) {
            System.debug('Inside After Update event');
            VoteTriggerHandler.filterVoteRecords(Trigger.new, Trigger.oldMap);
        }
    } // End of if
}