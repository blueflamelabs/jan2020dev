/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy  Description
*     1.0        011119         VennScience_BFL_Amruta                             This is the apex trigger for User object.                               
**********************************************************************************************************************************************************/
trigger UserTrigger on User (after insert, after update) {
    if(Trigger.isInsert && Trigger.isAfter) {
        // Call handler method in order to add the new user to chatter group when its related contact is populated on any group member
        UserTriggerHandler.filterUserRecords(Trigger.new, new Map<Id,User>());
        //Call handler methord mail send to Community profile User
        CommunityUserMailSend.MailSendToUser(Trigger.new);
        if(!UserTriggerHandler.isUserTriggerRunned) {
            // Whenever new User will be created then pull data from related Contact and update it on User
            UserTriggerHandler.FilterUserToupdateFromContact(Trigger.new);
        }
    } // End of if
    if(Trigger.isUpdate && Trigger.isAfter) {
        // Call handler method in order to add the new user to chatter group when its related contact is populated on any group member
        UserTriggerHandler.filterUserRecords(Trigger.new, Trigger.oldMap);
        if(!UserTriggerHandler.isUserTriggerRunned) {
            // Whenever new User will be updated then update data to related Contact to keep them in sync
            UserTriggerHandler.filterUserRecordToUpdateContact(Trigger.new, Trigger.oldMap);
            CommunityUserMailSend.MailSendToUser(Trigger.new);
        }
        
        
    } // End of if
    
}