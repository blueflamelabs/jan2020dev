trigger AccountRecordShare on Account (before insert,after insert,after update,before update) {
    if(Trigger.isInsert && Trigger.isAfter) {
        AssociationRecordShareCTRL.AccoutRecordShare(Trigger.new);
    }
    
    //FormAssembly Field Update on Account Trigger Functionlity Start..... Ticket No -"T-000102"
    if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore){
        List<Account> objAccList = new List<Account>();
        String CorporateMemberRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId();
        String IndividualMemberRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
        for(Account objAcc : Trigger.new) {
        
            if(Trigger.isInsert && (objAcc.RecordTypeId == CorporateMemberRecTypeId || objAcc.RecordTypeId == IndividualMemberRecTypeId) && 
                        ((String.isNotBlank(objAcc.Company_Association_Membership__c) && objAcc.RecordTypeId == CorporateMemberRecTypeId) || (String.isNotBlank(objAcc.Association__c) && objAcc.RecordTypeId == IndividualMemberRecTypeId))){
                objAccList.add(objAcc);
            }
            
            if(Trigger.isUpdate){
                if((objAcc.RecordTypeId == CorporateMemberRecTypeId || objAcc.RecordTypeId == IndividualMemberRecTypeId) 
                    && ((String.isNotBlank(objAcc.Company_Association_Membership__c) && objAcc.Company_Association_Membership__c != Trigger.oldMap.get(objAcc.id).Company_Association_Membership__c && objAcc.RecordTypeId == CorporateMemberRecTypeId)
                        || ((String.isNotBlank(objAcc.Association__c) && objAcc.Association__c != Trigger.oldMap.get(objAcc.id).Association__c && objAcc.RecordTypeId == IndividualMemberRecTypeId)))){
                    objAccList.add(objAcc);
                }
            }
        }
        if(objAccList.size() > 0){
            System.debug('####objAccListobjAccList#####3 '+objAccList);
            FormAssemblyIdUpdateAccount.listOfAccounts(objAccList);
        }
    }
    //FormAssembly Field Update on Account Trigger Functionlity End.....Ticket No -"T-000102"
    
    //Group Member Record Upsert Functionlity Start .... Ticket No - "T-000884"
    if(Trigger.IsInsert && Trigger.IsAfter){
        WorkingGroupMemberHandler.insertGroupMember(Trigger.new);
    }
    
    if(Trigger.IsUpdate && Trigger.IsAfter){
        List<Account> objAccList = new List<Account>();
        for(Account objAcc : Trigger.New) {
            if(objAcc.Board_Member__pc != Trigger.oldMap.get(objAcc.id).Board_Member__pc){
                objAccList.add(objAcc);
            }
        }
        if(objAccList.size() > 0) {
           WorkingGroupMemberHandler.updateGroupMember(objAccList);   
        }

        // 261119 - T-000876 - VennScience_BFL_Amruta - Call AccountTriggerHandler so as to sync the Person Contact with related User
        if(!AccountTriggerHandler.isAccountTriggerRunned) {
            System.debug('Account trigger has been fired');
            AccountTriggerHandler.filterPersonAccountRecords(Trigger.new, Trigger.oldMap);
        }
        
    }
    //Group Member Record Upsert Functionlity End .... Ticket No - "T-000884"
}