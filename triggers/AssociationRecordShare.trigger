trigger AssociationRecordShare on Association_Team__c (after insert,after update, before delete, after delete) {
    if(Trigger.isInsert && Trigger.isAfter){
        
        AssociationRecordShareCTRL.recordShare(Trigger.new);
        // T-000736 - 311019 - VennScience_BFL_Amruta - Add association team member in related chatter group
        AddAssociationTeamIntoChatterGroup.filterAssociationTeamRecords(Trigger.new, new Map<Id,Association_Team__c>(), false);
    }
    if(Trigger.isUpdate && Trigger.isAfter){
             
        List<Association_Team__c> objAssTeamList = new List<Association_Team__c>();
        for(Association_Team__c objAssTeam : Trigger.new){
            if(String.isNotBlank(objAssTeam.User__c) && objAssTeam.User__c != Trigger.oldMap.get(objAssTeam.id).User__c){
                objAssTeamList.add(objAssTeam);
            }
        }
        if(objAssTeamList.size() > 0){
            // @Reminder: Uncomment below line
            //AssociationRecordShareCTRL.recordShare(objAssTeamList);
        }
        
        // T-000736 - 311019 - VennScience_BFL_Amruta - Add association team member in related chatter group
        AddAssociationTeamIntoChatterGroup.filterAssociationTeamRecords(Trigger.new,Trigger.oldMap, false);
    }
    if(Trigger.isDelete && Trigger.isAfter){
        AssociationRecordShareCTRL.RecordDelete(Trigger.Old);
        // T-000736 - 311019 - VennScience_BFL_Amruta - Remove association team member in related
        AddAssociationTeamIntoChatterGroup.filterAssociationTeamRecords(Trigger.old, new Map<Id,Association_Team__c>(), true); 
    }
}