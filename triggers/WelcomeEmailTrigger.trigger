/*
    Created By - Manas
    Created Date - 26/11/2019
    Description - Call WelcomeEmailCTRL Class handle Duplicate Error.
*/
trigger WelcomeEmailTrigger on Welcome_Emails__c (before insert,before update) {
    if(Trigger.isBefore && Trigger.isInsert){
        WelcomeEmailCTRL.DuplicateWelcomEmail(Trigger.new);
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        List<Welcome_Emails__c> objWelList = new List<Welcome_Emails__c>();
        for(Welcome_Emails__c objWel : Trigger.new){
            if((String.isNotBlank(objWel.Association__c) && objWel.Association__c != Trigger.oldMap.get(objWel.Id).Association__c) || 
                    (String.isNotBlank(objWel.Member_Type__c) && objWel.Member_Type__c != Trigger.oldMap.get(objWel.Id).Member_Type__c)){
                objWelList.add(objWel);
            }
        }
        if(objWelList.size() > 0){
            WelcomeEmailCTRL.DuplicateWelcomEmail(objWelList);        
        }
    }
}