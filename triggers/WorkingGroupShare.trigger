trigger WorkingGroupShare on Working_Group__c (after insert, after update, before delete) {
    
    
    // T-000736 - 241019 - VennScience_BFL_Amruta - Check if this is an after insert event
    if(Trigger.isInsert && Trigger.isAfter) {
        AssociationRecordShareCTRL.WorkingGroupRecord(Trigger.new);
        //Ticket - T-000884 Manas
        WorkingAccountGroupMemberHandler.insertWorkingGroup(Trigger.new);
        // Call handler method in order to filter the Working Group records and create chatter groups
        WorkingGroupTriggerHandler.filterWorkingGroupRecords(Trigger.new, new Map<Id,Working_Group__c>());
    } // End of if
    // T-000736 - 241019 - VennScience_BFL_Amruta - Check if this is an after update event
    if(Trigger.isUpdate && Trigger.isAfter) {
        AssociationRecordShareCTRL.WorkingGroupRecord(Trigger.new);
        //Ticket - T-000884 Manas   
        List<Working_Group__c> objWorkGroupList = new List<Working_Group__c>();
        for(Working_Group__c objWorkGRP : Trigger.new){
            if(objWorkGRP.Board_Member_Group__c != Trigger.oldMap.get(objWorkGRP.id).Board_Member_Group__c && objWorkGRP.Board_Member_Group__c == true){
                objWorkGroupList.add(objWorkGRP);
            }
        }
        if(objWorkGroupList.size() > 0){
            WorkingAccountGroupMemberHandler.updateWorkingGroup(objWorkGroupList);
        }
        // Check the boolean variable to handle recursion
        if(!WorkingGroupTriggerHandler.isRecursiveCall) {
            // Call handler method in order to filter the Working Group records and create chatter groups
            WorkingGroupTriggerHandler.filterWorkingGroupRecords(Trigger.new, Trigger.oldMap);
        } // End of if
    } // End of if
    // T-000736 - 241019 - VennScience_BFL_Amruta - Check if this is a before delete event
    if(Trigger.isDelete && Trigger.isBefore) {
        // Call handler method in order to delete the related chatter groups
        WorkingGroupTriggerHandler.deleteChatterGroup(Trigger.old);
    } // End of if
}