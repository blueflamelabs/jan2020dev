/*********************************************************************************************************************************
*   Version  CreateDate   CreatedBy               ModifiedDate   ModifiedBy  Description
*   1.0      121119       VennScience_BFL_Amruta                             This is the apex trigger for AssociationTriggerHandler object.                               
************************************************************************************************************************************/

trigger AssociationTrigger on Association__c (After Update) {
    // T-00882 - 121119 - VennScience_BFL_Amruta - Check if this is an after update event
	 if(Trigger.isUpdate && Trigger.isAfter) {
        // Call handler method in order to change the related chatter group's owner when Association's owner is changed
        AssociationTriggerHandler.filterAssRecords(Trigger.new, Trigger.oldMap);
     } // End of if               
}