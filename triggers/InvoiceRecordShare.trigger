trigger InvoiceRecordShare on Invoice__c (after insert,after update) {
    AssociationRecordShareCTRL.InvoiceRecord(Trigger.new);
    
    // 261119 - T-000934 - VennScience_BFL_Amruta - Check if this is an after update event
    if(Trigger.isAfter && Trigger.IsUpdate) {
        // Call handler method so as to send Invoice as attachment to related Lead
        InvoiceTriggerHandler.filterInvoiceRecords(Trigger.new, Trigger.oldMap);
    } // End of if
}