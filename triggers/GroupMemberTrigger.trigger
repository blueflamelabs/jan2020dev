/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy  Description
*     1.0        241019         VennScience_BFL_Amruta                             This is the apex trigger for Group Member object.                               
**********************************************************************************************************************************************************/
trigger GroupMemberTrigger on Group_Member__c (after insert, after update, before delete, after delete) {
    if(Trigger.isInsert && Trigger.isAfter) {
        // Call handler method in order to filter the group Member records and add the members to Working grp's chatter grp
        GroupMemberTriggerHandler.filterGroupMemberRecords(Trigger.new, new Map<Id,Group_Member__c>());
        // 181219 - T-000974 - VennScience_BFL_Amruta - Call handler method in order to send group request/rejection notifications
        GroupMemberTriggerHandler.filterGroupMemberRecordsToSendNotifications(Trigger.new, new Map<Id,Group_Member__c>());
    } // End of if
    if(Trigger.isUpdate && Trigger.isAfter) {
        // Call handler method in order to filter the group Member records and add/remove the members to/from 
        // Working grp's chatter grp
        GroupMemberTriggerHandler.filterGroupMemberRecords(Trigger.new, Trigger.oldMap);
        // 181219 - T-000974 - VennScience_BFL_Amruta - Call handler method in order to send group request/rejection notifications
        GroupMemberTriggerHandler.filterGroupMemberRecordsToSendNotifications(Trigger.new, Trigger.oldMap);
    } // End of if
    if(Trigger.isDelete && Trigger.isAfter) {
        // Call handler method in order to remove the group member from Working grp's chatter groups
        GroupMemberTriggerHandler.deleteMembersFromChatterGroup(Trigger.old);
        // Call handler to delete related working group share records
        GroupMemberTriggerHandler.filterToDeleteShareRecord(Trigger.old);
    } // End of if
}