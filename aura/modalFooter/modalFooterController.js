({
	
    handleClose : function(component, event, helper) {
	component.find("overlayLib").notifyClose();
    },
    
    handleSave : function(component, event, helper) {
       
        var appEvent = $A.get("e.c:saveAll");
        appEvent.setParams({ "message" : "save" });
		appEvent.fire();
    },
    
    handleValidation : function(component, event, helper) {
        console.log('iniside handleValidation====');
        var validate = component.get("v.validateCount");
        var file = component.get("v.fileCount")
        console.log("1",validate, file, event.getParam("message"));
        if(event.getParam("message") == "save"){
            validate++;
            component.set("v.validateCount",validate);
        }
        console.log("2",validate, file);
        if(validate == file){
            component.find("overlayLib").notifyClose();
            window.setTimeout(
                $A.getCallback(function() {
                    var appEvent = $A.get("e.c:refreshRelatedList");
                    appEvent.fire();
            }),1000);

            console.log("close");
        }
        console.log("3",validate, file);
    }
})