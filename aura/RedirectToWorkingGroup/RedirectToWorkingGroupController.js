({
	doInit : function(component, event, helper) {
		//console.log('doInit called');
        //console.log('recordId====',component.get("v.recordId"));
        
        var action = component.get("c.getRelatedWorkingGroup");
        action.setParams({
            'chatterGroupId': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS') {
                var resValue = response.getReturnValue();
                //console.log('resValue member category -',resValue);
                var checkId = resValue.includes("Please");
                //console.log('checkId=======',checkId);
                if(!checkId) {
                    // Navigate to working group's detail page
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                      "recordId": resValue
                    });
                    navEvt.fire();
                } else {
                    component.set("v.errorMessage", resValue);
                    component.set("v.isNavigation", false);
                }
            }
        });
        $A.enqueueAction(action);
	},
})