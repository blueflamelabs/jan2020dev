({
	fetchData : function(cmp,event,helper,reload) {
        // Load all file data
        var fileIds = cmp.get("v.fileIds");
        var action = cmp.get("c.getAllFiles");
        action.setParams({  
            fileIds : fileIds  
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('filesList response',response.getReturnValue());
                cmp.set("v.records", response.getReturnValue());
                console.log('records  --',cmp.get("v.records"));
                console.log("Data Fetched");
            }
            else {
                console.log("Fetch Fail");
            }
           
        });
         $A.enqueueAction(action);
	},
    
    getUploadedData : function(cmp,event,helper,reload) {
        // Load all file data
        var fileIds = cmp.get("v.fileIds");
        var action = cmp.get("c.getAllUploadedFiles");
        action.setParams({ 
            parentId : cmp.get('v.recordId'),
            fileIds : fileIds  
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('filesList response'+response.getReturnValue());
                cmp.set("v.records", response.getReturnValue());
                console.log('records  --',cmp.get("v.records"));
                console.log("Data Fetched");
            }
            else {
                console.log("Fetch Fail");
            }
           
        });
         $A.enqueueAction(action);
	},
    
    getfolder : function(cmp,event,helper,reload) {
        
        var action = cmp.get("c.getAllFolder");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('folder response',response.getReturnValue());
                cmp.set("v.folderList", response.getReturnValue());
                console.log('allFolder  --',cmp.get("v.folderList"));
                console.log("allFolder Fetched");
            }
            else {
                console.log("Fetch Fail");
            }
           
        });
         $A.enqueueAction(action);
	},
})