({
	handleOnChange : function(component, event, helper) {
		console.log('Inside Handle change');
        console.log('value======',event.getSource().get("v.value"));
        var getUserName = event.getSource().get("v.value");
        var action = component.get("c.getCommunityUsers");
        action.setParams({
            'userName': getUserName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS') {
                var resValue = response.getReturnValue();
                console.log('resValue=======',resValue);
            }
        });
        $A.enqueueAction(action);
	}
})