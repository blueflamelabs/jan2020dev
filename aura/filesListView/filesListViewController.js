({
	doInit : function(component, event, helper) {
		helper.doInit(component, event, helper);
        helper.getCurrentUrl(component, event, helper);
        helper.getUserAccess(component, event, helper);
	},
    handleRecordUpdated: function(component, event, helper) {
        helper.doInit(component, event, helper);
	},
    
    cancel: function(component, event, helper) {
       component.set("v.isEdited",false);
   	 helper.doInit(component, event, helper);

	},
    
    handleViewAllClick   : function(component, event, helper) {
        
		var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        relatedListEvent.setParams({
            "relatedListId": "AttachedContentDocuments",
            "parentRecordId": component.get("v.recordId")
        });
        relatedListEvent.fire();
    },
        
    closeListModal: function(component, event, helper) {
      // Set isModalOpen attribute to false  
      component.set("v.isListModalOpen", false);
   },
})