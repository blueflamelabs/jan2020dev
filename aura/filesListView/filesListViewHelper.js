({
	doInit : function(component,event,helper) {
        let action = component.get("c.getFilesClone");
        
        action.setParams({'parentId':component.get("v.recordId")});
        
        action.setCallback(this,(response)=>{
            const state = response.getState();
            console.log(state,response.getReturnValue());
            console.log('get on load response--',response.getReturnValue());
            if(state === 'SUCCESS') {
            let recordMap = response.getReturnValue();
            component.set("v.isCommunityUser",recordMap.isCommunityUser);
            component.set("v.files",recordMap.files);
        } else if(state === 'ERROR'){
            
        }
    });
    $A.enqueueAction(action);
	},
 
 getUserAccess : function(component, event, helper) {
    console.log('parentid----',component.get('v.recordId'));
    var action = component.get("c.getPublisherUser");
    action.setParams({
        "parentId": component.get('v.recordId')
    });
    action.setCallback(this, function(response) {
        var state = response.getState();
        if( state === 'SUCCESS') {
            var responseValue = response.getReturnValue();
            console.log('responseValue current user access -',responseValue);  
            component.set('v.isUserPublishAccess',responseValue);
            console.log('file upload user access-',component.get("v.isUserPublishAccess"));
        }
    });
    $A.enqueueAction(action);
},
 
    showToast : function(component, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },
        
    showSpinner : function(component,event,helper) {
        if($A.util.hasClass(component.find('spinner'),'slds-hide')){
            $A.util.removeClass(component.find('spinner'),'slds-hide');
        }
    },
        
    hideSpinner : function(component,event,helper) {
        if(!$A.util.hasClass(component.find('spinner'),'slds-hide')){
            $A.util.addClass(component.find('spinner'),'slds-hide');
        }
    },  

     getCurrentUrl : function(component, event, helper){
         var action = component.get("c.getPathPrefix");
         action.setCallback(this, function(response) {
         var state = response.getState();
             if (state === "SUCCESS") {
                 var storeResponse = response.getReturnValue();
                  console.log('respone---',storeResponse);
                    if($A.util.isEmpty(storeResponse)  ||  storeResponse == null){
                        component.set('v.displayColumn',true);
                    } else {
                        component.set('v.displayColumn',false);
                    }
                }
         });
         $A.enqueueAction(action);
      },     
})