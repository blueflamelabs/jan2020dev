({
    // Handle component initialization
    doInit : function(component, event, helper) {
        
        //helper.getFeeds(component, event, helper);
        helper.doInit(component, event, helper);
        helper.getUserAccess(component, event, helper);
        helper.getNetworkId(component, event, helper);
        helper.getAllfeedRecords(component, event, helper);
        helper.getRelatedMembers(component, event, helper);
        helper.userAnnouncementAccess(component, event, helper);
        helper.getAnnounceFeed(component, event, helper);
        var date = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD hh:mm:ss a");
        console.log('todaydate====',date);

        component.set('v.expireDate',date);
        
        var currentDateTime = $A.localizationService.formatDateTimeUTC(new Date(), "MMMM dd yyyy, hh:mm a");
        console.log('====currentDateTime====',currentDateTime);
        component.set('v.currentDate',currentDateTime);
        
        var urlString = window.location.href;
        console.log('urlString -- ',urlString);
        var urlValue = urlString.substring(0,urlString.indexOf("/r"));
        console.log('url -- ',urlValue);
        var baseURL = urlString.substring(0, urlString.indexOf("/s"));
        console.log('baseURL -- ',baseURL);
        component.set('v.baseurl',baseURL);
        //this.postTabChange(component, event, helper);
        //var posttab = component.find('postId');
        //$A.util.addClass(posttab, 'slds-active');
    },
    
    showSpinner: function(component, event, helper) {
        // remove slds-hide class from mySpinner
        var loadFeedList = component.get('v.feedWraperList');
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // add slds-hide class from mySpinner    
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    
    toggleVisibility : function(component, event, helper) {
        var RelatedTo=component.get("v.userValue");
        if (RelatedTo==null || RelatedTo.length==0) {
            var ddDiv = component.find('ddId');
            $A.util.toggleClass(ddDiv,'slds-is-open');
            console.log("Toggle");
        }
        else
            console.log('We will not toggle as the value is selected');
    },
    
    itemSelected : function(component, event, helper) {
        
        console.log('We are in Select function');
        var target = event.target;   
        var SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");  
        if(SelIndex){
            console.log('inside if');
            var selectedOption = component.get("v.GroupMemeberList");
            var selItem = selectedOption[SelIndex];
            if(selItem.label){
                console.log('Selected Value : ',selItem.label);
                component.set("v.userValue",selItem.label);
            }
        }
    },
    
    serverCall : function(component, event, helper) {  
        var target = event.target;  
        var RelatedTo = target.value; 
        var PreviousRelatedValue=component.get("v.userValue");
        if (RelatedTo.length==0 || RelatedTo==null)
        {
            var ddDiv = component.find('ddId');
            $A.util.toggleClass(ddDiv,'slds-is-open');
            console.log('Value in input box is empty');
        }
        else if ((PreviousRelatedValue==undefined && (RelatedTo.length>0)) ||
                 ((PreviousRelatedValue!=undefined) && (PreviousRelatedValue.length==0) && (RelatedTo.length>0)))
        {
            var ddDiv = component.find('ddId');
            $A.util.toggleClass(ddDiv,'slds-is-open');
        }
            else
            {
                console.log('Value in the Input Box:',RelatedTo);
            }
        component.set("v.userValue",RelatedTo);
        console.log('Value in Variable :',component.get("v.userValue"));
    },
    
    handleValueChangeParent:function(component, event, helper) {
        
        console.log("==handleValueChangeParent value: " + component.get('v.feedValue'));
        let inputText = component.find("inputTextVal").get("v.value"); 
        if(inputText.length > 1){  
            component.set('v.isButtonActiveParent',false);
        }else{
            component.set('v.isButtonActiveParent',true);
        }
    },
    
    doHandleinputValue : function(component, event, helper) {
        //let inputText = component.find("inputVal").get("v.value");
        var searchString = component.find("inputVal").get("v.value");
			console.log('searchString --- ',searchString);
            if(!$A.util.isUndefinedOrNull(searchString)){
               searchString = searchString.trim(); 
            }
            var allRecords = c.get('v.GroupMemeberList1');
            var filteredRecords = [];
            var displayedFields = [];
            displayedFields.push('Name');
            if (!$A.util.isEmpty(searchString) && !$A.util.isUndefinedOrNull(searchString)) {
                for (var i = 0; i < allRecords.length; i++) {
                    var allRecord = allRecords[i];
                    var isFound = false;
                    for (var key in allRecord) {
                        if (allRecord.hasOwnProperty(key)) {
                            console.log('allRecord[key])>>',allRecord[key]);
                            if(allRecord[key] !== null && allRecord[key] !== undefined) //remove toString error
                                if ((((allRecord[key]).toString()).toLowerCase()).indexOf(searchString.toLowerCase()) > -1 && displayedFields.indexOf(key) > -1) {
                                    isFound = true;
                                }
                        }
                    }
                    if (isFound) {
                        filteredRecords.push(allRecord);
                    }else {
                        var noRecordMsg = 'No Record found.';
                        //c.set('v.noRecord', noRecordMsg);
                    }
                }
                console.log('filteredRecords ',filteredRecords);
                c.set('v.GroupMemeberList1', filteredRecords);
                
            } else {
                //c.set('v.recordList', c.get('v.usersTempWrapper'));
                
            }
        
    },
    handleValueChangeChild:function(component, event, helper){
        
        console.log("==handleValueChangeChild value: " + component.get('v.feedValueComment'));
        let inputText = component.find("inputTextVal").get("v.value"); 
        console.log("==handleValueChangeChild inputText: " + inputText);
        if(inputText.length > 1){  
            component.set('v.isButtonActiveChild',false);
        }else{
            component.set('v.isButtonActiveChild',true);
        }
    },

    onChangeType : function(component, event, helper) {
        var typeSelect = component.find("typeSelect");
        var type = typeSelect.get("v.value");
        component.set("v.type", type);

        // Dynamically create the feed with the specified type
        $A.createComponent("forceChatter:feed", {"type": type}, function(feed) {
            var feedContainer = component.find("feedContainer");
            feedContainer.set("v.body", feed);
        });
    },
    
	openModel: function(component, event, helper) {
		// for Display Model,set the "isOpen" attribute to "true"
		console.log('button click');
		component.set("v.communityVal", true);
	},
    
	opentextBox : function(component, event, helper) {
		// for Display Model,set the "isOpen" attribute to "true"
		console.log('button click to open text');
		console.log('====Initialzsed filelist =========');
		component.set("v.fileList", []);
		component.set("v.fileIds", []);
		console.log('====Initialzsed filelist =========',component.get("v.fileList"));
		component.set("v.isOpenTextBox", true);

		//helper.openFileupload(component, event, helper);
	},
	
    onshareRecord : function(component, event, helper) {
        
        helper.createFeedRecord(component, event, helper, component.get("v.fileIds"));
        //helper.getUploadedFiles(component, event, helper);
    },
    
    openboxforAddUser : function(component, event, helper) {
        component.set('v.isSearch',true);
        
        //component.set('v.isSearch1',true);
    },
	
    openboxforAddUser1 : function(component, event, helper) {
        //component.set('v.isSearch',true);
        component.set('v.isSearch1',true);
    },
    
    openboxforEdit : function(component, event, helper) {
        //component.set('v.isSearch',true);
        component.set('v.editComment',true);
    },
    
    handleCommentClick : function(component, event, helper) {
        console.log('=====Comment button clicked handleCommentClick ========');
        console.log('on comment button click');
        var selectedFeedId= event.getSource().get("v.value");
        console.log('selectedFeedId===',selectedFeedId);
        var feedWraperList = component.get('v.feedWraperList');
        console.log('feedWraperList===',feedWraperList);
        for(var i=0; i< feedWraperList.length; i++) {
            console.log('inside for');
            var feedItemWrapListId = feedWraperList[i].feedItemWrapList[0].Id;
            console.log('feedrecord commnt opn--',feedItemWrapListId);
            if(selectedFeedId == feedItemWrapListId) {
                console.log('inside for if');
                console.log('feedWraperList[i].isChildCommentPost==', feedWraperList[i].isChildCommentPost);
                if(feedWraperList[i].isChildCommentPost == false){
                feedWraperList[i].isChildCommentPost = true;
                console.log('feedWraperList[i].isChildCommentPost==', feedWraperList[i].isChildCommentPost);
                }else if(feedWraperList[i].isChildCommentPost == true){
                    feedWraperList[i].isChildCommentPost = false;
                }
            }
        }
        console.log('feedrecord commnt opn-feedWraperList-',feedWraperList);
        component.set("v.feedValue",'');
        component.set("v.feedWraperList", feedWraperList);
        component.set("v.isOpenRichTextBox", true);
        console.log('========Open rich text box=========');
        //helper.openFileupload1(component, event, helper);
        //helper.createChildPost(component, event, helper, );
    },
    
    onClickshareChildPost : function(component, event, helper) {
        console.log('=====Comment button clicked onClickshareChildPost ========');
        var selectedFeedId= event.getSource().get("v.value");
        console.log('selectedFeedId===',selectedFeedId);
        var wrapperlist = component.get('v.feedWraperList');
        console.log('wrapperlist child comment --',wrapperlist);
        var commentBody = '';
        for(var i=0; i<wrapperlist.length; i++) {
            if(wrapperlist[i].feedItemWrapList[0].Id == selectedFeedId) {
                commentBody = wrapperlist[i].newChildCommentPost;
                console.log('commentBody --',commentBody);
            }
        }
        var action = component.get("c.getFilesClone");
        action.setParams({
            'parentId':component.get("v.recordId")
        });
        action.setCallback(this,(response)=>{
            const state = response.getState();
            console.log(state,response.getReturnValue());
            console.log('get on load response files--',response.getReturnValue());
            if(state === 'SUCCESS') {
				let recordMap = response.getReturnValue();
				component.set("v.isCommunityUser",recordMap.isCommunityUser);
				component.set("v.files",recordMap.files);
				helper.createChildFeedPost(component, event, helper, selectedFeedId, commentBody);//component.get("v.files")
			} else if(state === 'ERROR'){
				
			}
		});
		$A.enqueueAction(action);
	},
    
    handleLikeClick : function(component, event, helper) {
        console.log('on like button click');
        var selectedFeedId= event.getSource().get("v.value");
		console.log('selectedFeedId=for like==',selectedFeedId);
		helper.createFeedLike(component, event, helper, selectedFeedId);
	},
	
    handleRemoveLike : function(component, event, helper) {
        console.log('on like button click');
        var selectedFeedId= event.getSource().get("v.value");
        console.log('selectedFeedId=for like==',selectedFeedId);
        helper.deleteFeedLike(component, event, helper, selectedFeedId);
    },
        
    handleSelect : function(component, event, helper) {
        console.log('hanle select');
        var selectedMenuItemValue = event.getParam("value");
        //alert("Menu item selected with value: " + selectedMenuItemValue);
        //component.set("v.sourceObj", event.currentTarget.name);
        if(selectedMenuItemValue === 'Edit'){
            var selectedFeedId= event.getSource().get("v.value");
            console.log('selectedFeedId===',selectedFeedId);
            component.set("v.selectedFeedId",selectedFeedId);
            var selectedFeedbody= event.getSource().get("v.class");
            console.log('selectedFeedId===',selectedFeedbody);
            component.set("v.editFeedItemBody",selectedFeedbody);
            component.set("v.isEditFeedItems",true);
            //helper.openFileupload2(component, event, helper);
            
            //helper.toggleEditSave(component, true, selecteduserId );
            //helper.toEditUserRecord(component, event, helper, selecteduserId);
        }else if(selectedMenuItemValue == 'Delete') {
            var selectedFeedId= event.getSource().get("v.value");
            console.log('selectedFeedId===',selectedFeedId);
            helper.deleteFeedItemReord(component, event, helper, selectedFeedId);
        }
    },
    
    closeModel : function(component, event, helper) {
        component.set("v.isEditFeedItems",false);
    },
	
    saveFeedRecord : function(component, event, helper) {
        helper.editFeedRecord(component, event, helper);
    },
	
    handleClickChildComments : function(component, event, helper) {
        console.log('click more comments.');
        //console.log('click more comments.');
        var selectedFeedId= event.getSource().get("v.value");
        console.log('slected id: ' + selectedFeedId);
        var feedWraperList = component.get('v.feedWraperList');
        console.log('feedWraperList=116==',feedWraperList);
        for(var i=0; i< feedWraperList.length; i++) {
            console.log('inside for');
            var feedItemWrapListId = feedWraperList[i].feedItemWrapList[0].Id;
            console.log('feedrecord commnt opn--',feedItemWrapListId);
            if(selectedFeedId == feedItemWrapListId) {
                console.log('inside for if');
                feedWraperList[i].isMoreComments = false;
                feedWraperList[i].isDisplayComments = true;
                console.log('feedWraperList[i].isChildCommentPost==', feedWraperList[i].isMoreComments);
            }
        }
        component.set('v.closeDiv', false);
        console.log('feedrecord commnt opn-feedWraperList-',feedWraperList);
        component.set("v.feedWraperList", feedWraperList);
        //component.set('v.isMoreComments',true);
    },
         
    loadMoreComments : function(component, event, helper) { 
        console.log('=========loadMoreComments==========');
        var counter = component.get('v.counter');
        console.log('=========counter==========', counter);
        var incrementedcounter = counter + 1;
        console.log('=========incrementedcounter==========', incrementedcounter);
        component.set('v.counter',incrementedcounter);
        
        var originalWeedWraperList = component.get('v.originalWeedWraperList');
        var wrapperToDisplay = [];
        var counter = component.get('v.counter');
        console.log('==counter======', counter);
        console.log('==originalWeedWraperList.length======', originalWeedWraperList.length);
        if(originalWeedWraperList.length > 10) {
            for(var i=0; i<10*counter; i++) { 
                wrapperToDisplay.push(originalWeedWraperList[i]);
            }
        } else {
            component.set('v.isLoadMoreComments',false);
            for(var i=0; i<originalWeedWraperList.length; i++) { 
                wrapperToDisplay.push(originalWeedWraperList[i]);
            }
        } 
        
        if(wrapperToDisplay.lenth > 10*counter){
            component.set('v.isLoadMoreComments',true);
        } else {
            component.set('v.isLoadMoreComments',false);
        }
        console.log('==wrapperToDisplay======', wrapperToDisplay);
        
        component.set('v.feedWraperList',wrapperToDisplay);
        
	},
    
    handleEditDelete : function(component, event, helper) {
        var selectedMenuItemValue = event.getParam("value");
        //alert("Menu item selected with value: " + selectedMenuItemValue);
        //component.set("v.sourceObj", event.currentTarget.name);
        if(selectedMenuItemValue === 'Edit'){
            var selectedFeedId= event.getSource().get("v.value");
            console.log('selectedFeedId===',selectedFeedId);
            component.set("v.selectedFeedId",selectedFeedId);
            var selectedFeedbody= event.getSource().get("v.class");
            console.log('selectedFeedId===',selectedFeedbody);
            component.set("v.editFeedItemBody",selectedFeedbody);
            component.set("v.isEditFeedItems",true);
            helper.openFileupload2(component, event, helper);
            
            //helper.toggleEditSave(component, true, selecteduserId );
            //helper.toEditUserRecord(component, event, helper, selecteduserId);
        }else if(selectedMenuItemValue == 'Delete') {
            var selectedFeedId= event.getSource().get("v.value");
            console.log('selectedFeedId===',selectedFeedId);
            helper.deleteFeedItemReord(component, event, helper, selectedFeedId);
        }
    },
    
    deleteCommentedPost : function(component, event, helper) {
        helper.deleteComment(component, event, helper);
    },
        
    navigateToPreview :function(component,event,helper){
		console.log('preview file  == ',event.target.getAttribute("data-recId"));
		$A.get('e.lightning:openFiles').fire({
			recordIds: [component.get("v.row.fileId")]
		});
		
	},
            
	doHandleChange : function(component, event, helper) {
		console.log('=====');
		helper.onHandleChange(component, event, helper);
	},
           
	doHandleChangeComment : function(component, event, helper) {
	   console.log('=====forchild');
	   helper.onHandleChangeComment(component, event, helper);
	},
	
	doHandleChangeEditComment : function(component, event, helper) {
	   console.log('=====forchild Edit');
	   helper.onHandleChangeEditComment(component, event, helper);
	},
           
	savecursor:function(cmp, event, helper) {
		var sel, range, html; 
		sel = window.getSelection();
		console.log(JSON.stringify(sel));
		range = sel.getRangeAt(0);
		console.log(JSON.stringify(range));
		range.deleteContents(); 
		//cmp.set("v.selection",sel);
		//cmp.set("v.range",range);
	},
      
    closeuploadModel: function(component, event, helper) {
		console.log('===Cancllled file index  == ', event.getSource().get("v.value"));
		// for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
		//component.set("v.isOpen1", true);
		//component.set("v.isOpen2", true);
		//component.set("v.sendEmailOpen", true);
		/* var fileList  = component.get("v.fileList");
		console.log('fileList == ',fileList);
		var uploadedfile = [];
		for(var index = 0; index < fileList.length; index++){
		if(fileList[index].upload){
		uploadedfile.push(fileList[index]);
		}
		}
		component.set("v.fileList", uploadedfile);*/
		helper.uploadedSinglefiles(component, event, helper);
    },
    
    closeMainUploadModel: function(component, event, helper) { 
        console.log('===closeMainUploadModel  == ');
        helper.uploadedMultiplefiles(component, event, helper);
    }, 
              
	openbox : function(component, event, helper ){
		component.set('v.fileList', []);
		component.set("v.fileIds", []);
		component.set("v.isOpen2", true);
		console.log('Opn Box called=====',component.get('v.fileList'));
		var selectedFeedId= event.getSource().get("v.value");
		console.log('selectedFeedId= upload compo==',selectedFeedId);
		component.set("v.selectedFeedforFile", selectedFeedId);
		component.set('v.isEditFeedItems', false);
	},
                
	onDelete : function(cmp, event, helper){
		var index = event.getSource().get("v.value");
		if(confirm("Are you sure!")){
			var fileList  = cmp.get("v.fileName");
			if(fileList[index].upload){
				helper.deleteAttchament(cmp, event,fileList[index].id, index);
			}else{
				var removed = fileList.splice(index,1);
				cmp.set("v.fileName", fileList);
			} 
		}
	},
                    
	onRemovePill: function(cmp, event, helper) {
		var pillId = event.getSource().get('v.name');
		console.log('pilllss---',pillId);
		var pills = cmp.get('v.fileList');
		console.log('pilllss---',pills);
		for (var i = 0; i < pills.length; i++) {
			if (pillId === pills[i].name) {
				pills.splice(i, 1);
				break;
			}
		}
		
		cmp.set('v.fileList', pills);
	},
                        
	uploadNewFile: function(component, event, helper) {
		// Added by Amruta(Timer logic)
		//window.timer = setTimeout($A.getCallback(function(){
		//console.log('Post is not shared remove uploaded file');
		//helper.uploadedMultiplefiles(component, event, helper);

		// }),60000);
		// Timer logic added upto here
		// 
		console.log('======Add File button called===');
		component.set('v.isOpen2',false);
		//component.set('v.isEditFeedItems', true);
		var feedItemWrapListId1 = component.get("v.selectedFeedforFile");
		console.log('=====selectedFeedforFile Files ===', feedItemWrapListId1);
		if(feedItemWrapListId1 != null && feedItemWrapListId1 != '' && feedItemWrapListId1 != undefined) {
			console.log('=========IF Block=====');
			console.log('feedItemWrapListId - ',feedItemWrapListId);
			var feedWraperList = component.get('v.feedWraperList');
			console.log('feedWraperList==fileeee=',feedWraperList);
			for(var i=0; i< feedWraperList.length; i++) {
				console.log('=======INSIDE FOR=====');
				var feedItemWrapListId = feedWraperList[i].feedItemWrapList[0].Id;
				console.log('feedrecord commnt opn--',feedItemWrapListId);
				if(feedItemWrapListId1 == feedItemWrapListId) {
					console.log('=======INSIDE INNER IF=====');
					console.log('feedWraperList[i].isfileUpload==', feedWraperList[i].isfileUpload);
					feedWraperList[i].isfileUpload = true;
					console.log('feedWraperList[i].isfileUpload==', feedWraperList[i].isfileUpload);
				}
			}
			console.log('======feedrecord commnt opn-feedWraperList===',feedWraperList);
			//component.set("v.feedWraperList", feedWraperList);
		} else {
			console.log('===========ELSE=======');
			component.set('v.displayFiles',true);
		}
        helper.deletedUploadedFiles(component, event, helper, component.get("v.fileIds"));
	},                        
                    
	handleUploadFinished: function (component, event,helper) {
		var uploadedFiles = event.getParam("files");
		console.log('========uploadedFiles======' + uploadedFiles);
		
		//var result = event.target.result;
		//console.log('========uploadedFiles======' + uploadedFiles);
		
		console.log('uploaded files full',uploadedFiles);
		var ids = [];
		for (var i= 0 ; i < uploadedFiles.length ; i++){
			console.log('========Single record uploadedFiles======' , uploadedFiles[i]);
			ids.push(uploadedFiles[i].documentId);
			
			console.log('========Single record uploadedFiles body======' , uploadedFiles[i].body);
		}
		
		
		console.log('uploaded files'+ids);
		component.set("v.fileIds",ids);
		component.set("v.fileList",uploadedFiles);
		console.log('fileslist--',JSON.stringify(component.get("v.fileList")));
		//component.set('v.displayFiles',true);
		//component.set("v.isOpen2", false);
		console.log('open new modal for post');
		var modalBody;
        var modalFooter;
        $A.createComponents([
            ["c:fileUploadForChatterPost", { fileIds : ids}],
            ["c:modalFooterForChatterPost",{fileCount : ids.length}]
        ],
                            function(components, status) {
                                if (status === "SUCCESS") {
                                    modalBody = components[0];
                                    modalFooter = components[1];
                                    component.find('overlayLib').showCustomModal({
                                        header: "Upload Files",
                                        body: modalBody, 
                                        footer: modalFooter,
                                        showCloseButton: true
                                        
                                    })   
                                }
                            });
							   
		
	},
	
	closeDeleteModel : function(component, event,helper) {
		component.set('v.deletePost', false);
	},
	
    listenforMentions : function(component, event, helper) {
        console.log('Inside listenforMentions');
        // T-000957 - 161219 - VennScience_BFL_Amruta -  Added code to hide/show share button
        console.log("==handleValueChangeParent value: " + component.get('v.feedValue'));
        let inputText = component.find("inputTextVal").get("v.value"); 
        if(inputText.length > 1){  
            component.set('v.isButtonActiveParent',false);
        }else{
            component.set('v.isButtonActiveParent',true);
        }
        // T-000957 - 161219 - VennScience_BFL_Amruta - Added code for mention user fucntionality
        clearTimeout(window.timer);
        window.timer = setTimeout($A.getCallback(function(){
            var commentBody = component.get("v.feedValue");
            var regex = new RegExp('@[a-z0-9_-]+', 'gi');
            /* Check if the comment has @handle & get the last used handle */
            if(regex.test(commentBody)) {
                var handles = commentBody.match(regex);
                console.log('handles--',handles);
                var querystring = handles[handles.length-1];
                component.set("v.queryString",querystring);
            }
        }),1000);
        var getFeedValue = component.get("v.feedValue");
        console.log('getFeedValue========',getFeedValue);
        console.log('getFeedValue length========',getFeedValue.length);
        // Set dynamic margin for mention user list
        var finalMargin  = getFeedValue.length;
        //component.set('v.setMentionUserMargin ',finalMargin );
        
        // Hide user list when no character is typed
        window.timer = setTimeout($A.getCallback(function(){
            console.log('Inside timer');
            var getFeedValueUpdated = component.get("v.feedValue");
            var regex = new RegExp('@[a-z0-9_-]+', 'gi');
            /* Check if the comment has @handle & get the last used handle */
            if(regex.test(getFeedValueUpdated)){
                var handles = getFeedValueUpdated.match(regex);
                var querystring = handles[handles.length-1];
                console.log('Match found');
                
            } else {
                console.log('No match found');
                component.set("v.renderUserList",false);
            }
            //
        }),500);
    },
	
    getuserhandles : function(component, event, helper) {
        console.log('Inside getuserhandles');
        // T-000957 - 161219 - VennScience_BFL_Amruta -  Added function for mention user functionality
        var user_handle = component.get("v.queryString");
        console.log('Inside getuserhandles');
        console.log('Test is ', user_handle.replace(/@/g, ''));
        var getQueryString = user_handle.replace(/@/g, '');
        console.log('getQueryString=======',getQueryString);
        console.log('getQueryString length=======',getQueryString.length);
        var menu = component.find("menu");
        console.log('menu======',menu);
        
        // Added logic to toggle hide/show
        if(getQueryString != undefined && getQueryString.length == 1) {
            console.log('Inside if');
            component.set("v.renderUserList",false);
        } 
                
        var action = component.get("c.fetchRecords");
        action.setParams({
            "searchString" : user_handle.replace(/@/g, '') 
           // "contextId" : "0D56F00008140a1SAA"
        });
		
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state==="SUCCESS"){
                /* show menu */
                //var menu = component.find("menu");
                //$A.util.toggleClass(menu, 'slds-hide');
				
                var possible_mentions = response.getReturnValue();
                console.log('possible_mentions ',possible_mentions);
                var predictions = new Array();
                for(var i=0; i< possible_mentions.length; i++){
                    console.log(possible_mentions[i].id)
                    predictions.push({"label" : possible_mentions[i].label,
                                     "value" : possible_mentions[i].value});
                   
                }
                if(predictions.length > 0){
					
                    var result = {
                        "needle" : user_handle,
                        "haystack" : predictions
                    };
                    console.log('====result======',result);
                    component.set("v.renderUserList",true);
                    component.set("v.results",result);
                    
                    
                } 
                
            }
        });
        $A.enqueueAction(action);
    },
	
    search_replace : function(component, event, helper){
        console.log('Inside search_replace');
        // T-000957 - 161219 - VennScience_BFL_Amruta -  Added function for mention user functionality
        var key = event.srcElement.dataset.key;
        console.log('key======',key);
        var value = event.srcElement.dataset.value; 
        var target = event.target; 
        var selectedId = target.getAttribute("data-id");
        console.log('selectedId is ',selectedId);
        var urlString = window.location.href;
        console.log('urlString -- ',urlString);
        var baseURL = urlString.substring(0, urlString.indexOf("/s"));
        console.log('baseURL -- ',baseURL);
        var isContenteditable = false;
        //var changed_comment = component.get("v.feedValue").replace(key,'<a href="#" data-userid="005...">@['+value+']</a>');
        //var changed_comment = component.get("v.feedValue").replace(key,' &nbsp;<a id="hredId" href="'+baseURL+'/s/profile/'+selectedId+'" data-userid="005..." contenteditable="false">@['+value+']</a>&nbsp;');
        var changed_comment = component.get("v.feedValue").replace(key,' &nbsp;<a href="'+baseURL+'/s/profile/'+selectedId+'">@['+value+']</a>&nbsp;');
        console.log('======changed_comment=====',changed_comment);
        var updatedChanged_comment = changed_comment + ' ';
        console.log('======updatedChanged_comment=====',updatedChanged_comment);
        component.set("v.feedValue",updatedChanged_comment );
        
        /*hide menu*/
        //var menu = component.find("menu");
        //$A.util.toggleClass(menu, 'slds-hide');
        component.set("v.renderUserList",false);
        console.log('renderUserList=======',component.get("v.renderUserList"));
    },
	
    handleChildCommentChange : function(component, event, helper) {
        console.log('Inside handleChildCommentChange');
        var wrapperlist = component.get('v.feedWraperList');
        console.log('wrapperlist child comment --',wrapperlist);
    },
        
    postTabChange : function(component, event, helper) {
        console.log('onclick Post');
        component.set('v.isPost',true);
        component.set('v.isAnnouncement',false);
        var tab = component.find('postId');
        $A.util.addClass(tab, 'slds-active');
        var tab2 = component.find('announcementId');
        $A.util.removeClass(tab2, 'slds-active');
        
       
    },
     
    announcementTabChange : function(component, event, helper) {
        console.log('onclick announcment');
        component.set('v.isPost',false);
        component.set('v.isAnnouncement',true);
        var tab2 = component.find('announcementId');
        $A.util.addClass(tab2, 'slds-active');
        var tab = component.find('postId');
        $A.util.removeClass(tab, 'slds-active');
        
    },
        
    listenforAnnouncement : function(component, event, helper) { 
        console.log("==handleValueChangeParent value: " + component.get('v.announcementValue'));
       var inputText1 = component.find("textValue").get("v.value"); 
        console.log("==inputText value: ",inputText1.length);
        if(inputText1.length >= 1){  
           component.set('v.isActiveAnnouncement',false);
        }else{
           component.set('v.isActiveAnnouncement',true);
        }  
        
        clearTimeout(window.timer);
        window.timer = setTimeout($A.getCallback(function(){
            var commentBody = component.get("v.announcementValue");
            var regex = new RegExp('@[a-z0-9_-]+', 'gi');
            /* Check if the comment has @handle & get the last used handle */
            if(regex.test(commentBody)){
                var handles = commentBody.match(regex);
                var querystring = handles[handles.length-1];
                component.set("v.queryString1",querystring);
            }
        }),1000);
        var getannouncementValue = component.get("v.announcementValue");
        console.log('announcementValue========',getannouncementValue);
        console.log('getFeedValue length========',getannouncementValue.length);
        // Set dynamic margin for mention user list
        var finalMargin  = getannouncementValue.length;
        //component.set('v.setMentionUserMargin ',finalMargin );
        
        // Hide user list when no character is typed
        window.timer = setTimeout($A.getCallback(function(){
            console.log('Inside timer');
            var getFeedValueUpdated = component.get("v.announcementValue");
            var regex = new RegExp('@[a-z0-9_-]+', 'gi');
            /* Check if the comment has @handle & get the last used handle */
            if(regex.test(getFeedValueUpdated)){
                var handles = getFeedValueUpdated.match(regex);
                var querystring = handles[handles.length-1];
                console.log('Match found');
                
            } else {
                console.log('No match found');
                component.set("v.renderUserList1",false);
            }
            //
        }),200);
     },
         
     getuserhandlesForAnnouncement : function(component, event, helper) {
        console.log('Inside getuserhandles');
        // T-000957 - 161219 - VennScience_BFL_Amruta -  Added function for mention user functionality
        var user_handle = component.get("v.queryString1");
        console.log('Inside getuserhandles');
        console.log('Test is ', user_handle.replace(/@/g, ''));
        var getQueryString = user_handle.replace(/@/g, '');
        console.log('getQueryString=======',getQueryString);
        console.log('getQueryString length=======',getQueryString.length);
        var menu = component.find("menu1");
        console.log('menu======',menu);
        
        // Added logic to toggle hide/show
        if(getQueryString != undefined && getQueryString.length == 1) {
            console.log('Inside if');
            component.set("v.renderUserList1",false);
        } 
                
        var action = component.get("c.fetchRecords");
        action.setParams({
            "searchString" : user_handle.replace(/@/g, '') 
           // "contextId" : "0D56F00008140a1SAA"
        });
		
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state==="SUCCESS"){
                /* show menu */
                //var menu = component.find("menu");
                //$A.util.toggleClass(menu, 'slds-hide');
				
                var possible_mentions = response.getReturnValue();
                console.log('possible_mentions ',possible_mentions);
                var predictions = new Array();
                for(var i=0; i< possible_mentions.length; i++){
                    console.log(possible_mentions[i].id)
                    predictions.push({"label" : possible_mentions[i].label,
                                     "value" : possible_mentions[i].value});
                   
                }
                if(predictions.length > 0){
					
                    var result = {
                        "needle" : user_handle,
                        "haystack" : predictions
                    };
                    console.log('====result======',result);
                    component.set("v.renderUserList1",true);
                    component.set("v.results1",result);
                    
                    
                } 
                
            }
        });
        $A.enqueueAction(action);
    },
    
    search_replaceAnnouncement : function(component, event, helper){
        console.log('Inside search_replace');
        // T-000957 - 161219 - VennScience_BFL_Amruta -  Added function for mention user functionality
        var key = event.srcElement.dataset.key;
        console.log('key---',key);
        var value = event.srcElement.dataset.value; 
        var target = event.target; 
        var selectedId = target.getAttribute("data-id");
        console.log('selectedId is ',selectedId);
        var urlString = window.location.href;
        console.log('urlString -- ',urlString);
        var baseURL = urlString.substring(0, urlString.indexOf("/s"));
        console.log('baseURL -- ',baseURL);
        var isContenteditable = false;
        console.log('value========',value);
        console.log('announ value========',component.get("v.announcementValue"));
        //var changed_comment = component.get("v.feedValue").replace(key,'<a href="#" data-userid="005...">@['+value+']</a>');
        //var changed_comment = component.get("v.feedValue").replace(key,' &nbsp;<a id="hredId" href="'+baseURL+'/s/profile/'+selectedId+'" data-userid="005..." contenteditable="false">@['+value+']</a>&nbsp;');
        var changed_comment = component.get("v.announcementValue").replace(key,' &nbsp;<a href="'+baseURL+'/s/profile/'+selectedId+'">@['+value+']</a>&nbsp;');
        console.log('======changed_comment=====',changed_comment);
        var updatedChanged_comment = changed_comment + ' ';
        console.log('======updatedChanged_comment=====',updatedChanged_comment);
        component.set("v.announcementValue",updatedChanged_comment );
        
        /*hide menu*/
        //var menu = component.find("menu");
        //$A.util.toggleClass(menu, 'slds-hide');
        component.set("v.renderUserList1",false);
        console.log('renderUserList=======',component.get("v.renderUserList1"));
    },
        
        
    onshareAnnounceRecord : function(component, event, helper) {
        console.log('value announcement---',component.get('v.announcementValue'));
        console.log('value date---',component.get('v.expireDate'));
        helper.createFeedAnnouncement(component, event, helper);    
    },

})