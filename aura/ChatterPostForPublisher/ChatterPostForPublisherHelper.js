({
    doInit : function(component,event,helper) {
        let action = component.get("c.getFilesClone");
        action.setParams({
            'parentId':component.get("v.recordId")
        });
        action.setCallback(this,(response)=>{
            const state = response.getState();
            //console.log(state,response.getReturnValue());
            //console.log('get on load response files--',response.getReturnValue());
            if(state === 'SUCCESS') {
            let recordMap = response.getReturnValue();
            component.set("v.isCommunityUser",recordMap.isCommunityUser);
            component.set("v.files",recordMap.files);
        } else if(state === 'ERROR'){
            
        }
    });
    $A.enqueueAction(action);
},
 
 userAnnouncementAccess : function(component,event,helper) {
        let action = component.get("c.getAnnouncementCreateAccess");
        action.setParams({
            'parentId':component.get("v.recordId")
        });
        action.setCallback(this,(response)=>{
            const state = response.getState();
            //console.log(state,response.getReturnValue());
            //console.log(' user announcement access--',response.getReturnValue());
            if(state === 'SUCCESS') {
            let recordMap = response.getReturnValue();
            console.log(' user announcement access--',recordMap);
            component.set("v.isCreateAnnouncement",recordMap);
            component.set("v.files",recordMap.files);
        } else if(state === 'ERROR'){
            
        }
    });
    $A.enqueueAction(action);
},
    getAnnounceFeed : function(component,event,helper) {
        let action = component.get("c.getFeedRelatedAnnouncement");
        action.setParams({
            'parentId':component.get("v.recordId")
        });
        action.setCallback(this,(response)=>{
            const state = response.getState();
            //console.log(state,response.getReturnValue());
            //console.log(' user announcement access--',response.getReturnValue());
            if(state === 'SUCCESS') {
            let announcementRecord = response.getReturnValue();
            console.log('  announcement List--',announcementRecord);
            component.set("v.announcementRecord",announcementRecord);
            var expirationDateOfAnnouncement = $A.localizationService.formatDateTimeUTC(announcementRecord.ExpirationDate, "MMMM dd yyyy, hh:mm a");
            console.log('expirationDateOfAnnouncement== test==', expirationDateOfAnnouncement);
            component.set('v.expirationDateOfAnnouncement',expirationDateOfAnnouncement);
            
            var currentDateTime = $A.localizationService.formatDateTimeUTC(new Date(), "MMMM dd yyyy, hh:mm a");
            console.log('====currentDateTime====',currentDateTime);
            
            var createdDateOfAnnouncement = $A.localizationService.formatDateTimeUTC(announcementRecord.CreatedDate, "MMMM dd yyyy, hh:mm a");
            console.log('createdDateOfAnnouncement====', createdDateOfAnnouncement);
            component.set('v.createdDateOfAnnouncement',createdDateOfAnnouncement);
            var currentDate = new Date(currentDateTime);
            var announcementDate = new Date(createdDateOfAnnouncement);
            
            var timeDifferenceInHours = Math.round((currentDate-announcementDate)/(1000*60*60));
            console.log('timeDifferenceInHours====', timeDifferenceInHours);
            
            if(timeDifferenceInHours == 0 ) {
            	console.log('timeDifferenceInHours is 0====');
            	component.set('v.createdAnnouncementTime','Just now');
        	} else if(timeDifferenceInHours > 23) {
            	console.log('timeDifferenceInHours is > 23====');
            	component.set('v.createdAnnouncementTime', announcementRecord.CreatedDate);
            } else if(timeDifferenceInHours > 0 && timeDifferenceInHours <= 23){
                console.log('timeDifferenceInHours is <= 23 AND > 0====');
            	component.set('v.createdAnnouncementTime', timeDifferenceInHours + 'h ago');
    		}
            
        } else if(state === 'ERROR') {
            
        }
    });
    $A.enqueueAction(action);
},
 
 getIndexFrmParent : function(target,helper,attributeToFind){
    //User can click on any child element, so traverse till intended parent found
    var SelIndex = target.getAttribute(attributeToFind);
    while(!SelIndex){
        target = target.parentNode ;
        SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);           
    }
    return SelIndex;
},
 
 uploadedSinglefiles: function(component,event,helper) {
    console.log('====cancelled file Index============',event.getSource().get("v.value"));
    
    var cancelledfilesList = component.get('v.fileIds');
    var fileList = component.get('v.fileList');
    component.set("v.isOpen2", true);
    var action = component.get("c.removeCancelledFiles");
    var updtedFileList = [];
    action.setParams({
        'fileIdsList': JSON.stringify(cancelledfilesList),
        'recordId' : component.get("v.recordId"),
        'index' : event.getSource().get("v.value")
    });
    action.setCallback(this,function(response) {
        var state = response.getState();
        if(state === 'SUCCESS') {
            var recordMap = response.getReturnValue();
            console.log('remove files');
            var cancelledfilesListJson = JSON.stringify(cancelledfilesList);
            console.log('==BEFORE==cancelledfilesListJson===========',cancelledfilesListJson); 
            delete cancelledfilesList[event.getSource().get("v.value")];
            console.log('==AFTER DELTE==cancelledfilesListJson===========',cancelledfilesListJson); 
            for(var index = 0; index < fileList.length; index++) {
            	console.log('====fileList Item============',fileList[index].documentId); 
                if(index != event.getSource().get("v.value")){
                    updtedFileList.push(fileList[index]);
                }
            }
            component.set("v.fileList", updtedFileList);
            //$A.get('e.force:refreshView').fire();
        }
        
    });
    $A.enqueueAction(action);
},
    
uploadedMultiplefiles: function(component,event,helper) {
    var cancelledfilesList = component.get('v.fileIds');
    component.set("v.isOpen2", false);
    var action = component.get("c.removeCancelledFiles");
    action.setParams({
        'fileIdsList': JSON.stringify(cancelledfilesList),
        'recordId' : component.get("v.recordId")
    });
    action.setCallback(this,function(response) {
        var state = response.getState();
        if(state === 'SUCCESS') {
            var recordMap = response.getReturnValue();
            console.log('remove files');
            $A.get('e.force:refreshView').fire();
        }
        
    });
    $A.enqueueAction(action);
},
    
    getRelatedMembers : function(component,event,helper) {
        var action = component.get("c.getMembersRelatedToChatterGroup");
            action.setParams({
                'parentId':component.get("v.recordId")
            });
            action.setCallback(this,function(response) {
                const state = response.getState();
                //console.log(state,response.getReturnValue());
                //console.log('get on load response files--',response.getReturnValue());
                if(state === 'SUCCESS') {
                    var recordMap = response.getReturnValue();
                    var option = [];
                    var memberValue;
                    for(var i=0;i < recordMap.length; i++){
                        option.push({
                            value : recordMap[i].Id,
                            label : recordMap[i].Name
                        });
                        
                    }
                    component.set("v.memberValue",memberValue);
                    component.set("v.GroupMemeberList",option);
                    //cmp.set("v.selObj",acctId);
                    //helper.onHandleChange(component, event,helper);
                    //var options = recordMap;
                    //console.log('get group members - ',options);
                    component.set('v.GroupMemeberList1',recordMap);
                } else if(state === 'ERROR'){
                    
                }
            });
            $A.enqueueAction(action);
        },
                
    getNetworkId : function(component,event,helper) {
        var action = component.get("c.getPathPrefix");
    
    action.setCallback(this,(response)=>{
        var state = response.getState();
        //console.log(state,response.getReturnValue());
        //console.log('get networkId--',response.getReturnValue());
        if(state === 'SUCCESS') {
        var recordMap = response.getReturnValue();
        //console.log('recordMap == ',recordMap);
    }
                       });
    $A.enqueueAction(action);
},
    
    getFeeds : function(component,event,helper) {
        var action = component.get("c.getNewsFeedCount");
        
        action.setCallback(this,function(response){
            var state = response.getState();
            //console.log(state,response.getReturnValue());
            //console.log('get networkId-444-',response.getReturnValue());
            if(state === 'SUCCESS') {
                var recordMap = response.getReturnValue();
                //console.log('getNewsFeedCount == ',recordMap);
            }
        });
        $A.enqueueAction(action);
    },
        
        getUserAccess : function(component, event, helper) {
        //console.log('parentid----',component.get('v.recordId'));
        var action = component.get("c.getPublisherUser");
        action.setParams({
            "parentId": component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS') {
                var responseValue = response.getReturnValue();
                console.log('responseValue current user access -',responseValue);  
                component.set('v.isUserPublishAccess',responseValue);
                //console.log('responseValue set in variable-',component.get("v.isUserPublishAccess"));
               /* $A.createComponent('component:' + 'filesListView', {
                    'isUserPublishAccess': component.getReference("v.isUserPublishAccess")
                }, function (contentComponent, status, error) {
                    if (status === "SUCCESS") {
                        console.log('successfully set user access variable  ');
                        //c.set('v.fileUploadBody', contentComponent);
                        //c.set("v.showSpinner",false);
                    } else {
                        throw new Error(error);
                    }
                });*/
            }
        });
        $A.enqueueAction(action);
    },
        
        createFeedLike : function(component,event,helper, selectedFeedId) {
            console.log('selectedFeedId for feed like--',selectedFeedId);
            var action = component.get("c.createFeedLikeRecord");
            action.setParams({
                "feedCommentId": selectedFeedId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if( state === 'SUCCESS') {
                    var responseValue = response.getReturnValue();
                    console.log('responseValue created feed item like-',responseValue);
                    helper.getAllfeedRecords(component, event, helper);
                    
                }
            });
            $A.enqueueAction(action);
        },
            
            deleteFeedLike : function(component,event,helper, selectedFeedId) {
                console.log('selectedFeedId for feed like--',selectedFeedId);
                var action = component.get("c.removeFeedLikeRecord");
                action.setParams({
                    "feedRecordId": selectedFeedId
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if( state === 'SUCCESS') {
                        var responseValue = response.getReturnValue();
                        console.log('responseValue created feed item like-',responseValue);
                        helper.getAllfeedRecords(component, event, helper);
                        
                    }
                });
                $A.enqueueAction(action);
            },
            
            createChildFeedPost : function(component,event,helper, selectedFeedId, commentBody) {
                console.log('selectedFeedId for feed like--',selectedFeedId);
                console.log('selectedFeedId for feed child comment -- ',commentBody);
                var comment = component.get('v.feedValueComment');
                var action = component.get("c.createFeedCommentRecord");
                action.setParams({
                    "feedItemId": selectedFeedId,
                    "commentBody" : commentBody,
                    "parentId" : component.get('v.recordId')
                    //"fileIdsList" : JSON.stringify(component.get('v.fileIds'))
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if( state === 'SUCCESS') {
                        var responseValue = response.getReturnValue();
                        //console.log('responseValue created feed item like-',responseValue);
                        var feedWraperList = component.get('v.feedWraperList');
                        //console.log('feedWraperList===',feedWraperList);
                        for(var i=0; i< feedWraperList.length; i++) {
                            //console.log('inside for');
                            var feedItemWrapListId = feedWraperList[i].feedItemWrapList[0].Id;
                            //console.log('feedrecord commnt opn--',feedItemWrapListId);
                            if(selectedFeedId == feedItemWrapListId) {
                                //console.log('inside for if');
                                //console.log('feedWraperList[i].isChildCommentPost==', feedWraperList[i].isChildCommentPost);
                                feedWraperList[i].isChildCommentPost = false;
                                //console.log('feedWraperList[i].isChildCommentPost==', feedWraperList[i].isChildCommentPost);
                            }
                        }
                        //console.log('feedrecord commnt opn-feedWraperList-',feedWraperList);
                        component.set("v.feedWraperList", feedWraperList);
                        component.set("v.feedValueComment",'');
                        helper.getAllfeedRecords(component, event, helper);
                        
                    }
                });
                $A.enqueueAction(action);
            },
                
                getUploadedFiles : function(component,event,helper) {
                    var action = component.get("c.getFilesClone");
                    action.setParams({
                        'parentId':component.get("v.recordId")
                    });
                    action.setCallback(this,function(response){
                        const state = response.getState();
                        //console.log(state,response.getReturnValue());
                        //console.log('get on load response files--',response.getReturnValue());
                        if(state === 'SUCCESS') {
                        let recordMap = response.getReturnValue();
                        component.set("v.isCommunityUser",recordMap.isCommunityUser);
                        component.set("v.files",recordMap.files);
                        helper.createFeedRecord(component, event, helper, component.get("v.files"));
                    } else if(state === 'ERROR'){
                        
                    }
                });
              $A.enqueueAction(action);
             },
    
    createFeedRecord : function(component, event, helper, files) {
        console.log('==>>files--to createfeed--',files);
        console.log('==>>JSON.stringify(files) files--to createfeed--',JSON.stringify(files));
        var listOfUserId = component.set('v.listOfUserId');
        //console.log('files--to createfeed--',component.get('v.fileIds'));
        //console.log('parentid----',component.get('v.recordId'));
        var action = component.get("c.createFeedItemRecord");
        action.setParams({
            "parentId": component.get('v.recordId'),
            "body"    : component.get('v.feedValue'),
            "fileIdsList" : JSON.stringify(files)
            //"listOfUserId" : JSON.stringify(listOfUserId)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS') {
                var responseValue = response.getReturnValue();
                //console.log('responseValue created feed item -',responseValue);
                component.set('v.isOpenTextBox', false);
                component.set('v.feedValue','');
                component.set('v.fileList',[]);
                helper.getAllfeedRecords(component, event, helper);
                component.set('v.displayFiles',false);
                //$A.get('e.force:refreshView').fire();
                //location.reload();
                //console.log('refresh window');
            }
        });
        $A.enqueueAction(action);
    },
	
	deleteFeedItemReord : function(component, event, helper, selectedFeedId) {
		//console.log('156---',selectedFeedId);
		component.set("v.selectedFeedId", selectedFeedId);
		component.set("v.deletePost", true);
	},

	deleteComment : function(component, event, helper) {
		component.set("v.deletePost", false);
		var action = component.get("c.deleteSelectedFeedItem");
		action.setParams({
			"feedItemId": component.get("v.selectedFeedId")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if( state === 'SUCCESS') {
				var responseValue = response.getReturnValue();
				//console.log('responseValue created feed item -',responseValue);
				//helper.showToast(component, event, helper,responseValue);
				helper.getAllfeedRecords(component, event, helper);
                helper.getAnnounceFeed(component, event, helper);
				$A.get('e.force:refreshView').fire();
				//console.log('refresh window');
			}
		});
		$A.enqueueAction(action);
	},

	getAllfeedRecords : function(component, event, helper) {	
		//console.log('--get records using helper----');

		var today = new Date();
		//console.log('--today----',today);
		var sec = today.getSeconds();
		//console.log('--sec----',sec);
		//var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
		//console.log('--date----',date);
		//var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		//console.log('--time----',time);
		//var dateTime = date+' '+time;
		//console.log('--dateTime----',dateTime);
		var now = new Date();
		var currentTime = $A.localizationService.formatDate(now, "MMMM dd yyyy, hh:mm a");
		// Returns date in the format "Jun 8, 2017"
		//console.log('current date --',$A.localizationService.formatDate(now, "MMMM dd yyyy, hh:mm a"));

		var action = component.get("c.getFeedItemRecords");
		action.setParams({
			"parentId": component.get('v.recordId')
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if( state === 'SUCCESS') {
				let responseValue = response.getReturnValue();
				console.log('responseValue wrapper List feed item -',responseValue);
				var years ,day, hour, minute, seconds,yesterdatPost, newDate;
				console.log('======responseValue.length====',responseValue.length);
				for(var i=0; i<responseValue.length; i++) {
					if(responseValue[i].feedCommentWrapList.length > 0) {
						responseValue[i].isMoreComments = true;
						//console.log('=== -',responseValue[i].feedCommentWrapList);
					}
					for(var j=0; j<responseValue[i].feedItemWrapList.length; j++) {
						var today1 = new Date(responseValue[i].feedItemWrapList[j].CreatedDate);
						//console.log('today1 -',today1);

						responseValue[i].displayPostTime = $A.localizationService.formatDateTimeUTC(responseValue[i].feedItemWrapList[j].CreatedDate, "MMMM dd yyyy, hh:mm a")
						//console.log('timezone--', $A.localizationService.formatDateTimeUTC(now1, "MMMM dd yyyy, hh:mm a"));

					}
					for(var j=0; j<responseValue[i].feedCommentWrapList.length; j++) {
						var today1 = new Date(responseValue[i].feedCommentWrapList[j].CreatedDate);
						//console.log('today1 -',today1);
						responseValue[i].displayPostTimeComment = $A.localizationService.formatDateTimeUTC(responseValue[i].feedCommentWrapList[j].CreatedDate, "MMMM dd yyyy, hh:mm a")
						//console.log('timezone--', $A.localizationService.formatDateTimeUTC(now1, "MMMM dd yyyy, hh:mm a"));

					}
				}
				component.set('v.originalWeedWraperList',responseValue);
				var wrapperToDisplay = [];
				var counter = component.get('v.counter');
				console.log('==counter======', counter);
				if(responseValue.length > 10) {
					component.set('v.isLoadMoreComments',true);
					for(var i=0; i<10*counter; i++) { 
						wrapperToDisplay.push(responseValue[i]);
					}
				} else {
					component.set('v.isLoadMoreComments',false);
					for(var i=0; i<responseValue.length; i++) { 
						wrapperToDisplay.push(responseValue[i]);
					}
				}                            
				component.set('v.feedWraperList',wrapperToDisplay);
			}
		});
		$A.enqueueAction(action);
	},
	editFeedRecord : function(component, event, helper) {
		var action = component.get("c.editFeedItem");
		action.setParams({
			"selectedId": component.get('v.selectedFeedId'),
			"editFeedBody":component.get('v.editFeedItemBody')
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if( state === 'SUCCESS') {
				var responseValue = response.getReturnValue();
				//console.log('responseValue wrapper List feed item -',responseValue);
				component.set("v.isEditFeedItems",false);
				helper.getAllfeedRecords(component, event, helper);
			}
		});
	$A.enqueueAction(action); 
	},

	openFileupload : function(c, e, h){
		try {

			$A.createComponent('c:' + 'fileUpload', {
				'isOpenTextBox': c.getReference("v.isOpenTextBox"),
				'fileIds': c.getReference("v.fileIds"),
				'recordId': c.getReference("v.recordId"),
				'isCommunityUser': c.getReference("v.isCommunityUser")
			}, function (contentComponent, status, error) {
				if (status === "SUCCESS") {
					c.set('v.fileUploadBody', contentComponent);
					//c.set("v.showSpinner",false);
				} else {
					throw new Error(error);
				}
			});
		}catch (e) {
			console.log('ERROR:: ' + e);
			//c.set("v.showSpinner",false);
		}
	},

	openFileupload1 : function(c, e, h) {
		console.log('open file upload component.');
		try {
			console.log('try block');
			$A.createComponent('c:' + 'fileUpload', {
				'isOpenRichTextBox': c.getReference("v.isOpenRichTextBox"),
				'fileIds': c.getReference("v.fileIds"),
				'recordId': c.getReference("v.recordId"),
				'isCommunityUser': c.getReference("v.isCommunityUser"),
			}, function (contentComponent, status, error) {
				if (status === "SUCCESS") {
					c.set('v.fileUploadBody', contentComponent);
					//c.set("v.showSpinner",false);
				} else {
					throw new Error(error);
				}
			});
		}catch (e) {
			console.log('ERROR:: ' + e);
			c.set("v.showSpinner",false);
		}
	},

	openFileupload2 : function(c, e, h) {
		console.log('open file upload component.');
		try {

			$A.createComponent('c:' + 'fileUpload', {
				'isEditFeedItems': c.getReference("v.isEditFeedItems"),
				'fileIds': c.getReference("v.fileIds"),
				'recordId': c.getReference("v.recordId"),
				'isCommunityUser': c.getReference("v.isCommunityUser"),
			}, function (contentComponent, status, error) {
			if (status === "SUCCESS") {
				c.set('v.fileUploadBody', contentComponent);
				//c.set("v.showSpinner",false);
			} else {
				throw new Error(error);
			}
			});
		}catch (e) {
			console.log('ERROR:: ' + e);
			c.set("v.showSpinner",false);
		}
	},

	showToast : function(component, event, helper,msg) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			title: "Success!",
			message: msg,
			duration:'5000',
			type: 'Success'
		});
		toastEvent.fire();  
	},
                            
	onHandleChange : function(component, event, helper){
		var selectedvalue = component.get("v.feedValue");
		console.log('=**selectedvalue***====',selectedvalue);
		var selectedOptionValue = event.getParam("value");
		console.log('=selectedOptionValue*****====',selectedOptionValue);
		var userList = component.get("v.GroupMemeberList1");
		console.log('listdsb -- ',userList);
		var username = '';
		var userId = '';
		var listOfUserId = [];
		for(var i=0;i<userList.length;i++) {
			if(selectedOptionValue == userList[i].Id) {
				username = userList[i].Name;
				userId = userList[i].Id;
				listOfUserId.push(userList[i].Id);
			}
		}
		console.log('username --',username);
		console.log('userId --',userId);
		component.set('v.listOfUserId',listOfUserId);
		var urlString = window.location.href;
		console.log('urlString -- ',urlString);
		var urlValue = urlString.substring(0,urlString.indexOf("/r"));
		console.log('url -- ',urlValue);
		var baseURL = urlString.substring(0, urlString.indexOf("/s"));
		console.log('baseURL -- ',baseURL);
		if(selectedvalue != '' && selectedvalue != undefined) {
			console.log('===In If selectedvalue is not null ================');
			console.log('===selectedvalue is================' , selectedvalue);
			selectedvalue = selectedvalue.replace('</p>', '');
			console.log('=replace paragraph tags================' , selectedvalue);
			if(baseURL != null && baseURL != '') {
				selectedvalue = selectedvalue+'<a href="'+baseURL+'/s/profile/'+userId+'">[@'+username+']</a>';
				console.log('=IF=baseUrl =selectedvalue after apending test is================' , selectedvalue);
			}else {
				selectedvalue = selectedvalue+'<a href="'+urlValue+'/r/User/'+userId+'">[@'+username+']</a>';
				console.log('=IF==elsee urlValue apending test is================' , selectedvalue);
			}
		}else {
			console.log('===In Else selectedvalue is null ================');
			console.log('else   ', userId);
			if(baseURL != null && baseURL != '') {
				selectedvalue = ''+'<a href="'+baseURL+'/s/profile/'+userId+'">[@'+username+']</a>';
				console.log('=IF==selectedvalue after apending test is================' , selectedvalue);
			}else {
				selectedvalue = ''+'<a href="'+urlValue+'/r/User/'+userId+'">[@'+username+']</a>';
				console.log('=IF=elllse =urlValue==selectedvalue after apending test is================' , selectedvalue);
			}
			//selectedvalue = ''+'<a href="'+baseURL+'/s/profile/'+userId+'">[@' + username + ']</a>';

			//console.log('==ELSE=selectedvalue after apending test is================' , selectedvalue);
		}
		console.log('newText --400 - ',selectedvalue);
		component.set("v.feedValue",selectedvalue);
		console.log("Option selected with value: '" + selectedOptionValue + "'");
		component.set('v.isSearch',false);
		helper.insertTextAtCursor(component, event, selectedOptionValue);
	},
              
	onHandleChangeComment : function(component, event, helper){
		//var selectedvalue = component.get("v.feedValueComment");
		//console.log('=*****====',selectedvalue);  
		var wrapperlist = component.get('v.feedWraperList');
		console.log('wrapperlist child comment --',wrapperlist);
		var selectedvalue = '';
		for(var i=0; i<wrapperlist.length; i++) {
			selectedvalue = wrapperlist[i].newChildCommentPost;
			console.log('commentBody --',selectedvalue);
		}                
		var selectedOptionValue = event.getParam("value");
		console.log('=selectedOptionValue*****====',selectedOptionValue);
		var userList = component.get("v.GroupMemeberList1");
		console.log('listdsb -- ',userList);
		var username = '';
		var userId = '';
		var listOfUserId = [];
		for(var i=0;i<userList.length;i++) {
			if(selectedOptionValue == userList[i].Id) {
				username = userList[i].Name;
				userId = userList[i].Id;
				listOfUserId.push(userList[i].Id);
			}
		}
		console.log('username --',username);
		console.log('userId --',userId);
		component.set('v.listOfUserId',listOfUserId);
		var urlString = window.location.href;
		console.log('urlString -- ',urlString);
		var urlValue = urlString.substring(0,urlString.indexOf("/r"));
		console.log('url -- ',urlValue);
		var baseURL = urlString.substring(0, urlString.indexOf("/s"));
		console.log('baseURL -- ',baseURL);
		var newText= '';
		if(selectedvalue != '' && selectedvalue != undefined) {
			console.log('iffff');
			selectedvalue = selectedvalue.replace('</p>', '');
			//selectedvalue = selectedvalue.substring(0,selectedvalue.lastIndexOf('@')-1);
			newText = selectedvalue+'<a href="'+baseURL+'/s/profile/'+userId+'">[@'+username+']</a>';
		}else {
			console.log('else   ', userId);
			newText = ''+'<a href="'+baseURL+'/s/profile/'+userId+'">[@'+username+']</a>';
			console.log('newText --400 - ',newText);
		}
		console.log('newText --400 - ',newText);
		for(var i=0; i<wrapperlist.length; i++) {
			wrapperlist[i].newChildCommentPost = newText;
			//console.log('commentBody --',selectedvalue);
		}  
		component.set('v.feedWraperList',wrapperlist);
		//component.set("v.feedValueComment",newText);
		console.log("Option selected with value: '" + selectedOptionValue + "'");
		component.set('v.isSearch1',false);
		helper.insertTextAtCursor(component, event, selectedOptionValue);
	},
                  
	onHandleChangeEditComment : function(component, event, helper){
		var selectedvalue = component.get("v.editFeedItemBody");
		console.log('=*****====',selectedvalue);                  
		var selectedOptionValue = event.getParam("value");
		console.log('=selectedOptionValue*****====',selectedOptionValue);
		var userList = component.get("v.GroupMemeberList1");
		console.log('listdsb -- ',userList);
		var username = '';
		var userId = '';
		var listOfUserId = [];
		for(var i=0;i<userList.length;i++) {
			if(selectedOptionValue == userList[i].Id) {
				username = userList[i].Name;
				userId = userList[i].Id;
				listOfUserId.push(userList[i].Id);
			}
		}
		console.log('username --',username);
		console.log('userId --',userId);
		component.set('v.listOfUserId',listOfUserId);
		var urlString = window.location.href;
		console.log('urlString -- ',urlString);
		var urlValue = urlString.substring(0,urlString.indexOf("/r"));
		console.log('url -- ',urlValue);
		var baseURL = urlString.substring(0, urlString.indexOf("/s"));
		console.log('baseURL -- ',baseURL);
		var newText = '';
		if(selectedvalue != '' && selectedvalue != undefined) {
			console.log('=====In If selected value is not null==========');
			selectedvalue = selectedvalue.replace('</p>', '');
			//selectedvalue = selectedvalue.substring(0,selectedvalue.lastIndexOf('@')-1);
			newText = selectedvalue+'<a href="'+baseURL+'/s/profile/'+userId+'">[@'+username+']</a>';
		}else {
			//console.log('else   ', userId);
			newText = ''+'<a href="'+baseURL+'/s/profile/'+userId+'">[@'+username+']</a>';
			//console.log('newText --400 - ',newText);
		}
		console.log('newText --400 - ',newText);
		component.set("v.editFeedItemBody",newText);
		//console.log("Option selected with value: '" + selectedOptionValue + "'");
		component.set('v.editComment',false);
		//helper.insertTextAtCursor(component, event, selectedOptionValue);
	},
                                
    /**
     * Method to insert cursour point.
     *  
     */
    insertTextAtCursor:function(component, event, text) {
        var sel, range, html; 
        sel = component.get("v.selection");
        range = component.get("v.range");
        console.log('insert range-',range);
        if(range == null){
            //alert('Wrong Move');
            return false;
        }
        range.deleteContents(); 
        console.log("cmp.getElements(): ", component.getElements());
        var currntnode = component.getElements();
        console.log("currntnode: ", currntnode);
        //console.log("div1: ", component.find("inputTextVal").getElement());
        //var textNode = currntnode.TEXT_NODE;
        var textNode = document.createTextNode(text);
        console.log('textNode='+textNode);
        var p1 = document.getElementById("inputTextVal1");
        console.log('p1='+p1);
        range.insertNode(textNode);
        range.setStartAfter(textNode);
        sel.removeAllRanges();
        sel.addRange(range); 
        //component.set("v.isOpen1", false);
    },
        
    /**
     * Method to remove attachments from the list.
     *
     * @param component, event.
     */
    deleteAttchament :function(component, event, fileId, index){
        console.log('test to remove from table');
        var action = component.get("c.DeleteAttahcment");
        action.setParams({
            attId: fileId
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                console.log('Deleted Successfully');
                console.log(fileId);
                var fileList  = component.get("v.fileName");
                var removed = fileList.splice(index,1);
                component.set("v.fileName", fileList);
            }else if (state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    var fileInput = component.get("v.fileName");
                    fileInput[index].error =true;
                    fileInput[index].isUploading =false;
                    fileInput[index].message = errors; 
                    if (errors && Array.isArray(errors) && errors.length > 0) {
                        var msg = 'Errors :';
                        for(var i = 0 ; i< errors.length; i++){
                            msg += errors[i].message;
                        }
                        fileInput[index].message = msg;
                    }
                    component.set("v.fileName",fileInput);
                } else {
                    console.log("Unknown error");
                }
            } 
        });
        
        $A.enqueueAction(action);
    },   
    
    deletedUploadedFiles :function(component, event, helper, files){ 
    	console.log('===deletedUploadedFiles=====');
        console.log('==>>deletedUploadedFiles JSON.stringify(files) files--to createfeed--',JSON.stringify(files));
        var action = component.get("c.deleteCreatedFiles");
        action.setParams({
            "fileIdsList": JSON.stringify(files),
            "recordId": component.get('v.recordId'),
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                console.log('===Deleted Successfully===');
            }
        });
        $A.enqueueAction(action);
    },
    createFeedAnnouncement : function(component, event, helper) {
       console.log('selectedFeedId for feed like--',component.get('v.expireDate'));
       var action = component.get("c.createFeedAnnouncementRecord");
       action.setParams({
           "parentId" : component.get('v.recordId'),
           "feedvalue": component.get('v.announcementValue'),
           "expirationDateString" : component.get('v.expireDate')
       });
       action.setCallback(this, function(response) {
       var state = response.getState();
        if( state === 'SUCCESS') {
             var responseValue = response.getReturnValue();
                 console.log('responseValue created feed item like-',responseValue);
                helper.getAllfeedRecords(component, event, helper);
                helper.getAnnounceFeed(component, event, helper);
                var date = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD hh:mm:ss a");
                console.log('todaydate====',date);
                
                component.set('v.expireDate',date);
                component.set('v.announcementValue','')  ;
           	  
             }
         });
       $A.enqueueAction(action);  
    }
})