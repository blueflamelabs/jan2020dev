({
    doInit : function(component, event, helper) {
        let file = component.get("v.file");
        let caseMoniterWithContentDocMap = component.get("v.caseMoniterWithContentDocMap") || {};
        console.log(JSON.stringify(caseMoniterWithContentDocMap[file.Id]),file.Id);
        if(file.ContentDocumentId && caseMoniterWithContentDocMap[file.Id]){
            console.log(JSON.stringify(caseMoniterWithContentDocMap[file.Id]));
            let caseMoniterRecord = caseMoniterWithContentDocMap[file.Id];
            component.set("v.caseMoniterRecord",caseMoniterRecord);
        }
        
        var icon;
        switch(file.FileType){
            case "PDF":
                icon="doctype:pdf";
                break;
            case "JPG":
                icon="doctype:image";
                break;
            case "JPEG":
                icon="doctype:image";ent
                break;
            case "PNG":
                icon="doctype:image";
                break;
            case "WORD":
                icon="doctype:word";
                break;
            case "WORD_X":
                icon="doctype:word";
                break;
            case "ZIP":
                icon="doctype:zip";
                break;
            case "EXCEL":
                icon="doctype:excel";
                break;
            case "EXCEL_X":
                icon="doctype:excel";
                break;
            case "CSV":
                icon="doctype:csv";
                break;
            case "POWER_POINT":
                icon="doctype:ppt";
                break;
            case "POWER_POINT_X":
                icon="doctype:ppt";
                break;
            case "RTF":
                icon="doctype:rtf";
                break;
            default:
                icon="doctype:attachment";
        }
        component.set("v.icon",icon);
        // 190619 - T - 00185 - call helper to get the owner of the file.
		//helper.fileReviewForUser(component, event, helper);
	},
    
	handleSelect : function(component, event, helper) {
       // var click = event.getParam("value");
        var action,modalBody;
        //if(click == "view"){
            $A.createComponents([
                      ["c:fileViewer", { record : component.get("v.file") }],
            ],
                                function(components, status) {
                                    if (status === "SUCCESS") {
                                        modalBody = components[0];
                                        var body = component.get("v.body");
                                        body.push(modalBody);
                                        component.set("v.body", body);
                                    }
                                });

      //  }
       // else{action = "edit";}
       /* var navService = component.find("navService");
        var pageReference = {    
               "type": "standard__recordPage",
               "attributes": {
                   "recordId": component.get("v.file.ContentDocumentId"),
                   "objectApiName": "ContentDocument",
                   "actionName": action
               }
        };
        console.log(pageReference);
        event.preventDefault();
        navService.navigate(pageReference);*/
		
	},
   
    onCheckChange:function(component,event,helper){
       //component.set("v.isEdited",true);  
       console.log('--93--caseMonito',component.get("v.caseMoniterRecord"));  
       let action = component.get("c.saveRecord");
        
        action.setParams({'cmRecord':component.get("v.caseMoniterRecord")});
        
        action.setCallback(this,(response)=>{
            const state = response.getState();
            console.log(state,response.getReturnValue());
            if(state === 'SUCCESS'){
            	//helper.hideSpinner(component, event, helper);
            	// case detail was not refreshing
            	$A.get("e.force:refreshView").fire();

            } else if(state === 'ERROR'){
            	//helper.hideSpinner(component, event, helper);
        	}
                           
        });
        
        $A.enqueueAction(action);         
    },
                
})