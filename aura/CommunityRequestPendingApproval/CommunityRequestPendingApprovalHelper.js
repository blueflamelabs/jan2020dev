({
    performNavigation : function(component,event) {
        component.set("v.pWrapperlist",  event.getParam("pWrapperlist"));
        component.set("v.endPosn", event.getParam("endPosn"));
        component.set("v.startPosn", event.getParam("startPosn"));
    },
    getWorkingGroup : function(component) {
        var action = component.get("c.WorkingGroupsList");
        action.setCallback(this, function(resp) {
            var state=resp.getState();
            if(state === "SUCCESS"){
                component.set("v.WorkingGroupList", resp.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getGroupMemPickValue : function(component) {
        var action = component.get("c.GroupMemberRoleVoting");
        action.setCallback(this, function(resp) {
            var state=resp.getState();
            if(state === "SUCCESS"){
                 component.set("v.GroupMemRole", resp.getReturnValue().RoleOptions);
                 component.set("v.GroupMemVoting", resp.getReturnValue().VotingOptions);
            }
        });
        $A.enqueueAction(action);
    },
    paginate : function(component) {
        var wlist = component.get("v.pMasterWrapperlist");
        component.set("v.pWrapperlist", wlist);
            var subWrapperlist = [];
            for(var i=0; i<component.get("v.pageSize"); i++){
                if(wlist[i]){
                	subWrapperlist.push(wlist[i]);    
                }
            }
            component.set("v.pWrapperlist", subWrapperlist);
            component.set("v.totalPages", Math.ceil(wlist.length/component.get("v.pageSize")));
            component.set("v.currentPageNumber",1);
            this.generatePageList(component, 1);
    },
    getContacts : function(component) {
        var SelectedWorkingGrupId = component.get("v.SelectWorkingGroupId");
        var action = component.get("c.PendingApprovalGroupMember");
        action.setParams({
            "WorkingGrupId" : SelectedWorkingGrupId
        });
        action.setCallback(this, function(resp) {
            var state=resp.getState();
            if(state === "SUCCESS"){
                component.set("v.pMasterWrapperlistTotal", resp.getReturnValue());
                component.set("v.pMasterWrapperlist", resp.getReturnValue());
                //component.set("v.RepresentativeAccountId", resp.getReturnValue()[0].RepresentativeAccId);
                component.set("v.masterlistSize", component.get("v.pMasterWrapperlist").length);
                component.set("v.startPosn",0);
                component.set("v.endPosn",component.get("v.pageSize")-1);
                component.set("v.totalPages", Math.ceil(resp.getReturnValue().length/component.get("v.pageSize")));
                component.set("v.currentPageNumber",1);
                this.buildData(component);
            }
        });
        $A.enqueueAction(action);
    },
    sortBy: function(component,helper,field) {
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.pMasterWrapperlist");
        sortAsc = sortField != field || !sortAsc;
        if(field != 'Working_Group__r.Name' && field != 'Grou_Member__r.Name'){
            records.sort(function(a,b){
                var t1 = a.objAcc[field] == b.objAcc[field],
                    t2 = (!a.objAcc[field] && b.objAcc[field]) || (a.objAcc[field] < b.objAcc[field]);
                return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
            });
        }else if(field == 'Working_Group__r.Name'){
            records.sort(function(a,b){
                if(a.objAcc.Working_Group__c != undefined && b.objAcc.Working_Group__c != undefined){
                    console.log(a.objAcc.Working_Group__r['Name']);
                    var t1 = a.objAcc.Working_Group__r['Name'] == b.objAcc.Working_Group__r['Name'],
                        t2 = (!a.objAcc.Working_Group__r['Name'] && b.objAcc.Working_Group__r['Name']) 
                    			|| (a.objAcc.Working_Group__r['Name'] < b.objAcc.Working_Group__r['Name']);
                	return t1? 0: (sortAsc?-1:1)*(t2?1:-1);    
                }else{
                    console.log(a.objAcc.Working_Group__c);
                    var t1 = a.objAcc.Working_Group__c == b.objAcc.Working_Group__c,
                        t2 = (!a.objAcc.Working_Group__c && b.objAcc.Working_Group__c) 
                    			|| (a.objAcc.Working_Group__c < b.objAcc.Working_Group__c);
                	return t1? 0: (sortAsc?-1:1)*(t2?1:-1);  
                }
            });  
        }else if(field == 'Grou_Member__r.Name'){
            records.sort(function(a,b){
                if(a.objAcc.Grou_Member__c != undefined && b.objAcc.Grou_Member__c != undefined){
                    console.log(a.objAcc.Grou_Member__r['Name']);
                    var t1 = a.objAcc.Grou_Member__r['Name'] == b.objAcc.Grou_Member__r['Name'],
                        t2 = (!a.objAcc.Grou_Member__r['Name'] && b.objAcc.Grou_Member__r['Name']) 
                    			|| (a.objAcc.Grou_Member__r['Name'] < b.objAcc.Grou_Member__r['Name']);
                	return t1? 0: (sortAsc?-1:1)*(t2?1:-1);    
                }else{
                    console.log(a.objAcc.Grou_Member__c);
                    var t1 = a.objAcc.Grou_Member__c == b.objAcc.Grou_Member__c,
                        t2 = (!a.objAcc.Grou_Member__c && b.objAcc.Grou_Member__c) 
                    			|| (a.objAcc.Grou_Member__c < b.objAcc.Grou_Member__c);
                	return t1? 0: (sortAsc?-1:1)*(t2?1:-1);  
                }
            });  
        }
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.pMasterWrapperlist", records);
        var startPosn = component.get("v.startPosn");
        var endPosn = component.get("v.endPosn");
        var pageSize = component.get("v.pageSize");
        startPosn=0;
        endPosn=pageSize-1;
        component.set("v.startPosn", startPosn);
        component.set("v.endPosn", endPosn);
        //component.set("v.pWrapperlist", records);
        helper.paginate(component);
    },
    
    buildData : function(component, helper) {
        var filtervalue = component.get("v.singleFilter");
        component.set("v.pWrapperlist", '');
        var data = [];
        var pageNumber = component.get("v.currentPageNumber");
        var pageSize = component.get("v.pageSize");
        var allData = component.get("v.pMasterWrapperlist");
        var x = (pageNumber-1)*pageSize;
        //creating data-table data
        for(; x<=(pageNumber)*pageSize; x++){
            if(allData[x]){
                data.push(allData[x]);
            }
        }
        component.set("v.pWrapperlist", data);
        
        this.generatePageList(component, pageNumber);
    },
    
    /*
     * this function generate page list
     * */
    generatePageList : function(component, pageNumber){
        pageNumber = parseInt(pageNumber);
        var pageList = [];
        var totalPages = component.get("v.totalPages");
        if(totalPages > 1){
            if(totalPages <= 10){
                var counter = 2;
                for(; counter < (totalPages); counter++){
                    pageList.push(counter);
                } 
            } else{
                if(pageNumber < 5){
                    pageList.push(2, 3, 4, 5, 6);
                } else{
                    if(pageNumber>(totalPages-5)){
                        pageList.push(totalPages-5, totalPages-4, totalPages-3, totalPages-2, totalPages-1);
                    } else{
                        pageList.push(pageNumber-2, pageNumber-1, pageNumber, pageNumber+1, pageNumber+2);
                    }
                }
            }
        }
        console.log('!pageList  @@@@@@@@@@! '+pageList);
        component.set("v.pageList", pageList);
    },
})