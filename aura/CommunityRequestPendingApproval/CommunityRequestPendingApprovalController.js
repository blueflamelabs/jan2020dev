({
    
    doInit:function(component,event,helper){
        helper.getWorkingGroup(component);
        helper.getGroupMemPickValue(component);
        helper.getContacts(component);
        helper.sortBy(component,helper, "Name"); 
        var a=component.get("v.sortAsc");
        component.set("v.Name",a);
    },
    closeModal :function(component,event,helper){
        var modal = component.find("contactModal");
        var modalBackdrop = component.find("contactModalBackdrop");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    },
    updateGroupMem :function(component,event,helper){
        var SelectedGroupMemberId = component.get("v.SelectGroupMemberId");
        var ButtonAppralReject = component.get("v.SelectApprovalReject");
        var SelectedRole = component.get("v.SelectRole");
        var SelectedVoting = component.get("v.SelectVoting");
        var SelectReason = component.get("v.SelectReason");
        var isError = false;
        if(ButtonAppralReject == 'Reject' && SelectReason == ''){
            isError = true;
            var inputRejReason = component.find("RejReason");
            inputRejReason.set("v.errors", [{message:"Please Enter Rejected Reason"}]);
        }else{
            if(ButtonAppralReject == 'Reject'){
                var inputRejReason = component.find("RejReason");
                inputRejReason.set("v.errors", null);
            }
        }
        if(isError == false){
            var action = component.get("c.updateGroupMember");
            action.setParams({
                "GroupMemberId" : SelectedGroupMemberId,
                "Role" : SelectedRole,
                "Voting" : SelectedVoting,
                "RejectionReason" : SelectReason,
                "StatusName" : ButtonAppralReject
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Success Message',
                        message: 'Group Member Update Successfully',
                        duration:' 2000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    helper.getContacts(component);
                    var modal = component.find("contactModal");
                    var modalBackdrop = component.find("contactModalBackdrop");
                    $A.util.removeClass(modal,"slds-fade-in-open");
                    $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message:'Something is wrong group member not update',
                        messageTemplate: 'Mode is pester ,duration is 5sec and Message is overrriden',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
    OpenUpdateGroupMem :function(component,event,helper){
        component.set("v.SelectRole",'');
        component.set("v.SelectVoting",'');
        var SelectedGroupMemberId = event.getSource().get("v.value");
        var SelectedGroupMemberButtonName = event.getSource().get("v.label");
        component.set("v.SelectGroupMemberId", SelectedGroupMemberId);
        component.set("v.SelectApprovalReject", SelectedGroupMemberButtonName);
        var modal = component.find("contactModal");
        var modalBackdrop = component.find("contactModalBackdrop");
        $A.util.addClass(modal,"slds-fade-in-open");
        $A.util.addClass(modalBackdrop,"slds-backdrop_open");
    },
    SelectedRecName:function(component,event,helper){
        helper.getContacts(component);
        helper.sortBy(component,helper, "Name"); 
        var a=component.get("v.sortAsc");
        component.set("v.Name",a);
    },
    processMe : function(component, event, helper) {
        component.set("v.currentPageNumber", parseInt(event.target.name));
        helper.buildData(component, helper);
    },
    next:function(component,event,helper){
        //helper.next(component);
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber+1);
        helper.buildData(component, helper);
    },
    previous:function(component,event,helper){
        // helper.previous(component);
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber-1);
        helper.buildData(component, helper);
    },
    First:function(component,event,helper){
        //helper.First(component);
        component.set("v.currentPageNumber", 1);
        helper.buildData(component, helper);
    },
    Last:function(component,event,helper){
        //helper.Last(component);
        component.set("v.currentPageNumber", component.get("v.totalPages"));
        helper.buildData(component, helper);
    },
    onSelectChange: function(component, event, helper) {
        component.set("v.pageSize",component.find("recordSize").get("v.value"));
        helper.paginate(component);
    },
    doSomething: function(component, event, helper) {
        var ctarget = event.currentTarget;
        var mailToUser = ctarget.dataset.value;
        window.open("mailto:"+mailToUser+"","_blank");
    },
    sortByName: function(component, event, helper) {
        helper.sortBy(component,helper, "Grou_Member__r.Name"); 
        var a=component.get("v.sortAsc");
        component.set("v.Name",a);
        component.set("v.NameArrow",true);
        component.set("v.CreatedArrow",false);
        component.set("v.CompanyArrow",false);
        component.set("v.WorkingGroupArrow",false);
    },
    sortByWorkingGroup: function(component, event, helper) {
        helper.sortBy(component,helper, "Working_Group__r.Name"); 
        var a=component.get("v.sortAsc");
        component.set("v.WorkingGroupName",a);
        component.set("v.NameArrow",false);
        component.set("v.CreatedArrow",false);
        component.set("v.CompanyArrow",false);
        component.set("v.WorkingGroupArrow",true);
    },
    sortByCompany: function(component, event, helper) {
        helper.sortBy(component,helper, "Corporate_Member__c"); 
        var a=component.get("v.sortAsc");
        component.set("v.Company",a);
        component.set("v.CompanyArrow",true);
        component.set("v.NameArrow",false);
        component.set("v.CreatedArrow",false);
        component.set("v.WorkingGroupArrow",false);
    },
    
    sortByCreatedDate: function(component, event, helper) {
        helper.sortBy(component,helper, "CreatedDate");
        var a=component.get("v.sortAsc");
        component.set("v.CreatedDateRec",a);
        component.set("v.CreatedArrow",true);
        component.set("v.NameArrow",false);
        component.set("v.CompanyArrow",false);
        component.set("v.WorkingGroupArrow",false);
    },
    
    
    alphaClick: function(component, event, helper) {
        var selectedItem = event.currentTarget; 
        var searchValue = selectedItem.dataset.record; 
        component.set("v.singleFilter",searchValue);
        var SelectedWorkingGrupId = component.get("v.SelectWorkingGroupId");
        if(searchValue != 'All'){
            var action = component.get("c.PendingApprovalGroupMemberSearch");
            action.setParams({
                "WorkingGrupId" : SelectedWorkingGrupId,
                "SearchValue"  : searchValue,
                "isSingleSearch" : true
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    //component.set("v.pWrapperlist",response.getReturnValue());
                    component.set("v.pMasterWrapperlist", response.getReturnValue());
                    component.set("v.masterlistSize", component.get("v.pMasterWrapperlist").length);
                    component.set("v.startPosn",0);
                    component.set("v.endPosn",component.get("v.pageSize")-1);
                    helper.paginate(component);
                }
            });
            $A.enqueueAction(action);
        }else{
            // component.set("v.pMasterWrapperlist", component.get("v.pMasterWrapperlistTotal"));
            //helper.paginate(component);
            helper.getContacts(component);
        }
    },
    searchTextFU : function(component, event, helper) {
        var SelectedWorkingGrupId = component.get("v.SelectWorkingGroupId");
        var searchInput = component.find("searchInput");
        var searchValue = searchInput.get("v.value");
        if(searchValue != ''){
            var action = component.get("c.PendingApprovalGroupMemberSearch");
            action.setParams({
                "WorkingGrupId" : SelectedWorkingGrupId,
                "SearchValue"  : searchValue,
                "isSingleSearch" : false
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    //component.set("v.pWrapperlist",response.getReturnValue());
                    component.set("v.pMasterWrapperlist", response.getReturnValue());
                    component.set("v.masterlistSize", component.get("v.pMasterWrapperlist").length);
                    component.set("v.startPosn",0);
                    component.set("v.endPosn",component.get("v.pageSize")-1);
                    helper.paginate(component);
                }
            });
            $A.enqueueAction(action);
        }else{
            helper.getContacts(component);
        }
    }
})