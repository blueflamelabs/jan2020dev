({
    
    doInit:function(component,event,helper){
        component.set("v.recordId",component.get("v.AssociationRecordId"));
        component.set("v.recordId18Digit",component.get("v.AssociationRecordId")+'QAA');
        helper.getContacts(component);
        helper.sortBy(component,helper, "Name"); 
        var a=component.get("v.sortAsc");
        component.set("v.Name",a);
    },
    closeModal :function(component,event,helper){
        var modal = component.find("contactModal");
        var modalBackdrop = component.find("contactModalBackdrop");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    },
    IndividualMem :function(component,event,helper){
        var CorMemAccId = event.getSource().get("v.value");
        var action = component.get("c.AccountsMemberRecord");
        action.setParams({
            "RecId" : CorMemAccId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.AccountRec",response.getReturnValue()); 
                var modal = component.find("contactModal");
                var modalBackdrop = component.find("contactModalBackdrop");
                $A.util.addClass(modal,"slds-fade-in-open");
                $A.util.addClass(modalBackdrop,"slds-backdrop_open");
            }
        });
        $A.enqueueAction(action);
    },
    SelectedRecName:function(component,event,helper){
        helper.getContacts(component);
        helper.sortBy(component,helper, "Name"); 
        var a=component.get("v.sortAsc");
        component.set("v.Name",a);
    },
    CopytoClipboard : function(component, event, helper) {
        var EmailList = [];
        var pWrapperlistRecord = component.get("v.pMasterWrapperlistTotal");
        for(var i=0; i<pWrapperlistRecord.length; i++){
            if(pWrapperlistRecord[i].isCheck == true){
                if(pWrapperlistRecord[i].objAcc.PersonEmail != undefined){
                	EmailList.push(pWrapperlistRecord[i].objAcc.PersonEmail);
                }
            }
        }
        var EmailString;
        if(EmailList.length >0)
            EmailString = EmailList.join(';');
         // Create an auxiliary hidden input
          var aux = document.createElement("input");
          // Get the text from the element passed into the input
          aux.setAttribute("value", EmailString);
          // Append the aux input to the body
          document.body.appendChild(aux);
          // Highlight the content
          aux.select();
          // Execute the copy command
          document.execCommand("copy");
          // Remove the input from the body
          document.body.removeChild(aux);
        if(EmailList.length >=1){  
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Success Message',
                message: 'Successfully Copied to Clipboard',
                messageTemplate: 'duration is 1sec and Message is overrriden',
                duration:' 1000',
                key: 'info_alt',
                type: 'success',
                mode: 'pester'
            });
            toastEvent.fire();
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error Message',
                message:'You need to select at least one record',
                messageTemplate: 'duration is 1sec and Message is overrriden',
                duration:' 1000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            });
            toastEvent.fire();
        }   
    },
    isAllSelect : function(component, event, helper) {
        var pWrapperlistRecord = component.get("v.pMasterWrapperlistTotal");
        var isParentCheck = true;
        for(var i=0; i<pWrapperlistRecord.length; i++){
            if(pWrapperlistRecord[i].isCheck == false){
                isParentCheck = false;
            }
        }
        component.find("SelectAllcheckbox").set("v.value",isParentCheck);
    },
    selectALL : function(component, event, helper) {
        var selectAllCheck = component.find("SelectAllcheckbox").get("v.value");
         if(selectAllCheck == true){
             var pWrapperlistRecord = component.get("v.pMasterWrapperlistTotal");
             for(var i=0; i<pWrapperlistRecord.length; i++){
                pWrapperlistRecord[i].isCheck = true;
            }
            helper.buildData(component);
         }else{
             var pWrapperlistRecord = component.get("v.pMasterWrapperlistTotal");
             for(var i=0; i<pWrapperlistRecord.length; i++){
                pWrapperlistRecord[i].isCheck = false;
            }
            helper.buildData(component);
         }
    },
     processMe : function(component, event, helper) {
        component.set("v.currentPageNumber", parseInt(event.target.name));
        helper.buildData(component, helper);
    },
    next:function(component,event,helper){
        //helper.next(component);
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber+1);
        helper.buildData(component, helper);
    },
    previous:function(component,event,helper){
       // helper.previous(component);
        var pageNumber = component.get("v.currentPageNumber");
        component.set("v.currentPageNumber", pageNumber-1);
        helper.buildData(component, helper);
    },
    First:function(component,event,helper){
        //helper.First(component);
        component.set("v.currentPageNumber", 1);
        helper.buildData(component, helper);
    },
    Last:function(component,event,helper){
        //helper.Last(component);
        component.set("v.currentPageNumber", component.get("v.totalPages"));
        helper.buildData(component, helper);
    },
    onSelectChange: function(component, event, helper) {
        component.set("v.pageSize",component.find("recordSize").get("v.value"));
        helper.paginate(component);
    },
    doSomething: function(component, event, helper) {
         var ctarget = event.currentTarget;
    	 var mailToUser = ctarget.dataset.value;
         window.open("mailto:"+mailToUser+"","_blank");
    },
    sortByName: function(component, event, helper) {
        helper.sortBy(component,helper, "Name"); 
        var a=component.get("v.sortAsc");
        component.set("v.Name",a);
        component.set("v.NameArrow",true);
        component.set("v.PhoneArrow",false);
        component.set("v.MobileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.MmberArrow",false);
        component.set("v.AssociationRoleArrow",false);
        component.set("v.AssociationAccountArrow",false);
        component.set("v.RegionArrow",false);
        component.set("v.RenewalArrow",false);
    },
    sortByRegion: function(component, event, helper) {
        helper.sortBy(component,helper, "Region_Name__c"); 
        var a=component.get("v.sortAsc");
        component.set("v.Region",a);
        component.set("v.RegionArrow",true);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",false);
        component.set("v.MobileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.MmberArrow",false);
        component.set("v.AssociationRoleArrow",false);
        component.set("v.AssociationAccountArrow",false);
        component.set("v.RenewalArrow",false);
    },
    sortByRenewal: function(component, event, helper) {
        helper.sortBy(component,helper, "Next_Renewal_Date__c"); 
        var a=component.get("v.sortAsc");
        component.set("v.Renewal",a);
        component.set("v.RenewalArrow",true);
        component.set("v.RegionArrow",false);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",false);
        component.set("v.MobileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.MmberArrow",false);
        component.set("v.AssociationRoleArrow",false);
        component.set("v.AssociationAccountArrow",false);
    },
    AssociationRole: function(component, event, helper) {
        helper.sortBy(component,helper, "Association_Role__c"); 
        var a=component.get("v.sortAsc");
        component.set("v.AssociationRoles",a);
        component.set("v.AssociationRoleArrow",true);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",false);
        component.set("v.MobileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.AssociationAccountArrow",false);
        component.set("v.RegionArrow",false);
        component.set("v.RenewalArrow",false);
    },
    AssociationAccount: function(component, event, helper) {
        helper.sortBy(component,helper, "Association_Account__r.Name"); 
        var a=component.get("v.sortAsc");
        component.set("v.AssociationAccounts",a);
        component.set("v.AssociationAccountArrow",true);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",false);
        component.set("v.MobileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.AssociationRoleArrow",false);
        component.set("v.RegionArrow",false);
        component.set("v.RenewalArrow",false);
    },
    sortByPhone: function(component, event, helper) {
        helper.sortBy(component,helper, "Phone");
        var a=component.get("v.sortAsc");
        component.set("v.phone",a);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",true);
        component.set("v.MobileArrow",false);
        component.set("v.EmailArrow",false);
        component.set("v.AssociationAccountArrow",false);
        component.set("v.AssociationRoleArrow",false);
        component.set("v.RegionArrow",false);
        component.set("v.RenewalArrow",false);
    },
    sortByMobile: function(component, event, helper) {
        //helper.sortBy(component,helper, "PersonMobilePhone");
        helper.sortBy(component,helper, "Join_Date__c");
        var a=component.get("v.sortAsc");
        component.set("v.Mobile",a);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",false);
        component.set("v.MobileArrow",true);
        component.set("v.EmailArrow",false);
        component.set("v.MmberArrow",false);
        component.set("v.AssociationAccountArrow",false);
        component.set("v.AssociationRoleArrow",false);
        component.set("v.RegionArrow",false);
        component.set("v.RenewalArrow",false);
    },
    sortByMember: function(component, event, helper) {
        //helper.sortBy(component,helper, "PersonMobilePhone");
        helper.sortBy(component,helper, "Member_Type__r.Name");
        var a=component.get("v.sortAsc");
        component.set("v.Member",a);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",false);
        component.set("v.MobileArrow",false);
        component.set("v.MmberArrow",true);
        component.set("v.EmailArrow",false);
        component.set("v.AssociationAccountArrow",false);
        component.set("v.AssociationRoleArrow",false);
        component.set("v.RegionArrow",false);
        component.set("v.RenewalArrow",false);
    },
    sortByEmail: function(component, event, helper) {
        helper.sortBy(component,helper, "PersonEmail");
        var a=component.get("v.sortAsc");
        component.set("v.email",a);
        component.set("v.NameArrow",false);
        component.set("v.PhoneArrow",false);
        component.set("v.MobileArrow",false);
        component.set("v.EmailArrow",true);
        component.set("v.AssociationAccountArrow",false);
        component.set("v.AssociationRoleArrow",false);
        component.set("v.RegionArrow",false);
        component.set("v.RenewalArrow",false);
    },
    alphaClick: function(component, event, helper) {
         var selectedItem = event.currentTarget; 
     	 var searchValue = selectedItem.dataset.record; 
        component.set("v.singleFilter",searchValue);
        var recordIdParent = component.get("v.recordId");
       //  recordIdParent = 'a000m000002i0Fr';
        var recordTyName = component.get("v.SelectRecTypeName");
        if(searchValue != 'All'){
            var action = component.get("c.getAccounts");
            action.setParams({
                "RecId" : recordIdParent,
                "SearchText"  : searchValue,
                "isSingleChar" : true,
                "RecName" : recordTyName
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    //component.set("v.pWrapperlist",response.getReturnValue());
                    component.set("v.pMasterWrapperlist", response.getReturnValue());
                    component.set("v.masterlistSize", component.get("v.pMasterWrapperlist").length);
                    component.set("v.startPosn",0);
                    component.set("v.endPosn",component.get("v.pageSize")-1);
                    helper.paginate(component);
                }
            });
            $A.enqueueAction(action);
        }else{
           // component.set("v.pMasterWrapperlist", component.get("v.pMasterWrapperlistTotal"));
            //helper.paginate(component);
            helper.getContacts(component);
        }
    },
    searchTextFU : function(component, event, helper) {
        var recordIdParent = component.get("v.recordId");
        // recordIdParent = 'a000m000002i0Fr';
        var searchInput = component.find("searchInput");
    	var searchValue = searchInput.get("v.value");
        var recordTyName = component.get("v.SelectRecTypeName");
        if(searchValue != ''){
            var action = component.get("c.getAccounts");
            action.setParams({
                "RecId" : recordIdParent,
                "SearchText"  : searchValue,
                "isSingleChar" : false,
                "RecName" : recordTyName
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    //component.set("v.pWrapperlist",response.getReturnValue());
                    component.set("v.pMasterWrapperlist", response.getReturnValue());
                    component.set("v.masterlistSize", component.get("v.pMasterWrapperlist").length);
                    component.set("v.startPosn",0);
                    component.set("v.endPosn",component.get("v.pageSize")-1);
                    helper.paginate(component);
                }
            });
            $A.enqueueAction(action);
        }else{
           helper.getContacts(component);
        }
    }
})