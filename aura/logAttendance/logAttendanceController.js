({
    handleClick : function(component, event, helper) {
        alert('Log Attendance button clicked');
    },

    openModel: function(component, event, helper) {
        // Set isModalOpen attribute to true
        component.set("v.isModalOpen", true);
     },
    
    closeModel: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
    },
    
    submitDetails: function(component, event, helper) {
        // Set isModalOpen attribute to false
        //Add your code to call apex method or do some processing
        component.set("v.isModalOpen", false);
    },

    doInit: function(component, event, helper) {
        //console.log('====doInit Called==============');
        
        helper.doInitHelper(component, event);
    },
   
       
    handleMouseHover: function(component, event, helper) {
        //console.log('====handleMouseHover==============');
        var getEvent = component.get('v.Events');
        // console.log('====getEventList==============', getEvent);

        var recId = event.srcElement.id;
        //console.log('====handleMouseHover====recId==========', recId);
        component.set("v.reId", recId);
        helper.getMiniLayout(component, event, helper);
    },

    handleMouseOut: function(component, event, helper) {
        //console.log('====handleMouseOut==============');
        component.set("v.hoverRow", -1);
        component.set("v.togglehover", false);
    },

    searchKeyChange: function(component, event, helper) {
        // console.log('====searchKeyChange==============');
        helper.searchStringChanged(component, event, helper);
    },

    /* javaScript function for pagination */
    navigation: function(component, event, helper) {
        var sObjectList = component.get("v.Events");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var whichBtn = event.getSource().get("v.name");
        // check if whichBtn value is 'next' then call 'next' helper method
        if (whichBtn == 'next') {
            component.set("v.currentPage", component.get("v.currentPage") + 1);
            helper.next(component, event, sObjectList, end, start, pageSize);
        }
        // check if whichBtn value is 'previous' then call 'previous' helper method
        else if (whichBtn == 'previous') {
            component.set("v.currentPage", component.get("v.currentPage") - 1);
            helper.previous(component, event, sObjectList, end, start, pageSize);
        }
    },

    createParticipationRecord: function(component, event, helper) { 
        // console.log('====createParticipationRecord==============');
        var eventId = event.srcElement.id;
        // console.log('====eventId==========', eventId);
        helper.doCreateParticipationRecord(component, event, eventId);
    }
    
})