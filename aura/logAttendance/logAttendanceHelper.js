({
    helperMethod : function() {

    },

    doInitHelper: function(component, event) {
        // console.log('=== helper method called===============');
        var action = component.get("c.fetchEvent");
        action.setCallback(this, function(response) {
            var state = response.getState();
            var noRecordMsg = 'No recent events to display.';
            // console.log('state ---->' + state);
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                // console.log('result ---->' + JSON.stringify(result));
                // console.log('result.eventList.length ---->' + result.eventList.length);
                if(result.eventList.length > 0){
                    component.set('v.Events', result.eventList);
                    var pageSize = component.get("v.pageSize");
                    var totalRecordsList = result.eventList;
                    var totalLength = totalRecordsList.length ;
                    component.set("v.totalRecordsCount", totalLength);
                    component.set("v.startPage",0);
                    component.set("v.endPage",pageSize-1);
                    
                    var PaginationList = [];
                    for(var i=0; i < pageSize; i++){
                        if(component.get("v.Events").length > i){
                            PaginationList.push(result.eventList[i]);    
                        } 
                    }
                    component.set('v.eventListToDisplay', PaginationList);
                    //component.set("v.selectedCount" , 0);
                    //use Math.ceil() to Round a number upward to its nearest integer
                    component.set("v.totalPagesCount", Math.ceil(totalLength / pageSize));    
                }else{
                    // if there is no records then display message
                    component.set("v.noRecord" , noRecordMsg);
                } 
            }
            else{
                component.set("v.noRecord" , noRecordMsg);
            }
        });
        // enqueue the action 
        $A.enqueueAction(action);
    },

    //Fetch the releted account on mouseHover 
    getMiniLayout:function(component, event, helper){
        // console.log('====getMiniLayout==============');
        
        var getEventList = component.get('v.Events');
        // console.log('====getEventList==============', getEventList);

        var recId = component.get("v.reId");
        console.log('====component.get(v.reId)==============', recId);

        for(var i = 0; i < getEventList.length; i++){
            if(getEventList[i].Id == recId){
                
                component.set('v.mouseHoverData', getEventList[i].Description);
                break;
            }
        }
        component.set("v.hoverRow", parseInt(event.target.dataset.index));
        component.set("v.togglehover",true);
    },
    
    searchStringChanged: function (component, event, helper) {
        try{
            var searchString = component.get('v.searchKeyword');
            // console.log('======ssearchString=======',searchString);
            if(!$A.util.isUndefinedOrNull(searchString)){
               searchString = searchString.trim(); 
            }
            var eventList = component.get('v.Events');
            // console.log('eventList=========',eventList);
            var filteredRecords = [];
            var displayedFields = [];
            var noRecordMsg = 'No recent events to display.';
            displayedFields.push('Subject');
            displayedFields.push('StartDateTime');
            displayedFields.push('Location');
            displayedFields.push('What.Name');
            displayedFields.push('zoom_app__Zoom_Event__r.zoom_app__Zoom_Meeting_Id__c');
            if (!$A.util.isEmpty(searchString) && !$A.util.isUndefinedOrNull(searchString)) {
                for (var i = 0; i < eventList.length; i++) {
                    var singleRecord = eventList[i];
                    var isMatchFound = false;
                    for (var key in singleRecord) {
                        // console.log('key========',key);
                        // console.log('======singleRecord[key])=======',singleRecord[key]);
                        
                        if (singleRecord.hasOwnProperty(key)) {
                            // console.log('========singleRecord[key]) type of ==========',typeof singleRecord[key]);
                            if(singleRecord[key] !== null && singleRecord[key] !== undefined) {//remove toString error
                                if ((((singleRecord[key]).toString()).toLowerCase()).indexOf(searchString.toLowerCase()) > -1 //) {
                                    && displayedFields.indexOf(key) > -1) {
                                        // console.log('====2nd IF====== MATCH FOUND======');
                                        isMatchFound = true; 
                                } else if(singleRecord[key] !== null && singleRecord[key] !== undefined &&
                                    typeof singleRecord[key] === 'object') {
                                        // console.log('====2nd IF====== MATCH FOUND=singleRecord[key]=====' , singleRecord[key].Name);
                                        //// console.log('====2nd IF====== MATCH FOUND=singleRecord[key]=====' , singleRecord[key].zoom_app__Zoom_Event__r.zoom_app__Zoom_Meeting_Id__c);
                                        // console.log('Key is of object type');
                                        if ((((singleRecord[key].Name != undefined && 
                                            singleRecord[key].Name).toString()).toLowerCase()).indexOf(searchString.toLowerCase()) > -1 
                                        ) {
                                            // 121219 - T-000947 - VennScience_BFL_Amruta - Added logic to search in WhatId.Name
                                            // console.log('====2nd IF====== MATCH FOUND on Name======');
                                            isMatchFound = true; 
                                        } else if(key === 'zoom_app__Zoom_Event__r') {
                                            // 121219 - T-000947 - VennScience_BFL_Amruta - Added logic to search in meeting id
                                            // console.log('Zoom meeting id is present');
                                            // console.log('*****singleRecord[key])=======',singleRecord[key].zoom_app__Zoom_Meeting_Id__c);
                                            if ((((singleRecord[key].zoom_app__Zoom_Meeting_Id__c != undefined && 
                                                singleRecord[key].zoom_app__Zoom_Meeting_Id__c).toString()).toLowerCase()).indexOf(searchString.toLowerCase()) > -1 
                                                ) {
                                                    // console.log('====2nd IF====== MATCH FOUND on Meeting Id======');
                                                    isMatchFound = true; 
                                            } // End of inner most if
                                        } // End of inner else-if
                                    } // End of outer else-if  
                            } // End of outer if
                        } // End of outer most if
                    } // End of inner  for
                    // console.log('=====isMatchFound======' + isMatchFound);
                    if (isMatchFound) {
                        filteredRecords.push(singleRecord);
                    } else {
                        var noRecordMsg = 'No recent events to display.';
                        // console.log('=====NO MATCH FOUND======');
                        component.set('v.noRecord', noRecordMsg);
                    } // End of else-if block
                } // End of outer for
                // console.log('======filteredRecords======',filteredRecords);
                //component.set('v.eventListToDisplay', filteredRecords);

                var pageSize = component.get("v.pageSize");
                // console.log('======pageSize======',pageSize);
                
                var totalLength = filteredRecords.length ;
                component.set("v.totalRecordsCount", totalLength);
                // console.log('======totalLength======',totalLength);

                component.set("v.startPage",0);
                component.set("v.endPage",pageSize-1);

                // console.log('======endPage======',pageSize-1);
                
                var PaginationList = [];
                for(var i=0; i < pageSize; i++){
                    if(filteredRecords.length > i){
                        PaginationList.push(filteredRecords[i]);    
                    } 
                }
                // console.log('======PaginationList======',PaginationList);
                // console.log('startPage======',component.get("v.startPage"));
                // console.log('endPage======',component.get("v.endPage"));
                // console.log('currentpage=====',component.get("v.currentPage"));
                component.set('v.eventListToDisplay', PaginationList);
                component.set('v.currentPage', 1);
                component.set("v.totalPagesCount", Math.ceil(totalLength / pageSize));                
            } else {
                // show original list
                component.set('v.eventListToDisplay', component.get('v.Events'));
                // console.log('startPage======',component.get("v.startPage"));
                // console.log('endPage======',component.get("v.endPage"));
                // console.log('currentpage=====',component.get("v.currentPage"));
                // Amruta - Added code from here
                
                //var getEventList = [];
                
            }
        }
        catch(e){
            //// console.log('Error --->>'+e);
        }
    },

    // navigate to next pagination record set   
    next : function(component, event, sObjectList, end, start, pageSize){
        // console.log('NEXT --->>');
        var Paginationlist = [];
        var counter = 0;
        for(var i = end + 1; i < end + pageSize + 1; i++){
            if(sObjectList.length > i){ 
                Paginationlist.push(sObjectList[i]);
            }
            counter ++ ;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.eventListToDisplay', Paginationlist);
    },
   // navigate to previous pagination record set   
    previous : function(component,event,sObjectList,end,start,pageSize){
        // console.log('previous --->>');
        var Paginationlist = [];
        var counter = 0;
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
                Paginationlist.push(sObjectList[i]);
                counter ++;
            }else{
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.eventListToDisplay', Paginationlist);
    },
    doCreateParticipationRecord: function(component, event, eventId) { 
        // console.log('=== helper method called doCreateParticipationRecord===============');
        var eventIdToPass = event.srcElement.id;
        // console.log('====eventIdToPass==========', eventIdToPass);

        var action = component.get("c.CreateParticipation");
        // set the parameters to method 
        action.setParams({
            "eventId": eventIdToPass
         });
        action.setCallback(this, function(response) { 
            var state = response.getState();
            // console.log('state ---->' + state);
            if (state === "SUCCESS"){ 
                // console.log('======Record Created Successfully==========');
                var result = response.getReturnValue();
                // console.log('======isParticipationCreated ==========', result);
                // console.log('======isParticipationCreated ==========', result.isParticipationCreated);
                // console.log('======participationRelatedEvent==========',result.participationRelatedEvent);
                if(result.isParticipationCreated == true){
                    // show toast message
                    // 121219 - T-000947 - VennScience_BFL_Amruta - Commented below code
                    // alert('Record is created');
                    // 121219 - T-000947 - VennScience_BFL_Amruta - Added toast message block ends here
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Success',
                        message: 'Attendance Successfully Submitted for'+' '+result.participationRelatedEvent.Subject,
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    // 121219 - T-000947 - VennScience_BFL_Amruta - Added toast message block ends here
                    // console.log('result.eventList======',result.eventList);
                    // Update the list of event
                    if(result.eventList.length > 0){
                        component.set('v.Events', result.eventList);
                        var pageSize = component.get("v.pageSize");
                        var totalRecordsList = result.eventList;
                        var totalLength = totalRecordsList.length ;
                        component.set("v.totalRecordsCount", totalLength);
                        component.set("v.startPage",0);
                        component.set("v.endPage",pageSize-1);
                        
                        var PaginationList = [];
                        for(var i=0; i < pageSize; i++){
                            if(component.get("v.Events").length > i){
                                PaginationList.push(result.eventList[i]);    
                            } 
                        }
                        component.set('v.eventListToDisplay', PaginationList);
                        //component.set("v.selectedCount" , 0);
                        //use Math.ceil() to Round a number upward to its nearest integer
                        component.set("v.totalPagesCount", Math.ceil(totalLength / pageSize));    
                    } else{
                        // if there is no records then display message
                        var noRecordMsg = 'No recent events to display.';
                        // console.log('=====NO MATCH FOUND======');
                        component.set('v.noRecord', noRecordMsg);
                        component.set('v.eventListToDisplay', result.eventList);
                        // console.log('Inside no record block');
                    } 
                }
            } else if(state === "ERROR") {
                // console.log('Error occurred');
                var result = response.getReturnValue();
                // console.log('======isParticipationCreated ==========', result);
                // 121219 - T-000947 - VennScience_BFL_Amruta - Added toast message block starts here
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error',
                    message:'Attendance is not submitted',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
                // 121219 - T-000947 - VennScience_BFL_Amruta - Added toast message block ends here
            }
        });
        // enqueue the action 
        $A.enqueueAction(action);
    }
})