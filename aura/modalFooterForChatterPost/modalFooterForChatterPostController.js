({
    
    handleClose : function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    },
    
    handleSave : function(component, event, helper) {
        var appEvent = $A.get("e.c:saveAll");
        appEvent.setParams({ "message" : "save" });
        appEvent.fire();
    },
    
    handleValidation : function(component, event, helper) {
        var validate = component.get("v.validateCount");
        var file = component.get("v.fileCount")
        if(event.getParam("message") == "save"){
            validate++;
            component.set("v.validateCount",validate);
        }
        if(validate == file){
            component.find("overlayLib").notifyClose();
            window.setTimeout(
                $A.getCallback(function() {
                    var appEvent = $A.get("e.c:refreshRelatedList");
                    appEvent.fire();
                }),1000);
            
        }
    }
})