({
	save : function(component, event, helper) {
		let action = component.get("c.saveLeadRecord");
        
        action.setParams({'rec':component.get("v.record")});
        
        action.setCallback(this,(response) => {
            if(response.getState() === 'SUCCESS') {
            	component.set("v.record",response.getReturnValue());
            	this.changeStep(component, event, helper);
            } else if(response.getState() === 'ERROR') {
                 const errors = response.getError();
               	 helper.handleErrors(errors);
            }
        });
        
        $A.enqueueAction(action);
	},
    changeStep: function(component, event, helper){
      const currentStepName = component.get("v.viewState");
      let stepName = '';
    
        if(currentStepName === 'step1'){
            stepName = 'step2';
        } else if(currentStepName === 'step2'){
            stepName = 'step3';
        } else if(currentStepName === 'step3'){
            stepName = 'step4';
        }else if(currentStepName === 'step4'){
            component.find('wizard').closeModal();
            this.displayError('Record Saved Successfully','Success','success','dismissible');
        }
    
        component.set("v.viewState",stepName);
    },
    handleErrors : function( errors) {
        // Retrieve and display the error message(s) sent by the server
        let isUnknownError = true;
        if (typeof errors !== 'undefined' && Array.isArray(errors) && errors.length > 0) {
            errors.forEach(error => {
                // Check for 'regular' errors
                if (typeof error.message !== 'undefined') {
                    this.displayError(error.message,'Error','error','sticky');
                    isUnknownError = false;
                }
                // Check for 'pageError' errors
                const pageErrors = error.pageErrors;
                if (typeof pageErrors !== 'undefined' && Array.isArray(pageErrors) && pageErrors.length > 0) {
                    pageErrors.forEach(pageError => {
                        if (typeof pageError.message !== 'undefined') {
                            this.displayError(pageError.message,'Error','error','sticky');
                            isUnknownError = false;
                        }
                    });
                }
            });
        }
        // Make sure that we display at least one error message
        if (isUnknownError) {
            this.displayError('Unknown error','Error','error','sticky');
        }
        // Display raw error stack in console
        console.error(JSON.stringify(errors));
    },

    displayError : function(message,title,type,mode) {
        // Display error in console
        // Fire error toast if available
        const toastEvent = $A.get("e.force:showToast");
        if (typeof toastEvent !== 'undefined') {
            toastEvent.setParams({
                title : title,
                message : message,
                type : type,
                mode: mode
            });
            toastEvent.fire();
        }
    }

})