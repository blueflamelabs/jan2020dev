({
    closeData : function(component, event, helper){
        component.find('wizard').closeModal();
    },
    doinit: function(component, event, helper){
        component.set("v.viewState",'step1');
        component.find('wizard').openModal();
    },
    next: function(component, event, helper){
        const currentStepName = component.get("v.viewState");
        let stepName = '';
        
        if(currentStepName === 'step1'){
            let allValid = true;
            component.find('fieldValidation').map(function(inputCmp) {
                if(inputCmp.get("v.value") == null || inputCmp.get("v.value") == ''){
                    $A.util.addClass(inputCmp, 'slds-visible slds-has-error');
                    allValid = false;
                } else {
                    $A.util.removeClass(inputCmp, 'slds-visible slds-has-error'); 
                }
            });
            if(!allValid){
                return;
            }
        } 
        helper.save(component, event, helper);
    },
    previous: function(component, event, helper){
      const currentStepName = component.get("v.viewState");
      let stepName = '';
      
        if(currentStepName === 'step2'){
            stepName = 'step1';
        } else if(currentStepName === 'step3'){
            stepName = 'step2';
        } else if(currentStepName === 'step4'){
            stepName = 'step3';
        }
        
        component.set("v.viewState",stepName);
    },
    save :function(component, event, helper){
        component.find('wizard').closeModal();
    },
   
})