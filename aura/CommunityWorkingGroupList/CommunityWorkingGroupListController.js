({
    doInit:function(component,event,helper){
        var AssrecordId = component.get("v.Association");
        var action = component.get("c.WorkingGroupsRecord");
        action.setParams({
            "RecId" : AssrecordId
        });
        action.setCallback(this, function(resp) {
            var state=resp.getState();
            if(state === "SUCCESS"){
                component.set("v.WorkGroupWrapperlist",resp.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
   closeModel: function(component, event, helper) {
      // Set isModalOpen attribute to false  
      component.set("v.isModalOpen", false);
   },
   memberCreate : function(component,event,helper){
      
        var selectedButtonName = event.getSource().get("v.label");
        var SelectId = event.getSource().get("v.value");
        var AssrecordId = component.get("v.Association");
        var action = component.get("c.MemberGroupsRecordCreate");
        action.setParams({
            "RecId" : AssrecordId,
            "SelectedRecId" : SelectId,
            "StatusName" : selectedButtonName
        });
        action.setCallback(this, function(resp) {
            var state=resp.getState();
            if(state === "SUCCESS"){
                component.set("v.WorkGroupWrapperlist",resp.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    openPopupUnjoin:function(component,event,helper){
        component.set("v.isModalOpen", true);
        var SelectId = event.getSource().get("v.value");
         var action = component.get("c.getWorkingGroupName");
        action.setParams({
            "SelectedRecId" : SelectId
        });
         action.setCallback(this, function(resp) {
            var state=resp.getState();
            if(state === "SUCCESS"){
                component.set("v.workingGroupName",resp.getReturnValue());
                component.set("v.workingGroupId",SelectId);
            }
        });
        $A.enqueueAction(action);
    },
    unjoinWorkingGroup:function(component,event,helper){
         component.set("v.isModalOpen", false);
        var SelectId = component.get("v.workingGroupId");
        var AssrecordId = component.get("v.Association");
        var action = component.get("c.updateGroupMember");
        action.setParams({
             "RecId" : AssrecordId,
            "SelectedRecId" : SelectId
        });
         action.setCallback(this, function(resp) {
            var state=resp.getState();
            if(state === "SUCCESS"){
               component.set("v.WorkGroupWrapperlist",resp.getReturnValue());
                 
            }
        });
        $A.enqueueAction(action);
       
    }
})