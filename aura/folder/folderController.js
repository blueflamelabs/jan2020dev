({
	init: function (component, event, helper) {
        console.log('folder component init called');
        var headerActions = [
                                {
                                    label: 'All',
                                    checked: true,
                                    name:'all',
                                    iconName: ''
                                }
            ]
        // Set column headers for root folder table starts from here
        //Setting up colum information
        component.set("v.rootFolderTableColumns",
                      [
                          {
                              label : '',
                              fieldName : 'folderName', 
                              cellAttributes : { 
                                  		       iconName: { fieldName: 'folderIconName' }, 
                                               //iconLabel: { fieldName: 'folderIconLabel' }, 
                          				       iconPosition: 'left'
                              				 },
                              type : 'text',
                              typeAttributes : {iconName: 'standard:folder'}
                          }
                      ]);
        // Set column headers for root folder table ends here
        
        // Call helper to fetch root folder data from backend
        helper.fetchParentFolderData(component,event,helper);
        // Call helper to fetch all file folder data from backend
        helper.fetchFileFolderData(component,event,helper);
    },
    handleLoadMore : function(component,event,helper) {
        console.log('Inside handleLoadMore');
        console.log('currentCount=======',component.get("v.currentCount"));
        console.log('totalRows=======',component.get("v.totalRows"));
        if(!(component.get("v.currentCount") >= component.get("v.totalRows"))) {
            console.log('Inside if');
            //To display the spinner
            event.getSource().set("v.isLoading", true); 
            //To handle data returned from Promise function
            helper.loadData(component).then(function(data){ 
                console.log('data=======',data);
                var currentData = component.get("v.rootFolderList");
                console.log('currentData=======',currentData);
                var newData = currentData.concat(data);
                console.log('newData=======',newData);
                component.set("v.rootFolderList", newData);
                //To hide the spinner
                event.getSource().set("v.isLoading", false); 
            });
        }
        else{
            console.log('Inside Else');
            //To stop loading more rows
            component.set("v.enableInfiniteLoading",false);
            event.getSource().set("v.isLoading", false);
            // Show toast message once all records are fetched
            /*
            var toastReference = $A.get("e.force:showToast");
            toastReference.setParams({
                "type":"Success",
                "title":"Success",
                "message":"All Folders are loaded",
                "mode":"dismissible"
            });
            toastReference.fire();
            */
        }
    },
})