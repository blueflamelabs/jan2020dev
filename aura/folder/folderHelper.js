({
	fetchParentFolderData : function(component,event,helper,reload) {
        // Get parent record Id
        var getRecordId = component.get("v.recordId");
        console.log('getRecordId=====',getRecordId);
        
        var action = component.get("c.getParentFolderWrapper");
        action.setParams({  
            parentRecordId : getRecordId,
            recordLimit: component.get("v.initialRows"),
            recordOffset: 0
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var getRootFolderWrapper = [];
            var getRootFolderList = [];
            var getRootFolderListTemp = [];
            // Check call back response status
            if (state === "SUCCESS") {
                console.log('root folder response======='+response.getReturnValue());
                getRootFolderWrapper = response.getReturnValue();
                console.log('getRootFolderWrapper=======',getRootFolderWrapper);
                console.log('getRootFolderWrapper.listRootFolder.length=======',getRootFolderWrapper.listRootFolder.length);
                // set total rows count from response result
                component.set("v.totalRows",getRootFolderWrapper.totalRecords);
                console.log("Data Fetched");
                if(getRootFolderWrapper != undefined && getRootFolderWrapper.listRootFolder.length > 0) {
                	getRootFolderListTemp = getRootFolderWrapper.listRootFolder;    
                }
                
                if(getRootFolderListTemp.length > 0) {
                    for(var i=0; i < getRootFolderListTemp.length; i++) {
                    	getRootFolderList.push({
                        	folderId: getRootFolderListTemp[i].Id,
                            folderName: getRootFolderListTemp[i].Name,
                            folderIconName: 'standard:folder',
                            folderIconLabel: 'folder'
                        });      
                    } // End of for
                } // End of if
                console.log('getRootFolderList=======',getRootFolderList);
                component.set("v.rootFolderList",getRootFolderList);
            }
            else {
                console.log("Fetch Failed");
            } 
        });
        $A.enqueueAction(action);
	},
    fetchFileFolderData : function(component,event,helper,reload) {
        // Get parent record Id
        var getRecordId = component.get("v.recordId");
        console.log('getRecordId=====',getRecordId);
        
        var action = component.get("c.getFileFolderWrapperCopy");
        action.setParams({  
            parentRecordId : getRecordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var getFileFolderWrapperList = [];
            var getRootFolderList = [];
            // Check call back response status
            if (state === "SUCCESS") {
                console.log('filesfolder response======='+response.getReturnValue());
                getFileFolderWrapperList = response.getReturnValue();
                console.log('getFileFolderWrapperList=======',getFileFolderWrapperList);
                console.log('getFileFolderWrapperList length======='+getFileFolderWrapperList.length);
                console.log("Data Fetched");
                /*
                if(getFileFolderWrapperList != undefined &&
                   getFileFolderWrapperList.length > 0) {
                    for(var i=0; i < getFileFolderWrapperList.length; i++) {
                    	getRootFolderList.push({
                        	folderId: getFileFolderWrapperList[i].folderId,
                            folderName: getFileFolderWrapperList[i].folderName,
                            folderIconName: 'standard:folder',
                            folderIconLabel: 'folder'
                        });      
                    } // End of for
                } // End of if
                */
                //console.log('getRootFolderList=======',getRootFolderList);
                //component.set("v.rootFolderList",getRootFolderList);
            }
            else {
                console.log("Fetch Failed");
            } 
        });
        $A.enqueueAction(action);
	},
    loadData : function(component){
        return new Promise($A.getCallback(function(resolve) {
            var getRecordId = component.get("v.recordId");
        	console.log('getRecordId=====',getRecordId);
            var limit = component.get("v.initialRows");
            var offset = component.get("v.currentCount");
            var totalRows = component.get("v.totalRows");
            console.log('limit==========',limit);
            console.log('offset==========',offset);
            console.log('totalRows==========',totalRows);
            if(limit + offset > totalRows){
                limit = totalRows - offset;
            }
            console.log('limit after==========',limit);
            console.log('offset after==========',offset);
            console.log('totalRows after==========',totalRows);
            console.log('call action');
            var action = component.get("c.getParentFolderWrapper");
            action.setParams({  
                parentRecordId : getRecordId,
                recordLimit: limit,
                recordOffset: offset
        	});
            action.setCallback(this,function(response) {
                var getRootFolderWrapper = [];
                //var getRootFolderWrapperList = [];
                var getRootFolderListTemp = [];
                var getRootFolderList = [];
                var state = response.getState();
                console.log('state=======',state);
                if(state == "SUCCESS") {
                    getRootFolderWrapper = response.getReturnValue();
                    console.log('getRootFolderWrapper in load more=======',getRootFolderWrapper);
                    console.log('getRootFolderWrapper.listRootFolder length in load more======='+getRootFolderWrapper.listRootFolder.length);
                    if(getRootFolderWrapper != undefined && getRootFolderWrapper.listRootFolder.length > 0) {
                		getRootFolderListTemp = getRootFolderWrapper.listRootFolder;    
                	}
                
                    if(getRootFolderListTemp.length > 0) {
                        for(var i=0; i < getRootFolderListTemp.length; i++) {
                            getRootFolderList.push({
                                folderId: getRootFolderListTemp[i].Id,
                                folderName: getRootFolderListTemp[i].Name,
                                folderIconName: 'standard:folder',
                                folderIconLabel: 'folder'
                            });      
                        } // End of for
                    } // End of if
                console.log('getRootFolderList=======',getRootFolderList);
                    resolve(getRootFolderList);
                    var currentCount = component.get("v.currentCount");
                    currentCount += component.get("v.initialRows");
                    // set the current count with number of records loaded 
                    component.set("v.currentCount",currentCount);
                } else {
                    console.log('Load more fetch failed');
                }
            });
            $A.enqueueAction(action);
        }));
    }
})