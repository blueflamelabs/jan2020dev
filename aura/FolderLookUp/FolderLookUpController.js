({
    // To prepopulate the seleted value pill if value attribute is filled
	doInit : function( component, event, helper ) {
        component.set("v.disableValue",true);
        component.set("v.recordCount",100)
        //console.log('AccountId === ',component.get('v.AccountId'));
        /*if(component.get('v.AccountId') != '') {
         helper.showDefaultUser(component, event, helper);
        }
        else{
            helper.showDefaultUserCRE(component, event, helper); 
        }*/
    	$A.util.toggleClass(component.find('resultsDiv'),'slds-is-open');
		if( !$A.util.isEmpty(component.get('v.value')) ) {
			helper.searchRecordsHelper( component, event, helper, component.get('v.value') );
		}
	},

    // When a keyword is entered in search box
	searchRecords : function( component, event, helper ) {
        if( !$A.util.isEmpty(component.get('v.searchString')) ) {
		    helper.searchRecordsHelper( component, event, helper, '' );
        } else {
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
        }
	},

    // When an item is selected
	selectItem : function( component, event, helper ) {
         
        if(!$A.util.isEmpty(event.currentTarget.id)) {
    		var recordsList = component.get('v.folderList');
    		var index = recordsList.findIndex(x => x.value === event.currentTarget.id)
            if(index != -1) {
                var selectedRecord = recordsList[index];
            }
            component.set('v.searchString', "");
            component.set('v.selectedFolder',selectedRecord);
            component.set('v.value',selectedRecord.value);
            console.log('selectedRecord==========',selectedRecord);
          	$A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
             // $A.util.addClass(slds-pill__remove , ' display: block');
        }
	},
	
    // To remove the selected item.
	removeItem : function( component, event, helper ){
        component.set('v.selectedFolder','');
        component.set('v.value','');
        component.set('v.searchString','');
        setTimeout( function() {
            component.find( 'inputLookup' ).focus();
        }, 250);
    },
	
    // To close the dropdown if clicked outside the dropdown.
    blurEvent : function( component, event, helper ){
    	$A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
    },
    	
})