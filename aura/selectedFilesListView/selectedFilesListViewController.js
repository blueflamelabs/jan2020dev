({
	myAction : function(component, event, helper) {
		
	},
    
    init: function(cmp, event, helper) {
    helper.init(cmp, helper);
  },

  handlePillsChanged: function(cmp, event, helper) {
    helper.parsePillsToField(cmp, helper);
  },

  handleValueChanged: function(cmp, event, helper) {
    if (cmp.get('v.valueDataLoaded') === false && cmp.get('v.scriptsLoaded') === true && cmp.get('v.value') !== null) {
      cmp.set('v.valueDataLoaded', true);
      helper.parseFieldToPills(cmp, helper);
    }
  },

  onKeyUpInput: function(cmp, event, helper) {
    var delimiter = cmp.get('v.delimiter');
    var inputText = cmp.find('inputText').getElement();
    var currentInput = inputText.value;

    if (currentInput[currentInput.length - 1] === delimiter || event.keyCode === 13) {
      helper.addNewPills(cmp, helper, currentInput.split(delimiter));
      inputText.value = '';
    }
  },

  onRemovePill: function(cmp, event, helper) {
    var pillId = event.getSource().get('v.name');
    var pills = cmp.get('v.pills');

    for (var i = 0; i < pills.length; i++) {
      if (pillId === pills[i].id) {
        pills.splice(i, 1);
        break;
      }
    }

    cmp.set('v.pills', pills);
  },
})