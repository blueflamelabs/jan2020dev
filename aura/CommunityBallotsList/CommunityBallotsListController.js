({
    doInit : function(component, event, helper) {
        // Retrieve contacts during component initialization
        helper.loadBallots(component);
    },
    goToRecord : function(component, event, helper) {
        // Fire the event to navigate to the Ballot record
        var sObjectEvent = $A.get("e.force:navigateToSObject");
       ({
            "recordId": component.get("v.Ballot.Id")
        })
        sObjectEvent.fire();
    },
    
    handleSelect : function(component, event, helper) {
      helper.handleSelectHelper(component);
    }
})