({
    loadBallots : function(cmp) {
        // Load all Ballot data
        var selectVal='Next 10 Days';
        var action = cmp.get("c.getBallots");
         action.setParams({
            "selectVal" : selectVal
        });
        action.setCallback(this, function(response) {
           var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.Ballots", response.getReturnValue());
                this.updateTotal(cmp);
            }
        });
        $A.enqueueAction(action);
    },
    updateTotal: function(cmp) {
        var Ballots = cmp.get("v.Ballots");
        cmp.set("v.totalBallots", Ballots.length);
        },
    handleSelectHelper:function(cmp){
        var selectVal = cmp.get("v.ballotsDueValue");
        var action = cmp.get("c.filterRecord");
         action.setParams({
            "selectVal" : selectVal
        });
        action.setCallback(this, function(response) {
           var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.Ballots", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})