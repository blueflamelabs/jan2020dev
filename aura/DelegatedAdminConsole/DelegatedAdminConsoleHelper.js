({
    getCurrentUser : function(component, event, helper) {
        var action = component.get("c.getCurrentMemberCategoryUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                console.log('resValue member category -',resValue);
                component.set("v.isMemberCategoryUser",resValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getProfileRecordforEdit : function(component, selecteduserId) {
        var action = component.get("c.getCustomMetaDataProfile");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                var profileId;
                var profileForNonDefault;
                var option = [];
                for(var i=0;i < resValue.length; i++) {
                    option.push({
                        value : resValue[i].Profile_ID__c,
                        label : resValue[i].Profile_ID__c
                    });
                }
                component.set("v.profileForNonDefault",profileForNonDefault);
                //component.set("v.profileId",profileId);
                component.set("v.profileMetaData",option);
                component.set("v.profileMetaDataList",resValue);
                //helper.saveProfileonUser(component, event, helper);
            }
            
        });
        $A.enqueueAction(action);
    },
    
    getUsersList: function(component, event, helper) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var action = component.get("c.getCurrentLoggedInUsers");
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                console.log('display list response onload user ',resValue[0].userWrapperList);
                var newWrapperSobjectList = [];
                
                var userList = resValue[0].userWrapperList;
                for(var i=0; i<userList.length; i++) {
                        newWrapperSobjectList.push({
                            Name : userList[i].Contact.Account.Name,
                            FirstName : userList[i].Contact.FirstName,
                            LastName : userList[i].Contact.LastName,
                            Title : userList[i].Contact.Title,
                            Email : userList[i].Contact.Email,
                            Phone : userList[i].Contact.Phone,
                            IsActive : userList[i].IsActive,
                            Id : userList[i].Id,
                            ContactId : userList[i].ContactId,
                            AccountId : userList[i].Contact.AccountId,
                            ProfileName : userList[i].Profile.Name,
                            isPickval : false,
                            isEdit : false,
                            isProfileEditable : false,
                            isBillingCon : userList[i].Contact.Billing_Contact__c,
                            isLegalCon : userList[i].Contact.Legal_Contact__c
                            
                        });
                }
                if(resValue[0].contactWrapperList != undefined) {
                    var contactList = resValue[0].contactWrapperList;
                    for(var i=0; i< contactList.length; i++) {
                        newWrapperSobjectList.push({
                            Name : contactList[i].Account.Name,
                            FirstName : contactList[i].FirstName,
                            LastName : contactList[i].LastName,
                            Title : contactList[i].Title,
                            Email : contactList[i].Email,
                            Phone : contactList[i].Phone,
                            IsActive : contactList[i].IsActiveForToggle__c,
                            Id : contactList[i].Id,
                            ContactId : contactList[i].Id,
                            AccountId : contactList[i].AccountId,
                            ProfileName : '',
                            isPickval : false,
                            isEdit : false,
                            isProfileEditable : false,
                            isBillingCon : contactList[i].Billing_Contact__c,
                            isLegalCon : contactList[i].Legal_Contact__c
                        });
                    }
                }
                
                console.log('after comparing ', newWrapperSobjectList);
                component.set("v.recordList",newWrapperSobjectList);
                component.set("v.usersWrapper",resValue);
                component.set("v.usersTempWrapper",newWrapperSobjectList);
                component.set("v.tempListWrapper",resValue);
                component.set("v.loggedInUser",userId);
                helper.sortBy(component,helper, "Name");
            }
            
        });
        $A.enqueueAction(action);
    },
    
    updateUser: function(component,event, helper) {
        var status = event.getSource().get("v.checked");
        console.log('status ==',status);
        var userName = event.getSource().get("v.value");
        var userId= event.getSource().get("v.name");
        console.log('userId ',userId);
        // 200819 - T-00555 - VennScience_BFL_Amruta - Added confirm alert when user is deactivated
        if(status == false) {
            var result = confirm("Are you sure you want to deactivate this user?"); 
            if (result == true) {
                var action = component.get('c.updateSelectedUser');
                action.setParams({
                    "userId": userId,
                    "status" : status
                });
                
                action.setCallback(this, function(actionResult) {
                    var state= actionResult.getState();
                    var isAcivateUser = actionResult.getReturnValue();
                    if(state=='SUCCESS') {
                        if(isAcivateUser != null) {
                            console.log('inside if ==');
                            console.log('inside if isAcivateUser==',isAcivateUser);
                            this.showErrorMsgToastfire(isAcivateUser);
                            event.getSource().set("v.checked",false);
                        }else {
                            if(status == false) {
                                alert(userName+' is deactivated');
                            }
                            if(status == true) {
                                alert(userName+' is activated');
                            } 
                            this.updateUserStatus(component,event, helper, status, userId);
                        }
                    }
                });
                
                $A.enqueueAction(action);
            } else {
                event.getSource().set("v.checked",true);
            } 
        } else {
            var action = component.get('c.updateSelectedUser');
            action.setParams({
                "userId": userId,
                "status" : status
            });
            var self = this;
            action.setCallback(this, function(actionResult) {
                var state= actionResult.getState();
                var isAcivateUser = actionResult.getReturnValue();
                console.log('isAcivateUser ==',isAcivateUser);
                if(state=='SUCCESS') {
                    console.log('inside success ==');
                    if(isAcivateUser != null && isAcivateUser != '') {
                        console.log('inside if ==');
                        console.log('inside if isAcivateUser==',isAcivateUser);
                        this.showErrorMsgToastfire(isAcivateUser);
                        event.getSource().set("v.checked",false);
                    }else {
                        console.log('inside else ==');
                        if(status == false) {
                            alert(userName+' is deactivated');
                        }
                        if(status == true) {
                            alert(userName+' is activated');
                        } 
                        this.updateUserStatus(component, event, helper, status, userId);
                    }
                }
            });
            
            $A.enqueueAction(action);
        } // End of if else
    },
    
    updateUserStatus : function(component, event, helper, status, selecteduserId) {
        console.log('tst toggle set recordlist');
        var recordList = component.get("v.recordList");
        for(var i=0; i< recordList.length; i++) {
            if(recordList[i].Id == selecteduserId)  {
                recordList[i].IsActive = status;
            }
        }
        component.set('v.recordList',recordList);
    },
    
    fetchTimeZonePicklist : function(component){
        var action = component.get("c.getTimeZonePicklistvalues");
        action.setParams({
            'objectName': component.get("v.organizationName"),
            'field_apiname': component.get("v.timeZone")
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                var picklistVal = a.getReturnValue();
                console.log('get time zone --',picklistVal);
                component.set("v.timeZoneSidKeyPicklist", picklistVal);
            }
        });
        $A.enqueueAction(action);
    },
    
 
    searchStringChanged: function (c, e, h) {
        try{
           
            var searchString = c.get('v.searchKeyword');
            if(!$A.util.isUndefinedOrNull(searchString)){
               searchString = searchString.trim(); 
            }
            var allRecords = c.get('v.usersTempWrapper');
            var filteredRecords = [];
            var displayedFields = [];
            displayedFields.push('Name');
            displayedFields.push('Email');
            if (!$A.util.isEmpty(searchString) && !$A.util.isUndefinedOrNull(searchString)) {
                for (var i = 0; i < allRecords.length; i++) {
                    var allRecord = allRecords[i];
                    var isFound = false;
                    for (var key in allRecord) {
                        if (allRecord.hasOwnProperty(key)) {
                            console.log('allRecord[key])>>',allRecord[key]);
                            if(allRecord[key] !== null && allRecord[key] !== undefined) //remove toString error
                                if ((((allRecord[key]).toString()).toLowerCase()).indexOf(searchString.toLowerCase()) > -1 && displayedFields.indexOf(key) > -1) {
                                    isFound = true;
                                }
                        }
                    }
                    if (isFound) {
                        filteredRecords.push(allRecord);
                    }else {
                        var noRecordMsg = 'No Record found.';
                        c.set('v.noRecord', noRecordMsg);
                    }
                }
                console.log('filteredRecords ',filteredRecords);
                c.set('v.recordList', filteredRecords);
                
            } else {
                c.set('v.recordList', c.get('v.usersTempWrapper'));
                
            }
            this.sortBy(c,h, c.get("v.sortByFieldName"));
        }
        catch(e){
            //console.log('Error --->>'+e);
        }
    },
    
    saveUserRecord : function(cmp, event, helper,selecteduserId ) {
        console.log('update user record',selecteduserId);
        console.log('checkbox save ',cmp.find('billingCheckbox').get('v.checked'));
        var recordList1 = cmp.get('v.recordList');
        console.log('recordList==========',recordList1);
        var editedRecord;
        for(var j=0; j< recordList1.length; j++) {
            console.log('updated ====inside ======');
            var contactList = recordList1[j];
            if(selecteduserId == contactList.Id) {
                console.log('updated value==========',contactList);
                editedRecord = contactList;
            }
        }
        console.log('editedRecord==========',editedRecord);
        //var profile = cmp.get('v.changedProfile');
        var action = cmp.get('c.updateRecordList');
        action.setParams({
            "selectedId": selecteduserId,
            "updatedRecord" : JSON.stringify(editedRecord)
            /*"name" : cmp.get('v.editName'),
            "title" : cmp.get('v.editTitle'),
            "email" : cmp.get('v.editEmail'),
            "phone" : cmp.get('v.editPhone')*/
            //"profile" : profile
        });
        action.setCallback(this, function(response) {
            var state= response.getState();
            if(state=='SUCCESS') {
                var updatedRecord = response.getReturnValue();
                console.log('updatedRecord response ',updatedRecord.updatedUser);
                var updatedUserRecord = updatedRecord.updatedUser;
                var updatedContactRecord = updatedRecord.updatedContact;
                var recordList = cmp.get('v.recordList');
                console.log('recordList==========',recordList);
                for(var j=0; j< recordList.length; j++) {
                    var contactList = recordList[j];
                    console.log('--inside contact loop-12-',contactList);
                    
                    if(updatedUserRecord != null && updatedUserRecord.Id == contactList.Id) {
                        console.log('--inside if-',contactList.Id);
                        contactList.Name = updatedUserRecord.Name;
                        contactList.Title = updatedUserRecord.Title;
                        contactList.Email = updatedUserRecord.Email;
                        contactList.Phone = updatedUserRecord.Phone;
                    }
                    if(updatedContactRecord != null && updatedContactRecord.Id == contactList.Id) {
                        console.log('--inside if contact update-',contactList.Id);
                        contactList.Name = updatedContactRecord.Name;
                        contactList.Title = updatedContactRecord.Title;
                        contactList.Email = updatedContactRecord.Email;
                        contactList.Phone = updatedContactRecord.Phone;
                    }
                }
                console.log('--get updated-',recordList);
                cmp.set('v.recordList',recordList);
                console.log('--get updated===-',cmp.get('v.recordList'));
                //if(cmp.get('v.isChange')== true){
                helper.showSuccMsgToastfire( updatedRecord.successMsg);
                console.log('--get error===-',updatedRecord.errorMsg);
                if(updatedRecord.errorMsg != '' && updatedRecord.errorMsg != null ) {
                    helper.showErrorMsgToastfire(updatedRecord.errorMsg);
                }
                helper.getUsersList(cmp, event, helper);
                //$A.get('e.force:refreshView').fire();
                console.log('--get updated after refresh===-',cmp.get('v.recordList'));
                //}
                //cmp.set('v.isChange',false);
            }
        });
        $A.enqueueAction(action);
    },
    
    
    saveTestCommunityUser : function(cmp, event, helper ) {
        console.log('helper save community user method');
        var userfisrtname = cmp.get('v.userFirstName');
        //var profileName = cmp.get('v.profileId');
        console.log('Billing Contact====',cmp.get('v.billingContact'));
        console.log('Legal Contact====',cmp.get('v.legalContact'));
        
        var action = cmp.get("c.saveNewRelatedCommunityUser"); 
        action.setParams({
            "firstName" :  cmp.get('v.userFirstName'),
            "lastName" : cmp.get('v.userLasttName'),
            "userEmail" : cmp.get('v.userEmail'),
            "timeZone" :  cmp.get('v.timeZoneSidKeyValue'),
           // "profileName" : profileName,
            "contactId" : cmp.get('v.contactId'),
            "phone" : cmp.get('v.userPhone'),
            "mobileNo" : cmp.get('v.userMobileNo'),
            "title" : cmp.get('v.userTitle'),
            "isBillingCon" : cmp.get('v.billingContact'),
            "isLegalCon" : cmp.get('v.legalContact')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                console.log('save community user method',resValue);
                if(!$A.util.isEmpty(resValue.successMsg) || !$A.util.isUndefinedOrNull(resValue.successMsg)) {
                    helper.showSuccMsgToastfire(resValue.successMsg);
                    cmp.set("v.isOpen", false);
                    $A.get('e.force:refreshView').fire();
                }else {
                    helper.showErrorMsgToastfire(resValue.errorMsg);
                }
                
            }else if(state === 'ERROR') {
                var errorMsg;
                helper.showErrorMsgToastfire(errorMsg);
            }
            
        });
        $A.enqueueAction(action);
    },
    
    showSuccMsgToastfire : function( msg) {
          //console.log('--get updated succss===-',cmp.get('v.recordList'));
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: msg,
            duration:'5000',
            type: 'Success'
        });
        toastEvent.fire();
        
    },
    
    showErrorMsgToastfire : function(msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error',
            message: msg,
            duration:'8000',
            type: 'Error'
        });
        toastEvent.fire();
    },
    
   /* searchStringChanged1: function (c, e, h) {
        try{
            var searchString = c.get('v.searchKeyword');
            
            var tempListSearch = c.get('v.tempListWrapper');
            var questREc = c.get('v.usersTempWrapper');
            var allRecords; 
            for(var i=0; i < questREc.length; i++ ) {
                
                allRecords = questREc[0].contactWrapperList;
                
            }
            var displayedFields = [];
            displayedFields.push('Name');
            var contactFilteredRecords = [];
            if (searchString !== null || searchString !== '') {
                
                for (var i = 0; i < allRecords.length; i++) {
                    var record = allRecords[i];
                    var isFound = false;
                    for (var key in record) {
                        if (record.hasOwnProperty(key)) {
                            
                            if(record[key] !== null && record[key] !== undefined){
                                if ((((record[key]).toString()).toLowerCase()).indexOf(searchString.toLowerCase()) > -1 && displayedFields.indexOf(key) > -1) {
                                    isFound = true;
                                }
                            } //remove toString error
                            
                        }
                    }
                    if (isFound) {
                        contactFilteredRecords.push(record);
                    }
                }
                
                var contactWrapperObjList = [];
                for(var i=0; i <= contactFilteredRecords.length; i++) {
                    if(!$A.util.isUndefinedOrNull(contactFilteredRecords[i])){
                        contactWrapperObjList.push(contactFilteredRecords[i]);
                    }
                }
                
                // this functionality to search for the user.
                var allRecordsforUser; 
                for(var i=0; i < questREc.length; i++ ) {
                    allRecordsforUser = questREc[0].userWrapperList;
                }
                
                var displayedFields = [];
                displayedFields.push('Name');
                var userFilteredRecords = [];
                for (var i = 0; i < allRecordsforUser.length; i++) {
                    var record = allRecordsforUser[i].userList;
                    var isFound = false;
                    for (var key in record) {
                        if (record.hasOwnProperty(key)) {
                            
                            if(record[key] !== null && record[key] !== undefined){
                                if ((((record[key]).toString()).toLowerCase()).indexOf(searchString.toLowerCase()) > -1 && displayedFields.indexOf(key) > -1) {
                                    isFound = true;
                                }
                            } 
                        }
                    }
                    if (isFound) {
                        userFilteredRecords.push(record);
                    }
                }
                var userWrapperObjList = [];
                for(var i=0; i <= userFilteredRecords.length; i++) {
                    if(!$A.util.isUndefinedOrNull(userFilteredRecords[i])){
                        userWrapperObjList.push({
                            userList : userFilteredRecords[i],
                            isPickval : false
                        });
                    }
                }
                var finalUserWrapper = [];
                finalUserWrapper.push({
                    contactWrapperList : [],
                    userWrapperList : []
                });
                for(var i=0; i < finalUserWrapper.length; i++ ) {
                    finalUserWrapper[i].contactWrapperList = contactWrapperObjList ;
                    finalUserWrapper[i].userWrapperList = userWrapperObjList;
                }
                c.set('v.usersWrapper', finalUserWrapper);
            } else {
                c.set('v.usersWrapper', questREc);
            }
        }
        catch(e){
            //console.log('Error --->>'+e);
        }
    },*/
    sortBy: function(component,helper,field) {
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField");
        var userList = component.get("v.recordList");
        var records = userList;
        records.sort(function(a,b){
            var x ;
            var y ;
            if(a[field]){
              x = a[field].toLowerCase();  
            }else{
              x  =  a[field]; 
            } 
                 
            if(b[field]){
              y = b[field].toLowerCase();  
            }else{
              y  =  b[field]; 
            } 
               
            var t1 = x == y,
                t2 = x > y;
            if(x && y) { 
                if(t1){
                        return 0;
                }
                else
                    if(t2){
                       return (sortAsc?1:-1)  
                    }
                    else{
                        return (sortAsc?-1:1)  
                    }
            }else{       	
                if(x && !y){
                    return (sortAsc?-1:1);
                }
                else {
                    if(!x && y){
                        return (sortAsc?1:-1);
                    }
                    else
                    return 0;    
                }
                    
            }
        });
        
        component.set("v.recordList", records);
        
    },
    
    toggleEditSave : function(component, toggleToValue, selecteduserId) {
        
        var recordList = component.get("v.recordList");
        console.log('recordList ', recordList);
        
        console.log('recordList[index].isEdit1111'+recordList.length); 
        for(var index=0; index<recordList.length; index++){
            
            //console.log('Id ',recordList[index].Id); 
            
            if(recordList[index].Id == selecteduserId){
                recordList[index].isEdit = toggleToValue;
                //console.log('recordList[index].isEdit',recordList[index].isEdit);
            }
        }
        component.set("v.recordList",recordList);
        
    },
    
    toEditUserRecord:function(cmp, event, helper, selectedId) {
        console.log(' edit existing user==',selectedId);
       
        cmp.set('v.selectedRecordId',selectedId);
        //cmp.set('v.isToggleClicked',checkvalue);
        
        var recordList = cmp.get('v.recordList');
        console.log('recordList==========',recordList);
        var name;
        var title;
        var Email;
        var phone;
        
        //var contactWrapList = wrapperList[0].contactWrapperList;
        //console.log('contactWrapList==========',contactWrapList);
        for(var j=0; j< recordList.length; j++) {
            var contactList = recordList[j];
            console.log('--inside contact loop-12-',contactList);
            console.log('--inside contact loop-12-',contactList.Id);
            if(selectedId == contactList.Id) {
                console.log('--inside contact if-');
                name = contactList.Name;
                console.log('--inside name-',name);
                title = contactList.Title;
                Email = contactList.Email;
                phone = contactList.Phone;
            }
            console.log('--inside42 firstName-',name);
        }
        console.log('--out if-',name);
        cmp.set('v.editName',name);
        cmp.set('v.editTitle',title);
        cmp.set('v.editEmail',Email);
        cmp.set('v.editPhone',phone);
    },
    
})