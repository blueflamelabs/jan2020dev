({
    
    handleChange : function(component, event, helper) {
        component.set("v.editContentVersion",true);
	},
    
    handleCapture : function(cmp,event,helper){
        var record = cmp.get("v.record");
        
        if(event.getParam("message") == "save" && cmp.get("v.editContentVersion") == true ) {
            cmp.find('recordViewForm').submit();
            var appEvent = $A.get("e.c:fileValidationError");
            appEvent.setParams({ "message" : "save" });
            appEvent.fire();
        } 
        
    },   
})