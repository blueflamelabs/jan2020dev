({
	doInit: function(component, event, helper) {
        helper.getIndustryPicklist(component, event, helper);
        //helper.getProfileMetaDataRecord(component, event, helper);
        helper.getUser(component, event, helper);
        helper.getDefaultGroup(component, event, helper);
        helper.getUserGroup(component, event, helper);
        helper.getCurrentUser(component, event, helper);
        // 111219 - T-000941 - VennScience_BFL_Amruta - Call helper method to fetch related working groups 
        helper.getWorkingGroupMap(component, event, helper);
       
    },
    
    handleChange : function(component, event, helper) {
        console.log('change value');
        var radioGrpValue = component.get("v.loggedInUser.DefaultGroupNotificationFrequency");
        //var radioGrpValue = event.getsource.get('v.value');
        console.log('DefaultGroupVal----',radioGrpValue);
        component.set('v.selectedfequency',radioGrpValue);
        // 121219 - T-000941 - VennScience_BFL_Amruta - Added below line(Default group frequency will be save on click event)
        helper.SaveUserNotification(component, event, helper);
    },
    
    handleClickSave : function(component, event, helper) {
        helper.SaveUserNotification(component, event, helper);
    },
    
    onGroup: function(component, event, helper ){
        var selectedValue = event.getSource().get("v.value"); 
		var indexValue = event.getSource().get("v.name");     
        console.log('selectedValue--',selectedValue);
        console.log('indexValue--',indexValue);
        var groupList = component.get('v.userGroupList');
        var selectedId = groupList[indexValue].Id;
        console.log('selectedId--',selectedId);
        helper.onChangeGroupMNotify(component, event, helper, selectedId, selectedValue);
           
      }
})