({
	getUser : function(component, event, helper) {
        var action = component.get("c.getCurrentUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                console.log('resValue member category -',resValue);
                component.set("v.isMemberCategoryUser",resValue);
            }
        });
        $A.enqueueAction(action);
    },
    getWorkingGroupMap : function(component, event, helper) {
        // 111219 - T-000941 - VennScience_BFL_Amruta - Added function to get related working groups for
        // current user
        console.log('Inside getWorkingGroupMap');
        var action = component.get("c.getCurrentUserRelatedWorkingGroups");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS') {
                
            	var resValue = response.getReturnValue();
                console.log('resValue member category -',resValue);
                var arrayMapKeys = [];
                for(var key in resValue){
                    arrayMapKeys.push({key: key, value: resValue[key]});
                }
                component.set("v.chatterGrpIdVSWorkingGrpNameMap",arrayMapKeys);
                console.log('chatterGrpIdVSWorkingGrpNameMap=====',component.get("v.chatterGrpIdVSWorkingGrpNameMap"));
            } else {
                console.log('Error occurred');
            }
        });
        
        $A.enqueueAction(action);
    },
    
    getCurrentUser : function(component, event, helper) {
        var action = component.get("c.getCurrentUservalue");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                component.set("v.loggedInUser",resValue);
                
                console.log('resValue member category -',resValue);
                var newValue = '';
                if(resValue.DefaultGroupNotificationFrequency == 'N') {
                    newValue = 'Never';
                }else if(resValue.DefaultGroupNotificationFrequency == 'P') {
                    newValue = 'On every post';
                }else if(resValue.DefaultGroupNotificationFrequency == 'D') {
                    newValue = 'Daily';
                }else if(resValue.DefaultGroupNotificationFrequency == 'W'){
                    newValue = 'Weekly';
                }
                //var labelVal = '';
                var labelVal ={
                    "label": newValue,
                    "value": resValue.DefaultGroupNotificationFrequency
                };
                console.log('labelVal ==  -',labelVal);
                component.set("v.DefaultGroupVal",labelVal);
                
            }
        });
        $A.enqueueAction(action);
    },
    
    getDefaultGroup : function(component){
        var action = component.get("c.getDefaultGroupNotificationFreqValues");
        /*action.setParams({
            'objectName': component.get("v.organizationName"),
            'field_apiname': component.get("v.timeZone")
        });*/
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                var picklistVal = a.getReturnValue();
                console.log('get notify --',picklistVal);
                // 061219 - T-000941 - VennScience_BFL_Amruta - Change 'Never' to 'Limited'
                if(picklistVal != undefined && picklistVal.length > 0) {
                    for(var i=0; i < picklistVal.length; i++) {
                        console.log('picklistVal[i].value=====',picklistVal[i].value);
                        if(picklistVal[i].value == 'N') {
                            picklistVal[i].label = 'Limited';
                        } // End of inner if
                	} // End of for
                } // End of if
                console.log('picklistVal after changing=====',picklistVal);
                component.set("v.DefaultGroupNotificationFrequencyPicklist", picklistVal);
            }
        });
        $A.enqueueAction(action);
    },
    
    getUserGroup : function(component, event, helper) {
        var action = component.get("c.getCurrentUserRelatedGroups");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                console.log('resValue member category -',resValue);
                component.set('v.userGroupList',resValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    SaveUserNotification : function(component, event, helper) {
        var action = component.get("c.saveFrequencyNotification");
        action.setParams({
            'frequencyValue': component.get('v.selectedfequency')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS'){
                var resValue = response.getReturnValue();
                console.log('resValue member category -',resValue);
                helper.showSuccessToast(resValue);
                hepler.getCurrentUser(component, event, helper);
               
            }
        });
        $A.enqueueAction(action);
    },
    
    showSuccessToast : function(msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: msg,
            duration:'5000',
            type: 'success'
        });
        toastEvent.fire();
    },
    
    //get Industry Picklist Value
    getIndustryPicklist: function(component, event) {
        var action = component.get("c.getGroupNotificationFrequency");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var notificationMap = [];
                for(var key in result){
                    // 061219 - T-000941 - VennScience_BFL_Amruta - Change 'Never' to 'Limited'
                    if(key == 'N') {
                        notificationMap.push({label: 'Limited', value: key});
                    } else {
                        notificationMap.push({label: result[key], value: key});
                    } // End of if-else block
                    
                } // End of for
                console.log('notificationMap=======',notificationMap);
                component.set("v.notificationMap", notificationMap);
            }
        });
        $A.enqueueAction(action);
    },
    
    onChangeGroupMNotify: function(component, event, helper, selectedId, changedValue) {
        console.log('on Change GroupM Notify ');
        console.log('selectedId -- ',selectedId);
        console.log('changedValue -- ',changedValue);
        
        var action = component.get("c.updateNotificationFrequency");
        action.setParams({
            'selectedId': selectedId,
            'changedValue' : changedValue
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result=msg==',result);
                helper.showSuccessToast(result);
                //helper.getUserGroup(component, event, helper);
                //$A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
        
    },
})