({
    setFileIds : function(cmp, event,helper) {
        var uploadedFiles = event.getParam("files");
        var ids=new Array();
        for (var i= 0 ; i < uploadedFiles.length ; i++){
            ids.push(uploadedFiles[i].documentId);
        }
        
        var idListJSON=JSON.stringify(ids);
        cmp.set("v.fileIds",idListJSON);
    },
    
    updateCaseFlag: function(component, event,helper,fileIds,isCommunityUser) {
        let action = component.get("c.updateCaseRecord");
        action.setParams({
                'parentId':component.get("v.recordId"),
                'fileIds':fileIds,
                'isCommunityUser':isCommunityUser
       });
        
        action.setCallback(this,(response)=>{
            const state = response.getState();
            
            if(state === 'SUCCESS') {
            
            	console.log('saved');
        	}
            
        });
        $A.enqueueAction(action);
    },
    createChatterFeedAttachment : function(component, event,helper, fileIds) {
        console.log('fileIds uploded=====33',fileIds);
        console.log('fileIds uploded=====33',JSON.stringify(fileIds));
        var action = component.get("c.createFeedItemRecord");
        action.setParams({
            "parentId": component.get('v.recordId'),
            "fileIdsList"    : JSON.stringify(fileIds)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if( state === 'SUCCESS') {
                var responseValue = response.getReturnValue();
                console.log('responseValue created feed item -',responseValue);
                component.set('v.isOpenTextBox', false);
                //location.reload();
                 $A.get('e.force:refreshView').fire();
                
                //console.log('refresh window');
            }
        });
        $A.enqueueAction(action);
    },
    
})