({
	doInit : function(component, event, helper) {
		helper.getAssociationRelatedAssQuest(component, event, helper);
        //helper.getMasterQuesData(component, event, helper);
        helper.setColumnWrapper(component, event,helper);
        helper.fetchCategoryPicklist(component, event, helper);
        
	},
    onClickMasterAccordion : function(cmp, event, helper) {
        var chek = event.getSource().get("v.value");
        chek = chek == true ? false : true;
        console.log('--MasterAccordion--',chek);
        event.getSource().set("v.value",chek);
    },
    cancelAndClose : function(){
        $A.get("e.force:closeQuickAction").fire()
    },
    saveAndClose: function(component, event, helper){
        helper.saveAndClose(component, event, helper,'Close');
    },
    saveAndContinue: function(component, event, helper){
        helper.saveAndClose(component, event, helper,'Continue');
    },
    searchKeyChange : function(component, event, helper){
        helper.searchKeyChange(component, event, helper);
    },
    deleteAssociationQuest : function(component, event, helper) {
        var deletedRecordIndex = event.currentTarget.name;
        var setOfAddedQuestion = component.get("v.setOfAddedQuestion");
        var wraperAnsList= component.get("v.wrapMasterAssociationQuestDataList");
        var wraperAnsListDelete = component.get("v.wrapDeleteAssociationQuestDataList");
        var pageNumber = component.get("v.currentPageNumber1");
        var pageSize = component.get("v.pageSize");
        var mainIndex = ((pageNumber-1) * pageSize) + parseInt(deletedRecordIndex);
        var index = setOfAddedQuestion.indexOf(wraperAnsList[mainIndex].masterQuestAns.Master_Question__c);
        setOfAddedQuestion.splice(index,1);
        if(wraperAnsList[mainIndex].masterQuestAns.Id != null){
           wraperAnsListDelete.push(wraperAnsList[mainIndex]); 
        }
        wraperAnsList.splice(mainIndex,1);
        component.set("v.setOfAddedQuestion",setOfAddedQuestion);
        component.set("v.wrapMasterAssociationQuestDataList",wraperAnsList);
        helper.getMasterQuesData(component,event,helper);
        helper.buildData1(component);
    },
    checkval : function(component, event, helper){
        var chek = event.getSource().get("v.name");
        var masterQusList = component.get("v.masterMainQuestWrapperList");
        var masterQusListTemp = component.get("v.masterMainQuestWrapperListTemp");
        var masterQusAnsList = component.get("v.wrapMasterAssociationQuestDataList");
        var setOfAddedQuestion = component.get("v.setOfAddedQuestion");
        for(var x = 0;x < masterQusList.length; x++){
            if(chek == masterQusList[x].masterQuest.Id){
                var data = {
                    isDelete:false,
                    masterQuestAns:{}
                };
                data.isDeleteIcon = true;
                data.checkAccordion = false;
                data.masterQuestAns.Displayed_Question__c = masterQusList[x].masterQuest.Master_Question__c;
                data.masterQuestAns.Association__c = component.get('v.recordId');
                data.masterQuestAns.Required__c = true;
                data.masterQuestAns.Active__c = true;
                data.masterQuestAns.Master_Question__c = chek;
                data.masterQuestAns.View_in_Member_Directory__c = false;
                var associationQuestAnsValueList = [];
                console.log(masterQusList[x]);
                for(var y = 0;y < masterQusList[x].masterQuestAnsValue.length ; y++){
                    var MasterQuestionAnswerValueWrapper = {};
                    MasterQuestionAnswerValueWrapper.value = masterQusList[x].masterQuestAnsValue[y].Value__c;
                    MasterQuestionAnswerValueWrapper.isSelected = true;
                    MasterQuestionAnswerValueWrapper.associationQuestAnsValue = {};
                    MasterQuestionAnswerValueWrapper.associationQuestAnsValue.Master_Question_Answer_Value__c = masterQusList[x].masterQuestAnsValue[y].Id;
                    associationQuestAnsValueList.push(MasterQuestionAnswerValueWrapper);
                }
                data.associationQuestAnsValueList = associationQuestAnsValueList;
                console.log(data);
                masterQusAnsList.splice( 0, 0,data);
                masterQusList.splice(x,1);
               	setOfAddedQuestion.push(chek);
                
                component.set("v.setOfAddedQuestion",setOfAddedQuestion);
            }
        }
        for(var x = 0;x < masterQusListTemp.length; x++){
            if(chek == masterQusListTemp[x].masterQuest.Id){
                masterQusListTemp.splice(x,1);
            }
        }
        console.log(masterQusAnsList);
        component.set("v.masterMainQuestWrapperList",masterQusList);
        component.set("v.masterMainQuestWrapperListTemp",masterQusListTemp);
        component.set("v.wrapMasterAssociationQuestDataList",masterQusAnsList);
        helper.sortBy(component,event,helper);
        helper.buildData1(component);
        
    },
    checkValueOfAnsSelection : function(component, event, helper){
        var associationQuestionIndex = event.getSource().get("v.name");
        var indexArray = associationQuestionIndex.split('-');
        var chekValue = event.getSource().get("v.value");
        var wraperAnsList= component.get("v.wrapMasterAssociationQuestDataList");
		var pageNumber = component.get("v.currentPageNumber1");
        var pageSize = component.get("v.pageSize");
        var mainIndex = ((pageNumber-1) * pageSize) + parseInt(indexArray[0]);
        wraperAnsList[mainIndex].associationQuestAnsValueList[indexArray[1]].isSelected = !chekValue;
        component.set("v.wrapMasterAssociationQuestDataList",wraperAnsList);
        helper.buildData1(component);
        
    },
    onClickEyeStrike : function(component, event, helper){
        var associationQuestionIndex = event.getSource().get("v.name");
        var chekValue = event.getSource().get("v.value");
        var wraperAnsList= component.get("v.wrapMasterAssociationQuestDataList");
		var pageNumber = component.get("v.currentPageNumber1");
        var pageSize = component.get("v.pageSize");
        var mainIndex = ((pageNumber-1) * pageSize) + parseInt(associationQuestionIndex);
        wraperAnsList[mainIndex].masterQuestAns.Active__c = !chekValue;
        component.set("v.wrapMasterAssociationQuestDataList",wraperAnsList);
        helper.buildData1(component);
    },
    sortByFields : function(component, event, helper){
        helper.sort(component, event, helper);
    },
    first : function(component, event, helper){
        component.set("v.currentPageNumber",1);
        helper.buildData(component);
    },
    previous : function(component, event, helper){
        component.set("v.currentPageNumber",component.get("v.currentPageNumber")-1);
        helper.buildData(component);
    },
    next : function(component, event, helper){
        component.set("v.currentPageNumber",component.get("v.currentPageNumber")+1);
        helper.buildData(component);
    },
    last : function(component, event, helper){
        component.set("v.currentPageNumber",component.get("v.totalPages"));
        helper.buildData(component);
    },
    first1 : function(component, event, helper){
        component.set("v.currentPageNumber1",1);
        helper.buildData1(component);
    },
    previous1 : function(component, event, helper){
        component.set("v.currentPageNumber1",component.get("v.currentPageNumber1")-1);
        helper.buildData1(component);
    },
    next1 : function(component, event, helper){
        component.set("v.currentPageNumber1",component.get("v.currentPageNumber1")+1);
        helper.buildData1(component);
    },
    last1 : function(component, event, helper){
        component.set("v.currentPageNumber1",component.get("v.totalPages1"));
        helper.buildData1(component);
    }
})