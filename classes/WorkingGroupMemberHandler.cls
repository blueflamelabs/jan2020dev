public class WorkingGroupMemberHandler{

   /*
       Created By - Manas
       Created Date - 13/11/2019
       Description  - Create or Update Group Member in Working Group Object 
       Ticket - T-000884
   */
   public static void insertGroupMember(List<Account> objAccList){
        Id InviMemRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
        Id CorRepreRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
        Id StafRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Staff').getRecordTypeId();
        Set<Id> objSetAssId = new Set<Id>();
        Set<Id> objSetAccId = new Set<Id>();
        for(Account objAcc : objAccList){
            if((objAcc.RecordTypeId == InviMemRecTypeId || objAcc.RecordTypeId == CorRepreRecTypeId ||objAcc.RecordTypeId == StafRecordTypeId) && String.isNotBlank(objAcc.Association__c) && objAcc.Board_Member__pc == true){
                objSetAssId.add(objAcc.Association__c);
                objSetAccId.add(objAcc.Id);
            }
        }
        
        if(objSetAssId.size() > 0){
            Set<Id> objSetWorkGrupID = new Set<Id>();
            Map<Id,List<Working_Group__c>> objMapAssIdWorkGRPList = new Map<Id,List<Working_Group__c>>();
            List<Working_Group__c> objWorkingGroupList = [Select Id,Name,Association__c,Board_Member_Group__c from Working_Group__c where Association__c IN : objSetAssId and Board_Member_Group__c = true];
            for(Working_Group__c objWorkingGrp : objWorkingGroupList){
                objSetWorkGrupID.add(objWorkingGrp.Id);
                if(!objMapAssIdWorkGRPList.keySet().Contains(objWorkingGrp.Association__c)){
                    objMapAssIdWorkGRPList.put(objWorkingGrp.Association__c,new List<Working_Group__c>{objWorkingGrp});
                }else{
                    List<Working_Group__c> objListWorGRP = objMapAssIdWorkGRPList.get(objWorkingGrp.Association__c);
                    objListWorGRP.add(objWorkingGrp);
                    objMapAssIdWorkGRPList.put(objWorkingGrp.Association__c,objListWorGRP);
                }
            }
            Map<Id,Contact> objContactMap = new Map<Id,Contact>([Select Id,Account.Association__c from Contact Where AccountId IN : objSetAccId]);
            Map<String,Group_Member__c> objMapAccWorGroIdGrouMem = new Map<String,Group_Member__c>();
            List<Group_Member__c> objGroupMembr = [Select Id,End_Date__c,Status__c,Working_Group_Role__c,Working_Group__c,Grou_Member__c,Start_Date__c from Group_Member__c where Working_Group__c IN : objSetWorkGrupID and Grou_Member__c IN : objContactMap.keySet()];
            for(Group_Member__c objGrouMem : objGroupMembr){
                objMapAccWorGroIdGrouMem.put(objGrouMem.Grou_Member__c+''+objGrouMem.Working_Group__c,objGrouMem);
            }
            
            if(objMapAssIdWorkGRPList.size() > 0){
                List<Group_Member__c> GroupMemberList = new List<Group_Member__c>();
                for(Contact objCon : objContactMap.values()){
                    if(objMapAssIdWorkGRPList.keySet().Contains(objCon.Account.Association__c)){
                        for(Working_Group__c objWorkGP : objMapAssIdWorkGRPList.get(objCon.Account.Association__c)){
                            if(!objMapAccWorGroIdGrouMem.keySet().Contains(objCon.Id+''+objWorkGP.Id)){
                                Group_Member__c objGrMem = new Group_Member__c();
                                objGrMem.Working_Group__c = objWorkGP.Id;
                                objGrMem.Start_Date__c = Date.today();
                                objGrMem.Status__c = 'Active';
                                objGrMem.Working_Group_Role__c = 'Member';
                                objGrMem.Grou_Member__c = objCon.Id;
                                objGrMem.Voting_Level__c = 'Primary';
                                GroupMemberList.add(objGrMem);
                            }
                        }
                    }
                }
                if(GroupMemberList.size() > 0){
                    insert GroupMemberList;
                }
            }
        }
     }
     
      public static void updateGroupMember(List<Account> objAccList){
        Id InviMemRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
        Id CorRepreRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
        Id StafRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Staff').getRecordTypeId();
        Set<Id> objSetAssId = new Set<Id>();
        Set<Id> objSetAccId = new Set<Id>();
        for(Account objAcc : objAccList){
            if((objAcc.RecordTypeId == InviMemRecTypeId || objAcc.RecordTypeId == CorRepreRecTypeId ||objAcc.RecordTypeId == StafRecordTypeId) && String.isNotBlank(objAcc.Association__c)){
                objSetAssId.add(objAcc.Association__c);
                objSetAccId.add(objAcc.Id);
            }
        }
        
        if(objSetAssId.size() > 0){
            Set<Id> objSetWorkGrupID = new Set<Id>();
            Map<Id,List<Working_Group__c>> objMapAssIdWorkGRPList = new Map<Id,List<Working_Group__c>>();
            List<Working_Group__c> objWorkingGroupList = [Select Id,Name,Association__c,Board_Member_Group__c from Working_Group__c where Association__c IN : objSetAssId and Board_Member_Group__c = true];
            for(Working_Group__c objWorkingGrp : objWorkingGroupList){
                objSetWorkGrupID.add(objWorkingGrp.Id);
                if(!objMapAssIdWorkGRPList.keySet().Contains(objWorkingGrp.Association__c)){
                    objMapAssIdWorkGRPList.put(objWorkingGrp.Association__c,new List<Working_Group__c>{objWorkingGrp});
                }else{
                    List<Working_Group__c> objListWorGRP = objMapAssIdWorkGRPList.get(objWorkingGrp.Association__c);
                    objListWorGRP.add(objWorkingGrp);
                    objMapAssIdWorkGRPList.put(objWorkingGrp.Association__c,objListWorGRP);
                }
            }
            Map<Id,Contact> objContactMap = new Map<Id,Contact>([Select Id,Account.Association__c,Account.Board_Member__pc from Contact Where AccountId IN : objSetAccId]);
            Map<String,Group_Member__c> objMapAccWorGroIdGrouMem = new Map<String,Group_Member__c>();
            List<Group_Member__c> objGroupMembr = [Select Id,End_Date__c,Status__c,Working_Group_Role__c,Working_Group__c,Grou_Member__c,Start_Date__c from Group_Member__c where Working_Group__c IN : objSetWorkGrupID and Grou_Member__c IN : objContactMap.keySet()];
            for(Group_Member__c objGrouMem : objGroupMembr){
                objMapAccWorGroIdGrouMem.put(objGrouMem.Grou_Member__c+''+objGrouMem.Working_Group__c,objGrouMem);
            }
            if(objMapAssIdWorkGRPList.size() > 0){
                List<Group_Member__c> GroupMemberList = new List<Group_Member__c>();
                for(Contact objCon : objContactMap.values()){
                    if(objMapAssIdWorkGRPList.keySet().Contains(objCon.Account.Association__c)){
                            for(Working_Group__c objWorkGP : objMapAssIdWorkGRPList.get(objCon.Account.Association__c)){
                                if(objCon.Account.Board_Member__pc == true){ 
                                    String ConWorId = objCon.Id+''+objWorkGP.Id;
                                    if(!objMapAccWorGroIdGrouMem.keySet().Contains(ConWorId)){
                                        Group_Member__c objGrMem = new Group_Member__c();
                                        objGrMem.Working_Group__c = objWorkGP.Id;
                                        objGrMem.Start_Date__c = Date.today();
                                        objGrMem.Status__c = 'Active';
                                        objGrMem.Working_Group_Role__c = 'Member';
                                        objGrMem.Grou_Member__c = objCon.Id;
                                        objGrMem.Voting_Level__c = 'Primary';
                                        GroupMemberList.add(objGrMem);
                                    }else{
                                        Group_Member__c objGrMem = objMapAccWorGroIdGrouMem.get(ConWorId);
                                        objGrMem.Start_Date__c = Date.today();
                                        objGrMem.Status__c = 'Active';
                                        objGrMem.Voting_Level__c = 'Primary';
                                        objGrMem.End_Date__c = null;
                                        GroupMemberList.add(objGrMem);
                                    }
                                }else{
                                    if(objMapAccWorGroIdGrouMem.keySet().Contains(objCon.Id+''+objWorkGP.Id)){
                                        Group_Member__c objGrouMemUpdate = objMapAccWorGroIdGrouMem.get(objCon.Id+''+objWorkGP.Id);
                                        objGrouMemUpdate.Status__c = 'Past Member';
                                        objGrouMemUpdate.End_Date__c = Date.today();
                                        GroupMemberList.add(objGrouMemUpdate);
                                    }
                                }
                            }
                        }
                   
                }
                if(GroupMemberList.size() > 0){
                    upsert GroupMemberList;
                }
            }
        }
     }
     
}