public without sharing class RequestPendingApprovalCTRL {
/*
	Created Date - 8/12/2019
	Created By - Manas
	Description - RequestPendingApproval Component Add in Community
	Ticket - T-000942
	TestClass - 
*/
    public class wrapperClass{
        @AuraEnabled public Group_Member__c objAcc{get;set;}
        public wrapperClass(Group_Member__c objAcc){
            this.objAcc = objAcc;
        }
    }
    public class WrapperClassPickValue{
        @AuraEnabled public List<String> RoleOptions{get;set;}
        @AuraEnabled public List<String> VotingOptions{get;set;}
        public WrapperClassPickValue(List<String> RoleOptions,List<String> VotingOptions){
            this.RoleOptions = RoleOptions;
            this.VotingOptions = VotingOptions;
        }
    }
    @AuraEnabled
    public static void updateGroupMember(String GroupMemberId,String Role,String Voting,String RejectionReason,String StatusName) {
       Group_Member__c objGroupMem = new Group_Member__c();
       objGroupMem.Id = GroupMemberId;
       if(StatusName == 'Approve'){
           objGroupMem.Working_Group_Role__c = Role;
           objGroupMem.Voting_Level__c = Voting;
           objGroupMem.Start_Date__c = Date.today();
           objGroupMem.Status__c = 'Active';   
        }
        else{
            objGroupMem.Status__c = 'Rejected';
        }
       objGroupMem.Rejection_Reason__c = RejectionReason;
       update objGroupMem;
    } 
    
    @AuraEnabled
    public static WrapperClassPickValue GroupMemberRoleVoting() {
        List<String> RoleOptions = new List<String>();
        Schema.DescribeFieldResult RolefieldResult = Group_Member__c.Working_Group_Role__c.getDescribe();
        List<Schema.PicklistEntry> RolePle = RolefieldResult.getPicklistValues();
        for( Schema.PicklistEntry RolePickValue : RolePle){
            RoleOptions.add(RolePickValue.getValue());
        }
        List<String> VotingOptions = new List<String>();
        Schema.DescribeFieldResult VotingfieldResult = Group_Member__c.Voting_Level__c.getDescribe();
        List<Schema.PicklistEntry> VotingPle = VotingfieldResult.getPicklistValues();
        for( Schema.PicklistEntry VotingPickValue : VotingPle){
            VotingOptions.add(VotingPickValue.getValue());
        }
        WrapperClassPickValue objWraPickValue = new WrapperClassPickValue(RoleOptions,VotingOptions);
        return objWraPickValue;
    }     
    @AuraEnabled
    public static List<Working_Group__c> WorkingGroupsList() {
        String CommunityUserId = UserInfo.getUserId();
        List<User> objUserList = [Select Id,ContactId from User where Id =:CommunityUserId and ContactId != null];
        String UserId='';
        if(objUserList.size() > 0)
            UserId = objUserList[0].ContactId;
        List<Group_Member__c> objGrouMemListUser = [Select Id,Working_Group__c,Working_Group__r.Name,Working_Group_Role__c,Grou_Member__c,Grou_Member__r.Name,Corporate_Member__c,CreatedDate from Group_Member__c where Grou_Member__c =:UserId and
                                                (Working_Group_Role__c = 'Chair' OR Working_Group_Role__c ='Co-Chair') and Status__c = 'Pending'];
        Set<Id> objSetWorkingGroupId = new Set<Id>();
        for(Group_Member__c objGroupMem : objGrouMemListUser){
            objSetWorkingGroupId.add(objGroupMem.Working_Group__c);
        }
        List<Working_Group__c> objWorkGroupList = [Select Id,Name from Working_Group__c where Id IN : objSetWorkingGroupId];
        return objWorkGroupList;
    } 
    @AuraEnabled
    public static List<wrapperClass> PendingApprovalGroupMember(String WorkingGrupId) {
        List<wrapperClass> objWrapList = new List<wrapperClass>();
        
        String CommunityUserId = UserInfo.getUserId();
        List<User> objUserList = [Select Id,ContactId from User where Id =:CommunityUserId and ContactId != null];
        String UserId='';
        if(objUserList.size() > 0)
            UserId = objUserList[0].ContactId;
        
        List<Group_Member__c> objGrouMemList = New List<Group_Member__c>();
        if(String.isBlank(WorkingGrupId)){
           List<Group_Member__c> objGrouMemListUser = [Select Id,Working_Group__c,Working_Group__r.Name,Working_Group_Role__c,Grou_Member__c,Grou_Member__r.Name,Corporate_Member__c,CreatedDate from Group_Member__c where Grou_Member__c =:UserId and
                                                (Working_Group_Role__c = 'Chair' OR Working_Group_Role__c ='Co-Chair')];
            Set<Id> objSetWorkingGroupId = new Set<Id>();
            for(Group_Member__c objGroupMem : objGrouMemListUser){
                objSetWorkingGroupId.add(objGroupMem.Working_Group__c);
            }
           if(objSetWorkingGroupId.size() > 0){
               objGrouMemList = [Select Id,Working_Group__c,Working_Group__r.Name,Working_Group_Role__c,Grou_Member__c,Grou_Member__r.Name,Corporate_Member__c,CreatedDate from Group_Member__c where Grou_Member__c !=:UserId and
                             Status__c = 'Pending' and Working_Group__c IN : objSetWorkingGroupId];
           }
        }else{
            List<Group_Member__c> objGrouMemListUser = [Select Id,Working_Group__c,Working_Group__r.Name,Working_Group_Role__c,Grou_Member__r.Name,Grou_Member__c,Corporate_Member__c,CreatedDate from Group_Member__c where Grou_Member__c =:UserId and
                                                (Working_Group_Role__c = 'Chair' OR Working_Group_Role__c ='Co-Chair') and Working_Group__c =:WorkingGrupId];
            Set<Id> objSetWorkingGroupId = new Set<Id>();
            for(Group_Member__c objGroupMem : objGrouMemListUser){
                objSetWorkingGroupId.add(objGroupMem.Working_Group__c);
            }
            if(objSetWorkingGroupId.size() > 0){
             	objGrouMemList = [Select Id,Working_Group__c,Working_Group__r.Name,Working_Group_Role__c,Grou_Member__c,Grou_Member__r.Name,Corporate_Member__c,CreatedDate from Group_Member__c where Grou_Member__c !=:UserId and
                                  Working_Group__c =:objSetWorkingGroupId and Status__c = 'Pending'];   
            }
        }
           
        for(Group_Member__c objGrMemRec : objGrouMemList){
            objWrapList.add(New wrapperClass(objGrMemRec));
        }
        System.debug('#####objGrouMemList##### '+objGrouMemList);
        return objWrapList;
    }
    @AuraEnabled
    public static List<wrapperClass> PendingApprovalGroupMemberSearch(String WorkingGrupId,String SearchValue,Boolean isSingleSearch) {
        List<wrapperClass> objWrapList = new List<wrapperClass>();
        
        String CommunityUserId = UserInfo.getUserId();
        List<User> objUserList = [Select Id,ContactId from User where Id =:CommunityUserId and ContactId != null];
        String UserId='';
        if(objUserList.size() > 0)
            UserId = objUserList[0].ContactId;
        
        String SearchTextName='';
        if(isSingleSearch == false)
            SearchTextName = '%'+SearchValue+'%';
        else
            SearchTextName = SearchValue+'%';
        
        List<Group_Member__c> objGrouMemList = New List<Group_Member__c>();
        if(String.isBlank(WorkingGrupId)){
            List<Group_Member__c> objGrouMemListUser = [Select Id,Working_Group__c,Working_Group__r.Name,Working_Group_Role__c,Grou_Member__c,Grou_Member__r.Name,Corporate_Member__c,CreatedDate from Group_Member__c where Grou_Member__c =:UserId and
                                                (Working_Group_Role__c = 'Chair' OR Working_Group_Role__c ='Co-Chair')];
            Set<Id> objSetWorkingGroupId = new Set<Id>();
            for(Group_Member__c objGroupMem : objGrouMemListUser){
                objSetWorkingGroupId.add(objGroupMem.Working_Group__c);
            }
           if(objSetWorkingGroupId.size() > 0){
               objGrouMemList = [Select Id,Working_Group__c,Working_Group__r.Name,Working_Group_Role__c,Grou_Member__c,Grou_Member__r.Name,Corporate_Member__c,CreatedDate from Group_Member__c where Grou_Member__c !=:UserId and
                             Status__c = 'Pending' and Working_Group__c IN : objSetWorkingGroupId and Grou_Member__r.Name LIKE :SearchTextName];
           }
        }else{
            List<Group_Member__c> objGrouMemListUser = [Select Id,Working_Group__c,Working_Group__r.Name,Working_Group_Role__c,Grou_Member__c,Grou_Member__r.Name,Corporate_Member__c,CreatedDate from Group_Member__c where Grou_Member__c =:UserId and
                                                (Working_Group_Role__c = 'Chair' OR Working_Group_Role__c ='Co-Chair') and Working_Group__c =:WorkingGrupId];
            Set<Id> objSetWorkingGroupId = new Set<Id>();
            for(Group_Member__c objGroupMem : objGrouMemListUser){
                objSetWorkingGroupId.add(objGroupMem.Working_Group__c);
            }
            if(objSetWorkingGroupId.size() > 0){
             	objGrouMemList = [Select Id,Working_Group__c,Working_Group__r.Name,Working_Group_Role__c,Grou_Member__c,Grou_Member__r.Name,Corporate_Member__c,CreatedDate from Group_Member__c where Grou_Member__c !=:UserId and
                                  Working_Group__c =:objSetWorkingGroupId and Status__c = 'Pending' and Grou_Member__r.Name LIKE :SearchTextName];   
            }
        }
           
        for(Group_Member__c objGrMemRec : objGrouMemList){
            objWrapList.add(New wrapperClass(objGrMemRec));
        }
        return objWrapList;
    }
    
}