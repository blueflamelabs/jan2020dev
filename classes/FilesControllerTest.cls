/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy     Description
*     1.0        120619         BFL Users     This class is built to test FilesController apex class
**********************************************************************************************************************************************************/ 
@isTest
public class FilesControllerTest {

    /**
    * Method Name : setupTestData
    * Parameters  : none
    * Description : This method is used to create test data
    **/
    @testSetup
    public static void setupTestData() {
    
        // Variable declarations

        // For account
        Integer accRecCount = 5;
        List<Account> accInsertionList = new List<Account>();
        // For contact
        Integer conRecCount = 5;
        List<Contact> conInsertionList = new List<Contact>();
        // For case
        Integer caseRecCount = 5;
        List<Case> caseInsertionList = new List<Case>();
        // For ContentVersion
        Integer contentVerRecCount = 5;
        List<ContentVersion> contentVerInsertionList = new List<ContentVersion>();
        // For ContentDocumentLink
        Integer contentDocLinkRecCount = 5;
        List<ContentDocumentLink> contentDocLinkInsertionList = new List<ContentDocumentLink>();
        
        // For ContentVersion for Account
        Integer contentVerRecCount1 = 5;
        List<ContentVersion> contentVerInsertionList1 = new List<ContentVersion>();
        
        // For ContentDocumentLink for Account
        Integer contentDocLinkRecCount1 = 5;
        List<ContentDocumentLink> contentDocLinkInsertionList1 = new List<ContentDocumentLink>();
       
        // Insert Account
        for(Integer i=0; i < accRecCount; i++) {
            Account objAcc = new Account();
            objAcc.Name = 'Test Account'+i;
            accInsertionList.add(objAcc);
        }
        insert accInsertionList;
        
        Account objAcc = new Account();
        objAcc.Name = 'Test without file';
        
        insert objAcc;
        
        // Insert Contact
        for(Integer i=0; i < conRecCount; i++) {
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact'+i;
            if(accInsertionList.size() >= i) {
                objContact.AccountId = accInsertionList[i].Id;
            } else if(!accInsertionList.isEmpty()) {
                objContact.AccountId = accInsertionList[0].Id;
            }
            conInsertionList.add(objContact);
        }
        insert conInsertionList;
        
        // Insert Case
        //Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
        //System.debug('RecordTypeIdCase======'+RecordTypeIdCase);
        for(Integer i=0; i < caseRecCount; i++) {
            Case objCase = new Case();
            objCase.Subject = 'Test Subject'+i;
            objCase.Description = 'Test Description'+i;
            //objCase.recordtypeid = RecordTypeIdCase;
            objCase.Status = 'New';
            objCase.Origin = 'Email';
            if(conInsertionList.size() >= i) {
                objCase.ContactId = conInsertionList[i].Id;
                //objCase.AccountId = conInsertionList[i].AccountId;
            } else if(!conInsertionList.isEmpty()) {
                objCase.ContactId = conInsertionList[0].Id;
                //objCase.AccountId = conInsertionList[0].AccountId;
            }
            caseInsertionList.add(objCase);
        }
        insert caseInsertionList;

        // Create a dummy case record for negative testing
        Case objDummyCase = new Case();
        objDummyCase.Subject = 'Dummy Case Subject';
        objDummyCase.Description = 'Dummy Case Description';
        //objDummyCase.recordtypeid = RecordTypeIdCase;
        objDummyCase.Status = 'New';
        objDummyCase.Origin = 'Email';
        objDummyCase.ContactId = conInsertionList[0].Id;
        insert objDummyCase;
        
        
        
        // Insert ContentVersion
        for(Integer i=0; i < contentVerRecCount; i++) {
            ContentVersion objContentVersion = new ContentVersion();
            objContentVersion.Title = 'Test Title';
            objContentVersion.PathOnClient = 'Test.jpg';
            objContentVersion.VersionData = Blob.valueOf('Test Content Data');
            objContentVersion.IsMajorVersion = true;
            contentVerInsertionList.add(objContentVersion);
        }
        insert contentVerInsertionList;
        
        // Fetch Content Document records
        List<ContentDocument> documents = [SELECT Id, 
                                                  Title, 
                                                  LatestPublishedVersionId 
                                             FROM ContentDocument];
        //System.debug('documents======='+documents);  

        // Insert ContentDocumentLink
        for(Integer i=0; i < contentDocLinkRecCount; i++) {
            ContentDocumentLink objContentDocLink = new ContentDocumentLink();
            if(caseInsertionList.size() >= i) {
                objContentDocLink.LinkedEntityId = caseInsertionList[i].Id;
            } 
            if(documents.size() >= i) {
                objContentDocLink.ContentDocumentId = documents[i].Id;
            } 
            objContentDocLink.Visibility = 'AllUsers';
            objContentDocLink.ShareType= 'V';
            contentDocLinkInsertionList.add(objContentDocLink);
        }  
        insert contentDocLinkInsertionList; 
        
        // Insert ContentVersion for Account.
        for(Integer i=0; i < contentVerRecCount1; i++) {
            ContentVersion objContentVersion = new ContentVersion();
            objContentVersion.Title = 'Test Title for Account';
            objContentVersion.PathOnClient = 'Test.jpg';
            objContentVersion.VersionData = Blob.valueOf('Test Content Data');
            objContentVersion.IsMajorVersion = true;
            contentVerInsertionList1.add(objContentVersion);
        }
        insert contentVerInsertionList1;
        
        // Fetch Content Document records
        List<ContentDocument> documents1 = [SELECT Id, 
                                                  Title, 
                                                  LatestPublishedVersionId 
                                             FROM ContentDocument];
        // Content Document insert for Account.
        for(Integer i=0; i < contentDocLinkRecCount1; i++) {
            ContentDocumentLink objContentDocLink = new ContentDocumentLink();
            if(accInsertionList.size() >= i) {
                objContentDocLink.LinkedEntityId = accInsertionList[i].Id;
            } 
            if(documents.size() >= i) {
                objContentDocLink.ContentDocumentId = documents1[i].Id;
            } 
            objContentDocLink.Visibility = 'AllUsers';
            objContentDocLink.ShareType= 'V';
            contentDocLinkInsertionList1.add(objContentDocLink);
        }  
        insert contentDocLinkInsertionList1;
        
      }
    
    /**
    * Method Name : getFilesClonePositiveTest
    * Parameters  : none
    * Description : This method is used to test the positive scenario of getFilesClone functionality
    **/
    @isTest
    public static void getFilesClonePositiveTest() {
        List<Case> listCase = new List<Case>();
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        listCase = [SELECT Id
                      FROM Case];
        //System.debug('listCase======='+listCase+'==========listCase size======='+listCase.size());
        if(!listCase.isEmpty()) {
            objFilesControllerWrapper = FilesController.getFilesClone(listCase[0].Id);
        }
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper.files.size() > 0,'Files list in Files Controller Wrapper is empty');
        
    }
    /**
    * Method Name : getFilesCloneNegativeTest
    * Parameters  : none
    * Description : This method is used to test the negative scenario of getFilesClone functionality
    **/
    @isTest
    public static void getFilesCloneNegativeTest() {

        List<Case> listCase = new List<Case>();
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        listCase = [SELECT Id
                      FROM Case
                      WHERE Subject = 'Dummy Case Subject'];
        //System.debug('listCase======='+listCase+'==========listCase size======='+listCase.size());
        if(!listCase.isEmpty()) {
            objFilesControllerWrapper = FilesController.getFilesClone(listCase[0].Id);
        }
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper.files.size() == 0,'Files list in Files Controller Wrapper is not empty');
        
    }
    
    /**
    * Method Name : getFilesClonePositiveTest
    * Parameters  : none
    * Description : This method is used to test the positive scenario of getFilesClone functionality
    **/
    @isTest
    public static void getFilesClonePositiveTestForAccount() {
        List<Account> listAccount = new List<Account>();
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        listAccount = [SELECT Id
                      FROM Account];
        //System.debug('listCase======='+listCase+'==========listCase size======='+listCase.size());
        if(!listAccount.isEmpty()) {
            objFilesControllerWrapper = FilesController.getFilesClone(listAccount[0].Id);
        }
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper.files.size() > 0,'Files list in Files Controller Wrapper is empty');
        
    }
    /**
    * Method Name : getFilesCloneNegativeTest
    * Parameters  : none
    * Description : This method is used to test the negative scenario of getFilesClone functionality
    **/
    @isTest
    public static void getFilesCloneNegativeTestForAccount() {

        List<Account> listAccount = new List<Account>();
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        listAccount = [SELECT Id
                       FROM Account
                       WHERE Name = 'Test without file'];
        //System.debug('listAccount======='+listAccount+'==========listAccount size======='+listAccount.size());
        if(!listAccount.isEmpty()) {
            objFilesControllerWrapper = FilesController.getFilesClone(listAccount[0].Id);
        }
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper.files.size() == 0,'Files list in Files Controller Wrapper is not empty');
        
    }
    
    
    /**
    * Method Name : getFilesPositiveTest
    * Parameters  : none
    * Description : This method is used to test positive scenario of getFiles functionality
    **/
    @isTest
    public static void getFilesPositiveTest() {
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case];
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        if(!listCase.isEmpty()) {
            Test.startTest();
            objFilesControllerWrapper = FilesController.getFiles(listCase[0].Id);
            Test.stopTest();
        }
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper != null,'File Controller Wrapper object is null');
    }
    /**
    * Method Name : getFilesNegativeTest
    * Parameters  : none
    * Description : This method is used to test negative scenario of getFiles functionality
    **/
    @isTest
    public static void getFilesNegativeTest() {
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case
                     WHERE Subject = 'Dummy Case Subject'];
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        if(!listCase.isEmpty()) {
            Test.startTest();
            objFilesControllerWrapper = FilesController.getFiles(listCase[0].Id);
            Test.stopTest();
        }
        //System.debug('objFilesControllerWrapper::::::::===='+objFilesControllerWrapper);
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper.files.size() == 0,'Files list in Files Controller Wrapper is not empty');
    }
    
    
    /**
    * Method Name : getAllFilesPositiveTest
    * Parameters  : none
    * Description : This method is used to test the positive scenarios of getAllFiles functionality
    **/
    @isTest
    public static void getAllFilesPositiveTest() {
        // Fetch Content Document records
        List<ContentDocument> documents = [SELECT Id, 
                                                  Title, 
                                                  LatestPublishedVersionId 
                                             FROM ContentDocument];
                        
        Test.startTest();
        List<ContentVersion> listContentVersion = FilesController.getAllFiles(new List<String>{documents[0].Id});
        Test.stopTest();
        
        // Assert : Check if there are related content version records related to the document that 
        // is been passed as parameter to the metod
        System.assert(!listContentVersion.isEmpty(),'Content Version list is empty');
    }
    /**
    * Method Name : getAllFilesNegativeTest
    * Parameters  : none
    * Description : This method is used to test the negative scenarios of getAllFiles functionality
    **/
    @isTest
    public static void getAllFilesNegativeTest() {
        // Fetch Content Document records
        
        Test.startTest();
        // Pass dummy document Id to the method
        List<ContentVersion> listContentVersion = FilesController.getAllFiles(new List<String>{'069241516172818','069536474829281'});
        Test.stopTest();
        
        // Assert : Check if there are related content version records related to the document that 
        // is been passed as parameter to the metod
        System.assert(listContentVersion.isEmpty(),'Content Version list is not empty');
    }
    
    /**
    * Method Name : saveFilesTest
    * Parameters  : none
    * Description : This method is used to test saveFiles functionality
    **/
    @isTest
    public static void saveFilesTest() {
        // Fetch Content Document records
        
        List<Case> listCase = new List<Case>();
        List<ContentVersion> listContentVersion = new List<ContentVersion>();
        List<ContentVersion> listContentVersionUpdated = new List<ContentVersion>();
        listCase = [SELECT Id
                    FROM Case];
        
        listContentVersion = [SELECT Id
                              FROM ContentVersion
                              LIMIT 1];
        
       Id contentVerId = listContentVersion[0].Id;
       if(!listContentVersion.isEmpty() && !listCase.isEmpty()) {
            
            FilesController.saveFiles(listContentVersion, listCase[0].Id);
            listContentVersionUpdated = [SELECT Id
                                         FROM ContentVersion
                                         WHERE Id = :contentVerId];
              
       }
    }
    /**
    * Method Name : updateCaseRecordTest
    * Parameters  : none
    * Description : This method is used to test updateCaseRecord functionality
    **/
    /*@isTest
    public static void updateCaseRecordTest() {
        String parentId;
        list<String> fileIds = new List<String>();
        Boolean isCommunityUser;
        Test.startTest();
        FilesController.updateCaseRecord(parentId, fileIds, isCommunityUser);
        Test.stopTest();
    }*/
}