global without sharing class AssociationPickValue extends VisualEditor.DynamicPickList {
    	
    VisualEditor.DesignTimePageContext context;

    global AssociationPickValue(VisualEditor.DesignTimePageContext context) {
       this.context = context;
    }
	global override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('', '');
        return defaultValue;
    }
    global override VisualEditor.DynamicPickListRows getValues() {
        VisualEditor.DynamicPickListRows  myValues = new VisualEditor.DynamicPickListRows();
        List<Association__c> objAssList = [Select Id,Name from Association__c limit 50000];
        for(Association__c objAss : objAssList){
          String RecId = objAss.Id;
          visualEditor.DataRow value1 = new VisualEditor.DataRow(objAss.Name,RecId);
          myValues.addRow(value1);
        }
        return myValues;
    }
}