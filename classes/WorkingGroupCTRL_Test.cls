/*
    Test Class Use for WorkingGroupCTRL Class
*/
@IsTest
public class WorkingGroupCTRL_Test {
    @IsTest
       static void AssociationCTRLTestMethod(){
            Association__c ass = new Association__c();
            ass.Name = 'test';
            ass.Community_URL__c = 'test.com';
            insert ass;
            
             Working_Group__c workgroup = new Working_Group__c();
            workgroup.Association__c = ass.id;
            insert workgroup;
            
            
            Association_Question__c assque = new Association_Question__c();
            assque.Association__c = ass.id;
            insert assque;
                   
           
           Id AccRecordTypeIdPer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
           Account accPer = new Account();
           accPer.RecordTypeId = AccRecordTypeIdPer;
           accPer.FirstName = 'Test Parent' ;
           accPer.LastName = 'Test' ;
           insert accPer;           
                   
           Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
           Account acc = new Account();
           acc.RecordTypeId = AccRecordTypeId;
           acc.Person_Account__c=accPer.id;
           acc.FirstName = 'Test Parent';
           acc.LastName = 'Test' ;
           acc.Association__c = ass.id;
           insert acc;
           
           
            Group_Member__c objGrMem = new Group_Member__c();
            objGrMem.Grou_Member__c = accPer.id;
            objGrMem.Working_Group__c = workgroup.id;
            objGrMem.Status__c = 'Active';
            insert objGrMem;
           
            System.debug('########acc#####3 '+acc);
            String RecName = 'Individual Member';
            String RecName1 = 'Corporate Member';
            String RecName2 = 'Group Members';
           
            WorkingGroupCTRL accCTRL = new WorkingGroupCTRL();
            WorkingGroupCTRL.AccountsMemberRecord(ass.id);
            WorkingGroupCTRL.getAccounts(ass.id,null,true,RecName);
            WorkingGroupCTRL.getAccounts(ass.id,'t',true,RecName1);
            WorkingGroupCTRL.getAccounts(ass.id,'t',false,RecName);
            WorkingGroupCTRL.getAccounts(ass.id,'t',false,RecName2);
            WorkingGroupCTRL.getAccounts(ass.id,'t',true,RecName2);
            WorkingGroupCTRL.getAccounts(ass.id,null,true,RecName2);
    }  
}