/********************************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                 ModifiedDate  ModifiedBy  Description
*       1.0      171019         VennScience_BFL_Amruta                              This Batch is built to send Renewal/Expiration alerts to Individual Members and 
*                                                                                   Corporate Representatives using Conga.                            
*********************************************************************************************************************************************************************/  
global class AutomatedRenewalsCongaBatch implements Database.Batchable<Sobject>, Database.AllowsCallouts, Database.Stateful {
	// Variable Declarations
	public Id individualMemId;
    public Id parentAssociationId;
    
    // Parameterised Constructor
    public AutomatedRenewalsCongaBatch(Id recordId, Id assId) {
        individualMemId = recordId;
        parentAssociationId = assId;
    }
    // Constructor
    public AutomatedRenewalsCongaBatch() {
    }
    // Batch start method
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // Variable Declarations
        String query = '';
        String accRecordTypeName = '';
        String accMemberCategory = '';
        String accStatus = 'Inactive';
        List<String> listAccRecordTypeNameTemp = new List<String>();
        List<String> listAccRecordTypeName = new List<String>();
        Individual_Members_Email_Alerts_Setting__c objCustomSetting = new Individual_Members_Email_Alerts_Setting__c();
        
        // Fetch custom setting data
        // @Reminder: Move getCustomSettingValues method to this batch's handler class
        objCustomSetting = NotifyIndividualMembersBatchHandler.getCustomSettingValues();
        // Check if there custom seting org default record is not created
        if(objCustomSetting == null || String.isBlank(objCustomSetting.Account_Record_Type_Name__c) ||
           String.isBlank(objCustomSetting.Account_Member_Category__c) || 
           String.isBlank(objCustomSetting.Default_Sender_Address__c)) {
           // If Custom setting values are null then return empty query result    
           return Database.getQueryLocator('SELECT Id FROM Account LIMIT 0');
        } // End of if
        accRecordTypeName = objCustomSetting.Account_Record_Type_Name__c;
        accMemberCategory = objCustomSetting.Account_Member_Category__c;
        
        // Split Account record type name seperated by commas
        if(String.isNotBlank(accRecordTypeName) && accRecordTypeName.contains(',')) {
            listAccRecordTypeNameTemp = accRecordTypeName.split(',');
        } else if(String.isNotBlank(accRecordTypeName) && !accRecordTypeName.contains(',')) {
            listAccRecordTypeNameTemp.add(accRecordTypeName);
        } // End of if-else block
        // Iterate over AccountRecordTypeName list and check if the custom setting value consist of any white spaces
        for(String strAccRecordTypeName : listAccRecordTypeNameTemp) {
            if(String.isNotBlank(strAccRecordTypeName)) {
              listAccRecordTypeName.add(strAccRecordTypeName.trim());
            } // End of if           
        } // End of for
        
        // Form query
        // @Reminder: Add condition where Status != Terminated
        if(individualMemId != null) {
            query = 'SELECT Id,'
                    +'Name,'
                	+'Next_Renewal_Date__c,'
                    +'Expiration_Date__c,'
                    +'PersonEmail,'
                    +'Association__c,'
                    +'PersonContactId,'
                    +'Most_Recent_Renewal_Invoice__c,'
                    +'RecordType.DeveloperName,'
                    +'BillingCountry,'
                    +'BillingState,'
                    +'BillingCity,'
                    +'BillingStreet,'
                    +'BillingPostalCode,'
                    +'Association_Account__c,'
                    +'Association_Account__r.BillingCountry,'
                    +'Association_Account__r.BillingState,'
                    +'Association_Account__r.BillingCity,'
                    +'Association_Account__r.BillingStreet,'
                    +'Association_Account__r.BillingPostalCode,'
                    +'Association__r.OwnerId'
                    +' FROM Account'
                    +' WHERE RecordType.DeveloperName IN :listAccRecordTypeName'
                    +' AND Member_Category__pc = :accMemberCategory'
                    +' AND Do_Not_Send_Automated_Emails__c = false'
                    +' AND Status__c != :accStatus'
                    +' AND Id = :individualMemId';
        } else {
            query = 'SELECT Id,'
                    +'Name,'
                	+'Next_Renewal_Date__c,'
                    +'Expiration_Date__c,'
                    +'PersonEmail,'
                    +'Association__c,'
                    +'PersonContactId,'
                    +'Most_Recent_Renewal_Invoice__c,'
                    +'RecordType.DeveloperName,'
                    +'BillingCountry,'
                    +'BillingState,'
                    +'BillingCity,'
                    +'BillingStreet,'
                    +'BillingPostalCode,'
                    +'Association_Account__c,'
                    +'Association_Account__r.BillingCountry,'
                    +'Association_Account__r.BillingState,'
                    +'Association_Account__r.BillingCity,'
                    +'Association_Account__r.BillingStreet,'
                    +'Association_Account__r.BillingPostalCode,'
                    +'Association__r.OwnerId'
                    +' FROM Account'
                    +' WHERE RecordType.DeveloperName IN :listAccRecordTypeName'
                    +' AND Member_Category__pc = :accMemberCategory'
                    +' AND Status__c != :accStatus'
                    +' AND Do_Not_Send_Automated_Emails__c = false';
        } // End of else-if
        System.debug('query========'+query);
        return Database.getQueryLocator(query);
    }
    // Batch execute method
    global void execute(Database.BatchableContext BC, List<Account> listAccountRecords) { 
        // Variable Declarations
        List<Association__c> listAssociationRecords = new List<Association__c>();
        Set<Id> setAssociationId = new Set<Id>();
        Map<Id,Association__c> mapAssIdVSAssRecord = new Map<Id,Association__c>();
        Map<Id,List<Account>> mapAssIdVSlistChildAccount = new Map<Id,List<Account>>();
        
        System.debug('listAccountRecords====='+listAccountRecords);
        // Iterate over Account list
        for(Account accRecord : listAccountRecords) {
            // Populate set of Association Id
            setAssociationId.add(accRecord.Association__c);
            // Populate AssociationId VS list of related Child Account map
            if(!mapAssIdVSlistChildAccount.containsKey(accRecord.Association__c)) {
                mapAssIdVSlistChildAccount.put(accRecord.Association__c, new List<Account>{accRecord});
            } else {
                List<Account> listPreviousAcc = new List<Account>();
                listPreviousAcc.addAll(mapAssIdVSlistChildAccount.get(accRecord.Association__c));
                listPreviousAcc.add(accRecord);
                mapAssIdVSlistChildAccount.put(accRecord.Association__c,listPreviousAcc);
            } // End of else-if block  
        } // End of for
        System.debug('mapAssIdVSlistChildAccount======'+mapAssIdVSlistChildAccount);
        
        // Fetch Association records
        listAssociationRecords = [SELECT Id,
                                         Name,
                                         First_Renewal_Alert_Email_Template__c,
                                         First_Renewal_Alert_Send_Days__c,
                                         Second_Renewal_Alert_Email_Template__c,
                                         Second_Renewal_Alert_Send_Days__c,
                                         Third_Renewal_Alert_Email_Template__c,
                                         Third_Renewal_Alert_Send_Days__c,
                                         Second_Expiration_Alert_Email_Template__c,
                                         Second_Expiration_Alert_Send_Days__c,
                                         Final_Termination_Notice_Email_Template__c,
                                         Final_Termination_Notice_Send_Days__c,
                                         First_Expiration_Alert_Email_Template__c,
                                         First_Expiration_Alert_Send_Days__c,
                                         Email_Sender_Address__c
                                    FROM Association__c
                                   WHERE Id IN :setAssociationId];
        System.debug('listAssociationRecords========'+listAssociationRecords);
        // Iterate over Association list
        for(Association__c assRecord : listAssociationRecords) {
            mapAssIdVSAssRecord.put(assRecord.Id, assRecord);
        } // End of for
        System.debug('mapAssIdVSAssRecord======='+mapAssIdVSAssRecord);
        // Call handler method
        AutomatedRenewalsCongaBatchHandler.sendRenewalExpirationEmailAlerts(mapAssIdVSAssRecord, mapAssIdVSlistChildAccount);
    }
    // Batch finish method
    global void finish(Database.BatchableContext bc) {   
    }
}