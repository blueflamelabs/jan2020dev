/*********************************************************************************************************************************************************
*     Version    CreateDate       CreatedBy               ModifiedDate   ModifiedBy  Description
*     1.0        26112019         VennScience_BFL_Amruta                             This is the batch class for sending the Invoices to related Leads.                               
**********************************************************************************************************************************************************/
global class SendInvoiceEmailsToLeadBatch implements Database.Batchable<Sobject>, Database.AllowsCallouts {
    // Variable Declarations
    public Set<Id> setInvoiceId = new Set<Id>();
    public static final String LEAD_PAYMENT_METHOD = 'Send Invoice';

    global SendInvoiceEmailsToLeadBatch(Set<Id> setInvoiceId) {

        this.setInvoiceId = setInvoiceId;
    }
    // Batch start method
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // Variable Declarations
        String query = '';

        System.debug('Inside start method');
        System.debug('setInvoiceId======='+setInvoiceId);
        
        // @Reminder: Check if roll up summary count is greater than zero
        query = 'SELECT Id,'
                +'Lead__c,'
                +'Lead__r.Email,'
                +'Association__c,'
                +'Association__r.OwnerId,'
                +'Association__r.Email_Sender_Address__c,'
                +'Additional_Contact__c,'
                +'Additional_Contact__r.Email_Address__c'
                +' FROM Invoice__c'
                +' WHERE Id IN :setInvoiceId'
                +' AND Lead__r.Payment_Method__c = :LEAD_PAYMENT_METHOD';
        System.debug('query========'+query);
        return Database.getQueryLocator(query);
    }
    // Batch execute method
    global void execute(Database.BatchableContext BC, List<Invoice__c> listInvoiceRecords) { 
        System.debug('Inside execute method===='+listInvoiceRecords);
        // Variable Declarations
        
        // Call method so as to check if the Association's sender email address is verified or not 
        SendInvoiceEmailsToLeadBatchHandler.getAssociationSenderAddress(listInvoiceRecords);
        // Call handler method so as to send the OnBoarding Invoice emails to related Leads
        //SendInvoiceEmailsToLeadBatchHandler.sendOnBoardingInvoiceEmails(listInvoiceRecords);
    }
    // Batch finish method
    global void finish(Database.BatchableContext bc) {   
    }
}