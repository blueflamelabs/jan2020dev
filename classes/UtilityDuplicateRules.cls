Public class UtilityDuplicateRules{
    Public Static String DuplicateError(List<Database.Error> duplicateRecordError,Map<Id,sObject> objMapAss){
        Datacloud.DuplicateResult duplicateResult;
        List<Datacloud.MatchRecord> matchRecords;
        List<sObject> objListRecords = new List<sObject>();
        for(Database.Error duplicateError : duplicateRecordError) {
            if (duplicateError instanceof Database.DuplicateError) {
                duplicateResult = ((Database.DuplicateError)duplicateError).getDuplicateResult();                   
            }
        }
        if(duplicateResult != null){ 
            for(Datacloud.MatchResult duplicateMatchResult : duplicateResult.getMatchResults()) {
                matchRecords = duplicateMatchResult.getMatchRecords();
            }
        }
        if(matchRecords != null){
            System.debug('total.--matchRecords--'+matchRecords);  
            for(Datacloud.MatchRecord duplicateMatchRecord : matchRecords) {
                objListRecords.add(duplicateMatchRecord.getRecord());
            }
        }
        System.debug('Total duplicate Accounts -- '+objListRecords);
        String htmlTable = '';
        if(objListRecords.size() > 0){
            htmlTable = '<html><body><br/><table  style="border-collapse: collapse;border:solid 1px #e0e3e5;">'
                +'<caption></caption>';
            Map<String, Object> objFieldValue = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(objListRecords[0]));
            htmlTable+='<tr style="background-color: #f2f3f3;font-weight: 600;">';
            for(String FieldName : objFieldValue.keySet()){
                if(FieldName != 'attributes' && FieldName != 'Id' && FieldName != 'OwnerId'){
                    if(FieldName.lastIndexOf('__c') > 0){
                        FieldName = FieldName.substring(0,FieldName.length()-1);
                    }
                    FieldName = FieldName.replace('_',' ');
                    
                    if(FieldName == 'Name'){
                        htmlTable+='<th style="width: 21%;">'+FieldName+'</th>';
                    }else if(FieldName == 'LastModifiedDate'){
                        htmlTable+='<th>Last Modified Date</th>';
                    }else{
                        htmlTable+='<th>'+FieldName+'</th>';
                    }
                }    
            }
            htmlTable+='</tr>';
            System.debug('##########objMapAssobjMapAssobjMapAss## '+objMapAss);
            for(sObject objRecord : objListRecords){
                System.debug('######objRecord######## '+objRecord);
                htmlTable+='<tr>';
                for(String FieldName : objFieldValue.keySet()){
                    if(FieldName != 'attributes' && FieldName != 'Id' && FieldName != 'OwnerId'){
                        if(FieldName == 'Name'){
                            String recId = 'Id';
                            htmlTable+='<td><a href="/'+objRecord.get(recId)+'">'+objRecord.get(FieldName)+'</a></td>';
                        }else if(FieldName == 'LastModifiedDate'){
                            DateTime dateTimeValue = DateTime.valueOf(objRecord.get(FieldName));
                            String dateFormatdateTimeValue = dateTimeValue.format('MM/dd/YYYY HH:MM:SS aaa');
                            htmlTable+='<td>'+dateFormatdateTimeValue+'</td>';
                        } else{
                            String RecordValue = String.valueOf(objRecord.get(FieldName));
                            if(String.isNotBlank(RecordValue)){
                                try{
                                    Id RecordId = (Id)RecordValue;
                                    if(objMapAss.keySet().Contains(RecordId)){
                                        htmlTable+='<td><a href="/'+objMapAss.get(RecordId).get('Id')+'">'+objMapAss.get(RecordId).get('Name')+'</a></td>';
                                    }
                                }catch(Exception ex){ 
                                    htmlTable+='<td>'+objRecord.get(FieldName)+'</td>';
                                }
                            }else{
                                htmlTable+='<td></td>';
                            }
                        } 
                    }
                }
                htmlTable+='</tr>';
            }
            htmlTable+='</table></body></html>';
            System.debug('$$htmlTable$$$$$ '+htmlTable);
        }
        
        return htmlTable;
    }
}