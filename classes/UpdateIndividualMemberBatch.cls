/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         150719      VennScience_BFL_Monali     This Batch is built to update the Termination Date of Corporate and Individual Member
                                                           Account
**********************************************************************************************************************************************************/   
global class UpdateIndividualMemberBatch implements Database.Batchable<Sobject> 
{
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // 210819 - T-00511 - VennScience_BFL_Amruta - Fetch the custom setting value
        Termination_Date_Update_Setting__c objCustomSetting = new Termination_Date_Update_Setting__c();
        objCustomSetting = UpdateIndividualMemberController.getCustomSettingValues();
        if(objCustomSetting == null || String.isBlank(objCustomSetting.Corporate_Member_RecordType_Name__c) ||
           String.isBlank(objCustomSetting.Individual_Member_s_RecordType_Name__c)) {
           return Database.getQueryLocator('SELECT Id FROM Account LIMIT 0');
        }
        // Fetch the record type name from Custom Setting
        String corporateAccRecordTypeName = objCustomSetting.Corporate_Member_RecordType_Name__c;//'Corporate Member'
        String individualAccRecordTypeName = objCustomSetting.Individual_Member_s_RecordType_Name__c;//'Individual Member'

        // 200819 - T-00511 - VennScience_BFL_Amruta - Fetch the record type Id for Corporate Member
        Id corporateMemberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
                                                       .get(corporateAccRecordTypeName)
                                                       .getRecordTypeId();
        // 200819 - T-00511 - VennScience_BFL_Amruta - Fetch the record type Id for Individual Member
        Id indMemberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
                                   .get(individualAccRecordTypeName)
                                   .getRecordTypeId();
        
        String query = 'SELECT Id,'
                              +'Association_Account__c,'
                              +'Expiration_Date__c,'
                              +'Termination_Date__c ,'
                              +'Association_Account__r.Termination_Date__c,'
                              +'Association_Account__r.Expiration_Date__c,'
                              +'Company_Association_Membership__c,'
                              +'Company_Association_Membership__r.Final_Termination_Notice_Send_Days__c,'
                              +'(SELECT Id,Termination_Date__c FROM Individuals__r'
                              +' WHERE RecordTypeId = :indMemberRecordTypeId)'
                        +' FROM Account'            
                        +' WHERE RecordTypeId = :corporateMemberRecordTypeId'
                        +' AND Termination_Date__c = null'
                        + ' AND Expiration_Date__c != null';
        return Database.getQueryLocator(query); 
    }
    
    global void execute(Database.BatchableContext BC, List<Account> listAccount) { 
        
        // 200819 - T-00511 - VennScience_BFL_Amruta - Commented below code
        //updateIndividualMemberController ctrl = new updateIndividualMemberController();
        //ctrl.updateChildAccoundRecord(listAccount);

        // 200819 - T-00511 - VennScience_BFL_Amruta - Call Controller 
        updateIndividualMemberController controllerInstance = new updateIndividualMemberController();
        controllerInstance.setTerminationDate(listAccount);
        
    }    

    global void finish(Database.BatchableContext bc){   }   

}