/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*     1.0        040919         VennScience_BFL_Amruta       This is to test the NotifyIndividualMembersScheduler apex class.
**********************************************************************************************************************************************************/
@isTest
public class NotifyIndividualMembersSchedulerTest {
    /**
    * Method Name : scheduleBatchForEveryMidnightTest
    * Parameters  : 
    * Description : Used to test the scheduleBatchForEveryMidnight method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static void scheduleBatchForEveryMidnightTest() {
        // Cron Expression
        String CRON_EXP = '0 0 0 * * ?';
        // Get Custom Setting value
        Individual_Members_Email_Alerts_Setting__c objSetting = new Individual_Members_Email_Alerts_Setting__c();
        Test.startTest();
        NotifyIndividualMembersScheduler objScheduler = new NotifyIndividualMembersScheduler();
        objScheduler.scheduleBatchForEveryMidnight();  
        
        objScheduler.scheduleBatchForEveryMidnight();        
        Test.stopTest();
        // Fetch jobs from CronTrigger
        List<CronTrigger> listcronTrigger = [SELECT Id, 
                                                    CronExpression, 
                                                    TimesTriggered, 
                                                    NextFireTime 
                                               FROM CronTrigger 
                                              WHERE CronExpression = :CRON_EXP];
        // Assert: Check if job is scheduled
        System.assert(listcronTrigger.size() > 0, 'Job is not scheduled');
    }
}