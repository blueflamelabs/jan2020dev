/*********************************************************************************************************************************************************
*     Version    CreateDate       CreatedBy               ModifiedDate   ModifiedBy  Description
*     1.0        26112019         VennScience_BFL_Amruta                             This is the handler class for Account apex trigger.                               
**********************************************************************************************************************************************************/
public class AccountTriggerHandler {
    // Variable Declarations
    public static boolean isAccountTriggerRunned = false;
    // 261119 - T-000876 - VennScience_BFL_Amruta - Used to store the visibility field values for Contact record
    public static final String VISIBILITY_ASSONLY = 'Association Only';
    public static final String VISIBILITY_ORGONLY = 'Organization Only';
    public static final String VISIBILITY_PUBLIC = 'Public';
    
    /**
    * Method Name : filterPersonAccountRecords
    * Parameters  : param1: List<Account>, Map<Id, Account>
    * Description : Used to filter the Person Account records.
    * Created By  : VennScience_BFL_Amruta                             
    * Created On  : 26112019         
    **/
    public static void filterPersonAccountRecords(List<Account> listAccount, Map<Id, Account> mapOldAcc) { 
        System.debug('===Inside filterContactRecords===');
        System.debug('listAccount========='+listAccount);
        System.debug('mapOldAcc========='+mapOldAcc);
        
        // Variable Declarations
        Map<Id, Account> personConIdMapToUpdateUser = new Map<Id, Account>();

         // Iterate over Contact records
        for(Account accRecord : listAccount) { 
             if(!mapOldAcc.isEmpty() 
                && mapOldAcc.containsKey(accRecord.Id) 
                && accRecord.IsPersonAccount && (
                    (accRecord.PersonEmail != null 
                    && accRecord.PersonEmail != mapOldAcc.get(accRecord.Id).PersonEmail)
                    || (accRecord.PersonMobilePhone != null 
                    && accRecord.PersonMobilePhone != mapOldAcc.get(accRecord.Id).PersonMobilePhone)
                    || (accRecord.PersonTitle != null 
                    && accRecord.PersonTitle != mapOldAcc.get(accRecord.Id).PersonTitle)
                    || (accRecord.Phone != null 
                    && accRecord.Phone != mapOldAcc.get(accRecord.Id).Phone)
                    || (accRecord.PhotoUrl != null 
                    && accRecord.PhotoUrl != mapOldAcc.get(accRecord.Id).PhotoUrl)
                    || accRecord.Email_Visibility__pc != mapOldAcc.get(accRecord.Id).Email_Visibility__pc
                    || accRecord.Title_Visibility__pc != mapOldAcc.get(accRecord.Id).Title_Visibility__pc
                    || accRecord.Mobile_Visibility__pc != mapOldAcc.get(accRecord.Id).Mobile_Visibility__pc
                    || accRecord.Phone_Visibility__pc != mapOldAcc.get(accRecord.Id).Phone_Visibility__pc
                    || accRecord.Photo_Visibility_Update__pc != mapOldAcc.get(accRecord.Id).Photo_Visibility_Update__pc
                )
             ) {
                System.debug('===Contact is updated so update related User details==');
                personConIdMapToUpdateUser.put(accRecord.personContactId, accRecord);
            } // End of if

            System.debug('===personConIdMapToUpdateUser===' + personConIdMapToUpdateUser);
        
            if(!personConIdMapToUpdateUser.isEmpty() && personConIdMapToUpdateUser.size() > 0) {
                // update related User's record
                updateUserRecords(personConIdMapToUpdateUser);
            }
        }
    }
    /**
    * Method Name : updateUserRecords
    * Parameters  : param1: Map<Id, Account>
    * Description : Used to update User record related to contact.
    * Created By  : VennScience_BFL_Amruta                             
    * Created On  : 26112019         
    **/
    public static void updateUserRecords(Map<Id, Account> personConIdMapToUpdateUser) { 
        System.debug('===Inside updateUserRecords===' + personConIdMapToUpdateUser);
        List<User> userListToUpdate = new List<User>();
        Boolean isUserUpdate = false;
        
        for(User userRecord : [ SELECT Id,
                                       ContactId,
                                       MobilePhone,
                                       SenderEmail,
                                       Title,
                                       Phone,
                                       BannerPhotoUrl,
                                       UserPreferencesShowEmailToGuestUsers,
                                       UserPreferencesShowEmailToExternalUsers,
                                       UserPreferencesShowTitleToGuestUsers,
                                       UserPreferencesShowTitleToExternalUsers,
                                       UserPreferencesShowWorkPhoneToGuestUsers,
                                       UserPreferencesShowWorkPhoneToExternalUsers,
                                       UserPreferencesShowMobilePhoneToGuestUsers,
                                       UserPreferencesShowMobilePhoneToExternalUsers,
                                       UserPreferencesShowProfilePicToGuestUsers
                                  FROM User
                                 WHERE ContactId IN :personConIdMapToUpdateUser.keySet()
                                   AND ContactId != null       
        ]) {
            
            if(personConIdMapToUpdateUser.containsKey(userRecord.ContactId)) {
                Account personAccRecord = personConIdMapToUpdateUser.get(userRecord.ContactId);
                //contactRecord = personConIdMapToUpdateUser.get(userRecord.ContactId);
                if(personAccRecord.PersonMobilePhone != null) {
                    isUserUpdate = true;
                    userRecord.MobilePhone = personAccRecord.PersonMobilePhone;
                }
                
                if(personAccRecord.PersonEmail != null) {
                    isUserUpdate = true; 
                    userRecord.Email = personAccRecord.PersonEmail;
                    System.debug('isUserUpdate===='+isUserUpdate);
                }
                
                if(personAccRecord.PersonTitle != null) {
                    isUserUpdate = true; 
                    userRecord.Title = personAccRecord.PersonTitle;
                }
                
                if(personAccRecord.Phone != null) {
                    isUserUpdate = true; 
                    userRecord.Phone = personAccRecord.Phone;
                }
                
                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Email visibility for User
                if(String.isNotBlank(personAccRecord.Email_Visibility__pc)) {
                    if(personAccRecord.Email_Visibility__pc.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // MEMBERS - Only members have permission to see Email
                        userRecord.UserPreferencesShowEmailToGuestUsers = false;
                        userRecord.UserPreferencesShowEmailToExternalUsers = true;
                    } else if(personAccRecord.Email_Visibility__pc.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecord.UserPreferencesShowEmailToGuestUsers = false;
                        userRecord.UserPreferencesShowEmailToExternalUsers = false;
                    } else if(personAccRecord.Email_Visibility__pc.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecord.UserPreferencesShowEmailToGuestUsers = true;
                        userRecord.UserPreferencesShowEmailToExternalUsers = true;
                    } // End of inner else-if block
                } // End of outer if that is set email visibility block

                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Title visibility for User
                if(String.isNotBlank(personAccRecord.Title_Visibility__pc)) {
                    if(personAccRecord.Title_Visibility__pc.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // MEMBERS - Only members have permission to see Email
                        userRecord.UserPreferencesShowTitleToGuestUsers = false;
                        userRecord.UserPreferencesShowTitleToExternalUsers = true;
                    } else if(personAccRecord.Title_Visibility__pc.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecord.UserPreferencesShowTitleToGuestUsers = false;
                        userRecord.UserPreferencesShowTitleToExternalUsers = false;
                    } else if(personAccRecord.Title_Visibility__pc.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecord.UserPreferencesShowTitleToGuestUsers = true;
                        userRecord.UserPreferencesShowTitleToExternalUsers = true;
                    } // End of inner else-if block
                } // End of outer if that is set title visibility block

                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Phone visibility for User
                if(String.isNotBlank(personAccRecord.Phone_Visibility__pc)) {
                    if(personAccRecord.Phone_Visibility__pc.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // MEMBERS - Only members have permission to see Email
                        userRecord.UserPreferencesShowWorkPhoneToGuestUsers = false;
                        userRecord.UserPreferencesShowWorkPhoneToExternalUsers = true;
                    } else if(personAccRecord.Phone_Visibility__pc.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecord.UserPreferencesShowWorkPhoneToGuestUsers = false;
                        userRecord.UserPreferencesShowWorkPhoneToExternalUsers = false;    
                    } else if(personAccRecord.Phone_Visibility__pc.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecord.UserPreferencesShowWorkPhoneToGuestUsers = true;
                        userRecord.UserPreferencesShowWorkPhoneToExternalUsers = true;   
                    } // End of inner else-if block
                } // End of outer if that is set phone visibility block

                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Mobile visibility for User
                if(String.isNotBlank(personAccRecord.Mobile_Visibility__pc)) {
                    if(personAccRecord.Mobile_Visibility__pc.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // MEMBERS - Only members have permission to see Email
                        userRecord.UserPreferencesShowMobilePhoneToGuestUsers = false;
                        userRecord.UserPreferencesShowMobilePhoneToExternalUsers = true;
                    } else if(personAccRecord.Mobile_Visibility__pc.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecord.UserPreferencesShowMobilePhoneToGuestUsers = false;
                        userRecord.UserPreferencesShowMobilePhoneToExternalUsers = false;
                    } else if(personAccRecord.Mobile_Visibility__pc.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecord.UserPreferencesShowMobilePhoneToGuestUsers = true;
                        userRecord.UserPreferencesShowMobilePhoneToExternalUsers = true;  
                    } // End of inner else-if block
                } // End of outer if that is set Mobile visibility block

                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Profile Picture visibility for User
                if(String.isNotBlank(personAccRecord.Photo_Visibility_Update__pc)) {
                    if(personAccRecord.Photo_Visibility_Update__pc.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecord.UserPreferencesShowProfilePicToGuestUsers = true;   
                    }
                    // 121219 - T-000876 - VennScience_BFL_Amruta - Commented below code as Photo Visibility field has been replaced
                    /*else if(personAccRecord.Photo_Visibility_Update__pc.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecord.UserPreferencesShowProfilePicToGuestUsers = false;
                    }*/
                      else if(personAccRecord.Photo_Visibility_Update__pc.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecord.UserPreferencesShowProfilePicToGuestUsers = false;
                    } // End of inner else-if block
                } // End of outer if that is set Photo visibility block

                /*if(personAccRecord.PhotoUrl != null) {
                    isUserUpdate = true; 
                    userRecord.SmallPhotoUrl= personAccRecord.PhotoUrl;
                }*/
                
                if(isUserUpdate){
                    System.debug('Is User Updated====');
                    userListToUpdate.add(userRecord);
                }                   
            }
        }
        System.debug('===userListToUpdate==' + userListToUpdate);
        if(userListToUpdate.size() > 0) {
            try {
                System.debug('Is userListToUpdate===='+userListToUpdate);
                isAccountTriggerRunned = true;
                UserTriggerHandler.isUserTriggerRunned = true;
                update userListToUpdate;
                System.debug('===SUCCESSFULLY UPDATED USERLIST===' + userListToUpdate);
            } catch(Exception e ) {
                System.debug('debug==='+e.getMessage());
                System.debug('===ERROR Occured while updating User List===' + userListToUpdate);
            }
        }
    }
}