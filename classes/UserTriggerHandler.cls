/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy              Description
*     1.0        011119         VennScience_BFL_Amruta                                         This is the handler class for UserTrigger apex trigger.
*     2.0                                               251119         VennScience_BFL_Amruta  Added the functionality of sharing the Working Group record
*                                                                                              with related Group Member whenever related contact's User 
*                                                                                              is inserted/updated
**********************************************************************************************************************************************************/
public class UserTriggerHandler {
    // Variable Declarations
    public static boolean isUserTriggerRunned = false;
    public static final String MEMBERSTATUSACTIVE = 'Active';
    // 261119 - T-000876 - VennScience_BFL_Amruta - Used to store the visibility field values for Contact record
    public static final String VISIBILITY_ASSONLY = 'Association Only';
    public static final String VISIBILITY_ORGONLY = 'Organization Only';
    public static final String VISIBILITY_PUBLIC = 'Public';
    
    /**
    * Method Name : filterUserRecords
    * Parameters  : param1: List<User>
    * Description : Used to filter the user records.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 011119
    **/
    public static void filterUserRecords(List<User> listUsers, Map<Id,User> userOldMap) {
        // Variable Declarations
        List<Group_Member__c> listGrpMember = new List<Group_Member__c>();
        Set<Id> setContactId = new Set<Id>();
        Set<Id> setActivedUserConId = new Set<Id>();
        Set<Id> setDeactivedUserConId = new Set<Id>();
        Set<Id> setWorkingGrpId = new Set<Id>();
        Set<Id> setConId = new Set<Id>();
        Map<Id, List<Id>> workingGrpVsGrpMemberToRemoveMap = new Map<Id, List<Id>>();
        
        System.debug('Inside filterUserRecords');
        // Iterate over new version of User records
        for(User userRecord : listUsers) {
            // V2.0 - 251119 - T-000932 - VennScience_BFL_Amruta - Check if this is an insert event
            if(userOldMap.isEmpty()) {
                if(userRecord.ContactId != null && userRecord.isActive) {
                    // Populate set of Contact Id
                    setContactId.add(userRecord.ContactId);
                    // V2.0 - 251119 - T-000932 - VennScience_BFL_Amruta - Populate set of Contact Id
                    setActivedUserConId.add(userRecord.ContactId);
                } // End of inner if
            } // End of outer if
            // V2.0 - 251119 - T-000932 - VennScience_BFL_Amruta - Check if this is an update event
            if(!userOldMap.isEmpty() && userOldMap.containsKey(userRecord.Id)) {
                // Check if user has been activated
                if(!userOldMap.get(userRecord.Id).isActive && userRecord.isActive) {
                    setActivedUserConId.add(userRecord.ContactId);       
                } // End of if
                // Check if user has been deactivated
                if(userOldMap.get(userRecord.Id).isActive && !userRecord.isActive) {
                    setDeactivedUserConId.add(userRecord.ContactId);          
                } // End of if
            } // End of outer if
        } // End of for
        
        // V2.0 - 251119 - T-000932 - VennScience_BFL_Amruta - Fetch the group members related to Active user's contact
        for(Group_Member__c grpMemRecord : [SELECT Id,
                                                   Working_Group__c
                                              FROM Group_Member__c
                                             WHERE Grou_Member__c IN :setActivedUserConId
                                               AND Status__c = :MEMBERSTATUSACTIVE]) {
            setWorkingGrpId.add(grpMemRecord.Working_Group__c);                                           
        } // End of for

        // V2.0 - 251119 - T-000932 - VennScience_BFL_Amruta - Fetch the group members related to Deactive user's contact
        for(Group_Member__c grpMemRecord : [SELECT Id,
                                                   Working_Group__c,
                                                   Grou_Member__c
                                              FROM Group_Member__c
                                             WHERE Grou_Member__c IN :setDeactivedUserConId
                                               AND Status__c = :MEMBERSTATUSACTIVE]) {
            setConId.add(grpMemRecord.Grou_Member__c);
            if(!workingGrpVsGrpMemberToRemoveMap.containsKey(grpMemRecord.Working_Group__c)) {
                workingGrpVsGrpMemberToRemoveMap.put(grpMemRecord.Working_Group__c, 
                                                     new List<Id>{grpMemRecord.Grou_Member__c});
            } else {
                List<Id> grpMemberList = new List<Id>();
                grpMemberList = workingGrpVsGrpMemberToRemoveMap.get(grpMemRecord.Working_Group__c);
                grpMemberList.add(grpMemRecord.Grou_Member__c);
                workingGrpVsGrpMemberToRemoveMap.put(grpMemRecord.Working_Group__c, grpMemberList);
            } // End of if-else block                                          
        } // End of for
        //System.debug('workingGrpVsGrpMemberToRemoveMap======='+workingGrpVsGrpMemberToRemoveMap);

        // Pass contactId set to method
        if(!setContactId.isEmpty()) {
            addUserToChatterGroup(setContactId);
        } // End of if   

        // V2.0 - 251119 - T-000932 - VennScience_BFL_Amruta - Call Group Member handler in order to share the Working group records
        // with inserted/updated users
        if(!setWorkingGrpId.isEmpty()) {
            
            addWorkingGroupShareRowsForUsers(setWorkingGrpId);
            //GroupMemberTriggerHandler.createWorkingGrpShare(setWorkingGrpId);
        } // End of if
        // V2.0 - 251119 - T-000932 - VennScience_BFL_Amruta - Convert map of Working Group VS Group Member Id list to JSON string
        String mapJSON = JSON.serialize(workingGrpVsGrpMemberToRemoveMap);
        // V2.0 - 251119 - T-000932 - VennScience_BFL_Amruta - Call Group Member handler in order to remove the sharing of Working group
        // records from deactivated users
        if(!workingGrpVsGrpMemberToRemoveMap.isEmpty() && !setConId.isEmpty()) {
            
            //GroupMemberTriggerHandler.removeWorkingGroupShare(workingGrpVsGrpMemberToRemoveMap, setConId);
            if(String.isNotBlank(mapJSON) && !setConId.isEmpty()) {
                removeWorkingGroupShareRowsForUsers(mapJSON, setConId);
            }
        } // End of if
    }
    /**
    * Method Name : addWorkingGroupShareRows
    * Parameters  : param1: Set<Id>
    * Description : Used to add the newly created/updated user to working group's chatter group.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 291119
    **/
    @future
    public static void addWorkingGroupShareRowsForUsers(Set<Id> setWorkingGroupId) {
        //System.debug('Inside addWorkingGroupShareRows');
        // Call method in order to add working Group share rows for users
        GroupMemberTriggerHandler.createWorkingGrpShare(setWorkingGroupId);
    }
    /**
    * Method Name : removeWorkingGroupShareRowsForUsers
    * Parameters  : param1: String, param2: Set<Id>
    * Description : Used to add the newly created/updated user to working group's chatter group.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 291119
    **/
    @future
    public static void removeWorkingGroupShareRowsForUsers(String mapJSONString, Set<Id> setContactId) {
        //System.debug('mapJSONString===='+mapJSONString);
        Map<Id,List<Id>> mapWorkingGroupIdVSGroupMemberIdList = new Map<Id,List<Id>>();

        mapWorkingGroupIdVSGroupMemberIdList = (Map<Id,List<Id>>) JSON.deserialize(mapJSONString, Map<Id,List<Id>>.class);
        //System.debug('mapWorkingGroupIdVSGroupMemberIdList========'+mapWorkingGroupIdVSGroupMemberIdList);
        GroupMemberTriggerHandler.removeWorkingGroupShare(mapWorkingGroupIdVSGroupMemberIdList, setContactId);   
    }
    /**
    * Method Name : addUserToChatterGroup
    * Parameters  : param1: Set<Id>
    * Description : Used to add the newly created/updated user to working group's chatter group.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 011119
    **/
    @future
    public static void addUserToChatterGroup(Set<Id> setContactId) {
        // Variable Declarations
        List<Group_Member__c> listGrpMember = new List<Group_Member__c>();
        
        System.debug('Inside addUserToChatterGroup');
        // Fetch group member records
        listGrpMember = [SELECT Id,
                                Grou_Member__c,
                                Status__c,
                                Working_Group__c
                           FROM Group_Member__c
                          WHERE Grou_Member__c IN :setContactId];
        System.debug('listGrpMember========'+listGrpMember);
        // Call method in order to add the user in Working Group's Chatter Group
        GroupMemberTriggerHandler.filterGroupMemberRecords(listGrpMember, new Map<Id,Group_Member__c>());
    }
    
    /**
    * Method Name : filterUserRecordToUpdateContact
    * Parameters  : param1: List<User> , Map<Id, User>
    * Description : Used to filter the user records to upate contact.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 21112019
    **/
    public static void filterUserRecordToUpdateContact(List<User> listUsers, Map<Id, User> mapOldUser) { 
        Map<Id, User> contactVsUserMap = new Map<Id, User>();
        
        for(User userRecord : listUsers) { 
            if(userRecord.ContactId != null 
            && mapOldUser.containsKey(userRecord.Id)
            &&  ( userRecord.MobilePhone != mapOldUser.get(userRecord.Id).MobilePhone 
            || userRecord.Email != mapOldUser.get(userRecord.Id).Email 
            || userRecord.Title != mapOldUser.get(userRecord.Id).Title 
            || userRecord.Phone != mapOldUser.get(userRecord.Id).Phone
            || userRecord.FirstName != mapOldUser.get(userRecord.Id).FirstName
            || userRecord.LastName != mapOldUser.get(userRecord.Id).LastName
            || userRecord.UserPreferencesShowTitleToExternalUsers != mapOldUser.get(userRecord.Id).UserPreferencesShowTitleToExternalUsers
            || userRecord.UserPreferencesShowTitleToGuestUsers != mapOldUser.get(userRecord.Id).UserPreferencesShowTitleToGuestUsers
            || userRecord.UserPreferencesShowEmailToExternalUsers != mapOldUser.get(userRecord.Id).UserPreferencesShowEmailToExternalUsers
            || userRecord.UserPreferencesShowEmailToGuestUsers != mapOldUser.get(userRecord.Id).UserPreferencesShowEmailToGuestUsers
            || userRecord.UserPreferencesShowMobilePhoneToExternalUsers != mapOldUser.get(userRecord.Id).UserPreferencesShowMobilePhoneToExternalUsers
            || userRecord.UserPreferencesShowMobilePhoneToGuestUsers != mapOldUser.get(userRecord.Id).UserPreferencesShowMobilePhoneToGuestUsers
            || userRecord.UserPreferencesShowWorkPhoneToExternalUsers != mapOldUser.get(userRecord.Id).UserPreferencesShowWorkPhoneToExternalUsers
            || userRecord.UserPreferencesShowWorkPhoneToGuestUsers != mapOldUser.get(userRecord.Id).UserPreferencesShowWorkPhoneToGuestUsers
            || userRecord.UserPreferencesShowProfilePicToGuestUsers != mapOldUser.get(userRecord.Id).UserPreferencesShowProfilePicToGuestUsers 
            )
            ) {
                contactVsUserMap.put(userRecord.ContactId, userRecord);
            }
        }
        System.debug('===contactVsUserMap==' + contactVsUserMap );
        if(contactVsUserMap.size() > 0) {
            updateContactRecord(contactVsUserMap);
        }
    }
    
    /**
    * Method Name : updateContactRecord
    * Parameters  : param1: Map<User, Contact>
    * Description : Used to update related contact from User 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 21112019
    **/
    public static void updateContactRecord(Map<Id, User> contactVsUserMap) { 
        System.debug('===updateContactRecord==' );
        List<Contact> contactListToUpdate = new List<Contact>();
        
        for(Contact contactRecord : [ SELECT Id,
                                             MobilePhone,
                                             FirstName,
                                             LastName,
                                             Email,
                                             Title,
                                             Phone,
                                             PhotoUrl,
                                             Email_Visibility__c,
                                             Mobile_Visibility__c,
                                             Phone_Visibility__c,
                                             Photo_Visibility_Update__c,
                                             Title_Visibility__c
                                        FROM Contact 
                                       WHERE Id IN :contactVsUserMap.keySet()
        ]) {  
            User userRecord = new User();
            userRecord = contactVsUserMap.get(contactRecord.Id);
            contactRecord.MobilePhone = userRecord.MobilePhone;
            contactRecord.Email = userRecord.Email;
            contactRecord.Title = userRecord.Title;    
            contactRecord.Phone = userRecord.Phone;
            // 261119 - T-000876 - VennScience_BFL_Amruta - Sync the modified FirstName and LastName to contact
            contactRecord.FirstName = userRecord.FirstName;
            contactRecord.LastName = userRecord.LastName;
            // 261119 - T-000876 - VennScience_BFL_Amruta - Sync the email visibility field on contact based on the updated values on user
            if(String.isNotBlank(contactRecord.Email_Visibility__c)) {
                if(userRecord.UserPreferencesShowEmailToExternalUsers && !userRecord.UserPreferencesShowEmailToGuestUsers &&
                   !contactRecord.Email_Visibility__c.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                    contactRecord.Email_Visibility__c = VISIBILITY_ASSONLY;
                }  else if(!userRecord.UserPreferencesShowEmailToExternalUsers && !userRecord.UserPreferencesShowEmailToGuestUsers &&
                        !contactRecord.Email_Visibility__c.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                    contactRecord.Email_Visibility__c = VISIBILITY_ORGONLY;
                } else if(userRecord.UserPreferencesShowEmailToExternalUsers && userRecord.UserPreferencesShowEmailToGuestUsers &&
                        !contactRecord.Email_Visibility__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                    contactRecord.Email_Visibility__c = VISIBILITY_PUBLIC;          
                }
            } else {
                if(userRecord.UserPreferencesShowEmailToExternalUsers && !userRecord.UserPreferencesShowEmailToGuestUsers) {
                    contactRecord.Email_Visibility__c = VISIBILITY_ASSONLY;
                }  else if(!userRecord.UserPreferencesShowEmailToExternalUsers && !userRecord.UserPreferencesShowEmailToGuestUsers) {
                    contactRecord.Email_Visibility__c = VISIBILITY_ORGONLY;
                } else if(userRecord.UserPreferencesShowEmailToExternalUsers && userRecord.UserPreferencesShowEmailToGuestUsers) {
                    contactRecord.Email_Visibility__c = VISIBILITY_PUBLIC;          
                }
            } // End of outer if-else block
            
            // End of logic to sync the email visibility on contact

            // 261119 - T-000876 - VennScience_BFL_Amruta - Sync the title visibility field on contact based on the updated values on user
            if(String.isNotBlank(contactRecord.Title_Visibility__c)) {
                if(userRecord.UserPreferencesShowTitleToExternalUsers && !userRecord.UserPreferencesShowTitleToGuestUsers &&
                !contactRecord.Title_Visibility__c.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                    contactRecord.Title_Visibility__c = VISIBILITY_ASSONLY;
                }  else if(!userRecord.UserPreferencesShowTitleToExternalUsers && !userRecord.UserPreferencesShowTitleToGuestUsers &&
                        !contactRecord.Title_Visibility__c.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                    contactRecord.Title_Visibility__c = VISIBILITY_ORGONLY;
                } else if(userRecord.UserPreferencesShowTitleToExternalUsers && userRecord.UserPreferencesShowTitleToGuestUsers &&
                        !contactRecord.Title_Visibility__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                    contactRecord.Title_Visibility__c = VISIBILITY_PUBLIC;          
                }
            } else{
                if(userRecord.UserPreferencesShowTitleToExternalUsers && !userRecord.UserPreferencesShowTitleToGuestUsers) {
                    contactRecord.Title_Visibility__c = VISIBILITY_ASSONLY;
                }  else if(!userRecord.UserPreferencesShowTitleToExternalUsers && !userRecord.UserPreferencesShowTitleToGuestUsers) {
                    contactRecord.Title_Visibility__c = VISIBILITY_ORGONLY;
                } else if(userRecord.UserPreferencesShowTitleToExternalUsers && userRecord.UserPreferencesShowTitleToGuestUsers) {
                    contactRecord.Title_Visibility__c = VISIBILITY_PUBLIC;          
                }
            } // End of outer if-else block
            
            // End of logic to sync the title visibility on contact

            // 261119 - T-000876 - VennScience_BFL_Amruta - Sync the mobile visibility field on contact based on the updated values on user
            if(String.isNotBlank(contactRecord.Mobile_Visibility__c)) {
                if(userRecord.UserPreferencesShowMobilePhoneToExternalUsers && !userRecord.UserPreferencesShowMobilePhoneToGuestUsers &&
                !contactRecord.Mobile_Visibility__c.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                    contactRecord.Mobile_Visibility__c = VISIBILITY_ASSONLY;
                }  else if(!userRecord.UserPreferencesShowMobilePhoneToExternalUsers && !userRecord.UserPreferencesShowMobilePhoneToGuestUsers &&
                        !contactRecord.Mobile_Visibility__c.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                    contactRecord.Mobile_Visibility__c = VISIBILITY_ORGONLY;
                } else if(userRecord.UserPreferencesShowMobilePhoneToExternalUsers && userRecord.UserPreferencesShowMobilePhoneToGuestUsers &&
                        !contactRecord.Mobile_Visibility__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                    contactRecord.Mobile_Visibility__c = VISIBILITY_PUBLIC;          
                }
            } else {
                if(userRecord.UserPreferencesShowMobilePhoneToExternalUsers && !userRecord.UserPreferencesShowMobilePhoneToGuestUsers) {
                    contactRecord.Mobile_Visibility__c = VISIBILITY_ASSONLY;
                }  else if(!userRecord.UserPreferencesShowMobilePhoneToExternalUsers && !userRecord.UserPreferencesShowMobilePhoneToGuestUsers) {
                    contactRecord.Mobile_Visibility__c = VISIBILITY_ORGONLY;
                } else if(userRecord.UserPreferencesShowMobilePhoneToExternalUsers && userRecord.UserPreferencesShowMobilePhoneToGuestUsers) {
                    contactRecord.Mobile_Visibility__c = VISIBILITY_PUBLIC;          
                }
            } // End of outer if-else block
            
            // End of logic to sync the mobile visibility on contact

            // 261119 - T-000876 - VennScience_BFL_Amruta - Sync the phone visibility field on contact based on the updated values on user
            if(String.isNotBlank(contactRecord.Phone_Visibility__c)) {
                if(userRecord.UserPreferencesShowWorkPhoneToExternalUsers && !userRecord.UserPreferencesShowWorkPhoneToGuestUsers &&
                !contactRecord.Phone_Visibility__c.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                    contactRecord.Phone_Visibility__c = VISIBILITY_ASSONLY;
                }  else if(!userRecord.UserPreferencesShowWorkPhoneToExternalUsers && !userRecord.UserPreferencesShowWorkPhoneToGuestUsers &&
                        !contactRecord.Phone_Visibility__c.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                    contactRecord.Phone_Visibility__c = VISIBILITY_ORGONLY;
                } else if(userRecord.UserPreferencesShowWorkPhoneToExternalUsers && userRecord.UserPreferencesShowWorkPhoneToGuestUsers &&
                        !contactRecord.Phone_Visibility__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                    contactRecord.Phone_Visibility__c = VISIBILITY_PUBLIC;          
                }
            } else {
                if(userRecord.UserPreferencesShowWorkPhoneToExternalUsers && !userRecord.UserPreferencesShowWorkPhoneToGuestUsers) {
                    contactRecord.Phone_Visibility__c = VISIBILITY_ASSONLY;
                }  else if(!userRecord.UserPreferencesShowWorkPhoneToExternalUsers && !userRecord.UserPreferencesShowWorkPhoneToGuestUsers) {
                    contactRecord.Phone_Visibility__c = VISIBILITY_ORGONLY;
                } else if(userRecord.UserPreferencesShowWorkPhoneToExternalUsers && userRecord.UserPreferencesShowWorkPhoneToGuestUsers) {
                    contactRecord.Phone_Visibility__c = VISIBILITY_PUBLIC;          
                }
            } // End of outer if-else block
            
            // End of logic to sync the phone visibility on contact

            // 261119 - T-000876 - VennScience_BFL_Amruta - Sync the photo visibility field on contact based on the updated values on user
            if(String.isNotBlank(contactRecord.Photo_Visibility_Update__c)) {
                if(userRecord.UserPreferencesShowProfilePicToGuestUsers &&
                !contactRecord.Photo_Visibility_Update__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                    contactRecord.Photo_Visibility_Update__c = VISIBILITY_PUBLIC;
                } else if(!userRecord.UserPreferencesShowProfilePicToGuestUsers &&
                !contactRecord.Photo_Visibility_Update__c.equalsIgnoreCase(VISIBILITY_ASSONLY )) {
                    // 101219 - T-000876 - VennScience_BFL_Amruta - Set contact's visibility to Association Only
                    contactRecord.Photo_Visibility_Update__c = VISIBILITY_ASSONLY;
                } // End of if-else block
                
            } else {
                if(userRecord.UserPreferencesShowProfilePicToGuestUsers) {
                    contactRecord.Photo_Visibility_Update__c = VISIBILITY_PUBLIC;
                } else if(!userRecord.UserPreferencesShowProfilePicToGuestUsers) {
                    // 101219 - T-000876 - VennScience_BFL_Amruta - Set contact's visibility to Association Only
                    contactRecord.Photo_Visibility_Update__c = VISIBILITY_ASSONLY;
                }
            }// End of outer if-else block
             
            // End of logic to sync the photo visibility on contact

            //contactRecord.PhotoUrl = userRecord.MediumPhotoUrl;    
            contactListToUpdate.add(contactRecord);   
        }
        System.debug('===contactListToUpdate==' + contactListToUpdate);
        
        if(contactListToUpdate.size() > 0) {
            try {
                AccountTriggerHandler.isAccountTriggerRunned = true;
                isUserTriggerRunned = true;
                update contactListToUpdate;
                System.debug('===SUCCESSFULLY UPDATED contactListToUpdate===' + contactListToUpdate);
            } catch(Exception e ) {
                System.debug('===ERROR Occured while updating contactListToUpdate List===' + contactListToUpdate);
            }
        } 
    }
    
    /**
    * Method Name : FilterUserToupdateFromContact
    * Parameters  : param1: Map<Id,List<User>>
    * Description : Used to pull values from related contact and update User 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 21112019
    **/
    public static void FilterUserToUpdateFromContact(List<User> listUsers) { 
        Map<Id,List<User>> contactIdVsUsersMap = new Map<Id,List<User>>();
        String contactIdVsUsersMapJSON = '';
        
        for(User userRecord : listUsers) { 
            if(userRecord.ContactId != null) {
                if(!contactIdVsUsersMap.containsKey(userRecord.ContactId)) {
                    contactIdVsUsersMap.put(userRecord.ContactId, new List<User>{userRecord});
                } else {
                    List<User> userList = new List<User>();
                    userList = contactIdVsUsersMap.get(userRecord.ContactId);
                    userList.add(userRecord);
                    contactIdVsUsersMap.put(userRecord.ContactId, userList);
                }
                
            }
        }
        // 091219 - VennScience_BFL_Amruta - Pass contactIdVsUsersMap as JSON to the future method
        if(contactIdVsUsersMap.size() > 0) {
        	contactIdVsUsersMapJSON = JSON.serialize(contactIdVsUsersMap);
            System.debug('contactIdVsUsersMapJSON========='+contactIdVsUsersMapJSON);
    	}
        if(String.isNotBlank(contactIdVsUsersMapJSON)) {
            updateUserFromContactRecord(contactIdVsUsersMapJSON);
        }
    }
    
    /**
    * Method Name : updateUserFromContactRecord
    * Parameters  : param1: Map<Id,List<User>>
    * Description : Used to pull values from related contact and update User 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 21112019
    **/
    @future
    public static void updateUserFromContactRecord(String contactIdVsUsersMapJSON) {
        // 091219 - VennScience_BFL_Amruta - Added future annotation so as to resolve the Mixed DML Exception contactIdVsUsersMap
        Map<Id,List<User>> contactIdVsUsersMap = new Map<Id,List<User>>();
        System.debug('===updateUserFromContactRecord==' );
        System.debug('===contactIdVsUsersMapJSON=='+contactIdVsUsersMapJSON);
        List<User> userListToUpdate = new List<User>();
        
        // 091219 - VennScience_BFL_Amruta - Deserialize JSON string to map
        contactIdVsUsersMap = (Map<Id,List<User>>) JSON.deserialize(contactIdVsUsersMapJSON, Map<Id,List<User>>.class);
        System.debug('contactIdVsUsersMap fater deserialization=========='+contactIdVsUsersMap);
        
        for(Contact contactRecord : [ SELECT Id,
                                             MobilePhone,
                                             Email,
                                             Title,
                                             Phone,
                                             PhotoUrl,
                                             Email_Visibility__c,
                                             Mobile_Visibility__c,
                                             Phone_Visibility__c,
                                             Photo_Visibility_Update__c,
                                             Title_Visibility__c
                                        FROM Contact 
                                       WHERE Id IN :contactIdVsUsersMap.keySet()
        ]) { 
            for(User userRecord : contactIdVsUsersMap.get(contactRecord.Id)) {
                User userRecordToUpdate = new User(Id = userRecord.Id);
                userRecordToUpdate.MobilePhone = contactRecord.MobilePhone;
                // 261119 - T-000876 - VennScience_BFL_Amruta - Check if contact's email is not blank
                if(String.isNotBlank(contactRecord.Email)) {
                    userRecordToUpdate.Email = contactRecord.Email;
                } // End of if
                userRecordToUpdate.Title = contactRecord.Title;
                userRecordToUpdate.Phone = contactRecord.Phone;
                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Email visibility for User
                if(String.isNotBlank(contactRecord.Email_Visibility__c)) {
                    if(contactRecord.Email_Visibility__c.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // MEMBERS - Only members have permission to see Email
                        userRecordToUpdate.UserPreferencesShowEmailToGuestUsers = false;
                        userRecordToUpdate.UserPreferencesShowEmailToExternalUsers = true;
                    } else if(contactRecord.Email_Visibility__c.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecordToUpdate.UserPreferencesShowEmailToGuestUsers = false;
                        userRecordToUpdate.UserPreferencesShowEmailToExternalUsers = false;
                    } else if(contactRecord.Email_Visibility__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecordToUpdate.UserPreferencesShowEmailToGuestUsers = true;
                        userRecordToUpdate.UserPreferencesShowEmailToExternalUsers = true;
                    } // End of inner else-if block
                } // End of outer if that is set email visibility block

                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Title visibility for User
                if(String.isNotBlank(contactRecord.Title_Visibility__c)) {
                    if(contactRecord.Title_Visibility__c.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // MEMBERS - Only members have permission to see Email
                        userRecordToUpdate.UserPreferencesShowTitleToGuestUsers = false;
                        userRecordToUpdate.UserPreferencesShowTitleToExternalUsers = true;
                    } else if(contactRecord.Title_Visibility__c.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecordToUpdate.UserPreferencesShowTitleToGuestUsers = false;
                        userRecordToUpdate.UserPreferencesShowTitleToExternalUsers = false;
                    } else if(contactRecord.Title_Visibility__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecordToUpdate.UserPreferencesShowTitleToGuestUsers = true;
                        userRecordToUpdate.UserPreferencesShowTitleToExternalUsers = true;
                    } // End of inner else-if block
                } // End of outer if that is set title visibility block

                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Phone visibility for User
                if(String.isNotBlank(contactRecord.Phone_Visibility__c)) {
                    if(contactRecord.Phone_Visibility__c.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // MEMBERS - Only members have permission to see Email
                        userRecordToUpdate.UserPreferencesShowWorkPhoneToGuestUsers = false;
                        userRecordToUpdate.UserPreferencesShowWorkPhoneToExternalUsers = true;
                    } else if(contactRecord.Phone_Visibility__c.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecordToUpdate.UserPreferencesShowWorkPhoneToGuestUsers = false;
                        userRecordToUpdate.UserPreferencesShowWorkPhoneToExternalUsers = false;    
                    } else if(contactRecord.Phone_Visibility__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecordToUpdate.UserPreferencesShowWorkPhoneToGuestUsers = true;
                        userRecordToUpdate.UserPreferencesShowWorkPhoneToExternalUsers = true;   
                    } // End of inner else-if block
                } // End of outer if that is set phone visibility block

                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Mobile visibility for User
                if(String.isNotBlank(contactRecord.Mobile_Visibility__c)) {
                    if(contactRecord.Mobile_Visibility__c.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // MEMBERS - Only members have permission to see Email
                        userRecordToUpdate.UserPreferencesShowMobilePhoneToGuestUsers = false;
                        userRecordToUpdate.UserPreferencesShowMobilePhoneToExternalUsers = true;
                    } else if(contactRecord.Mobile_Visibility__c.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecordToUpdate.UserPreferencesShowMobilePhoneToGuestUsers = false;
                        userRecordToUpdate.UserPreferencesShowMobilePhoneToExternalUsers = false;
                    } else if(contactRecord.Mobile_Visibility__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecordToUpdate.UserPreferencesShowMobilePhoneToGuestUsers = true;
                        userRecordToUpdate.UserPreferencesShowMobilePhoneToExternalUsers = true;  
                    } // End of inner else-if block
                } // End of outer if that is set Mobile visibility block

                // 261119 - T-000876 - VennScience_BFL_Amruta - Set Profile Picture visibility for User
                if(String.isNotBlank(contactRecord.Photo_Visibility_Update__c)) {
                    if(contactRecord.Photo_Visibility_Update__c.equalsIgnoreCase(VISIBILITY_PUBLIC)) {
                        // PUBLIC - Everyone has permission to see Email
                        userRecordToUpdate.UserPreferencesShowProfilePicToGuestUsers = true;   
                    } 
                    // 121219 - T-000876 - VennScience_BFL_Amruta - Commented below code as Photo Visibility field has been replaced
                    /*else if(contactRecord.Photo_Visibility_Update__c.equalsIgnoreCase(VISIBILITY_ORGONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecordToUpdate.UserPreferencesShowProfilePicToGuestUsers = false;
                    }*/ 
                    else if(contactRecord.Photo_Visibility_Update__c.equalsIgnoreCase(VISIBILITY_ASSONLY)) {
                        // RESTRICTED - Only internal users have permission to see Email
                        userRecordToUpdate.UserPreferencesShowProfilePicToGuestUsers = false;
                    } // End of inner else-if block
                } // End of outer if that is set Photo visibility block

                //userRecord.MediumPhotoUrl = contactRecord.PhotoUrl;
                userListToUpdate.add(userRecordToUpdate);
            }
        }
        System.debug('===userListToUpdate==' + userListToUpdate);
        if(userListToUpdate.size() > 0) {
            try {
                isUserTriggerRunned = true;
                AccountTriggerHandler.isAccountTriggerRunned = true;
                update userListToUpdate;
                System.debug('===SUCCESSFULLY UPDATED USERLIST===' + userListToUpdate);
            } catch(Exception e ) {
                System.debug('===ERROR Occured while updating User List===' + userListToUpdate);
            }
        } 
    }
    
}