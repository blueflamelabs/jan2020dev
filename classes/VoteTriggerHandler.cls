/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy  Description
*     1.0        041119         VennScience_BFL_Amruta                             This is the handler class for VoteTrigger apex trigger.                               
**********************************************************************************************************************************************************/
public class VoteTriggerHandler {
    // Variable Declarations
    public static final String VOTESTATUS = 'Activate';
    public static final String GRPMEMBERSTATUS = 'Active';
    public static final String GRPMEMBERVOTINGLEVEL = 'None';
    public static final String INDIVIDUALMEMBER = 'IndividualAccount';
    public static final String CORPORATEREP = 'Corporate_Representative';
    public static final String MEMBERCATEGORYPRIMARY = 'Primary Contact';
    public static final String MEMBERCATEGORYALTERNATE = 'Alternate Contact';
    public static Boolean isRecursiveCall = false;
    
    /**
    * Method Name : filterVoteRecords
    * Parameters  : param1: List<Vote__c>, Map<Id,Vote__c>
    * Description : Used to filter the Vote records as per the required criteria.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 041119
    **/
    public static void filterVoteRecords(List<Vote__c> listNewVote, Map<Id,Vote__c> mapOldVote) {
        // Variable Declarations
        List<Vote__c> listVoteForWorkingGrp = new List<Vote__c>();
        List<Vote__c> listVoteForAssociation = new List<Vote__c>();
        
        System.debug('Inside filterVoteRecords');
        // Iterate over Vote records
        for(Vote__c voteRecord : listNewVote) {
            // Check if Vote status has been updated and is set to 'Activate' and Working Group is not null
            if(!mapOldVote.isEmpty() && mapOldVote.containsKey(voteRecord.Id) &&
                mapOldVote.get(voteRecord.Id).Status__c != voteRecord.Status__c &&
                VOTESTATUS.equalsIgnoreCase(voteRecord.Status__c) && voteRecord.Working_Group__c != null) {
                System.debug('Vote status has been set to Activate');
                // Populate list of Vote
                listVoteForWorkingGrp.add(voteRecord);
            } // End of if
            // Check if Vote status has been updated and is set to 'Activate' and Working Group is null
            if(!mapOldVote.isEmpty() && mapOldVote.containsKey(voteRecord.Id) &&
                mapOldVote.get(voteRecord.Id).Status__c != voteRecord.Status__c &&
                VOTESTATUS.equalsIgnoreCase(voteRecord.Status__c) && voteRecord.Working_Group__c == null &&
                voteRecord.Association__c != null) {
                System.debug('Vote status has been set to Activate');
                // Populate list of Vote
                listVoteForAssociation.add(voteRecord);
            } // End of if
        } // End of for
        
        // Call method to create Ballot records for Vote's related Working Group
        if(!listVoteForWorkingGrp.isEmpty()) {
            createBallotsForWorkingGroup(listVoteForWorkingGrp);
        } // End of if
        
        // Call method to create Ballot records for Vote's related Association
        if(!listVoteForAssociation.isEmpty()) {
            createBallotsForAssociation(listVoteForAssociation);
        } // End of if
    }
    /**
    * Method Name : createBallotsForWorkingGroup
    * Parameters  : param1: List<Vote__c>
    * Description : Used to create Ballot records for Vote's related Working Group.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 041119
    **/
    public static void createBallotsForWorkingGroup(List<Vote__c> listVote) {
        // Variable Declarations
        List<Ballot__c> listBallotsToInsert = new List<Ballot__c>();
        List<Vote__c> listVoteToUpdate = new List<Vote__c>();
        Set<Id> setPersonContactId = new Set<Id>();
        Set<Id> setSuccesfullBallotId = new Set<Id>();
        Set<Vote__c> setVoteToUpdate = new Set<Vote__c>();
        //Set<Id> setVoteToBeUpdated = new Set<Id>();
        Map<Id,Set<Id>> mapWorkingGrpIdVSVoteIdSet = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapWorkingGrpIdVSGrpMemberIdSet = new Map<Id,Set<Id>>();
        Map<Id,Id> mapPersonConIdVSAccountId = new Map<Id,Id>();
        Map<Id,Set<Id>> mapVoteIdVSContactIdSet = new Map<Id,Set<Id>>();
        
        //20112019
        map<Id, List<Ballot__c>> mapVoteVsExitingBallot = new map<Id, List<Ballot__c>>();
        List<Ballot__c> ballotList = new List<Ballot__c>();
        
        System.debug('Inside createBallotsForWorkingGroup');
        // Iterate over Vote records
        for(Vote__c voteRecord : listVote) {
            // Populate map of Working Group Id VS set of Vote Id
            if(!mapWorkingGrpIdVSVoteIdSet.containsKey(voteRecord.Working_Group__c)) {
                mapWorkingGrpIdVSVoteIdSet.put(voteRecord.Working_Group__c, new Set<Id>{voteRecord.Id});    
            } else {
                Set<Id> setPreviousVoteId = new Set<Id>();
                setPreviousVoteId = mapWorkingGrpIdVSVoteIdSet.get(voteRecord.Working_Group__c);
                setPreviousVoteId.add(voteRecord.Id);
                mapWorkingGrpIdVSVoteIdSet.put(voteRecord.Working_Group__c, setPreviousVoteId);  
            } // End of if-else block           
        } // End of for
        System.debug('mapWorkingGrpIdVSVoteIdSet======'+mapWorkingGrpIdVSVoteIdSet);
        
        // Iterate over Working Group and fetch child Group Members
        for(Working_Group__c workingGrpRecord : [SELECT Id,
                                                        (SELECT Id,
                                                                Grou_Member__c
                                                           FROM Group_Members__r
                                                          WHERE Status__c = :GRPMEMBERSTATUS
                                                            AND Voting_Level__c != :GRPMEMBERVOTINGLEVEL
                                                            AND Grou_Member__c != null)
                                                   FROM Working_Group__c
                                                  WHERE Id IN :mapWorkingGrpIdVSVoteIdSet.keySet()]) {
            // Check if current Working Group has child Group Members
            for(Group_Member__c grpMemberRecord : workingGrpRecord.Group_Members__r) {
                // Populate set of Person Contact Id
                setPersonContactId.add(grpMemberRecord.Grou_Member__c);
                // Populate map of Working Group Id VS set of group member's related contact Id
                if(!mapWorkingGrpIdVSGrpMemberIdSet.containsKey(workingGrpRecord.Id)) {
                    mapWorkingGrpIdVSGrpMemberIdSet.put(workingGrpRecord.Id, new Set<Id>{grpMemberRecord.Grou_Member__c});                                              
                } else {
                    Set<Id> setPreviousContactId = new Set<Id>();
                    setPreviousContactId = mapWorkingGrpIdVSGrpMemberIdSet.get(workingGrpRecord.Id);
                    setPreviousContactId.add(grpMemberRecord.Grou_Member__c);
                    mapWorkingGrpIdVSGrpMemberIdSet.put(workingGrpRecord.Id, setPreviousContactId);
                } // End of if-else block                                                 
            } // End of for
        } // End of for
        System.debug('mapWorkingGrpIdVSGrpMemberIdSet========='+mapWorkingGrpIdVSGrpMemberIdSet);
        
        // Iterate over mapWorkingGrpIdVSVoteIdSet map
        for(Id workingGrpId : mapWorkingGrpIdVSVoteIdSet.keySet()) {
            for(Id voteId : mapWorkingGrpIdVSVoteIdSet.get(workingGrpId)) {
                // Populate map of Vote Id VS set of related person contact Id
                if(!mapWorkingGrpIdVSGrpMemberIdSet.isEmpty() && mapWorkingGrpIdVSGrpMemberIdSet.containsKey(workingGrpId)) {
                    mapVoteIdVSContactIdSet.put(voteId, mapWorkingGrpIdVSGrpMemberIdSet.get(workingGrpId));        
                } // End of if 
            } // End of inner for
        } // End of outer for
        System.debug('mapVoteIdVSContactIdSet======='+mapVoteIdVSContactIdSet);
        
        // Fetch Account records and get the related Account Id for each PersonContact populated on Group Member records
        for(Account accRecord : [SELECT Id,
                                        PersonContactId
                                   FROM Account
                                  WHERE PersonContactId IN :setPersonContactId
                                    AND (RecordType.DeveloperName = :INDIVIDUALMEMBER
                                     OR RecordType.DeveloperName = :CORPORATEREP)]) {
            // Populate map of PersonContactId VS Account Id
            mapPersonConIdVSAccountId.put(accRecord.PersonContactId, accRecord.Id);    
        } // End of for
        System.debug('mapPersonConIdVSAccountId========='+mapPersonConIdVSAccountId);
        
        // SOQL on Ballot__c and get related ballots to check of exiting ballots exist for related working group
        for(Ballot__c ballot : [ SELECT Id,
                                        Vote__c,
                                        Association_Member__c 
                                   FROM Ballot__c 
                                  WHERE Vote__c IN :mapVoteIdVSContactIdSet.keySet()        
        ]){
            // Check if exiting Ballot record is there for related Association
            for(Id personConId : mapVoteIdVSContactIdSet.get(ballot.Vote__c)) {
                if(mapPersonConIdVSAccountId.get(personConId) == ballot.Association_Member__c){
                    mapPersonConIdVSAccountId.remove(personConId);
                }
                
            }
        }  
        
        // Iterate over map of Working Grp Id VS Set of related Contact Id
        for(Id voteId : mapVoteIdVSContactIdSet.keySet()) {
           
            for(Id personConId : mapVoteIdVSContactIdSet.get(voteId)) {
                // Check if current Person Contact Id is present in mapPersonConIdVSAccountId map
                if(!mapPersonConIdVSAccountId.isEmpty() && mapPersonConIdVSAccountId.containsKey(personConId)) {
                    // Create Ballot records
                    Ballot__c ballotRecord = new Ballot__c();
                    ballotRecord.Vote__c = voteId;
                    ballotRecord.Association_Member__c = mapPersonConIdVSAccountId.get(personConId);
                    // Populate list of Ballot records to be inserted
                    listBallotsToInsert.add(ballotRecord);
                } // End of if
            } // End of inner for 
        } // End of outer for
        
        // Insert Ballot records
        Database.SaveResult[] srList = Database.insert(listBallotsToInsert, false);
        // Iterate over SaveResult
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // Operation was successful, get the ID of the record that was processed
                System.debug('Successfully inserted Ballot. Ballot ID: ' + sr.getId());
                // Populate set of succesfull inserted invoice id 
                setSuccesfullBallotId.add(sr.getId());
            } else {
                // Operation failed, get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Ballot fields that affected this error: ' + err.getFields());
                } // End of inner for
            } // End of else-if block
        } // End of outer for
        System.debug('setSuccesfullBallotId======'+setSuccesfullBallotId);
        
        // Fetch Successfully inserted Ballot records
        for(Ballot__c ballotRec : [SELECT Vote__c
                                     FROM Ballot__c
                                    WHERE Id IN :setSuccesfullBallotId]) {
            // Populate set of 
            //setVoteToBeUpdated.add(ballotRec.Vote__c);
            // Update vote status to 'Active'
            Vote__c voteRecordToUpdate = new Vote__c(Id = ballotRec.Vote__c);
            // @Reminder: move this status value to constant
            voteRecordToUpdate.Status__c = 'Active';
            setVoteToUpdate.add(voteRecordToUpdate);
        } // End of for
        System.debug('setVoteToUpdate========'+setVoteToUpdate);
        
         // Update Vote status
        if(!setVoteToUpdate.isEmpty()) {
            // Add set elements to list
            listVoteToUpdate.addAll(setVoteToUpdate);
            System.debug('listVoteToUpdate========'+listVoteToUpdate);
            try {
                isRecursiveCall = true;
                update listVoteToUpdate;
            } catch(Exception e) {
                System.debug('Error occurred while updating the Vote records:'+e.getMessage());
            } // End of try-catch block    
        } // End of if
    }
    /**
    * Method Name : createBallotsForAssociation
    * Parameters  : param1: List<Vote__c>
    * Description : Used to create Ballot records for Vote's related Association.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 041119
    **/
    public static void createBallotsForAssociation(List<Vote__c> listVote) {
        // Variable Declarations
        List<Ballot__c> listBallotsToInsert = new List<Ballot__c>();
        List<Vote__c> listVoteToUpdate = new List<Vote__c>();
        Set<Vote__c> setVoteToUpdate = new Set<Vote__c>();
        Set<Id> setAccountId = new Set<Id>();
        //Set<Id> setVoteToBeUpdated = new Set<Id>();
        Set<Id> setSuccesfullBallotId = new Set<Id>();
        Map<Id,Set<Id>> mapAssociationIdVSVoteIdSet = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapAssociationIdVSAccountIdSet = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapVoteIdVSAccountIdSet = new Map<Id,Set<Id>>();
        
        //20112019
        map<Id, List<Ballot__c>> mapVoteVsExitingBallot = new map<Id, List<Ballot__c>>();
        List<Ballot__c> ballotList = new List<Ballot__c>();
                
        System.debug('Inside createBallotsForAssociation');
        // Iterate over Vote records
        for(Vote__c voteRecord : listVote) {
            // Populate map of Working Group Id VS set of Vote Id
            if(!mapAssociationIdVSVoteIdSet.containsKey(voteRecord.Association__c)) {
                mapAssociationIdVSVoteIdSet.put(voteRecord.Association__c, new Set<Id>{voteRecord.Id});    
            } else {
                Set<Id> setPreviousVoteId = new Set<Id>();
                setPreviousVoteId = mapAssociationIdVSVoteIdSet.get(voteRecord.Association__c);
                setPreviousVoteId.add(voteRecord.Id);
                mapAssociationIdVSVoteIdSet.put(voteRecord.Association__c, setPreviousVoteId);  
            } // End of if-else block           
        } // End of for
        
         // Iterate over Working Group and fetch child Group Members
        for(Association__c assRecord : [SELECT Id,
                                                      (SELECT Id
                                                         FROM Accounts__r
                                                        WHERE (RecordType.DeveloperName = :INDIVIDUALMEMBER
                                                           OR RecordType.DeveloperName = :CORPORATEREP)
                                                          AND (Member_Category__pc = :MEMBERCATEGORYPRIMARY
                                                           OR  Member_Category__pc = :MEMBERCATEGORYALTERNATE)
                                                          AND Status__c = 'Active')
                                                   FROM Association__c
                                                  WHERE Id IN :mapAssociationIdVSVoteIdSet.keySet()]) {
            // Check if current Working Group has child Group Members
            for(Account accountRecord : assRecord.Accounts__r) {
                // Populate set of Person Contact Id
                setAccountId.add(accountRecord.Id);
               // Populate map of Working Group Id VS set of group member's related contact Id
                if(!mapAssociationIdVSAccountIdSet.containsKey(assRecord.Id)) {
                    mapAssociationIdVSAccountIdSet.put(assRecord.Id, new Set<Id>{accountRecord.Id});                                              
                } else {
                    Set<Id> setPreviousAccId = new Set<Id>();
                    setPreviousAccId = mapAssociationIdVSAccountIdSet.get(assRecord.Id);
                    setPreviousAccId.add(accountRecord.Id);
                    mapAssociationIdVSAccountIdSet.put(assRecord.Id, setPreviousAccId);
                } // End of if-else block                                                 
            } // End of for
        } // End of for
        System.debug('mapAssociationIdVSAccountIdSet========='+mapAssociationIdVSAccountIdSet);
        
        // Iterate over mapAssociationIdVSVoteIdSet map
        for(Id AssociationId : mapAssociationIdVSVoteIdSet.keySet()) {
            for(Id voteId : mapAssociationIdVSVoteIdSet.get(AssociationId)) {
                if(!mapAssociationIdVSAccountIdSet.isEmpty() && mapAssociationIdVSAccountIdSet.containsKey(AssociationId)) {
                    // Populate map of vote Id VS Account Id set
                    mapVoteIdVSAccountIdSet.put(voteId, mapAssociationIdVSAccountIdSet.get(AssociationId));    
                } // End of if 
            } // End of inner for
        } // End of outer for
        
        // SOQL on Ballot__c and get related ballots to check of exiting ballots exist for related working group
        for(Ballot__c ballot : [ SELECT Id,
                                        Vote__c,
                                        Association_Member__c 
                                   FROM Ballot__c 
                                  WHERE Vote__c IN :mapVoteIdVSAccountIdSet.keySet()        
        ]){
            if(!mapVoteVsExitingBallot.containsKey(ballot.Vote__c)) {                       
                mapVoteVsExitingBallot.put(ballot.Vote__c, new List<Ballot__c>{ballot});
            } else {
                ballotList = new List<Ballot__c>();
                ballotList = mapVoteVsExitingBallot.get(ballot.Vote__c);
                ballotList.add(ballot);
                mapVoteVsExitingBallot.put(ballot.Vote__c, ballotList);
            }
        }  
        System.debug('=====mapVoteVsExitingBallot==========' + mapVoteVsExitingBallot);
        
        // Iterate over map of Working Grp Id VS Set of related Contact Id
        for(Id voteId : mapVoteIdVSAccountIdSet.keySet()) {
            // Check if exiting Ballot record is there for related Association
            if(mapVoteVsExitingBallot.containsKey(voteId)) {
                for(Ballot__c ballot : mapVoteVsExitingBallot.get(voteId)) {
                    if(mapVoteIdVSAccountIdSet.get(voteId).contains(ballot.Association_Member__c)) {
                        mapVoteIdVSAccountIdSet.get(voteId).remove(ballot.Association_Member__c);
                    }
                }    
            }
            for(Id accId : mapVoteIdVSAccountIdSet.get(voteId)) {
                // Create Ballot records
                Ballot__c ballotRecord = new Ballot__c();
                ballotRecord.Vote__c = voteId;
                ballotRecord.Association_Member__c = accId;
                // Populate list of Ballot records to be inserted
                listBallotsToInsert.add(ballotRecord);
            } // End of inner for 
        } // End of outer for
        
        // Insert Ballot records
        Database.SaveResult[] srList = Database.insert(listBallotsToInsert, false);
        // Iterate over SaveResult
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // Operation was successful, get the ID of the record that was processed
                System.debug('Successfully inserted Ballot. Ballot ID: ' + sr.getId());
                // Populate set of succesfull inserted invoice id 
                setSuccesfullBallotId.add(sr.getId());
            } else {
                // Operation failed, get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Ballot fields that affected this error: ' + err.getFields());
                } // End of inner for
            } // End of else-if block
      } // End of outer for
        System.debug('setSuccesfullBallotId======'+setSuccesfullBallotId);
        
        // Fetch Successfully inserted Ballot records
        for(Ballot__c ballotRec : [SELECT Vote__c
                                     FROM Ballot__c
                                    WHERE Id IN :setSuccesfullBallotId]) {
            // Populate set of 
            //setVoteToBeUpdated.add(ballotRec.Vote__c);
            // Update vote status to 'Active'
            Vote__c voteRecordToUpdate = new Vote__c(Id = ballotRec.Vote__c);
            // @Reminder: move this status value to constant
            voteRecordToUpdate.Status__c = 'Active';
            setVoteToUpdate.add(voteRecordToUpdate);
        } // End of for
        System.debug('setVoteToUpdate========'+setVoteToUpdate);
        
        // Update Vote status
        if(!setVoteToUpdate.isEmpty()) {
            // Add set elements to list
            listVoteToUpdate.addAll(setVoteToUpdate);
            System.debug('listVoteToUpdate========'+listVoteToUpdate);
            try {
                isRecursiveCall = true;
                update listVoteToUpdate;
            } catch(Exception e) {
                System.debug('Error occurred while updating the Vote records:'+e.getMessage());
            } // End of try-catch block    
        } // End of if
    }
}