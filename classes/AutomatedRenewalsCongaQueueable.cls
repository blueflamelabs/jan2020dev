/********************************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                 ModifiedDate  ModifiedBy  Description
*      1.0       221019         VennScience_BFL_Amruta    Used to make Conga Callout in order to send Renewal/Expiration alerts.                                                     
*********************************************************************************************************************************************************************/ 
public class AutomatedRenewalsCongaQueueable implements Queueable, Database.AllowsCallouts {
    
    // Variable Declarations
    public Map<Id,Id> mapAccountIdVSInvoiceId = new Map<Id,Id>();
    public Map<Id,Id> mapAccIdVSBillingContactId = new Map<Id,Id>();
    public Map<Id,Id> mapAccountIdVSCongaTemId = new Map<Id,Id>();
    public Map<Id,Id> mapAssIdVSOrgWideAddId = new Map<Id,Id>();
    public Map<Id,String> mapPersonConIdVSTaskSub = new Map<Id,String>();
    public Map<Id,Id> mapPersonConIdVSAssAccId = new Map<Id,Id>();
    public Set<Id> setDefaultSendAddAssId = new Set<Id>();
    public Id defaultSenderAdd;
    public String congaWrapperJSONString = '';
    
    // Default constructor
    public AutomatedRenewalsCongaQueueable() {
    }
    // Parameterized Constructor
    public AutomatedRenewalsCongaQueueable(Map<Id,Id> mapAccountIdVSInvoiceId, Map<Id,Id> mapAccIdVSBillingContactId,
                                           Map<Id,Id> mapAccountIdVSCongaTemId, Map<Id,Id> mapAssIdVSOrgWideAddId,
                                           Map<Id,String> mapPersonConIdVSTaskSub, Map<Id,Id> mapPersonConIdVSAssAccId,
                                           Set<Id> setDefaultSendAddAssId, Id defaultSenderAdd,
                                           String congaWrapperJSONString) {
        this.mapAccountIdVSInvoiceId = mapAccountIdVSInvoiceId;
        this.mapAccIdVSBillingContactId = mapAccIdVSBillingContactId;
        this.mapAccountIdVSCongaTemId = mapAccountIdVSCongaTemId;
        this.mapAssIdVSOrgWideAddId = mapAssIdVSOrgWideAddId;
        this.mapPersonConIdVSTaskSub = mapPersonConIdVSTaskSub;
        this.mapPersonConIdVSAssAccId = mapPersonConIdVSAssAccId;
        this.setDefaultSendAddAssId = setDefaultSendAddAssId;
        this.defaultSenderAdd = defaultSenderAdd;
        this.congaWrapperJSONString = congaWrapperJSONString;
    }
    // Interface method
    public void execute(QueueableContext context) {
        System.debug('Inside Queueable');
        System.debug('mapAccountIdVSInvoiceId======'+mapAccountIdVSInvoiceId);
        System.debug('mapAccIdVSBillingContactId======'+mapAccIdVSBillingContactId);
        System.debug('mapAccountIdVSCongaTemId======'+mapAccountIdVSCongaTemId);
        System.debug('mapAssIdVSOrgWideAddId======'+mapAssIdVSOrgWideAddId);
        System.debug('congaWrapperJSONString======'+congaWrapperJSONString);
        // Variable Declarations
        AutomatedRenewalsCongaBatchHandler.CongaCalloutWrapper congaWrapperRecord = (AutomatedRenewalsCongaBatchHandler.CongaCalloutWrapper)JSON.deserialize(
                                                                                     congaWrapperJSONString, AutomatedRenewalsCongaBatchHandler.CongaCalloutWrapper.class);
        List<Invoice__c> listInvoicesToBeSent1 = new List<Invoice__c>();
        List<Account> listAccForSendingAlerts = new List<Account>();
        Set<Id> setSuccesfulAssId = new Set<Id>();
        Set<Id> setPersonContactId = new Set<Id>();
        Map<Id,String> MapInoiveIdVSInvoiceSendURL1 = new Map<Id,String>();
        Map<Id,Id> mapPersonConIdVSAssOwnerId = new Map<Id,Id>();
        Id getInvoiceId1;
        Id getTemplateId1;
        String getInvoiceURL1 = '';
        String getBaseURL1 = 'https://composer.congamerge.com/composer8/index.html'+
                               '?sessionId='+UserInfo.getSessionId()+
                               '&serverUrl='+EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm()+
                               '/services/Soap/u/29.0/'+UserInfo.getOrganizationId(), 'UTF-8');
        // Populate list of Account
        listAccForSendingAlerts = congaWrapperRecord.listAccToSendEmail;
        System.debug('listAccForSendingAlerts======'+listAccForSendingAlerts);
        System.debug('Inside makeCongaCalloutToSendAlerts');
        // Added Fetch Invoice records
        listInvoicesToBeSent1 = [SELECT Id,
                                       Send_Invoice_URL__c
                                  FROM Invoice__c
                                 WHERE Id IN :mapAccountIdVSInvoiceId.values()];
        System.debug('listInvoicesToBeSent======='+listInvoicesToBeSent1);
        for(Invoice__c invoiceRec : listInvoicesToBeSent1) {
            // Populate map of Invoice Id VS Send Invoice URL value
            MapInoiveIdVSInvoiceSendURL1.put(invoiceRec.Id, invoiceRec.Send_Invoice_URL__c);                                                      
        } // End of for
                                                        
         // Iterate over Account records to whom renewal/expiration email needs to be sent
         for(Account accRecToSendEmail : listAccForSendingAlerts) {
            getInvoiceId1 = null;
            getTemplateId1 = null;
            getInvoiceURL1 = getBaseURL1;
          // Fetch the Invoice Id for current acount record
            if(mapAccountIdVSInvoiceId.containsKey(accRecToSendEmail.Id)) {
                getInvoiceId1 = mapAccountIdVSInvoiceId.get(accRecToSendEmail.Id);
            }
            System.debug('Invoice id that needs to be sent as attachment==='+getInvoiceId1);
            // Fetch Conga Template Id for current account record
            if(mapAccountIdVSCongaTemId.containsKey(accRecToSendEmail.Id)) {
                getTemplateId1 = mapAccountIdVSCongaTemId.get(accRecToSendEmail.Id);
            }
            System.debug('Conga Template that needs to be used==='+getTemplateId1);
            System.debug('Email To id===='+mapAccIdVSBillingContactId.get(accRecToSendEmail.Id));
            System.debug('Email additional to===='+accRecToSendEmail.PersonContactId);
            System.debug('Email From Id===='+mapAssIdVSOrgWideAddId.get(accRecToSendEmail.Association__c));
            // Fetch Invoice Send URL for current account record
            if(getInvoiceId1 != null && MapInoiveIdVSInvoiceSendURL1.containsKey(getInvoiceId1) &&
               mapAccIdVSBillingContactId.containsKey(accRecToSendEmail.Id) &&
               getTemplateId1 != null && mapAssIdVSOrgWideAddId.containsKey(accRecToSendEmail.Association__c) &&
               mapAccIdVSBillingContactId.containsKey(accRecToSendEmail.Id)) {
              getInvoiceURL1 = getInvoiceURL1+''+MapInoiveIdVSInvoiceSendURL1.get(getInvoiceId1)+
                                //'&EmailToId='+mapAccIdVSBillingContactId.get(accRecToSendEmail.Id)+
                                //'&EmailAdditionalTo='+accRecToSendEmail.PersonContactId+
                                '&LGWhoId='+mapAccIdVSBillingContactId.get(accRecToSendEmail.Id)+
                                '&EmailToId='+mapAccIdVSBillingContactId.get(accRecToSendEmail.Id)+
                                '&EmailAdditionalTo='+accRecToSendEmail.PersonContactId+
                                '&EmailFromId='+mapAssIdVSOrgWideAddId.get(accRecToSendEmail.Association__c)+
                                '&CongaEmailTemplateId='+getTemplateId1;
            } // End of outer if
            System.debug('Invoice Send URL that needs to be used==='+getInvoiceURL1);
            // Make callout to Conga in order to send the Renewal/Expiration emails with related Invoice as attachment
            if(getInvoiceId1 != null && getTemplateId1 != null && String.isNotBlank(getInvoiceURL1)) {
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint(getInvoiceURL1);
                req.setMethod('GET');
                req.setTimeout(60000);
                
                // Send the request, and return a response
                HttpResponse res = http.send(req);
            
                System.debug(res);
                System.debug('Callout response:::::'+res.getStatus() + ' => ' + res.getBody());
                System.debug('get error======'+res.getBody());
                if(res.getStatusCode() == 200) {
                    System.debug('Callout made successfully:::::'+res.getStatus() + ' => ' + res.getBody());
                    System.debug('get body======'+res.getBody());
                    // Populate set of Association Id for which emails have been sent
                    setSuccesfulAssId.add(accRecToSendEmail.Association__c);
                    // Populate PersonContact Id set for which emails have been sent
                    setPersonContactId.add(accRecToSendEmail.PersonContactId);
                    // Populate Map of PersonConId VS AssociationOwnerId
                    mapPersonConIdVSAssOwnerId.put(accRecToSendEmail.PersonContactId, accRecToSendEmail.Association__r.OwnerId);
                    System.debug('mapPersonConIdVSAssOwnerId========='+mapPersonConIdVSAssOwnerId);
                } // End of if
            } // End of null check if
        } // End of for
        
        // Call method in order to send alerts to Admin and Association Owner if Association's sender add is blank or unverified
        sendErrorAlertToAdmins(setDefaultSendAddAssId, setSuccesfulAssId, defaultSenderAdd, false);
        
        // Call method in order to create Task records for the successfully sent emails
        createTaskForSuccededEmails(setPersonContactId, mapPersonConIdVSAssOwnerId);
    }
    /**
    * Method Name : sendErrorAlertToAdmins
    * Parameters  : 
    * Description : Used to send email to Admin users whenever the sender's email address on Association is not
    *               verified or it is blank.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 221019
    **/
    public void sendErrorAlertToAdmins(Set<Id> setTargetObjId, Set<Id> setSuccessfulEmailAssId, Id defaultSenderAddId,
                                       Boolean isOnBoardingEmail) {
        // Variable Declarations
        List<Association__c> listAssociation = new List<Association__c>();
        List<Messaging.SingleEmailMessage> mailList =  new List<Messaging.SingleEmailMessage>();
        List<User> listAdminUsers = new List<User>();
        List<String> sendToList = new List<String>();
        Set<String> setSendToAddresses = new Set<String>();
        Set<String> setAssociationName = new Set<String>();
        
        // Fetch the Association records
        listAssociation = [SELECT Id,
                                  Name,
                                  Owner.Email,
                                  Association_Full_Name__c
                             FROM Association__c
                            WHERE Id IN :setTargetObjId];
        // Iterate on Account list to get the parent Association's record owner
        for(Association__c objAss : listAssociation) {
            if(setSuccessfulEmailAssId.contains(objAss.Id)) {
               setSendToAddresses.add(objAss.Owner.Email);
               setAssociationName.add(objAss.Association_Full_Name__c); 
            } // End of if  
        } // End of for
        
        // Fetch all Admin users
        if(!setSuccessfulEmailAssId.isEmpty()) {
            listAdminUsers = [SELECT Id,
                                 Email
                            FROM User
                           WHERE Profile.Name = 'System Administrator'];
            System.debug('listAdminUsers=========='+listAdminUsers);
        } // End of if
        for(User objUser : listAdminUsers) {
            setSendToAddresses.add(objUser.Email);     
        } // End of for
        sendToList.addAll(setSendToAddresses);
        System.debug('setSendToAddresses==========='+setSendToAddresses);
        System.debug('sendToList==========='+sendToList);
        // Create mail list to send failure alert email
        Messaging.SingleEmailMessage failureAlertMail =  new Messaging.SingleEmailMessage();
        if(!sendToList.isEmpty()) {

            failureAlertMail.setSubject(mailSubject(isOnBoardingEmail));
            failureAlertMail.setHtmlBody(mailBody(setAssociationName, isOnBoardingEmail));
            failureAlertMail.setToAddresses(sendToList);
            failureAlertMail.setOrgWideEmailAddressId(defaultSenderAddId);
            // Add your email to the master list
            mailList.add(failureAlertMail); 
        } // End of if
        System.debug('mailList======='+mailList);
        if(!mailList.isEmpty()) {
            System.debug('====Inside send email alert to Admins====');
            try {
                Messaging.sendEmail(mailList);
                System.debug('Failure email sent');
            } catch(Exception e) {
                System.debug('Error occurred while sending failure alert email:'+e.getMessage());
            } // End of try-catch block
        } // End of if loop
    }
    /**
    * Method Name : createTaskForSuccededEmails
    * Parameters  : param1: Set<Id>, param2: Map<Id,Id>
    * Description : Used to create Task records for those alerts which were sent successfully.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 221019
    **/
    public void createTaskForSuccededEmails(Set<Id> setPersonConId, Map<Id,Id> mapPersonContactIdVSAssOwnerId) {
        // Variable Declarations
        List<Task> listTasksToBeInserted = new List<Task>();
        
        System.debug('Inside createTaskForSuccededEmails');
        System.debug('mapPersonConIdVSTaskSub======'+mapPersonConIdVSTaskSub);
        System.debug('mapPersonConIdVSAssAccId======'+mapPersonConIdVSAssAccId);
        System.debug('setPersonConId======'+setPersonConId);
        System.debug('mapPersonContactIdVSAssOwnerId======'+mapPersonContactIdVSAssOwnerId);
        
        // Iterate over PersonContactId set in order to create Task records
         for(Id contactId : setPersonConId) {
            Task taskRecord = new Task();
            taskRecord.WhoId = contactId;
            taskRecord.ActivityDate = System.today();
            taskRecord.Status = 'Completed';
            taskRecord.TaskSubtype = 'Email';
            if(!mapPersonConIdVSAssAccId.isEmpty() && mapPersonConIdVSAssAccId.containsKey(contactId)) {
                taskRecord.WhatId = mapPersonConIdVSAssAccId.get(contactId);    
            } // End of if
            if(!mapPersonConIdVSTaskSub.isEmpty() && mapPersonConIdVSTaskSub.containsKey(contactId)) {
                taskRecord.Subject = mapPersonConIdVSTaskSub.get(contactId);    
            } // End of if
            // T-00519 - 060819 - VennScience_BFL_Amruta - Add Association's owner as Task owner
            if(!mapPersonContactIdVSAssOwnerId.isEmpty() && mapPersonContactIdVSAssOwnerId.containsKey(contactId)) {
                taskRecord.OwnerId = mapPersonContactIdVSAssOwnerId.get(contactId);    
            } // End of if
            listTasksToBeInserted.add(taskRecord);
        } // End of for
        System.debug('listTasksToBeInserted============'+listTasksToBeInserted);
        try {
            insert listTasksToBeInserted; 
            // System.debug('Tasks created');
        } catch(Exception e) {
            System.debug('Error occurred while inserting Task record:'+e.getMessage());
        }
    }
    /**
    * Method Name : mailSubject
    * Parameters  : 
    * Description : This method is used to built email Subject
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 221019
    **/
    public static String mailSubject(Boolean isOnBoardingMail) {
        String subject = '';
        // 101219 - T-000934 - VennScience_BFL_Amruta - Set subject dynamically
        if(isOnBoardingMail) {
            subject = 'OnBoarding Alert was sent using default Sender Address';
        } else {
            subject = 'Renewal/Expiration Alert was sent using default Sender Address';
        } // End of if-else block
        return subject;
    }
    /**
    * Method Name : mailBody
    * Parameters  : param1: Set<String>
    * Description : This method is used to built email Body
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 221019
    **/
    public static String mailBody(Set<String> setRelatedAssociationName, Boolean isOnBoardingMail) {
        
        String body = '';
        Integer serialNo = 1;
        // 101219 - T-000934 - VennScience_BFL_Amruta - Set body dynamically
        if(isOnBoardingMail) {
            body = 'The OnBoarding Email was sent using the default sender email address as the Association\'s Sender Email Address is not verified yet. '
               +'Please find below the Association records for which the Sender Email Address is not verified/not present/is blank in the organization-wide address:'
               + '<br/>' + '<br/>';
        } else {
            body = 'The Expiration/Renewal Email was sent using the default sender email address as the Association\'s Sender Email Address is not verified yet. '
               +'Please find below the Association records for which the Sender Email Address is not verified/not present in the organization-wide address or is blank on Association record:'
               + '<br/>' + '<br/>';
        }
        
        for(String strAssociationName : setRelatedAssociationName) {
            if(serialNo <= setRelatedAssociationName.size()) {
                body+= serialNo+'. '+strAssociationName+ '<br/>' + '<br/>';
            }
            serialNo++;
        } // End of for
        return body;
    }
}