/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy               Description
*     1.0        231219         VennScience_BFL_Amruta                                          This is the controller class for folder lightning 
*																								component. 
*
**********************************************************************************************************************************************************/
public without sharing Class FolderController {
	
    /**
    * Method Name : getFileFolderWrapper
    * Description : Used to get the related folders, sub-folders and files for current parent object.
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 231219
    **/
    @AuraEnabled
    public static List<FolderContainerWrapper> getFileFolderWrapper(Id parentRecordId, Integer recordLimit,
                                                                    Integer recordOffset) {
        // Variable Declarations
		List<FileFolderWrapper> listFileFolderWrapper = new List<FileFolderWrapper>();
        Map<Id, List<FileFolderWrapper>> mapFolderIdVSFileFolderWrapperList = new Map<Id, List<FileFolderWrapper>>();
        Map<Id, Boolean> mapFolderIdVSIsParentFolder = new Map<Id, Boolean>();
        Map<Id,String> mapFolderIdVsFolderName = new Map<Id,String>();
        List<FolderContainerWrapper> listFolderContainerWrapper = new List<FolderContainerWrapper>();
        
        System.debug('Inside getFileFolderWrapper');
		System.debug('parentRecordId====='+parentRecordId);
		System.debug('recordLimit===='+recordLimit);
        System.debug('recordOffset===='+recordOffset);
                                                                        
		// Fetch related folders and files with current parent object
        for(Folder__c folderRecord : [SELECT Id,
                                             Name,
                                      		 Parent_Folder__c,
                                             (SELECT Id,
                                                     Title,
                                                     ContentDocumentId
                                                FROM Files__r),
                                             (SELECT Id,
                                                     Name
                                                FROM Folders__r)
                                      			FROM Folder__c
                                               WHERE Parent_Object_Id__c = :parentRecordId
                                            ORDER BY Name
                                               LIMIT :recordLimit OFFSET :recordOffset]) {
                                               //AND Parent_Folder__c = null]) {
            System.debug('folderRecord===='+folderRecord.Name);
            listFileFolderWrapper = new List<FileFolderWrapper>();
        	// Iterate over sub folder list
            for(Folder__c subFolderRecord : folderRecord.Folders__r) {
            	System.debug('subFolderRecord===='+subFolderRecord.Name);
                listFileFolderWrapper.add(new FileFolderWrapper(true, subFolderRecord, new ContentVersion()));
            } // End of child folder inner for
                                             
            // Iterate over child content versions list
            for(ContentVersion childContentVersionRecord : folderRecord.Files__r) {
            	System.debug('childContentVersionRecord===='+childContentVersionRecord);
                listFileFolderWrapper.add(new FileFolderWrapper(false, new Folder__c(), childContentVersionRecord));
            } // End of child folder inner for
            
            System.debug('folderRecord===='+folderRecord.Name);                                      
            // Populate map of Parent Folder ID vS list of filefolder wrapper list
		    mapFolderIdVSFileFolderWrapperList.put(folderRecord.Id, listFileFolderWrapper);                                    
            
            // Populate map of FolderId VS FolderName
            mapFolderIdVsFolderName.put(folderRecord.Id, folderRecord.Name);
                                                
            // Populate map of folder Id VS parent folder Id
            if(folderRecord.Parent_Folder__c != null) {
                mapFolderIdVSIsParentFolder.put(folderRecord.Id, false);                                       
            } else {
                mapFolderIdVSIsParentFolder.put(folderRecord.Id, true);
            } // End of inner if-else block
        } // End of for 
        System.debug('mapFolderIdVSFileFolderWrapperList========'+mapFolderIdVSFileFolderWrapperList);
        System.debug('mapFolderIdVsFolderName========'+mapFolderIdVsFolderName);
        System.debug('mapFolderIdVSIsParentFolder========'+mapFolderIdVSIsParentFolder);
        
        // Iterate over map of folder id vs folder name map
        for(Id folderId : mapFolderIdVsFolderName.keySet()) {
            if(!mapFolderIdVSFileFolderWrapperList.isEmpty() && 
               mapFolderIdVSFileFolderWrapperList.containsKey(folderId) &&
               !mapFolderIdVSIsParentFolder.isEmpty() &&
               mapFolderIdVSIsParentFolder.containsKey(folderId)) {
            	listFolderContainerWrapper.add(new FolderContainerWrapper(mapFolderIdVsFolderName.get(folderId), 
                                                                          folderId,
                                                                          mapFolderIdVSIsParentFolder.get(folderId),
                                               							  mapFolderIdVSFileFolderWrapperList.get(folderId)));
            } // End of if
        } // End of for
        System.debug('listFolderContainerWrapper========'+listFolderContainerWrapper);
        System.debug('listFolderContainerWrapper size========'+listFolderContainerWrapper.Size());
        for(FolderContainerWrapper wrapperRec : listFolderContainerWrapper) {
            System.debug('Folder Id==='+wrapperRec.folderId);
            System.debug('Folder Name==='+wrapperRec.folderName);
            System.debug('File Folder list==='+wrapperRec.listFolderIdVSFileFolderWrapperList);
        }
        return listFolderContainerWrapper;
    }
    /**
    * Method Name : getFileFolderWrapperCopy
    * Description : Used to get the related folders, sub-folders and files for current parent object.
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 231219
    **/
    @AuraEnabled
    public static List<FolderContainerWrapper> getFileFolderWrapperCopy(Id parentRecordId) {
        // Variable Declarations
		List<FileFolderWrapper> listFileFolderWrapper = new List<FileFolderWrapper>();
        Map<Id, List<FileFolderWrapper>> mapFolderIdVSFileFolderWrapperList = new Map<Id, List<FileFolderWrapper>>();
        Map<Id, Boolean> mapFolderIdVSIsParentFolder = new Map<Id, Boolean>();
        Map<Id,String> mapFolderIdVsFolderName = new Map<Id,String>();
        List<FolderContainerWrapper> listFolderContainerWrapper = new List<FolderContainerWrapper>();
        FileFolderWrapper fileFolderWrapInstance = new FileFolderWrapper();
        
        System.debug('Inside getFileFolderWrapper');
		System.debug('parentRecordId====='+parentRecordId);
                                                                        
		// Fetch related folders and files with current parent object
        for(Folder__c folderRecord : [SELECT Id,
                                             Name,
                                      		 Parent_Folder__c
                                      	FROM Folder__c
                                       WHERE Parent_Object_Id__c = :parentRecordId
                                         //AND Parent_Folder__c = null
                                    ORDER BY Name]) {
            System.debug('folderRecord===='+folderRecord.Name);
            // Populate map of FolderId VS FolderName
            mapFolderIdVsFolderName.put(folderRecord.Id, folderRecord.Name);
            // Populate map of folder Id VS is parent folder Id
            if(folderRecord.Parent_Folder__c != null) {
            	mapFolderIdVSIsParentFolder.put(folderRecord.Id, false);    
            } else {
                mapFolderIdVSIsParentFolder.put(folderRecord.Id, true);
            } // End of if-else block   
        } // End of for 
        System.debug('mapFolderIdVsFolderName========'+mapFolderIdVsFolderName);
        
        // Fetch the sub folders for parent folders
        for(Folder__c subFolderRecord : [SELECT Id,
                                                Name,
                                                Parent_Folder__c
                                           FROM Folder__c
                                          WHERE Parent_Object_Id__c = :parentRecordId
                                            AND Parent_Folder__c IN :mapFolderIdVsFolderName.keySet()]) {
                                                
        	System.debug('sub folderRecord===='+subFolderRecord.Name);
            fileFolderWrapInstance = new FileFolderWrapper(true, subFolderRecord, new ContentVersion());                                    
            if(!mapFolderIdVSFileFolderWrapperList.containsKey(subFolderRecord.Parent_Folder__c)) {
            	mapFolderIdVSFileFolderWrapperList.put(subFolderRecord.Parent_Folder__c, 
                                                       new List<FileFolderWrapper>{fileFolderWrapInstance});                                        
            } else {
                List<FileFolderWrapper> listExistingFileFolderWrapper = new List<FileFolderWrapper>();
                listExistingFileFolderWrapper = mapFolderIdVSFileFolderWrapperList.get(subFolderRecord.Parent_Folder__c);
                listExistingFileFolderWrapper.add(fileFolderWrapInstance);
                mapFolderIdVSFileFolderWrapperList.put(subFolderRecord.Parent_Folder__c, listExistingFileFolderWrapper);
            } // End of if-else block
        } // End of for
                                                                        
        // Fetch the child files for parent folders
        for(ContentVersion contentVersionRecord : [SELECT Id,
                                                	 	  Title,
                                                     	  Folder__c,
                                                	 	  ContentDocumentId
                                           			 FROM ContentVersion
                                          	   		WHERE Folder__c IN :mapFolderIdVsFolderName.keySet()]) {
                                                
        	System.debug('child file Record===='+contentVersionRecord.Title);
            fileFolderWrapInstance = new FileFolderWrapper(false, new Folder__c(), contentVersionRecord);                                    
            if(!mapFolderIdVSFileFolderWrapperList.containsKey(contentVersionRecord.Folder__c)) {
            	mapFolderIdVSFileFolderWrapperList.put(contentVersionRecord.Folder__c, 
                                                       new List<FileFolderWrapper>{fileFolderWrapInstance});                                        
            } else {
                List<FileFolderWrapper> listExistingFileFolderWrapper = new List<FileFolderWrapper>();
                listExistingFileFolderWrapper = mapFolderIdVSFileFolderWrapperList.get(contentVersionRecord.Folder__c);
                listExistingFileFolderWrapper.add(fileFolderWrapInstance);
                mapFolderIdVSFileFolderWrapperList.put(contentVersionRecord.Folder__c, listExistingFileFolderWrapper);
            } // End of if-else block
        } // End of for
        
        // Iterate over map of folder id vs folder name map
        /*
        for(Id folderId : mapFolderIdVSFileFolderWrapperList.keySet()) {
            if(!mapFolderIdVsFolderName.isEmpty() && 
               mapFolderIdVsFolderName.containsKey(folderId)) {
            	listFolderContainerWrapper.add(new FolderContainerWrapper(mapFolderIdVsFolderName.get(folderId), 
                                                                          folderId,
                                                                          mapFolderIdVSIsParentFolder.get(folderId),
                                                             			  mapFolderIdVSFileFolderWrapperList.get(folderId)));         
            } else if(mapFolderIdVsFolderName.isEmpty() ||
                      (!mapFolderIdVsFolderName.isEmpty() && !mapFolderIdVsFolderName.containsKey(folderId))) {
            	listFolderContainerWrapper.add(new FolderContainerWrapper(mapFolderIdVsFolderName.get(folderId), 
                                                                          folderId,
                                                                          mapFolderIdVSIsParentFolder.get(folderId),
                                                             			  mapFolderIdVSFileFolderWrapperList.get(folderId)));
            } // End of if-else block
        } // End of for
        */
        
        // Iterate over map of folder id vs folder name map
        for(Id folderId : mapFolderIdVsFolderName.keySet()) {
            if(!mapFolderIdVSFileFolderWrapperList.isEmpty() && 
                mapFolderIdVSFileFolderWrapperList.containsKey(folderId)) {
            	listFolderContainerWrapper.add(new FolderContainerWrapper(mapFolderIdVsFolderName.get(folderId), 
                                                                          folderId,
                                                                          mapFolderIdVSIsParentFolder.get(folderId),
                                                                          mapFolderIdVSFileFolderWrapperList.get(folderId)));         
            } else if(mapFolderIdVSFileFolderWrapperList.isEmpty() ||
                      (!mapFolderIdVSFileFolderWrapperList.isEmpty() && !mapFolderIdVSFileFolderWrapperList.containsKey(folderId))) {
            	listFolderContainerWrapper.add(new FolderContainerWrapper(mapFolderIdVsFolderName.get(folderId), 
                                                                          folderId,
                                                                          mapFolderIdVSIsParentFolder.get(folderId),
                                                                          new List<FileFolderWrapper>()));
            } // End of inner if-else block
        } // End of for
        System.debug('mapFolderIdVSFileFolderWrapperList========'+mapFolderIdVSFileFolderWrapperList);
        System.debug('listFolderContainerWrapper========'+listFolderContainerWrapper);
        System.debug('listFolderContainerWrapper size========'+listFolderContainerWrapper.Size());
        for(FolderContainerWrapper wrapperRec : listFolderContainerWrapper) {
            System.debug('Folder Id==='+wrapperRec.folderId);
            System.debug('Folder Name==='+wrapperRec.folderName);
            System.debug('File Folder list==='+wrapperRec.listFolderIdVSFileFolderWrapperList);
        }
        return listFolderContainerWrapper;
    }
    /**
    * Method Name : getParentFolderWrapper
    * Description : Used to get the list of root folders list for current parent object.
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 241219
    **/
    @AuraEnabled
    public static ParentFolderWrapper getParentFolderWrapper(Id parentRecordId, Integer recordLimit,
                                                             Integer recordOffset) {
        // Variable Declarations
        ParentFolderWrapper parentFolderWrapperInstance = new ParentFolderWrapper();
        List<ParentFolderWrapper> listParentFolderWrapper = new List<ParentFolderWrapper>();
        List<Folder__c> listRootFolders = new List<Folder__c>();
		
        System.debug('Inside getParentFolderWrapper');
		System.debug('parentRecordId====='+parentRecordId);
		System.debug('recordLimit===='+recordLimit);
        System.debug('recordOffset===='+recordOffset);
                                                                        
		// Fetch related folders and files with current parent object
        for(Folder__c folderRecord : [SELECT Id,
                                             Name,
                                      		 Parent_Folder__c
                                      	FROM Folder__c
                                       WHERE Parent_Object_Id__c = :parentRecordId
                                         AND Parent_Folder__c = null
                                    ORDER BY Name
                                       LIMIT :recordLimit OFFSET :recordOffset]) {
            System.debug('folderRecord===='+folderRecord.Name);
            // Populate map of FolderId VS FolderName
            listRootFolders.add(folderRecord);
            //listParentFolderWrapper.add(new ParentFolderWrapper(folderRecord.Id, folderRecord.Name));
        } // End of for 
        System.debug('listParentFolderWrapper========'+listParentFolderWrapper);
        if(!listRootFolders.isEmpty()) {
        	parentFolderWrapperInstance.listRootFolder = listRootFolders;
            parentFolderWrapperInstance.totalRecords = [SELECT count()
                                                          FROM Folder__c
                                                         WHERE Parent_Object_Id__c = :parentRecordId
                                                           AND Parent_Folder__c = null];
            parentFolderWrapperInstance.message = 'Folder records are loaded';
            parentFolderWrapperInstance.success = true;
        } else {
            parentFolderWrapperInstance.message = 'No result found';
            parentFolderWrapperInstance.success = false;
        }
        return parentFolderWrapperInstance;
    }
    /**
    * Class Name  : FolderContainerWrapper 
    * Description : This is the wrapper class used for storing root folder and its related data.
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 241219
    **/   
    public class ParentFolderWrapper {
        @AuraEnabled public Id folderId {get;set;}
        @AuraEnabled public String folderName {get;set;}
        @AuraEnabled public List<Folder__c> listRootFolder {get;set;}
        @AuraEnabled public Integer totalRecords = 0;
        @AuraEnabled
        public String message{get;set;}
        @AuraEnabled
        public Boolean success{get;set;}
        // Parameterized constructor
        /*public ParentFolderWrapper(Id folderId, String folderName) {
            this.folderId = folderId;
            this.folderName = folderName;
        }*/
    }
    /**
    * Class Name  : FolderContainerWrapper 
    * Description : This is the wrapper class used for storing root folder and its related child data.
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 231219
    **/   
    public class FolderContainerWrapper {
        @AuraEnabled public String folderName {get;set;}
        @AuraEnabled public Id folderId {get;set;}
        @AuraEnabled public Boolean isParentFolder{get;set;}
        @AuraEnabled public List<FileFolderWrapper> listFolderIdVSFileFolderWrapperList {get;set;}
        
        // Parameterized constructor
        public FolderContainerWrapper(String folderName, Id folderId, 
                                      Boolean isParentFolder, List<FileFolderWrapper> listFolderIdVSFileFolderWrapperList) {
            this.folderName = folderName;
            this.folderId = folderId;
            this.isParentFolder = isParentFolder;
            this.listFolderIdVSFileFolderWrapperList = listFolderIdVSFileFolderWrapperList;
        }
    }
    /**
    * Class Name  : ContainerWrapper 
    * Description : This is the wrapper class used for store information related to child content version/folder with parent folder.
    * Created By  : VennScience_BFL_Amruta
    * Created On  : 231219
    **/   
    public class FileFolderWrapper {
        @AuraEnabled public Boolean isFolder {get;set;}
        @AuraEnabled public Folder__c folderRecord {get;set;}
        @AuraEnabled public ContentVersion contentVersionRecord {get;set;}
        
        // Default Constructor
        public FileFolderWrapper() {
            
        }
        // Parameterized constructor
        public FileFolderWrapper(Boolean isFolder, Folder__c folderRecord, ContentVersion contentVersionRecord) {
            this.isFolder = isFolder;
            this.folderRecord = folderRecord;
            this.contentVersionRecord = contentVersionRecord;
        }
    }
}