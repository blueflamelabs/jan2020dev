public without sharing class WorkGroupAssociationCommunityCTRL {
    public class wrapperClass{
        @AuraEnabled public Working_Group__c objWorkGroup{get;set;}
        @AuraEnabled public Boolean isCheck{get;set;}
        @AuraEnabled public String GroupMemStatus{get;set;}
        @AuraEnabled public String RejectionReason{get;set;}
        public wrapperClass(Working_Group__c objWorkGroup,Boolean isCheck,String GroupMemStatus,String RejectionReason){
            this.objWorkGroup = objWorkGroup;
            this.isCheck = isCheck;
            this.GroupMemStatus = GroupMemStatus;
            this.RejectionReason = RejectionReason;
        }
    }
    @AuraEnabled
    public static List<wrapperClass> WorkingGroupsRecord(String RecId) {
        String CommunityUserId = UserInfo.getUserId();
        List<User> objUserList = [Select Id,ContactId from User where Id =:CommunityUserId and ContactId != null];
        String UserId='';
        if(objUserList.size() > 0)
            UserId = objUserList[0].ContactId;
        
        Map<Id,Group_Member__c> objMapWorkGroupIdGroupMem = new Map<Id,Group_Member__c>();
        List<Group_Member__c> objGrouMemList = [Select Id,Working_Group__c,Status__c,Rejection_Reason__c from Group_Member__c where Grou_Member__c =:UserId];
        for(Group_Member__c objGrMemRec : objGrouMemList){
            objMapWorkGroupIdGroupMem.put(objGrMemRec.Working_Group__c, objGrMemRec);
        }    
        
        List<wrapperClass> objWraList = new List<wrapperClass>();
        List<Working_Group__c> objWorGrList = [Select Id,Type__c,Name,Working_Group_Abbreviation__c,Description__c,Association__c
                                               from Working_Group__c where Association__c =:RecId];
        for(Working_Group__c objWrok : objWorGrList){
            if(objMapWorkGroupIdGroupMem.keySet().Contains(objWrok.Id)){
                String Reas = objMapWorkGroupIdGroupMem.get(objWrok.Id).Rejection_Reason__c+'\nIf you have any questions about why you were rejected from this Working Group, please submit a case.';
                objWraList.add(new wrapperClass(objWrok,true,objMapWorkGroupIdGroupMem.get(objWrok.Id).Status__c,Reas));    
            }else{
                objWraList.add(new wrapperClass(objWrok,false,'',''));    
            }
        }
        return objWraList;
    }
    @AuraEnabled
    public static List<wrapperClass> MemberGroupsRecordCreate(String RecId,String SelectedRecId,String StatusName) {
      system.debug('@@@@@@----hhhh'+SelectedRecId); 
        String CommunityUserId = UserInfo.getUserId();
        List<User> objUserList = [Select Id,ContactId from User where Id =:CommunityUserId and ContactId != null];
        String UserId='';
        if(objUserList.size() > 0){
            UserId = objUserList[0].ContactId;
         }   
      List<Group_Member__c> objGrou = [Select Id,Working_Group__c,Status__c,Rejection_Reason__c from Group_Member__c where Grou_Member__c =:UserId and Working_Group__c=:SelectedRecId and Status__c = 'Past Member' limit 1];    
         if(objGrou.size()>0){
            Group_Member__c objGrMem = new Group_Member__c();
            objGrMem.id=objGrou[0].id;
            objGrMem.Status__c = 'Active';
            objGrMem.Start_Date__c = Date.today();
            update objGrMem;            
          }else{
            Group_Member__c objGrMem = new Group_Member__c();
            objGrMem.Working_Group__c = SelectedRecId;
                if(StatusName == 'Request'){
                    objGrMem.Status__c = 'Pending';
                }else{
                    objGrMem.Status__c = 'Active';
                    objGrMem.Start_Date__c = Date.today();
                    objGrMem.Working_Group_Role__c = 'Member';
                }
            objGrMem.Grou_Member__c = UserId;
            insert objGrMem;
           } 
        Map<Id,Group_Member__c> objMapWorkGroupIdGroupMem = new Map<Id,Group_Member__c>();
        List<Group_Member__c> objGrouMemList = [Select Id,Working_Group__c,Status__c,Rejection_Reason__c from Group_Member__c where Grou_Member__c =:UserId ];
        for(Group_Member__c objGrMemRec : objGrouMemList){
            objMapWorkGroupIdGroupMem.put(objGrMemRec.Working_Group__c, objGrMemRec);
        }        
        List<wrapperClass> objWraList = new List<wrapperClass>();
        List<Working_Group__c> objWorGrList = [Select Id,Type__c,Name,Working_Group_Abbreviation__c,Description__c,Association__c
                                               from Working_Group__c where Association__c =:RecId];
        for(Working_Group__c objWrok : objWorGrList){
            if(objMapWorkGroupIdGroupMem.keySet().Contains(objWrok.Id)){
                String Reas = objMapWorkGroupIdGroupMem.get(objWrok.Id).Rejection_Reason__c+'<br/>If you have any questions about why you were rejected from this Working Group, please submit a case.';
                objWraList.add(new wrapperClass(objWrok,true,objMapWorkGroupIdGroupMem.get(objWrok.Id).Status__c,Reas));    
            }else{
                objWraList.add(new wrapperClass(objWrok,false,'',''));    
            }
        }
        return objWraList;
    }
    @AuraEnabled
    public static String getWorkingGroupName(String SelectedRecId) {
      Working_Group__c workGroup=[Select Id,Type__c,Name from Working_Group__c where id=:SelectedRecId];
      return workGroup.Name;
      
    }
   @AuraEnabled
    public static List<wrapperClass> updateGroupMember(String SelectedRecId,String RecId) {
     String CommunityUserId = UserInfo.getUserId();
        List<User> objUserList = [Select Id,ContactId from User where Id =:CommunityUserId and ContactId != null];
        String UserId='';
        if(objUserList.size() > 0)
          UserId = objUserList[0].ContactId;
         Group_Member__c objGrou = [Select Id,Working_Group__c,Status__c,Rejection_Reason__c from Group_Member__c where Grou_Member__c =:UserId and Working_Group__c=:SelectedRecId and Status__c='Active' limit 1];    
         
         Group_Member__c objGrouObj=new Group_Member__c ();
         objGrouObj.id=objGrou.id;
         objGrouObj.Status__c='Past Member';
         objGrouObj.End_Date__c=date.Today();
         update objGrouObj;
         Map<Id,Group_Member__c> objMapWorkGroupIdGroupMem = new Map<Id,Group_Member__c>();
        List<Group_Member__c> objGrouMemList = [Select Id,Working_Group__c,Status__c,Rejection_Reason__c from Group_Member__c where Grou_Member__c =:UserId];
        for(Group_Member__c objGrMemRec : objGrouMemList){
            objMapWorkGroupIdGroupMem.put(objGrMemRec.Working_Group__c, objGrMemRec);
        }        
        List<wrapperClass> objWraList = new List<wrapperClass>();
        List<Working_Group__c> objWorGrList = [Select Id,Type__c,Name,Working_Group_Abbreviation__c,Description__c,Association__c
                                               from Working_Group__c where Association__c =:RecId];
                                               
        for(Working_Group__c objWrok : objWorGrList){
            if(objMapWorkGroupIdGroupMem.keySet().Contains(objWrok.Id)){
                String Reas = objMapWorkGroupIdGroupMem.get(objWrok.Id).Rejection_Reason__c+'<br/>If you have any questions about why you were rejected from this Working Group, please submit a case.';
                objWraList.add(new wrapperClass(objWrok,true,objMapWorkGroupIdGroupMem.get(objWrok.Id).Status__c,Reas));    
            }else{
                objWraList.add(new wrapperClass(objWrok,false,'',''));    
            }
        }
        return objWraList;
         
       
    }
}