/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*       1.0         150619      VennScience_BFL_Monali       This class is built for test utility.
**********************************************************************************************************************************************************/ 
@isTest
public class TestDataFactory {
    public static Account getAcct() {
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Association Member Account').getRecordTypeId();
        
        Account ac = new Account();
        ac.RecordTypeId = recTypeId;
        ac.Name = 'Test Parent' ;
        //ac.Expiration_Date__c = System.today() - 2 ;
        return ac;
    }
    public static Account getchildAcct() {
        
        //230819 - VennScience_BFL_Amruta - Insert Custom Setting
        Individual_Members_Email_Alerts_Setting__c objCustomSetting = new Individual_Members_Email_Alerts_Setting__c();
        objCustomSetting = getCustomSettingRecord();
        //System.debug('Custom Setting in test class========'+objCustomSetting.Account_Record_Type_Name__c);
        //Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member Account').getRecordTypeId();
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
            .get(objCustomSetting.Account_Record_Type_Name__c)
            .getRecordTypeId();
        Account ac = new Account();
        ac.RecordTypeId = recTypeId;
        // ac.Association_Account__c = parentAcountId;
        ac.firstName = 'Test Individual' ;
        ac.LastName = 'Member';
        ac.Expiration_Manual_Extension__c = System.today() - 3;
        ac.Next_Renewal_Date__c = System.today() + 7;
        ac.PersonEmail = 'test@gmail.com';
        ac.Member_Category__pc = 'Primary Contact';
        return ac;
    }
/**
* Method Name : createAssociationRecords
* Parameters  : 
* Description : Used to create Association records
* Created By  : VennScience_BFL_Amruta 
* Created On  : 240719
**/
    public static Association__c createAssociationRecords() {
        List<EmailTemplate> emailTemplateList = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName = 'First_Renewal_Alert'
                                                 OR DeveloperName = 'Second_Renewal_Alert'
                                                 OR DeveloperName = 'Third_Renewal_Alert'
                                                 OR DeveloperName = 'First_Expiration_Alert'
                                                 OR DeveloperName = 'Second_Expiration_Alert'
                                                 OR DeveloperName = 'Final_Termination_Notice'];
        
        Map<String, Id> emailTemplateAPINameVsEmailTemplateIdMap = new Map<String, Id>();
        
        for(EmailTemplate emailTemplateObj : emailTemplateList) {
            
            emailTemplateAPINameVsEmailTemplateIdMap.put(emailTemplateObj.DeveloperName, emailTemplateObj.Id);
        }
        Association__c objAssociation = new Association__c();
        objAssociation.Name = 'Test Association';
        /*
if(!emailTemplateAPINameVsEmailTemplateIdMap.isEmpty()) {
objAssociation.First_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('First_Renewal_Alert');
objAssociation.Second_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Second_Renewal_Alert');
objAssociation.First_Expiration_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('First_Expiration_Alert');
objAssociation.Third_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Third_Renewal_Alert');
objAssociation.Second_Expiration_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Second_Expiration_Alert');
objAssociation.Final_Termination_Notice_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Final_Termination_Notice');    

}*/
        // 12.09.2019 - T-00637 - VennScience_BFL_Amruta - Populate Email Template field with Template's API name
        objAssociation.First_Renewal_Alert_Email_Template__c = 'First_Renewal_Alert';
        objAssociation.Second_Renewal_Alert_Email_Template__c = 'Second_Renewal_Alert';
        objAssociation.First_Expiration_Alert_Email_Template__c = 'First_Expiration_Alert';
        objAssociation.Third_Renewal_Alert_Email_Template__c = 'Third_Renewal_Alert';
        objAssociation.Second_Expiration_Alert_Email_Template__c = 'Second_Expiration_Alert';
        objAssociation.Final_Termination_Notice_Email_Template__c = 'Final_Termination_Notice'; 
        return objAssociation;
    } 
    @isTest
    public static Individual_Members_Email_Alerts_Setting__c getCustomSettingRecord() {
        Individual_Members_Email_Alerts_Setting__c settings = Individual_Members_Email_Alerts_Setting__c.getOrgDefaults();
        settings.SetupOwnerId = UserInfo.getOrganizationId();
        settings.Account_Record_Type_Name__c = 'IndividualAccount';
        settings.Account_Member_Category__c = 'Primary Contact';
        settings.Default_Sender_Address__c = 'vforceadmin@virtualinc.com';
        insert settings;
        return settings;
    }
/**
* Method Name : getTerminationCustomSettingRecord
* Parameters  : 
* Description : Used to create custom setting records for 'Termination Date Update Setting' custom setting
* Created By  : VennScience_BFL_Amruta 
* Created On  : 040919
**/
    @isTest
    public static Termination_Date_Update_Setting__c getTerminationCustomSettingRecord() {
        Termination_Date_Update_Setting__c settings = Termination_Date_Update_Setting__c.getOrgDefaults();
        settings.SetupOwnerId = UserInfo.getOrganizationId();
        settings.Corporate_Member_RecordType_Name__c = 'Association_Account';
        settings.Individual_Member_s_RecordType_Name__c = 'IndividualAccount';
        insert settings;
        return settings;
    }
    
    /**
* Method Name : createBulkAssociationRecords
* Parameters  : 
* Description : Used to create bulk Association records
* Created By  : VennScience_BFL_Amruta 
* Created On  : 040919
**/
    
    public static List<Association__c> createBulkAssociationRecords(Integer recordCount) {
        List<Association__c> listAssociation = new List<Association__c>();
        List<EmailTemplate> emailTemplateList = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName = 'First_Renewal_Alert'
                                                 OR DeveloperName = 'Second_Renewal_Alert'
                                                 OR DeveloperName = 'Third_Renewal_Alert'
                                                 OR DeveloperName = 'First_Expiration_Alert'
                                                 OR DeveloperName = 'Second_Expiration_Alert'
                                                 OR DeveloperName = 'Final_Termination_Notice'];
        
        Map<String, Id> emailTemplateAPINameVsEmailTemplateIdMap = new Map<String, Id>();
        
        for(EmailTemplate emailTemplateObj : emailTemplateList) {
            
            emailTemplateAPINameVsEmailTemplateIdMap.put(emailTemplateObj.DeveloperName, emailTemplateObj.Id);
        }
        for(Integer i=0; i < recordCount; i++) {
            Association__c objAssociation = new Association__c();
            objAssociation.Name = 'Test Association'+i;
            objAssociation.First_Renewal_Alert_Send_Days__c = 5;
            objAssociation.Second_Renewal_Alert_Send_Days__c = 4;
            objAssociation.First_Expiration_Alert_Send_Days__c= 3;
            objAssociation.Email_Sender_Address__c = 'vforceadmin@virtualinc.com';
            /*
if(!emailTemplateAPINameVsEmailTemplateIdMap.isEmpty()) {
objAssociation.First_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('First_Renewal_Alert');
objAssociation.Second_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Second_Renewal_Alert');
objAssociation.First_Expiration_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('First_Expiration_Alert');
objAssociation.Third_Renewal_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Third_Renewal_Alert');
objAssociation.Second_Expiration_Alert_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Second_Expiration_Alert');
objAssociation.Final_Termination_Notice_Email_Template__c = emailTemplateAPINameVsEmailTemplateIdMap.get('Final_Termination_Notice');    
} // End of if
*/
            // 12.09.2019 - T-00637 - VennScience_BFL_Amruta - Populate Email Template field with Template's API name
            objAssociation.First_Renewal_Alert_Email_Template__c = 'First_Renewal_Alert';
            objAssociation.Second_Renewal_Alert_Email_Template__c = 'Second_Renewal_Alert';
            objAssociation.First_Expiration_Alert_Email_Template__c = 'First_Expiration_Alert';
            objAssociation.Third_Renewal_Alert_Email_Template__c = 'Third_Renewal_Alert';
            objAssociation.Second_Expiration_Alert_Email_Template__c = 'Second_Expiration_Alert';
            objAssociation.Final_Termination_Notice_Email_Template__c = 'Final_Termination_Notice';    
            listAssociation.add(objAssociation);
        } // End of for
        return listAssociation;
    } 
    /**
* Method Name : createBulkAccountRecords
* Parameters  : 
* Description : Used to create bulk Association records
* Created By  : VennScience_BFL_Amruta 
* Created On  : 040919
**/
    
    public static List<Account> createBulkAccountRecords(List<Association__c> listParentAssociations){
        
        List<Account> listAccount = new List<Account>();
        Individual_Members_Email_Alerts_Setting__c objCustomSetting = new Individual_Members_Email_Alerts_Setting__c();
        objCustomSetting = getCustomSettingRecord();
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
            .get(objCustomSetting.Account_Record_Type_Name__c)
            .getRecordTypeId();
        for(Integer i=0; i < listParentAssociations.size(); i++) {
            Account ac = new Account();
            ac.RecordTypeId = recTypeId;
            ac.firstName = 'Test Individual' ;
            ac.LastName = 'Member';
            ac.Expiration_Manual_Extension__c = System.today() - 3;
            ac.Next_Renewal_Date__c = System.today() + 7;
            ac.PersonEmail = 'test@gmail.com';
            ac.Member_Category__pc = 'Primary Contact';
            ac.Association__c = listParentAssociations[i].Id;
            listAccount.add(ac);
        }
        return listAccount;
    }
    /**
* Method Name : createContact
* Parameters  : 
* Description : 
* Created By  : VennScience_BFL_Amruta 
* Created On  : 120819
**/
    public static Contact createContact() {
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'Test';
        contactRecord.LastName = 'Contact';
        return contactRecord;
    }  
    
       /**
    * Method Name : createPartnerCommunityUserTest
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 111119
    **/
    public static UserWrapper createPartnerCommunityUserTest1() {
        
        UserWrapper wrap = new UserWrapper();
        // Variable Declarations
        Account accountRecord = new Account();
        User communityUser = new User();
        Contact contactRecord = new Contact();
        
        UserRole userRoleRecord = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        
        insert userRoleRecord;
        System.debug('userRoleRecord for admin -> '+userRoleRecord);
        
        Profile profileRecord = [SELECT Id 
                              FROM Profile 
                             WHERE Name = 'System Administrator'];
        Integer i = 1;
        // Insert Admin User
        User adminUserRecord = new User(
            //UserRoleId = ur.Id,
            ProfileId = profileRecord.Id,
            Username = System.now().millisecond() + 'test_user@gmail.com',
            Alias = 'batman',
            Email='test_user@gmail.com',
            EmailEncodingKey='UTF-8',
            FirstName='TestAdmin',
            LastName='User',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago',
            UserRoleId = userRoleRecord.Id
        );
        insert adminUserRecord;
        wrap.adminUser = adminUserRecord;
        i++;
        System.runAs(adminUserRecord) {
            accountRecord = new Account(name ='Grazitti', OwnerId = adminUserRecord.Id) ;
            insert accountRecord;
            
            contactRecord = new Contact(LastName ='testContact',AccountId = accountRecord.Id);
            insert contactRecord;
            // @Reminder: Move the profile name to custom label
            // Fetch Community Profile Id
            List<Profile> listProfile = new List<Profile>();
            Id partnerCommunityProfileId;
            listProfile = [Select Id 
                            FROM Profile 
                           WHERE Name = 'Jabson Community' 
                           LIMIT 1];
            if(!listProfile.isEmpty()) {
                partnerCommunityProfileId = listProfile[0].Id;
            } else {
                listProfile = [SELECT Id 
                                 FROM Profile
                                WHERE Name LIKE '%Community%'
                               LIMIT 1];
                partnerCommunityProfileId = listProfile[0].Id;
            }
            
            /*UserRole ur = [Select Name, 
                              PortalType, 
                              PortalAccountId 
                       FROM UserRole
                       WHERE PortalType = :'CustomerPortal' 
                       AND DeveloperName = :'BlueFlameLabsCustomerUser'];*/
            UserRole ur = new UserRole(DeveloperName = 'TestCustomRole', Name = 'Test new Role');
            insert ur;
            System.debug('UserRole for community -> '+ur);
            
            //Id partnerCommunityProfileId = [select id from profile where Name = :Label.Partner_Community].Id;
            communityUser = new User(alias = 'test123', email='test_partner@noemail.com',
                                emailencodingkey='UTF-8', lastname='Test Community User', languagelocalekey='en_US',
                                localesidkey='en_US', profileid = partnerCommunityProfileId, country='United States',IsActive =true,
                                ContactId = contactRecord.Id, 
                                timezonesidkey='America/Los_Angeles', username=System.now().millisecond() +'test_partner@noemail.com');
                
            insert communityUser; 
            wrap.communityUser = communityUser;
        } // End of if
        return wrap;
    }
    
    public class UserWrapper {
        
        public User adminUser = new User();
        Public User communityUser = new User();
        Public List<User> listofCommunityUser = new List<User>();
        public UserWrapper() {
            
        }
    }
    
    /**
    * Method Name : getPersonAccount
    * Parameters  : 
    * Description : 
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 111219
    **/
    
    public static Account getPersonAccount() {
 
        //Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member Account').getRecordTypeId();
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
                       .get('Corporate_Representative').getRecordTypeId();
            
        Account ac = new Account();
        ac.RecordTypeId = recTypeId;
        //ac.Association_Account__c = parentAcountId;
        ac.firstName = 'Test Individual';
        ac.LastName = 'Member';
        ac.Expiration_Manual_Extension__c = System.today() - 3;
        ac.Next_Renewal_Date__c = System.today() + 7;
        ac.Member_Category__pc = 'Primary Contact';
        ac.PersonEmail= 'testperson@gmail.com';
        ac.PersonMobilePhone='9876543211';
        ac.PersonTitle='person Testing';
        ac.Phone = '7892345611';
        //ac.PhotoUrl='';
        ac.Email_Visibility__pc ='Association Only';
        ac.Title_Visibility__pc ='Association Only';
        ac.Mobile_Visibility__pc ='Association Only';
        ac.Phone_Visibility__pc ='Association Only';
        ac.Photo_Visibility_Update__pc ='Association Only';
        
        return ac;
        
    }
}