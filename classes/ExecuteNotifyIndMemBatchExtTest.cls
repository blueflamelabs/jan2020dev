/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                  Description
*     1.0        040919         VennScience_BFL_Amruta     This is to test the ExecuteNotifyIndMembersBatchExtension apex class.
**********************************************************************************************************************************************************/
@isTest
public class ExecuteNotifyIndMemBatchExtTest {
	/**
    * Method Name : executeBatchPositiveTest
    * Parameters  : 
    * Description : Used to test the positive scenarios of executeBatch method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static void executeBatchPositiveTest() {
        // Get Account record
        Account accRecord = new Account();
        accRecord = TestDataFactory.getchildAcct();
        // Populate Standard Controller
        ApexPages.StandardController sc = new ApexPages.StandardController(accRecord);
        ExecuteNotifyIndMembersBatchExtension objExtension = new ExecuteNotifyIndMembersBatchExtension(sc);
        // Call Handler method
        PageReference pageReferenceResult = objExtension.executeBatch();
        // Assert: Check if page refernece is not null
        System.assertNotEquals(null, pageReferenceResult,'Page Reference is null');
    }
    /**
    * Method Name : executeBatchNegativeTest
    * Parameters  : 
    * Description : Used to test the negative scenarios of executeBatch method
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static void executeBatchNegativeTest() {
        // Get Account record
        Account accRecord = new Account();
        //accRecord = TestDataFactory.getchildAcct();
        // Populate Standard Controller
        ApexPages.StandardController sc = new ApexPages.StandardController(accRecord);
        ExecuteNotifyIndMembersBatchExtension objExtension = new ExecuteNotifyIndMembersBatchExtension(sc);
        // Call Handler method
        PageReference pageReferenceResult = objExtension.executeBatch();
        PageReference pageReferenceEmpty = new PageReference('/null');
        // Assert: Check if page refernece is not null
        System.assertEquals(pageReferenceEmpty.getUrl(), pageReferenceResult.getUrl(),'Page Reference is not null');
    }
}