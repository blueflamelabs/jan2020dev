/*
Created Date - 14/10/2019
Created By - Manas
Description - it will query Group Members related to the Working Group.
Ticket No - T-000216
TestClass - WorkingGroupCTRL_Test
*/
public without sharing class WorkingGroupCTRL {
    public class wrapperClass{
        @AuraEnabled public Account objAcc{get;set;}
        @AuraEnabled public Boolean isCheck{get;set;}
        public wrapperClass(Account objAcc,Boolean isCheck){
            this.objAcc = objAcc;
            this.isCheck = isCheck;
        }
        @AuraEnabled public Group_Member__c objGrMem{set;get;}
        @AuraEnabled public Boolean isCheckGroup{get;set;}
        @AuraEnabled public Boolean isUserChairORCoChair{get;set;}
        public wrapperClass(Group_Member__c objGrMem,Boolean isCheckGroup,Boolean isUserChairORCoChair){
            this.objGrMem = objGrMem;
            this.isCheckGroup = isCheckGroup;
            this.isUserChairORCoChair = isUserChairORCoChair;
        }
    }
    @AuraEnabled
    public static List<Account> AccountsMemberRecord(String RecId) {
        Id CorporateRepresentativeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
        List<Account> objAccList = [Select Id,LastName,Name,Join_Date__c,Billing_Contact__pc,Association_Account__c,
                                    Member_Category__pc,Legal_Contact__pc,Association_Role__c,PersonEmail,Phone
                                    from Account where Association_Account__c =:RecId and RecordTypeId =: CorporateRepresentativeId];
        System.debug('######objAccList###3 '+objAccList);
        return objAccList;
    }
    @AuraEnabled
    public static List<wrapperClass> getAccounts(String RecId,String SearchText,Boolean isSingleChar,String RecName) {
        List<wrapperClass> objWrapList = new List<wrapperClass>(); 
        Boolean isUserChairORCoChair = true;
        List<User> objUserList = [Select Id,ContactId from User where Id =:UserInfo.getUserId()];
        if(objUserList.size() > 0){
            if(String.isNotBlank(objUserList[0].ContactId)){
                List<Group_Member__c> objGroupMemberList = [Select Id,Name,Corporate_Member__c,Grou_Member__c,Grou_Member__r.Name,Grou_Member__r.Account.PersonEmail,Working_Group__c,Working_Group_Role__c,Voting_Level__c,
                                                            Status__c,Start_Date__c,End_Date__c from Group_Member__c where ((Working_Group__c =:RecId and Status__c ='Active' and Grou_Member__c =:objUserList[0].ContactId) and
                                                                                                                            (Working_Group_Role__c = 'Chair' OR Working_Group_Role__c = 'Co-Chair')) ORDER BY Grou_Member__r.Name ASC];
                
                if(objGroupMemberList.size() == 0)
                    isUserChairORCoChair = false; 
            }                                                                                            	
        }
        if(RecName == 'Group Members'){
            if(String.isBlank(SearchText)){
                List<Group_Member__c> objGroupMember = [Select Id,Name,Corporate_Member__c,Grou_Member__c,Grou_Member__r.Name,Grou_Member__r.Account.PersonEmail,Working_Group__c,Working_Group_Role__c,Voting_Level__c,
                                                        Status__c,Start_Date__c,End_Date__c from Group_Member__c where Working_Group__c =:RecId and Status__c ='Active' ORDER BY Grou_Member__r.Name ASC];
                for(Group_Member__c objGroupMem : objGroupMember){
                    objWrapList.add(new wrapperClass(objGroupMem,false,isUserChairORCoChair));
                }
            }else{
                String SearchTextName='';
                if(isSingleChar == false)
                    SearchTextName = '%'+SearchText+'%';
                else
                    SearchTextName = SearchText+'%';
                
                List<Group_Member__c> objGroupMember = [Select Id,Name,Corporate_Member__c,Grou_Member__c,Grou_Member__r.Name,Grou_Member__r.Account.PersonEmail,Working_Group__c,Working_Group_Role__c,Voting_Level__c,
                                                        Status__c,Start_Date__c,End_Date__c from Group_Member__c where Working_Group__c =:RecId and Status__c ='Active' and Grou_Member__r.Name LIKE: SearchTextName];
                for(Group_Member__c objGroupMem : objGroupMember){
                    objWrapList.add(new wrapperClass(objGroupMem,false,isUserChairORCoChair));
                }
            }
        }
        return objWrapList;
    }
}