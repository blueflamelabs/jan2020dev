/*
    Test Class user for MemberDirectoryCTRL Class
*/
@IsTest
public class MemberDirectoryCTRL_Test {
    @IsTest
    static void AssociationCTRLTestMethod(){
        Association__c ass = new Association__c();
        ass.Name = 'test';
        ass.Community_URL__c = 'test.com';
        insert ass;
        
        Working_Group__c workgroup = new Working_Group__c();
        workgroup.Association__c = ass.id;
        insert workgroup;
        
        Association_Question__c assque = new Association_Question__c();
        assque.Association__c = ass.id;
        insert assque;
        
        Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = AccRecordTypeId;
        acc.LastName = 'Test Parent' ;
        acc.Company_Association_Membership__c =ass.id ;
        acc.Association__c = ass.id;
        acc.Join_Date__c = date.today();
        acc.Phone = '123455';
        insert acc;
        
        
        System.debug('########acc#####3 '+acc);
        String RecName = 'Individual Member';
        String RecName1 = 'Corporate Member';
        
        MemberDirectoryCTRL accCTRL = new MemberDirectoryCTRL();
        MemberDirectoryCTRL.AccountsMemberRecord(ass.id);
        MemberDirectoryCTRL.getAccounts(ass.id,null,true,RecName);
        MemberDirectoryCTRL.getAccounts(ass.id,'t',true,RecName1);
        MemberDirectoryCTRL.getAccounts(ass.id,'t',false,RecName);
    }  
}