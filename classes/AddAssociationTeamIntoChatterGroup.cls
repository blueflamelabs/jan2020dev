/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                ModifiedDate   ModifiedBy              Description
*     1.0        241019         VennScience_BFL_Amruta                                          This is the handler class for AssociationRecordShare apex trigger. 
*     2.0                                                121119         VennScience_BFL_Amruta  Add Association Team as Managers only when the Role is Client Lead,
*																								Senior  Lead or Working Group Lead.
**********************************************************************************************************************************************************/
public class AddAssociationTeamIntoChatterGroup { 
    // Variable Declarations
    // V2.0 - T-000882 - 121119 - VennScience_BFL_Amruta - Used to store the role of Association Team for manager role
    public static List<String> listAssTeamRole = new List<String>();
    
    /**
    * Method Name : populateAssTeamRoleList
    * Parameters  : 
    * Description : Used to populate the Association Team's role list by fetching the values from custom label.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 131119
    **/
    public static void populateAssTeamRoleList() {
        // Variable Declarations
        String strAssTeamRole = Label.Association_Team_Role;
        List<String> listAssTeamRoleTemp = new List<String>();
        
        // Fetch the Group Member roles for managers from custom label
        if(String.isNotBlank(strAssTeamRole) && strAssTeamRole.contains(',')) {
        	listAssTeamRoleTemp = strAssTeamRole.split(',');    
        } else if(String.isNotBlank(strAssTeamRole) && !strAssTeamRole.contains(',')) {
            listAssTeamRoleTemp.add(strAssTeamRole);
        } // End of if-else block
        System.debug('listAssTeamRoleTemp======='+listAssTeamRoleTemp);
        // Iterate over listAssTeamRoleTemp and check if the custom label value consist of any white spaces
        for(String strRole : listAssTeamRoleTemp) {
            if(String.isNotBlank(strRole)) {
              listAssTeamRole.add(strRole.trim());
            } // End of if           
        } // End of for
        System.debug('listAssTeamRole======='+listAssTeamRole);
    }
    /**
    * Method Name : filterAssociationTeamRecords
    * Parameters  : param1: List<Association_Team__c>, param2: Map<Id,Association_Team__c>, Boolean
    * Description : Used to filter the AssociationTeam records afterInsert.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 241019
    **/
    public static void filterAssociationTeamRecords(List<Association_Team__c> associationTeamList, Map<Id,Association_Team__c> mapOldAssTeam,
                                                Boolean isDeleteEvent) {
        // Variable declaration
        Set<Association_Team__c> setOldAssociationTeam = new Set<Association_Team__c>();
        Set<Id> associationIdSet = new Set<Id>();
        // V2.0 - T-000882 - 121119 - VennScience_BFL_Amruta - Used to store the role of Association Team temporarily
        List<String> listAssTeamRoleTemp = new List<String>();
        // Used to store the Custom Label value for getiing the Association Team Roles
        //String strAssTeamRole = Label.Association_Team_Role;
                                                 
        // Fetch the Association Team roles for managers from custom label
        populateAssTeamRoleList();
        /*
        if(String.isNotBlank(strAssTeamRole) && strAssTeamRole.contains(',')) {
          listAssTeamRoleTemp = strAssTeamRole.split(',');    
        } else if(String.isNotBlank(strAssTeamRole) && !strAssTeamRole.contains(',')) {
            listAssTeamRoleTemp.add(strAssTeamRole);
        } // End of if-else block
        System.debug('listAssTeamRoleTemp======='+listAssTeamRoleTemp);
         // Iterate over listGrpMemberRoleTemp and check if the custom label value consist of any white spaces
        for(String strRole : listAssTeamRoleTemp) {
            if(String.isNotBlank(strRole)) {
              listAssTeamRole.add(strRole.trim());
            } // End of if           
        } // End of for
		*/
                                                    
        System.debug('listAssTeamRole======='+listAssTeamRole);
                                                 
        // Iterate over new list
        for(Association_Team__c associationTeamRecord : associationTeamList) {
            // Check if this is an update event
            if(!mapOldAssTeam.isEmpty() && mapOldAssTeam.containsKey(associationTeamRecord.Id)) {
            	// Check if this is an update event and User is updated
                if(mapOldAssTeam.get(associationTeamRecord.Id).User__c != associationTeamRecord.User__c &&
                   mapOldAssTeam.get(associationTeamRecord.Id).User__c != null && associationTeamRecord.User__c != null && 
                   listAssTeamRole.contains(associationTeamRecord.Role__c)) {
                    // Populate the set of old Association Team records
                    setOldAssociationTeam.add(mapOldAssTeam.get(associationTeamRecord.Id));
                    // Populate the set of new Association Id
                    associationIdSet.add(associationTeamRecord.Association__c);
                } // End of if
                // V2.0 - 121119 - VennScience_BFL_Amruta - Check if this is an update event and Role is updated to Manager's corresponding role
                if(mapOldAssTeam.get(associationTeamRecord.Id).Role__c != associationTeamRecord.Role__c &&
                   associationTeamRecord.User__c != null && listAssTeamRole.contains(associationTeamRecord.Role__c) &&
                   !listAssTeamRole.contains(mapOldAssTeam.get(associationTeamRecord.Id).Role__c)) {
                    // Populate the set of new Association Id
                    associationIdSet.add(associationTeamRecord.Association__c);
                } // End of if
                // V2.0 - 121119 - VennScience_BFL_Amruta - Check if this is an update event and Role is updated from Manager's corresponding role
                // to some other role
                if(mapOldAssTeam.get(associationTeamRecord.Id).Role__c != associationTeamRecord.Role__c &&
                   mapOldAssTeam.get(associationTeamRecord.Id).User__c != null && !listAssTeamRole.contains(associationTeamRecord.Role__c) && 
                   listAssTeamRole.contains(mapOldAssTeam.get(associationTeamRecord.Id).Role__c)) {
                    // Populate the set of old Association Team records
                    setOldAssociationTeam.add(mapOldAssTeam.get(associationTeamRecord.Id));
                } // End of if
            } else if(mapOldAssTeam.isEmpty() && associationTeamRecord.User__c != null && !isDeleteEvent &&
                      listAssTeamRole.contains(associationTeamRecord.Role__c)) {
                // Check if this is an insert event
                // Populate set of Association Id
            	associationIdSet.add(associationTeamRecord.Association__c);
            } else if(mapOldAssTeam.isEmpty() && associationTeamRecord.Association__c != null && 
                      associationTeamRecord.User__c != null && isDeleteEvent) {
                // Check if this is a delete event
                // Populate the set of old Association Team records
                setOldAssociationTeam.add(associationTeamRecord);
            } // End of if-else block
        } // End of for loop 
        System.debug('===associationIdSet===='+ associationIdSet);
        System.debug('===setOldAssociationTeam===='+ setOldAssociationTeam);                                            
                                                    
        if(!setOldAssociationTeam.isEmpty()) {
            // Call method in order to remove Association Team's previous userfrom chatter group
            processOldAssociationTeamRecords(setOldAssociationTeam);
        } // End of if
        if(associationIdSet.size() > 0) {
            // Call method in order to add Association Team's user to chatter group
            processAssociationRecords(associationIdSet);
        } // End of if
    }
    /**
    * Method Name : processOldAssociationTeamRecords
    * Parameters  : param1: Set<Association_Team__c> 
    * Description : Used to process the Old version of Association Team records.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 311019
    **/
    public static void processOldAssociationTeamRecords(Set<Association_Team__c> setOldAssTeamRecords) {
        // Variable Declarations
		Map<Id,Set<Id>> mapAssIdVSRemovedUserId = new Map<Id,Set<Id>>();
        
        // Iterate over old version of Association Team records
        for(Association_Team__c assTeamRecord : setOldAssTeamRecords) {
            // Populate map of AssociationId VS Removed Association Team's user Id
            if(!mapAssIdVSRemovedUserId.containsKey(assTeamRecord.Association__c)) {
                mapAssIdVSRemovedUserId.put(assTeamRecord.Association__c, new Set<Id>{assTeamRecord.User__c});
            } else {
                Set<Id> setPreviousUserId = new Set<Id>();
                setPreviousUserId = mapAssIdVSRemovedUserId.get(assTeamRecord.Association__c);
                setPreviousUserId.add(assTeamRecord.User__c);
                mapAssIdVSRemovedUserId.put(assTeamRecord.Association__c, new Set<Id>{assTeamRecord.User__c});
            } // End of if-else block
        } // End of for
        
        // Call method in order to remove the Association Team's user from chatter group
        removeUserFromChatterGroup(mapAssIdVSRemovedUserId);
    }
    /**
    * Method Name : processAssociationRecords
    * Parameters  : param1: Set<Id> 
    * Description : Used to add Association team member in Working group related chatter group .
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 241019
    **/
    public static void processAssociationRecords(Set<Id> associationIdSet) {
        System.debug('===associationIdSet in processAssociationRecords====' + associationIdSet);
        
        // Variable declaration
        List<CollaborationGroupMember> listMembersToInsert = new List<CollaborationGroupMember>();
        Map<Id,Set<Id>> mapGrpMemIdVSGrpIdSet = new Map<Id,Set<Id>>();
        Map<Id, Set<Id>> associationTeamMemberMap = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> associationChatterMap = new Map<Id, Set<Id>>();
        Map<Id, Id> collaborationMemberIdMap = new Map<Id, Id>();
        Map<Id,Set<Id>> groupMemberMap = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapGroupIdVSGrpMemIdSet = new Map<Id,Set<Id>>();
        Set<Id> chatterGroupIdSet = new Set<Id>();
        
    	for(Association__c associationRecord : [ SELECT Id,
                                                		(SELECT Id,
                                                         		Chatter_Group__c
                                                           FROM Groups__r),
                                                		(SELECT Id,
                                                                Association__c,
                                                         		User__c
                                                           FROM Association_Teams__r
                                                          WHERE Role__c IN :listAssTeamRole)                                                	    
                                                   FROM Association__c
            									  WHERE Id IN :associationIdSet
        ]) {
			System.debug('===associationRecord====' + associationRecord);   
            for(Working_Group__c workingGroupRecord : associationRecord.Groups__r){
                if(workingGroupRecord.Chatter_Group__c != null) {
                    chatterGroupIdSet.add(workingGroupRecord.Chatter_Group__c);
                
                    // Prepare map of association vs chatter group 
                    if(!associationChatterMap.containsKey(associationRecord.Id)) {
                        associationChatterMap.put(associationRecord.Id, new Set<Id>{workingGroupRecord.Chatter_Group__c});
                    } else {
                        Set<Id> setPreviousGroupId = new Set<Id>();
                        setPreviousGroupId = associationChatterMap.get(associationRecord.Id);
                        setPreviousGroupId.add(workingGroupRecord.Chatter_Group__c);
                        associationChatterMap.put(associationRecord.Id, setPreviousGroupId);
                    } // End of if-else block
                }                
            } // End of inner for loop
            
            for(Association_Team__c associationTeamRecord : associationRecord.Association_Teams__r){
                if(associationTeamRecord.User__c != null) {
                    // Prepare map of association vs Team Member 
                    if(!associationTeamMemberMap.containsKey(associationRecord.Id)) {
                        associationTeamMemberMap.put(associationRecord.Id, new Set<Id>{associationTeamRecord.User__c});
                    } else {
                        Set<Id> setTeamMemberId = new Set<Id>();
                        setTeamMemberId = associationTeamMemberMap.get(associationRecord.Id);
                        setTeamMemberId.add(associationTeamRecord.User__c);
                        associationTeamMemberMap.put(associationRecord.Id, setTeamMemberId);
                    } // End of if-else block
                }
            } // End of inner for loop
        } // End of outer for loop
                
        if(chatterGroupIdSet.size() > 0 ) {
            // fetch related chatter group and chater group members
            for(CollaborationGroupMember collGrpMemRecord : [ SELECT Id,
                                                                     MemberId,
                                                                     CollaborationGroupId
                                                               FROM CollaborationGroupMember
                                                              WHERE CollaborationGroupId IN :chatterGroupIdSet
             ]) {
                System.debug('===collGrpMemRecord====' + collGrpMemRecord);
                 // @Reminder: (Remove this comment)prepare map4 here
                 // Populate map of GroupId VS UserIdSet
                 if(!mapGroupIdVSGrpMemIdSet.containsKey(collGrpMemRecord.CollaborationGroupId)) {
                     mapGroupIdVSGrpMemIdSet.put(collGrpMemRecord.CollaborationGroupId, new Set<Id>{collGrpMemRecord.MemberId});
                 } else {
                     Set<Id> setPreviousUserId = new Set<Id>();
                     setPreviousUserId = mapGroupIdVSGrpMemIdSet.get(collGrpMemRecord.CollaborationGroupId);
                     setPreviousUserId.add(collGrpMemRecord.MemberId);
                     mapGroupIdVSGrpMemIdSet.put(collGrpMemRecord.CollaborationGroupId, setPreviousUserId);
                 }// End of if-else block
            } // End of outer for loop
        }
        
        System.debug('===collaborationMemberIdMap====' + collaborationMemberIdMap);
		System.debug('mapGroupIdVSGrpMemIdSet============'+mapGroupIdVSGrpMemIdSet);
        
        System.debug('===associationChatterMap====' + associationChatterMap); 
        System.debug('===associationTeamMemberMap====' + associationTeamMemberMap);
        System.debug('===chatterGroupIdSet====' + chatterGroupIdSet);
        
        // Iterate over associationIdVSTeamMemberIdSetMap
        for(Id associationId : associationTeamMemberMap.keyset()) {
            for(Id teamMemberId : associationTeamMemberMap.get(associationId)) {
                if(associationChatterMap.containsKey(associationId)) {
                    mapGrpMemIdVSGrpIdSet.put(teamMemberId, associationChatterMap.get(associationId));
                } // End of if
            } // End of inner for
        } // End of outer for
        System.debug('mapGrpMemIdVSGrpIdSet============='+mapGrpMemIdVSGrpIdSet);
        
        for(Id grpMemId : mapGrpMemIdVSGrpIdSet.keySet()) {
            for(Id grpId : mapGrpMemIdVSGrpIdSet.get(grpMemId)) {
                if(!mapGroupIdVSGrpMemIdSet.isEmpty() && mapGroupIdVSGrpMemIdSet.containsKey(grpId) && !mapGroupIdVSGrpMemIdSet.get(grpId).contains(grpMemId)) {
                    // Create Collaboration Group Member record
                    CollaborationGroupMember groupMember = new CollaborationGroupMember();
                    groupMember.MemberId = grpMemId; // Id of group member
                    groupMember.CollaborationGroupId = grpId; //Id of chatter group
                    // V2.0 - T-000882 - 121119 - VennScience_BFL_Amruta - Add group member as Manager of Chatter group
                    groupMember.CollaborationRole = 'Admin';
                    listMembersToInsert.add(groupMember); 
                } else if(mapGroupIdVSGrpMemIdSet.isEmpty()) {
                    // Create Collaboration Group Member record
                    CollaborationGroupMember groupMember = new CollaborationGroupMember();
                    groupMember.MemberId = grpMemId; // Id of group member
                    groupMember.CollaborationGroupId = grpId; //Id of chatter group
                    // V2.0 - T-000882 - 121119 - VennScience_BFL_Amruta - Add group member as Manager of Chatter group
                    groupMember.CollaborationRole = 'Admin';
                    listMembersToInsert.add(groupMember);
                } else if(!mapGroupIdVSGrpMemIdSet.isEmpty() && !mapGroupIdVSGrpMemIdSet.containsKey(grpId)) {
                    // Create Collaboration Group Member record
                    CollaborationGroupMember groupMember = new CollaborationGroupMember();
                    groupMember.MemberId = grpMemId; // Id of group member
                    groupMember.CollaborationGroupId = grpId; //Id of chatter group
                    // V2.0 - T-000882 - 121119 - VennScience_BFL_Amruta - Add group member as Manager of Chatter group
                    groupMember.CollaborationRole = 'Admin';
                    listMembersToInsert.add(groupMember);
                } // End of if
            } // End of inner for
        } // End of outer for
        System.debug('listMembersToInsert======'+listMembersToInsert);
        
        // Insert Collaboration Group Members
        if(!listMembersToInsert.isEmpty()) {
            try {
            	//insert listMembersToInsert;
            	Database.insert(listMembersToInsert, false);
            } catch(Exception e) {
                System.debug('Association Team member is not added to chatter grp becoz the error:'+e.getMessage());
            }
        } // End of if
    }
    /**
    * Method Name : mapAssociationIdVSRemovedUserId
    * Parameters  : param1: Map<Id,Set<Id>> 
    * Description : Used to remove Association Team's user from related chatter group .
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 311019
    **/
    public static void removeUserFromChatterGroup(Map<Id,Set<Id>> mapAssociationIdVSRemovedUserId) {
        // Variables Declarations
        List<CollaborationGroupMember> listCollGrpMemToBeDeleted = new List<CollaborationGroupMember>();
        Map<Id, Set<Id>> associationTeamMemberMap = new Map<Id, Set<Id>>();
        Set<Id> chatterGroupIdSet = new Set<Id>();
        Set<Id> setMemberIdToBeRemoved = new Set<Id>();
        
        System.debug('Inside removeUserFromChatterGroup');
        System.debug('mapAssociationIdVSRemovedUserId======'+mapAssociationIdVSRemovedUserId);
        // Iterate over Association records
        for(Association__c associationRecord : [ SELECT Id,
                                                        (SELECT Id,
                                                                Chatter_Group__c
                                                           FROM Groups__r),
                                                           (SELECT Id,
                                                                   Association__c,
                                                                   User__c
                                                              FROM Association_Teams__r
                                                             WHERE Role__c IN :listAssTeamRole)                                                      
                                                   FROM Association__c
                                                  WHERE Id IN :mapAssociationIdVSRemovedUserId.keySet()
        ]) {
      		System.debug('===associationRecord====' + associationRecord);  
            // Iterate over child Working Groups
            for(Working_Group__c workingGroupRecord : associationRecord.Groups__r){
                if(workingGroupRecord.Chatter_Group__c != null) {
                    chatterGroupIdSet.add(workingGroupRecord.Chatter_Group__c); 
                } // End of if               
            } // End of inner for loop
            
            // Iterate over child Association Teams
            for(Association_Team__c associationTeamRecord : associationRecord.Association_Teams__r){
                if(associationTeamRecord.User__c != null) {
                    // Prepare map of association vs Team Member 
                    if(!associationTeamMemberMap.containsKey(associationRecord.Id)) {
                        associationTeamMemberMap.put(associationRecord.Id, new Set<Id>{associationTeamRecord.User__c});
                    } else {
                        Set<Id> setTeamMemberId = new Set<Id>();
                        setTeamMemberId = associationTeamMemberMap.get(associationRecord.Id);
                        setTeamMemberId.add(associationTeamRecord.User__c);
                        associationTeamMemberMap.put(associationRecord.Id, setTeamMemberId);
                    } // End of if-else block
                } // End of if
            } // End of inner for loop
        } // End of outer for loop
        System.debug('associationTeamMemberMap======'+associationTeamMemberMap);
        
        // Iterate over map of AssociationId VS DeletedAssociationTeam Id
        for(Id assId : mapAssociationIdVSRemovedUserId.keySet()) {
            for(Id deletedMemberId : mapAssociationIdVSRemovedUserId.get(assId)) {
                // Check if the deleted Association Team's user is not present in existing Association Team records for current Association
                if(!associationTeamMemberMap.isEmpty() && associationTeamMemberMap.containsKey(assId) &&
                   !associationTeamMemberMap.get(assId).contains(deletedMemberId)) {
                	setMemberIdToBeRemoved.add(deletedMemberId);    
                } else if(associationTeamMemberMap.isEmpty()) {
                    setMemberIdToBeRemoved.add(deletedMemberId); 
                } else if(!associationTeamMemberMap.isEmpty() && !associationTeamMemberMap.containsKey(assId)) {
                } // End of if-else block
            } // End of inner for
        } // End of outer for
        System.debug('setMemberIdToBeRemoved=========='+setMemberIdToBeRemoved);
        System.debug('chatterGroupIdSet=========='+chatterGroupIdSet);
        
        // Fetch the Collaboration Group Member records
        listCollGrpMemToBeDeleted = [SELECT Id
                                       FROM CollaborationGroupMember
                                      WHERE MemberId IN :setMemberIdToBeRemoved
                                        AND CollaborationGroupId IN :chatterGroupIdSet];
        System.debug('listCollGrpMemToBeDeleted======='+listCollGrpMemToBeDeleted);
        
        // Delete Chatter group members
        if(!listCollGrpMemToBeDeleted.isEmpty()) {
            try {
                delete listCollGrpMemToBeDeleted;
            } catch(Exception e) {
                System.debug('Error occurred while deleting the chatter group members:'+e.getMessage());
            } // End of try-catch block
        } // End of if
    }
}