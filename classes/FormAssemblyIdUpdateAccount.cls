/**
    Description -: Class use for FormAssembly Field Update on Account Object.
    Ticket Number -: T-000102
    Created Date -: 9/30/2019
    Created By -: Manas
**/

public class FormAssemblyIdUpdateAccount{

    public Static void listOfAccounts(List<Account> objListAccount){
        
        String CorporateMemberRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId();
        String IndividualMemberRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
        String RandomValue = RndInteger(1000000,9999999);
        for(Account objAcc : objListAccount){
                String AssociationRecordIdSub =''; 
                if(objAcc.recordTypeId == CorporateMemberRecTypeId)
                    AssociationRecordIdSub = String.valueOf(objAcc.Company_Association_Membership__c).substring(7);
                if(objAcc.recordTypeId == IndividualMemberRecTypeId)
                    AssociationRecordIdSub = String.valueOf(objAcc.Association__c).substring(7);
                Datetime currentDateTime = Datetime.now();
                Long currentMillSecondTime = currentDateTime.getTime();
                String MillSecondTime = String.valueOf(currentMillSecondTime); 
                String MillSecondSub = MillSecondTime.substring(5);
                String AccFormAssembly = RandomValue+AssociationRecordIdSub+MillSecondSub;
                objAcc.FormAssembly_ID__c = AccFormAssembly;   
        }
    }
    public Static String RndInteger(Integer min, Integer max) {
        Integer RandomValue = Integer.valueOf(Math.floor(Math.random() * (max - min)) + min);
        return String.valueOf(RandomValue);
    }     
}