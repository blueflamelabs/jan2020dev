/*********************************************************************************************************************************************************
*   Version  CreateDate   CreatedBy               ModifiedDate   ModifiedBy  Description
*   1.0      241219       VennScience_BFL_Monali                             This is the handler class for ContentDocumentLinkTrigger to update record visiblity for user. 
*	  
**********************************************************************************************************************************************************/
public class ContentDocumentLinkHandler {
    
    /**
    * Method Name : filterContentDocumentLinkRecords
    * Parameters  : param1: List<ContentDocumentLink>
    * Description : Used to filter the ContentDocumentLink records as per the required criteria.
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 241219
    **/
    public static void filterContentDocumentLinkRecords(List<ContentDocumentLink> listContentDocLink) {
        // Variable Declarations
        Map<Id,ContentDocumentLink> mapContentDocumentIdVSContentDocLink = new Map<Id,ContentDocumentLink>();
        
        //System.debug('Inside filterRecords');
        // Iterate over trigger.new list
        for(ContentDocumentLink contentDocLinkRecord : listContentDocLink) {
            // This is an insert event
            if(!String.isEmpty(contentDocLinkRecord.ContentDocumentId)) {
                mapContentDocumentIdVSContentDocLink.put(contentDocLinkRecord.ContentDocumentId, contentDocLinkRecord);
            } // End of else-if block
        } // End of for
        // Check if map is not empty
        if(!mapContentDocumentIdVSContentDocLink.isEmpty()) {
            updateContentDocLinkVisiblity(mapContentDocumentIdVSContentDocLink);
        }// End of if.
    }
    
    /**
    * Method Name : updateContentDocLinkVisiblity
    * Parameters  : param1: Map<Id, ContentDocumentLink>
    * Description : Used to update the ContentDocumentLink records as per the required criteria.
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 241219
    **/
    public static void updateContentDocLinkVisiblity(Map<Id, ContentDocumentLink> mapContentDocumentIdVSContentDocLink) {
        // Variable declaration.
        List<ContentDocumentLink> listOfContDocLink = new List<ContentDocumentLink>();
        //Iterate over the list
        for(ContentDocument contentDocumentRecord : [SELECT Id,
                                                            SharingPrivacy
                                                     FROM ContentDocument
                                                     WHERE Id IN :mapContentDocumentIdVSContentDocLink.keySet()]) 
        {
            System.debug('inside for loop content document');
            if(contentDocumentRecord.SharingPrivacy == 'N') {
                 System.debug('inside if content document visible to all');
                ContentDocumentLink contDocLink = mapContentDocumentIdVSContentDocLink.get(contentDocumentRecord.Id);
                System.debug('contDocLink ===='+contDocLink);
                if(!contDocLink.Visibility.equalsIgnoreCase('AllUsers')) { 
                    System.debug('inside if content document link not all user ');
                    contDocLink.Visibility = 'AllUsers';
                    listOfContDocLink.add(contDocLink);
                    System.debug('aded list new visiblity listOfContDocLink==='+listOfContDocLink);
                }//End of inner if
            }// End of outer if
        }// End of for
        System.debug('before update listOfContDocLink==='+listOfContDocLink);
        
    }

}