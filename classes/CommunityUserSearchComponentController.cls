public class CommunityUserSearchComponentController {
	/**
   * Method Name : getRelatedWorkingGroup
   * Description : Used to fetch Working Group record related to the corresponding chatter group.
   * Param       : param1: Id
   * Created By  : VennScience_BFL_Amruta 
   * Created On  : 111219
   **/
    @AuraEnabled
    public static void getCommunityUsers(String userName) {
        System.debug('Inside getRelatedWorkingGroup');
        System.debug('userName===='+userName);
        String searchUserString = userName+'%';
        List<User> listCommunityUsers = new List<User>();
        listCommunityUsers = [SELECT Id,
                             		 Name
                                FROM User
                               WHERE Name LIKE :searchUserString];
    }
}