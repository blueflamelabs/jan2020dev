/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                    Description
*     1.0        040919         VennScience_BFL_Amruta       This is to test the updateIndividualMember apex class.
**********************************************************************************************************************************************************/
@isTest
public class UpdateIndividualMemberSchedulerTest {
    /**
    * Method Name : scheduleBatchTest
    * Parameters  : 
    * Description : Used to call the scheduler and schedule the batch class using cron expression
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 040919
    **/
    @isTest
    public static void scheduleBatchTest() {
        // Cron Expression
        String CRON_EXP = '0 0 0 * * ?';
        // Get Custom Setting value
        Termination_Date_Update_Setting__c objSetting = new Termination_Date_Update_Setting__c();
        Test.startTest();
        //String jobId = System.schedule('UpdateIndividualMemberBatch Test',CRON_EXP, new UpdateIndividualMemberScheduler());
        UpdateIndividualMemberScheduler objScheduler = new UpdateIndividualMemberScheduler();
        objScheduler.scheduleBatchForEveryMidnight();
        
        objScheduler.scheduleBatchForEveryMidnight();        
        Test.stopTest();
        // Fetch jobs from CronTrigger
        List<CronTrigger> listcronTrigger = [SELECT Id, 
                                                    CronExpression, 
                                                    TimesTriggered, 
                                                    NextFireTime 
                                               FROM CronTrigger 
                                              WHERE CronExpression = :CRON_EXP];
        // Assert: Check if job is scheduled
        System.assert(listcronTrigger.size() > 0, 'Job is not scheduled');
    }
}