/*********************************************************************************************************************************************************
*     Version    CreateDate/Modified   CreatedBy                  Description
*     1.0        111219                VennScience_BFL_Amruta     This is the controller class for RedirectToWorkingGroup lightning component.
*
**********************************************************************************************************************************************************/
public Without Sharing class RedirectToWorkingGroupController {
   /**
   * Method Name : getRelatedWorkingGroup
   * Description : Used to fetch Working Group record related to the corresponding chatter group.
   * Param	     : param1: Id
   * Created By  : VennScience_BFL_Amruta 
   * Created On  : 111219
   **/
    @AuraEnabled
    public static String getRelatedWorkingGroup(Id chatterGroupId) {
        System.debug('Inside getRelatedWorkingGroup');
        System.debug('chatterGroupId========'+chatterGroupId);
        // Variable Declarations
        List<Working_Group__c> workingGroupRecord = new List<Working_Group__c>();
        String workingGroupId = '';
        
        if(chatterGroupId != null) {
            workingGroupRecord = [SELECT Id,
                                  		 Name
                                    FROM Working_Group__c
                                   WHERE Chatter_Group__c = :chatterGroupId
                                   LIMIT 1];
        } // End of chatter group id null check if
        System.debug('workingGroupRecord========'+workingGroupRecord);
        if(!workingGroupRecord.isEmpty() && workingGroupRecord.size() > 0) {
        	workingGroupId = workingGroupRecord[0].Id;
        } else {
            workingGroupId = 'Please navigate to Working Groups tab to access the group details.';
        } // End of working group null check if
        System.debug('workingGroupId========'+workingGroupId);
        return workingGroupId;
    }
}