/*********************************************************************************************************************************************************
*   Version  CreateDate   CreatedBy               ModifiedDate   ModifiedBy  Description
*   1.0      121119       VennScience_BFL_Amruta                             This is the handler class for AssociationTrigger apex trigger. 
*	  
**********************************************************************************************************************************************************/

public class AssociationTriggerHandler {
    
    /**
    * Method Name : filterAssRecords
    * Parameters  : param1: List<Association__c>, param2: Map<Id,Association__c>
    * Description : Update ChatterGroup Owner When Associated Owner is Update.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 121119
    **/
    public static void filterAssRecords(List<Association__c> newAssociationList, Map<Id,Association__c> oldAssociationMap) {
    	// Variable Declarations
    	List<Association__c> listFilteredAss = new List<Association__c>();
        Set<Id> setAssociactionId = new set<Id>();
        Map<Id,Set<Id>> mapPriviousOwnerIdVSAssoIdSet = new Map<Id,Set<Id>>();
        
        // Iterate over new version of Association records
        for(Association__c assRecord : newAssociationList) {
            // Check if this is an update event
            if(!oldAssociationMap.isEmpty() && oldAssociationMap.containsKey(assRecord.Id)) {
                // Check if Association's owner has been updated
                if(oldAssociationMap.get(assRecord.Id).OwnerId != assRecord.OwnerId) {
                    // Populate set of Association id
                    setAssociactionId.add(assRecord.Id);
                    // Populate map of PreviousOwnerId VS Set of Association id
                    Id priviousOwnerId = oldAssociationMap.get(assRecord.Id).OwnerId;
                    if(!mapPriviousOwnerIdVSAssoIdSet.containsKey(priviousOwnerId)) { 
                       mapPriviousOwnerIdVSAssoIdSet.put(priviousOwnerId, new Set<Id>{assRecord.Id});                          
                    } else {
                        Set<Id> setAssoId = new Set<Id>();
                        setAssoId = mapPriviousOwnerIdVSAssoIdSet.get(priviousOwnerId);
                        setAssoId.add(assRecord.Id);
                        mapPriviousOwnerIdVSAssoIdSet.put(priviousOwnerId, setAssoId);   
                    } // End of if-else block
                    // Populate list of filtered Association records
                	listFilteredAss.add(assRecord);    
                } // End of inner if 
            } // End of if
        } // End of for
        
        // Call method in order to change the related chatter group's owner
        if(!listFilteredAss.isEmpty()) {
        	updateChatterGroupOwner(listFilteredAss);    
        } // End of if
        
         if(!mapPriviousOwnerIdVSAssoIdSet.isEmpty() && !setAssociactionId.isEmpty()) {
        	removeAssOwnerFromChatterGroup(mapPriviousOwnerIdVSAssoIdSet, setAssociactionId);    
        } // End of if
    }
    /**
    * Method Name : removeAssOwnerFromChatterGroup
    * Parameters  : param1: Map<Id,Set<Id>>, param2: Set<Id>
    * Description : Remove ChatterGroup Member.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 131119
    **/
    public static void removeAssOwnerFromChatterGroup(Map<Id,Set<Id>> mapPriviousOwnerIdVSAssoIdSet, Set<Id> setAssoId) {
        
        Map<Id,Set<Id>> mapAssoIdVSChatterGrpIdSet = new Map<Id,Set<Id>>();
        Set<Id> setworkGrpId = new Set<Id>();
        Set<Id> setChatterGroupId = new Set<Id>();
        
        // Iterate over AssociationObj and get the Association Id And Working_Groups Id And Fill in mapAssoIdVSChatterGrpIdSet
        for(Association__c AssociationObj : [SELECT Id,
                                                    OwnerId,
                                                    (SELECT Id,
                                                            Chatter_Group__c 
                                                       FROM Groups__r) 
                                               FROM Association__c 
                                              WHERE Id IN :setAssoId]) {
            //Iterate on workObj and get the Work Group Id And ChatterGroup Id 
            for(Working_Group__c workObj : AssociationObj.Groups__r) {
                setworkGrpId.add(workObj.Id);
                setChatterGroupId.add(workObj.Chatter_Group__c);
                //checks the Assocition Id Contains or not in mapAssoIdVSChatterGrpIdSet
                if(!mapAssoIdVSChatterGrpIdSet.containsKey(AssociationObj.Id)){
                     //if not then Fill the map and create new set of ChatterGroup Id
                     mapAssoIdVSChatterGrpIdSet.put(AssociationObj.Id, new Set<Id>{workObj.Chatter_Group__c});
                } else {
                     Set<Id> setChatterGrpId = new Set<Id>();
                     //if it contains on Map thent get the setChatterGrpId
                     setChatterGrpId = mapAssoIdVSChatterGrpIdSet.get(AssociationObj.Id);
                     setChatterGrpId.add(workObj.Chatter_Group__c);
                     //Fill the existing map of AssociationObj Id and setChatterGrpId
                     mapAssoIdVSChatterGrpIdSet.put(AssociationObj.Id, setChatterGrpId); 
                }
            }                                        
        }
        List<Group_Member__c> listGroupMem = new List<Group_Member__c>();
        //Iterate on GroupMemObj and get the Work Group related Group Members
        for(Group_Member__c GroupMemObj : [SELECT Id,
                                                  Working_Group__c  
                                             FROM Group_Member__c 
                                            WHERE Working_Group__c IN :setworkGrpId 
                                              AND Status__c = 'Active']) {                                    
         	listGroupMem.add(GroupMemObj);                                                          
        }
        List<CollaborationGroupMember> listGrpMemToBeDeleted = new List<CollaborationGroupMember>();
        //Iterate on collabGroupMemObj and get the setChatterGroupId related Group Members
        for(CollaborationGroupMember collabGroupMemObj : [SELECT Id,
                                                                 MemberId,
                                                                 CollaborationGroupId
                                                            FROM CollaborationGroupMember 
                                                           WHERE CollaborationGroupId IN :setChatterGroupId]) {
            //check the collabGroupMem is contains or not in mapPriviousOwnerIdVSAssoIdSet                                                
            if(!mapPriviousOwnerIdVSAssoIdSet.isEmpty() && 
               mapPriviousOwnerIdVSAssoIdSet.containsKey(collabGroupMemObj.MemberId)) {
                for(Id associationId : mapPriviousOwnerIdVSAssoIdSet.get(collabGroupMemObj.MemberId)) {
                    //check the associationId is contains or not in mapAssoIdVSChatterGrpIdSet                                                
                    if(!mapAssoIdVSChatterGrpIdSet.isEmpty() && mapAssoIdVSChatterGrpIdSet.containsKey(associationId) &&
                       mapAssoIdVSChatterGrpIdSet.get(associationId).contains(collabGroupMemObj.CollaborationGroupId)) {
                        //added the collabGroupMem record in listGrpMemToBeDeleted for delete the Members   
                    	listGrpMemToBeDeleted.add(collabGroupMemObj);
                    } // End of inner if       
                } // End of inner for 
            } // End of outer if
        } // End of outer for
        
        if(!listGrpMemToBeDeleted.isEmpty()) {
            try {
                delete listGrpMemToBeDeleted; 
            }
            catch(exception e) {
                System.debug('error aucur while deleteing chatter group Member :'+e.getMessage());
            }
        }
        
        // Call Group Member Handler in order to add the removed owner from chatter group back to it if it is present as group member
        GroupMemberTriggerHandler.populateGroupMemberRoleList();
        GroupMemberTriggerHandler.addMembersToChatterGroup(listGroupMem);
        
        // Call Association Team Handler in order to add the removed owner from chatter group back to it if it is present as Association Team
		AddAssociationTeamIntoChatterGroup.populateAssTeamRoleList();
		AddAssociationTeamIntoChatterGroup.processAssociationRecords(setAssoId);
  }          
            
    /**
    * Method Name : updateChatterGroupOwner
    * Parameters  : param1: List<Association__c>
    * Description : Update ChatterGroup Owner When Associated Owner is Update.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 121119
    **/
    public static void updateChatterGroupOwner(List<Association__c> newAssociationList) {
        // Variable Declarations
        // Used to store the ChatterGroup Id And Association Id which are to be added as CollaborationGroup 
        Map<Id,Id> mapChatterIdVsAssoOwnerId = new Map<Id,Id>();
        // Used to store the CollaborationGroup Records
        List<CollaborationGroup> collabrationGroupList = new List<CollaborationGroup>();
        List<CollaborationGroupMember> collabrationMemGroupList = new List<CollaborationGroupMember>();
        List<CollaborationGroupMember> listCollaborationGrpMem = new List<CollaborationGroupMember>();
		Map<Id,Set<Id>> mapGrpIdVsMemberIdSet = new Map<Id,Set<Id>>();
        
        // Iterate over AssociationObj and get the Association Id And Working_Groups Id And Fill in mapChatterIdVsAssoOwnerId
        for(Association__c AssociationObj : [SELECT Id,
                                                    OwnerId,
                                                    (SELECT Id,Chatter_Group__c 
                                                       FROM Groups__r) 
                                               FROM Association__c 
                                              WHERE Id IN : newAssociationList])
        
        for(Working_Group__c workObj : AssociationObj.Groups__r) { 
          //fill the map with ChatterGroupId And Association OwnerId  
          mapChatterIdVsAssoOwnerId.put(workObj.Chatter_Group__c,AssociationObj.OwnerId);
        }
        
        // Fetch existing Collaboration Group Members for Chatter Group
        listCollaborationGrpMem = [SELECT CollaborationGroupId,
                                          MemberId
                                     FROM CollaborationGroupMember
                                    WHERE CollaborationGroupId IN :mapChatterIdVsAssoOwnerId.keySet()];
        System.debug('listCollaborationGrpMem======='+listCollaborationGrpMem);
        // Iterate over collaboration group list
        for(CollaborationGroupMember collGrpMem : listCollaborationGrpMem) {
            if(!mapGrpIdVsMemberIdSet.containsKey(collGrpMem.CollaborationGroupId)) {
                mapGrpIdVsMemberIdSet.put(collGrpMem.CollaborationGroupId, new Set<Id>{collGrpMem.MemberId});    
            } else {
                Set<Id> setPreviousMemId = new Set<Id>();
                setPreviousMemId = mapGrpIdVsMemberIdSet.get(collGrpMem.CollaborationGroupId);
                setPreviousMemId.add(collGrpMem.MemberId);
                mapGrpIdVsMemberIdSet.put(collGrpMem.CollaborationGroupId, setPreviousMemId);
            } // End of if-else block    
        } // End of for
        System.debug('mapGrpIdVsMemberIdSet======='+mapGrpIdVsMemberIdSet);
        
        // Iterate over collabObj and get the CollaborationGroup Id And OwnerId
        for(CollaborationGroup collabGroupObj : [SELECT Id,OwnerId 
                                                   FROM CollaborationGroup 
                                                  WHERE Id IN : mapChatterIdVsAssoOwnerId.keyset()])
        {
            // Fetch the collabGroupObj Id And Checks is it in Map or Not
            if(!mapChatterIdVsAssoOwnerId.isEmpty() && mapChatterIdVsAssoOwnerId.containsKey(collabGroupObj.Id)) {
              Id AssoOwnerId = mapChatterIdVsAssoOwnerId.get(collabGroupObj.Id);
                System.debug('AssoOwnerId+++'+AssoOwnerId);
                //Create new instance of CollaborationGroupMember 
                CollaborationGroupMember collaGroupMemObj = new CollaborationGroupMember();
                // Check if the Association Owner is not already a part of current chatter group
                if(!mapGrpIdVsMemberIdSet.isEmpty() && mapGrpIdVsMemberIdSet.containsKey(collabGroupObj.Id) &&
                   !mapGrpIdVsMemberIdSet.get(collabGroupObj.Id).contains(AssoOwnerId)) {
                	collaGroupMemObj.CollaborationGroupId = collabGroupObj.Id;
                    collaGroupMemObj.MemberId = AssoOwnerId;
                    collabrationMemGroupList.add(collaGroupMemObj);    
                } else if(mapGrpIdVsMemberIdSet.isEmpty()) {
                    collaGroupMemObj.CollaborationGroupId = collabGroupObj.Id;
                    collaGroupMemObj.MemberId = AssoOwnerId;
                    collabrationMemGroupList.add(collaGroupMemObj);            
                } else if(!mapGrpIdVsMemberIdSet.isEmpty() && !mapGrpIdVsMemberIdSet.containsKey(collabGroupObj.Id)) {
                    collaGroupMemObj.CollaborationGroupId = collabGroupObj.Id;
                    collaGroupMemObj.MemberId = AssoOwnerId;
                    collabrationMemGroupList.add(collaGroupMemObj);
                } // End of if-else block
                
                // Check if Chatter group owner is same as that of Association owner or not
                if(collabGroupObj.OwnerId != AssoOwnerId) {
                    //Check if it is not same then Update it.
                    collabGroupObj.OwnerId = AssoOwnerId;
                    //fill the updated CollaborationGroup Record in collabrationGroupList 
                    collabrationGroupList.add(collabGroupObj);
                }  
            }                                     
        }
        //insert new CollaborationGroupMember 
        if(!collabrationMemGroupList.isEmpty()) {
            insert collabrationMemGroupList;
        }
        //if it is not empty Update the collabrationGroupList 
        if(!collabrationGroupList.isEmpty()) {
            Update collabrationGroupList;
        } 
    }                                             
}