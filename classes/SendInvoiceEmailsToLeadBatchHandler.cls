/*********************************************************************************************************************************************************
*     Version    CreateDate       CreatedBy               ModifiedDate   ModifiedBy  Description
*     1.0        27112019         VennScience_BFL_Amruta                             This is the batch class handler for SendInvoiceEmailsToLeadBatchHandler 
*                                                                                    batch. Used for sending the Invoices to related Leads.                               
**********************************************************************************************************************************************************/
public class SendInvoiceEmailsToLeadBatchHandler {
    // Variable Declarations
    public static Map<Id,Id> mapAccIdVSFinalOrgWideAddId = new Map<Id,Id>();
    public static Set<Id> setAssIdWithDefaultSenderAdd = new Set<Id>();
    public static Id defaultSendAddOrgWideId;

    /**
    * Method Name : getAssociationSenderAddress
    * Parameters  : param1: Map<String,List<Id>>
    * Description : Used to find the org-wide default address record Id for corresponding Association's Sender Address.
    * Created By  : VennScience_BFL_Amruta                             
    * Created On  : 28112019         
    **/
    public static void getAssociationSenderAddress(List<Invoice__c> listInvoiceRecords) {
        // Variable Declarations
        List<OrgWideEmailAddress> listOrgWideAddress = new List<OrgWideEmailAddress>();
        Set<Id> setAssIdWithBlankSenderAddress = new Set<Id>();
        setAssIdWithDefaultSenderAdd = new Set<Id>();
        Map<String, Set<Id>> mapAssSenderAddressVSAssIdSet = new Map<String, Set<Id>>();
        Map<Id,Id> mapAssociationIdVSOrgWideId = new Map<Id,Id>();
        Map<Id,Id> mapAssIdVSLeadId = new Map<Id,Id>();
        Map<Id,Id> getAssSenderAddressResultMap = new Map<Id,Id>();
        mapAccIdVSFinalOrgWideAddId = new Map<Id,Id>();
        //Id defaultSendAddOrgWideId;
        String defaultSenderAddress = NotifyIndividualMembersBatchHandler.getCustomSettingValues().Default_Sender_Address__c;

        // Iterate over list of Invoice
        for(Invoice__c invoiceRecord : listInvoiceRecords) {
            
            // Check if Invoice's Association's semder email address is blank or not
            if(String.isNotBlank(invoiceRecord.Association__r.Email_Sender_Address__c)) {
                // Populate map of Association Id VS Lead Id
                mapAssIdVSLeadId.put(invoiceRecord.Association__c, invoiceRecord.Lead__c);
                // Populate map of Association Sender Address VS Set of Association Id
                if(!mapAssSenderAddressVSAssIdSet.containsKey(invoiceRecord.Association__r.Email_Sender_Address__c)) {
                    mapAssSenderAddressVSAssIdSet.put(invoiceRecord.Association__r.Email_Sender_Address__c, 
                    new Set<Id>{invoiceRecord.Association__c});
                } else {
                    Set<Id> setPreviousAssId = new Set<Id>();
                    setPreviousAssId = mapAssSenderAddressVSAssIdSet.get(invoiceRecord.Association__r.Email_Sender_Address__c);
                    setPreviousAssId.add(invoiceRecord.Association__c);
                    mapAssSenderAddressVSAssIdSet.put(invoiceRecord.Association__r.Email_Sender_Address__c, setPreviousAssId);
                }
            } else {
                // Populate set if Sender Email address is blank on Association
                setAssIdWithBlankSenderAddress.add(invoiceRecord.Association__c);
                // Populate set of Ass Id which will have default sender email address
                setAssIdWithDefaultSenderAdd.add(invoiceRecord.Association__c);
            } 
        } // End of for
        System.debug('mapAssIdVSLeadId========'+mapAssIdVSLeadId);

        // Fetch the organization-wide email addresses
        listOrgWideAddress = [SELECT Id,
                                     Address
                                FROM OrgWideEmailAddress];
        System.debug('listOrgWideAddress======='+listOrgWideAddress);
         // Iterate over org-wide address records
        for(OrgWideEmailAddress orgWideAddRec : listOrgWideAddress) {
            // Populate AssociationId VS OrgWideId map
            if(!mapAssSenderAddressVSAssIdSet.isEmpty() && 
               mapAssSenderAddressVSAssIdSet.containsKey(orgWideAddRec.Address)) {
                for(Id assId : mapAssSenderAddressVSAssIdSet.get(orgWideAddRec.Address)) {
                    mapAssociationIdVSOrgWideId.put(assId, orgWideAddRec.Id);
                } // End of inner for
            } // End of if
            // Get org-wide address record Id for default sender email address 
            if(String.isNotBlank(defaultSenderAddress) && String.isNotBlank(String.valueOf(orgWideAddRec.Address)) && 
               defaultSenderAddress.equalsIgnoreCase(String.valueOf(orgWideAddRec.Address))) {
                defaultSendAddOrgWideId = orgWideAddRec.Id;
            } // End of if
        } // End of outer for

        // Iterate over mapAssSenderAddressVSAssIdSet map
        for(String assSenderAddress : mapAssSenderAddressVSAssIdSet.keySet()) {
            for(Id assId : mapAssSenderAddressVSAssIdSet.get(assSenderAddress)) {
                // Set default sender address for those Association whose Sender Email address is not blank
                // but is not present in org-wide default address
                if(!mapAssociationIdVSOrgWideId.isEmpty() && !mapAssociationIdVSOrgWideId.containsKey(assId)) {
                    // Populate map of Association Id VS Sender Address
                    mapAccIdVSFinalOrgWideAddId.put(assId, defaultSendAddOrgWideId);
                    // Populate set of Ass Id which will have default sender email address
                    setAssIdWithDefaultSenderAdd.add(assId);
                } else if(!mapAssociationIdVSOrgWideId.isEmpty() && mapAssociationIdVSOrgWideId.containsKey(assId) &&
                          !mapAssIdVSLeadId.isEmpty() && !mapAssIdVSLeadId.containsKey(assId)) {
                    mapAssIdVSLeadId.remove(assId);
                }// End of if
            }
        } // End of for
        System.debug('mapAssIdVSLeadId========'+mapAssIdVSLeadId);

        // Set default sender address for those Association whose sender email address is blank
        for(Id associationId : setAssIdWithBlankSenderAddress) {
          // Populate map of Association Id VS Sender Address
          mapAccIdVSFinalOrgWideAddId.put(associationId, defaultSendAddOrgWideId);                                                        
        } // End of for
        System.debug('mapAccIdVSFinalOrgWideAddId before merging======='+mapAccIdVSFinalOrgWideAddId);

        // Call method so as to check if the Association's sender email address is verified or not
        getAssSenderAddressResultMap = AutomatedRenewalsCongaBatchHandler.verifyAssociationSenderAddress(mapAssociationIdVSOrgWideId, mapAssIdVSLeadId, defaultSendAddOrgWideId);
        System.debug('getAssSenderAddressResultMap========'+getAssSenderAddressResultMap);
        System.debug('AutomatedRenewalsCongaBatchHandler.setTargetObjId =============='+AutomatedRenewalsCongaBatchHandler.setTargetObjId);
        System.debug('setAssIdWithDefaultSenderAdd before merging======'+setAssIdWithDefaultSenderAdd);
        // Populate set of Ass Id which will have default sender email address
        if(!AutomatedRenewalsCongaBatchHandler.setTargetObjId.isEmpty()) {
            setAssIdWithDefaultSenderAdd.addAll(AutomatedRenewalsCongaBatchHandler.setTargetObjId);
        } // End of if
        System.debug('setAssIdWithDefaultSenderAdd after merging======'+setAssIdWithDefaultSenderAdd);

        // Set the final org-wide address id for Association from the obtained result
        for(Id assoId : getAssSenderAddressResultMap.KeySet()) {
            mapAccIdVSFinalOrgWideAddId.put(assoId, getAssSenderAddressResultMap.get(assoId));                                                          
        } // End of for  
        System.debug('mapAccIdVSFinalOrgWideAddId after merging======='+mapAccIdVSFinalOrgWideAddId);
        // Call Queueable
        System.enqueueJob(new SendInvoiceEmailsToLeadQueueable(setAssIdWithDefaultSenderAdd, defaultSendAddOrgWideId,
                                                               listInvoiceRecords, mapAccIdVSFinalOrgWideAddId));
    }
    /**
    * Method Name : sendOnBoardingInvoiceEmails
    * Parameters  : param1: List<Invoice__c>
    * Description : Used to send the email with Invoice as attachements to related Leads.
    * Created By  : VennScience_BFL_Amruta                             
    * Created On  : 27112019         
    **/
    public static void sendOnBoardingInvoiceEmails(List<Invoice__c> listInvoice) { 

        System.debug('Inside sendOnBoardingInvoiceEmails');
        // Variable Declarations
        Set<Id> setSuccesfulAssId = new Set<Id>();
        String getInvoiceURLToSend = '';
        String getBaseURL1 = 'https://composer.congamerge.com/composer8/index.html'+
                               '?sessionId='+UserInfo.getSessionId()+
                               '&serverUrl='+EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm()+
                               '/services/Soap/u/29.0/'+UserInfo.getOrganizationId(), 'UTF-8');

        // Iterate over Invoice records
        for(Invoice__c invoiceRecord : listInvoice) {
            getInvoiceURLToSend = getBaseURL1;
            getInvoiceURLToSend = getInvoiceURLToSend+
                                  '&Id='+invoiceRecord.Id+
                                  '&TemplateId=a0n5C000000K9lb'+
                                  '&APIMode=12'+
                                  '&QueryId=[InvoiceLineItem]a0f5C000000jQy4QAE,[Association]a0f5C000000jQy5QAE,[Account]a0f5C000000jQy9QAE'+
                                  '&DefaultPDF=1'+
                                  '&CongaEmailTemplateId=a0e5C000000OFtV'+
                                  '&EmailAdditionalTo='+invoiceRecord.Lead__c;
            System.debug('invoiceRecord.Additional_Contact__c'+invoiceRecord.Additional_Contact__c);
            // Set the Additional Contact Id in EmailTo parameter
            if(invoiceRecord.Additional_Contact__c != null) {
                System.debug('invoiceRecord.Additional_Contact__c'+invoiceRecord.Additional_Contact__c);
                getInvoiceURLToSend = getInvoiceURLToSend+''+
                                 ','+invoiceRecord.Additional_Contact__c;
            } // End of inner if
            // Set the Association's Sender Email Address in EmailFrom parameter
            if(!mapAccIdVSFinalOrgWideAddId.isEmpty() && mapAccIdVSFinalOrgWideAddId.containsKey(invoiceRecord.Association__c)) {
                getInvoiceURLToSend = getInvoiceURLToSend+''+
                                 '&EmailFromID='+mapAccIdVSFinalOrgWideAddId.get(invoiceRecord.Association__c);
            } // End of inner if
            System.debug('Invoice Send URL that needs to be used==='+getInvoiceURLToSend);
            // Make callout to Conga in order to send the On Boarding emails to Lead with related Invoice as attachment
            if(String.isNotBlank(getInvoiceURLToSend)) {
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint(getInvoiceURLToSend);
                req.setMethod('GET');
                req.setTimeout(60000);
                
                // Send the request, and return a response
                HttpResponse res = http.send(req);
            
                System.debug(res);
                System.debug('Callout response:::::'+res.getStatus() + ' => ' + res.getBody());
                System.debug('get error======'+res.getBody());
                if(res.getStatusCode() == 200) {
                    System.debug('Callout made successfully:::::'+res.getStatus() + ' => ' + res.getBody());
                    System.debug('get body======'+res.getBody());
                    // Populate set of Association Id for which emails have been sent
                    setSuccesfulAssId.add(invoiceRecord.Association__c);
                } 
            } // End of inner if
        } // End of for
        // Call method in order to send error alert to Admin users for those Association whose Sender Email Address
        // was blank/not present in org-wide default address/unverified in org-wide default address
        AutomatedRenewalsCongaQueueable queueableInstance = new AutomatedRenewalsCongaQueueable();
        queueableInstance.sendErrorAlertToAdmins(setAssIdWithDefaultSenderAdd, setSuccesfulAssId, defaultSendAddOrgWideId, true);
    }
}