/*
	Created Date - 14/10/2019
	Created By - Manas
	Description - Member Directory Component Add in Community
	Ticket - T-000724
	TestClass - MemberDirectoryCTRL_Test
*/
public without sharing class MemberDirectoryCTRL {
    public class wrapperClass{
        @AuraEnabled public Account objAcc{get;set;}
        @AuraEnabled public Boolean isCheck{get;set;}
        @AuraEnabled public Set<String> RepresentativeAccId{get;set;}
        public wrapperClass(Account objAcc,Boolean isCheck,Set<String> RepresentativeAccId){
            this.objAcc = objAcc;
            this.isCheck = isCheck;
            this.RepresentativeAccId = RepresentativeAccId;
        }
    }
    @AuraEnabled
    public static List<Account> AccountsMemberRecord(String RecId) {
        Id CorporateRepresentativeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
        List<Account> objAccList = [Select Association__c,Id,Photo_Visibility__pc,LastName,Name,Name_Visibility__pc,Join_Date__c,Billing_Contact__pc,Association_Account__c,
                                    Member_Category__pc,Legal_Contact__pc,Association_Role__c,Role_Visibility__pc,PersonEmail,Email_Visibility__pc,Phone,Phone_Visibility__pc
                                    from Account where Association_Account__c =:RecId and RecordTypeId =: CorporateRepresentativeId];
        System.debug('######objAccList###3 '+objAccList);
        return objAccList;
    }
    @AuraEnabled
    public static List<wrapperClass> getAccounts(String RecId,String SearchText,Boolean isSingleChar,String RecName) {
        List<wrapperClass> objWrapList = new List<wrapperClass>(); 
        Set<String> objSetIdAccRepresentative = new set<String>();
        Id CorporateRepresentativeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
        List<User> objUserList = [Select Id,ContactId from User where Id =: UserInfo.getUserId() and ContactId != null];
        if(objUserList.size() > 0){
            List<Contact> objConList = [Select Account.Id,Account.Name,Account.RecordTypeId,Account.Association_Account__c from Contact where Id =:objUserList[0].ContactId and Account.RecordTypeId =:CorporateRepresentativeId];
            System.debug('@@@@@objConListobjConList@@@@@@@222 '+objConList);
            if(objConList.size() > 0){
                Map<String,Account> objMapIdAccRepres = new Map<String,Account>([Select Id,Association_Account__c,RecordTypeId from Account where Association_Account__c =:objConList[0].Account.Association_Account__c and Account.RecordTypeId =:CorporateRepresentativeId]);
                if(objMapIdAccRepres.size() > 0){
                    objSetIdAccRepresentative.AddAll(objMapIdAccRepres.keySet());
                }
            }
        }
        System.debug('###objSetIdAccRepresentative####### '+objSetIdAccRepresentative);
        if(RecName == 'Individual Member'){
            Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(RecName).getRecordTypeId();
            List<Account> objListAccount =  null;
            if(String.isBlank(SearchText)){
                objListAccount = [SELECT Id,LastName,Name_Visibility__pc,Next_Renewal_Date__c,Association_Role__c,Role_Visibility__pc,Termination_Date__c, Name,Join_Date__c,Association_Account__c,Association_Account__r.Name,Phone,Phone_Visibility__pc,PersonMobilePhone,PersonEmail,Email_Visibility__pc,Association__c,PhotoUrl,Photo_Visibility__pc FROM Account 
                                  where (RecordTypeId =: AccRecordTypeId OR RecordTypeId =: CorporateRepresentativeId) and Association__c =:RecId and 
                                  (Termination_Date__c > Today OR Termination_Date__c = NULL) ORDER BY Name];
                for(Account objAcc : objListAccount){
                    objWrapList.add(new wrapperClass(objAcc,false,objSetIdAccRepresentative));
                }
            }else{
                String SearchTextName='';
                if(isSingleChar == false)
                    SearchTextName = '%'+SearchText+'%';
                else
                    SearchTextName = SearchText+'%';
                
                objListAccount = [SELECT Id,LastName,Name_Visibility__pc,Next_Renewal_Date__c,Association_Role__c,Role_Visibility__pc,Name,Termination_Date__c, Join_Date__c,Association_Account__c,Association_Account__r.Name,Phone,Phone_Visibility__pc,PersonMobilePhone,PersonEmail,Email_Visibility__pc,Association__c,PhotoUrl,Photo_Visibility__pc FROM Account 
                                  where (RecordTypeId =: AccRecordTypeId OR RecordTypeId =: CorporateRepresentativeId)  and Association__c =:RecId and 
                                  (Name LIKE :SearchTextName OR MiddleName Like :SearchTextName)
                                  and (Termination_Date__c > Today OR Termination_Date__c = NULL) ORDER BY Name];
                for(Account objAcc : objListAccount){
                    objWrapList.add(new wrapperClass(objAcc,false,objSetIdAccRepresentative));
                }
            }
        }   
        if(RecName == 'Corporate Member'){
            Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(RecName).getRecordTypeId();
            List<Account> objListAccount =  null;
            if(String.isBlank(SearchText)){
                objListAccount = [SELECT Id,LastName,Name_Visibility__pc,Region_Name__c,Next_Renewal_Date__c,Association_Role__c,Role_Visibility__pc,Member_Type__c,Member_Type__r.Name,Termination_Date__c, Name,Join_Date__c,Association_Account__c,Association_Account__r.Name,Phone,Phone_Visibility__pc,PersonMobilePhone,PersonEmail,Email_Visibility__pc,Association__c,PhotoUrl,Photo_Visibility__pc FROM Account 
                                  where RecordTypeId =: AccRecordTypeId and Company_Association_Membership__c =:RecId and 
                                  (Termination_Date__c > Today OR Termination_Date__c = NULL) ORDER BY Name];
                for(Account objAcc : objListAccount){
                    objWrapList.add(new wrapperClass(objAcc,false,objSetIdAccRepresentative));
                }
            }else{
                String SearchTextName='';
                if(isSingleChar == false)
                    SearchTextName = '%'+SearchText+'%';
                else
                    SearchTextName = SearchText+'%';
                
                objListAccount = [SELECT Id,LastName,Name_Visibility__pc,Region_Name__c,Next_Renewal_Date__c,Association_Role__c,Role_Visibility__pc,Name,Member_Type__c,Member_Type__r.Name,Termination_Date__c, Join_Date__c,Association_Account__c,Association_Account__r.Name,Phone,Phone_Visibility__pc,PersonMobilePhone,PersonEmail,Email_Visibility__pc,Association__c,PhotoUrl,Photo_Visibility__pc FROM Account 
                                  where RecordTypeId =: AccRecordTypeId and Company_Association_Membership__c =:RecId and 
                                  (Name LIKE :SearchTextName OR MiddleName Like :SearchTextName)
                                  and (Termination_Date__c > Today OR Termination_Date__c = NULL) ORDER BY Name];
                for(Account objAcc : objListAccount){
                    objWrapList.add(new wrapperClass(objAcc,false,objSetIdAccRepresentative));
                }
            }
        }   
        System.debug('########objWrapList## '+objWrapList);
        return objWrapList;
    }
}