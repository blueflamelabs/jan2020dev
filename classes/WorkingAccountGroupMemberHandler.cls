public class WorkingAccountGroupMemberHandler{

   /*
       Created By - Manas
       Created Date - 13/11/2019
       Description  - Create or Update Group Member in Working Group Object when Account Status !- Inactive and Board Member = true
       Ticket - T-000884
   */
   public static void insertWorkingGroup(List<Working_Group__c> objWorkGroupList){
       
       /*
           Created By - Manas
           Created Date - 13/11/2019
           Description  - Methord user for when Working Group Create Board Member Group True then Create Member Group
           Ticket - T-000884
       */
   
       List<Working_Group__c> objWorkGrpList = new List<Working_Group__c>();
       for(Working_Group__c objWorkGrp : objWorkGroupList){
           if(objWorkGrp.Board_Member_Group__c == true){
               objWorkGrpList.add(objWorkGrp);
           }
       }
       if(objWorkGrpList.size() > 0){
            Id InviMemRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
            Id CorRepreRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
            Id StafRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Staff').getRecordTypeId();
            List<Contact> objConList = [Select Id,Account.Board_Member__pc,Account.Status__c from Contact where ((Account.Board_Member__pc = true and Account.Status__c != 'Inactive')
                                         and (Account.RecordTypeId =: InviMemRecTypeId OR Account.RecordTypeId =: CorRepreRecTypeId OR Account.RecordTypeId =: StafRecordTypeId))];
            
            List<Group_Member__c> GroupMemberList = new List<Group_Member__c>();                        
            for(Working_Group__c objWorkGP : objWorkGrpList){
                for(Contact objCon : objConList){
                    Group_Member__c objGrMem = new Group_Member__c();
                    objGrMem.Working_Group__c = objWorkGP.Id;
                    objGrMem.Start_Date__c = Date.today();
                    objGrMem.Status__c = 'Active';
                    objGrMem.Working_Group_Role__c = 'Member';
                    objGrMem.Grou_Member__c = objCon.Id;
                    objGrMem.Voting_Level__c = 'Primary';
                    GroupMemberList.add(objGrMem);
                }
            }
            if(GroupMemberList.size() > 0)
                insert GroupMemberList;
       }
   }
   
    public static void updateWorkingGroup(List<Working_Group__c> objWorkGroupList){
       
       /*
           Created By - Manas
           Created Date - 13/11/2019
           Description  - Methord user for when Working Group Update Board Member Group True then Create Member Group
           Ticket - T-000884
       */
   
       List<Working_Group__c> objWorkGrpList = new List<Working_Group__c>();
       Set<Id> objWorkGruId = new Set<Id>();
       for(Working_Group__c objWorkGrp : objWorkGroupList){
           if(objWorkGrp.Board_Member_Group__c == true){
               objWorkGrpList.add(objWorkGrp);
               objWorkGruId.add(objWorkGrp.Id);
           }
       }
       if(objWorkGrpList.size() > 0){
            Id InviMemRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
            Id CorRepreRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Representative').getRecordTypeId();
            Id StafRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Staff').getRecordTypeId();
            Map<Id,Contact> objConMap = new Map<Id,Contact>([Select Id,Account.Board_Member__pc,Account.Status__c from Contact where ((Account.Board_Member__pc = true and Account.Status__c != 'Inactive')
                                         and (Account.RecordTypeId =: InviMemRecTypeId OR Account.RecordTypeId =: CorRepreRecTypeId OR Account.RecordTypeId =: StafRecordTypeId))]);
            
            Map<String,Group_Member__c> objMapAccWorGroIdGrouMem = new Map<String,Group_Member__c>();
            List<Group_Member__c> objGroupMembr = [Select Id,End_Date__c,Status__c,Working_Group_Role__c,Working_Group__c,Grou_Member__c,Start_Date__c from Group_Member__c where Working_Group__c IN : objWorkGruId and Grou_Member__c IN : objConMap.keySet()];
            for(Group_Member__c objGrouMem : objGroupMembr){
                objMapAccWorGroIdGrouMem.put(objGrouMem.Grou_Member__c+''+objGrouMem.Working_Group__c,objGrouMem);
            }
            
            List<Group_Member__c> GroupMemberList = new List<Group_Member__c>();                        
            for(Working_Group__c objWorkGP : objWorkGrpList){
                for(Contact objCon : objConMap.values()){
                    if(!objMapAccWorGroIdGrouMem.keySet().Contains(objCon.Id+''+objWorkGP.Id)){
                        Group_Member__c objGrMem = new Group_Member__c();
                        objGrMem.Working_Group__c = objWorkGP.Id;
                        objGrMem.Start_Date__c = Date.today();
                        objGrMem.Status__c = 'Active';
                        objGrMem.Working_Group_Role__c = 'Member';
                        objGrMem.Grou_Member__c = objCon.Id;
                        objGrMem.Voting_Level__c = 'Primary';
                        GroupMemberList.add(objGrMem);
                    }else{
                        Group_Member__c objGrMem = objMapAccWorGroIdGrouMem.get(objCon.Id+''+objWorkGP.Id);
                        System.debug('########objGrMemobjGrMemobjGrMem### '+objGrMem);
                        if(objGrMem.Status__c != 'Active'){
                            objGrMem.Start_Date__c = Date.today();
                            objGrMem.Status__c = 'Active';
                            objGrMem.Voting_Level__c = 'Primary';
                            objGrMem.End_Date__c = null;
                            GroupMemberList.add(objGrMem);
                        }
                    }
                }
            }
            if(GroupMemberList.size() > 0)
                upsert GroupMemberList;
       }
   }
   
}