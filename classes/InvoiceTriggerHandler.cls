/*********************************************************************************************************************************************************
*     Version    CreateDate       CreatedBy               ModifiedDate   ModifiedBy  Description
*     1.0        26112019         VennScience_BFL_Amruta                             This is the handler class for Invoice apex trigger.                               
**********************************************************************************************************************************************************/
public class InvoiceTriggerHandler {
    // Variable Declarations
    public static Boolean isRecursiveCall = false;

    /**
    * Method Name : filterInvoiceRecords
    * Parameters  : param1: List<Invoice__c>, Map<Id, Invoice__c>
    * Description : Used to filter the Invoice records.
    * Created By  : VennScience_BFL_Amruta                             
    * Created On  : 26112019         
    **/
    public static void filterInvoiceRecords(List<Invoice__c> listInvoice, Map<Id, Invoice__c> mapOldInvoice) { 
        // Variable Declarations
        List<Invoice__c> listFilteredInvoices = new List<Invoice__c>();
        Set<Id> setInvoiceId = new Set<Id>();

        System.debug('Trigger called');
        // Iterate over new version of Invoice records
        for(Invoice__c invoiceRecord : listInvoice) {
            // Check if this is an update event
            if(!mapOldInvoice.isEmpty() && mapOldInvoice.containsKey(invoiceRecord.Id)) {
                // Check if current Invoice has line items created and is related to a Lead
                System.debug('Invoice record===='+invoiceRecord);
                if(invoiceRecord.Lead__c != null && invoiceRecord.Billing_Member__c == null &&
                   mapOldInvoice.get(invoiceRecord.Id).Invoice_Line_Count__c != invoiceRecord.Invoice_Line_Count__c &&
                   invoiceRecord.Invoice_Line_Count__c > 0 &&
                   invoiceRecord.Association__c != null &&
                   !invoiceRecord.Is_Onboarding_Email_Sent__c) {
                    // Add filtered invoice records to list
                    listFilteredInvoices.add(invoiceRecord);
                    System.debug('Inside if');
                    // Populate set of filtered Invoice record Id
                    setInvoiceId.add(invoiceRecord.Id);
                } // End of inner if
            } // End of update event check if
        } // End of for 
        // Call method to process the filtered Invoice records
        // processFilteredInvoiceRecords(listFilteredInvoices); 
        // Call batch class for further processing
        if(!setInvoiceId.isEmpty()) {
            Database.executeBatch(new SendInvoiceEmailsToLeadBatch(setInvoiceId), 80);
        } // End of if
        
    }
    /**
    * Method Name : processFilteredInvoiceRecords
    * Parameters  : param1: List<Invoice__c>, Map<Id, Invoice__c>
    * Description : Used to process the filtered the Invoice records.
    * Created By  : VennScience_BFL_Amruta                             
    * Created On  : 27112019         
    **/
    public static void processFilteredInvoiceRecords(List<Invoice__c> listFilteredInvoice) { 
        System.debug('Inside processFilteredInvoiceRecords');
        // Variables Declarations
        Integer mapIndex = 0;
        Integer indexStartCounter = 1;
        Integer indexEndCounter = 99;
        Set<Id> setInvoiceId = new Set<Id>();
        Map<Integer, List<Invoice__c>> mapIndexVSInvoiceList = new Map<Integer, List<Invoice__c>>();

        //listFilteredInvoice = new List<Invoice__c>();

        for(Integer i = 0 ; i < 15000 ; i++) {

            listFilteredInvoice.add(listFilteredInvoice[0]);
        }

        // Iterate over filtered invoice record list
        for(Invoice__c invoiceRec : listFilteredInvoice) {
            setInvoiceId.add(invoiceRec.Id);
            if(indexStartCounter < indexEndCounter) {
                if(!mapIndexVSInvoiceList.containsKey(mapIndex)) {
                    mapIndexVSInvoiceList.put(mapIndex, new List<Invoice__c>{invoiceRec});
                } else {
                    List<Invoice__c> listPreviousInvoice = new List<Invoice__c>();
                    listPreviousInvoice = mapIndexVSInvoiceList.get(mapIndex);
                    listPreviousInvoice.add(invoiceRec);
                    mapIndexVSInvoiceList.put(mapIndex, listPreviousInvoice);
                }
                indexStartCounter++;
            } else {
                indexStartCounter = 1;
                mapIndex++;
                mapIndexVSInvoiceList.put(mapIndex, new List<Invoice__c>{invoiceRec});
            }
        } // End of for

        System.debug('mapIndexVSInvoiceList ==== ' + mapIndexVSInvoiceList);

        for(Integer index : mapIndexVSInvoiceList.keySet()) {
            System.debug('map value size========'+mapIndexVSInvoiceList.get(index).size());

            String cronExp = System.now().addMinutes(1).format('ss mm HH dd MM ? yyyy');
            System.debug('Datetime.now().milliSecond()========'+Datetime.now().milliSecond());
            //System.schedule('SendInvoiceEmailsToLead' + System.now()+''+Datetime.now().milliSecond(), cronExp, new SendInvoiceEmailsToLead(mapIndexVSInvoiceList.get(index)));
        } // End of for

    }
}