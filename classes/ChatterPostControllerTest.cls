@isTest
public class ChatterPostControllerTest {
    @testSetup static void setupData() {
    /*    UserRole userRoleObj = new UserRole(DeveloperName = 'MyCustomRole', Name = 'CEO');
        insert userRoleObj;
        User userObj = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'LastName',
            Email = 'test@123.com',
            Username = 'test@000.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'Developer',
            Alias = 'tst',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = userRoleObj.Id,
            ContactId = TestDataFactory.createContact().Id 
        );
        insert userObj;
        
        Working_Group__c objWorkingGrp = new Working_Group__c();
        objWorkingGrp.Create_Chatter_Group__c = TRUE ;
        insert objWorkingGrp;
        
        Group_Member__c objGroupMem = new Group_Member__c();
        objGroupMem.Working_Group__c = objWorkingGrp.Id;
        objGroupMem.Grou_Member__c = TestDataFactory.createContact().Id;
        objGroupMem.Working_Group_Role__c = 'Co-Chair';
        insert objGroupMem; */
    }
    
    @isTest static void getPublisherUserTest() {
        TestDataFactory.UserWrapper objGetUseData = TestDataFactory.createPartnerCommunityUserTest1();
        System.runAs(objGetUseData.adminUser) {
            Working_Group__c objWorkingGrp = new Working_Group__c();
            objWorkingGrp.Create_Chatter_Group__c = TRUE ;
            insert objWorkingGrp;
            
            Group_Member__c objGroupMem = new Group_Member__c();
            objGroupMem.Working_Group__c = objWorkingGrp.Id;
            objGroupMem.Grou_Member__c = TestDataFactory.createContact().Id;
            objGroupMem.Working_Group_Role__c = 'Co-Chair';
            insert objGroupMem;
            
            ChatterPostController.getPublisherUser(objWorkingGrp.Id); 
            ChatterPostController.createFeedItemRecord('test 1',objWorkingGrp.Id);
            //System.debug('result >> '+result);
            
        }
        
        
    }
}
/*        Id  workingGrpId = [SELECT Id FROM Working_Group__c ].Id;
Id  profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
Id userId = [SELECT Id FROM User WHERE ProfileId = :profileId].Id; 
User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];*/