public without sharing class LogAttendanceController {
    @AuraEnabled
    public static EventWrapper fetchEvent() {
        Set<Id> eventIdSet = new Set<Id>();
        Set<String> participationEventIdSet = new Set<String>();
        
        User loggedInUser = [SELECT Id,
                                    AccountId 
                               FROM User 
                              WHERE Id = :UserInfo.getUserId()];
        System.debug('=========loggedInUser=========' + loggedInUser);

        // get users event relation to get related events
        for(EventRelation eventRelationRec : [SELECT Id,
                                                     AccountId, 
                                                     Account.Name, 
                                                     EventId,
                                                     IsInvitee,
                                                     IsParent,
                                                     Status 
                                                FROM EventRelation 
                                               WHERE AccountId = :loggedInUser.AccountId]
        ){
            eventIdSet.add(eventRelationRec.EventId);
        }
        System.debug('=========related eventIdSet=========' + eventIdSet);

        // create a instance of wrapper class.
        EventWrapper eventWrapperInstance =  new EventWrapper();
        
        eventWrapperInstance.keyPrefixOfWorkingGroup = Working_Group__c.sobjecttype.getDescribe().getKeyPrefix();
        System.debug('=========eventWrapperInstance.keyPrefixOfWorkingGroup=========' + eventWrapperInstance.keyPrefixOfWorkingGroup);
        
        // Fetch related participation records
        for(Participation__c participationRec : [SELECT Id, 
                                                           Name,
                                                           Event_ID__c, 
                                                           Member__c
                                                      FROM Participation__c 
                                                      WHERE Member__c = :loggedInUser.AccountId 
        ]) {
            participationEventIdSet.add(participationRec.Event_ID__c);
        }
        System.debug('=========participationEventIdSet========' + participationEventIdSet);
        
        eventWrapperInstance.eventList = [SELECT Id,
                                                 WhatId,
                                                 What.Name,
                                                 Subject,
                                                 StartDateTime,
                                                 Location,
                                                 Description,
                                                 zoom_app__Zoom_Event__c,
                                                 zoom_app__Zoom_Event__r.zoom_app__Zoom_Meeting_Id__c 
                                            FROM Event 
                                           WHERE Id IN :eventIdSet 
                                             //AND StartDateTime >= LAST_N_DAYS:7
                                             AND Id NOT IN :participationEventIdSet
                                        ORDER BY Subject ];
                                          
        System.debug('=========eventWrapperInstance.eventList=========' + eventWrapperInstance.eventList);
        // return the wrapper class instance .
        return eventWrapperInstance;
    }

    @AuraEnabled
    public static EventWrapper CreateParticipation(String eventId) { 
        Set<Id> eventIdSet = new Set<Id>();
        Set<String> participationEventIdSet = new Set<String>();
        
        System.debug('=========CreateParticipation===eventId======' + eventId);
        String keyPrefixOfWorkingGroup = Working_Group__c.sobjecttype.getDescribe().getKeyPrefix();
        String keyPrefixOfAssociation = Association__c.sobjecttype.getDescribe().getKeyPrefix();

        User loggedInUser = [SELECT Id,
                                    AccountId,
                                    Account.Name
                               FROM User 
                              WHERE Id = :UserInfo.getUserId()];
        System.debug('=========loggedInUser=========' + loggedInUser);

        String associationId = '';
        Event event = [ SELECT Id,
                                WhatId,
                                What.Name,
                                Subject,
                                StartDateTime,
                                Location,
                                Description,
                                zoom_app__Zoom_Event__c,
                                zoom_app__Zoom_Event__r.zoom_app__Zoom_Meeting_Id__c 
                        FROM Event 
                        WHERE Id = :eventId];
        System.debug('=========event=========' + event);

        String relatedToId = event.WhatId;
        System.debug('=========relatedToId=========' + relatedToId);
        System.debug('=========ass key prefix=========' + relatedToId.startsWith(keyPrefixOfAssociation));
        if(!relatedToId.startsWith(keyPrefixOfAssociation) && relatedToId.startsWith(keyPrefixOfWorkingGroup)) {
            Working_Group__c workingGroup = [SELECT Id, Association__c FROM Working_Group__c WHERE Id = :relatedToId];
                associationId = workingGroup.Association__c != null ? workingGroup.Association__c : null;
        }
        System.debug('=========associationId=========' + associationId);
        String strGetAssId = relatedToId.startsWith(keyPrefixOfAssociation) ? relatedToId : associationId;
        System.debug('strGetAssId========'+strGetAssId);
        EventWrapper eventWrapperInstance =  new EventWrapper();
        eventWrapperInstance.isParticipationCreated = false;
        Participation__c participationRecord = new Participation__c();
        participationRecord.Event_ID__c = event.Id;
        participationRecord.Event_Name__c = event.Subject;
        participationRecord.Member__c = loggedInUser.AccountId;
        participationRecord.Participant__c = loggedInUser.Account.Name;
        participationRecord.Type__c = 'Attendance';
        participationRecord.Status__c = 'Pending';
        participationRecord.Attended__c = true;
        participationRecord.Working_Group__c = relatedToId.startsWith(keyPrefixOfWorkingGroup) ? event.WhatId : null;
        participationRecord.Association__c = relatedToId.startsWith(keyPrefixOfAssociation) ? relatedToId : associationId; 
        participationRecord.Meeting_ID__c = event.zoom_app__Zoom_Event__r.zoom_app__Zoom_Meeting_Id__c;
        try{
            System.debug('====BEFORE=====participationRecord=========' + participationRecord);
            insert participationRecord;
            eventWrapperInstance.participationRecord = participationRecord;
            eventWrapperInstance.participationRelatedEvent = event;
            System.debug('=========participationRecord=========' + participationRecord);
            eventWrapperInstance.isParticipationCreated = true;
            for(Participation__c participationRec : [SELECT Id, 
                                                           Name,
                                                           Event_ID__c, 
                                                           Member__c
                                                      FROM Participation__c 
                                                      WHERE Member__c = :loggedInUser.AccountId 
            ]) {
                participationEventIdSet.add(participationRec.Event_ID__c);
            }
            System.debug('=========participationEventIdSet========' + participationEventIdSet);
            // get users event relation to get related events
            for(EventRelation eventRelationRec : [SELECT Id,
                                                        AccountId, 
                                                        Account.Name, 
                                                        EventId,
                                                        IsInvitee,
                                                        IsParent,
                                                        Status 
                                                    FROM EventRelation 
                                                WHERE AccountId = :loggedInUser.AccountId]
            ){
                eventIdSet.add(eventRelationRec.EventId);
            }
            System.debug('eventIdSet========'+eventIdSet);
            eventWrapperInstance.eventList = new List<Event>();
            System.debug('eventWrapperInstance.eventList before========'+eventWrapperInstance.eventList);
            System.debug('eventWrapperInstance.eventList before size========'+eventWrapperInstance.eventList.size());
            
            eventWrapperInstance.eventList = [SELECT Id,
                                                    WhatId,
                                                    What.Name,
                                                    Subject,
                                                    StartDateTime,
                                                    Location,
                                                    Description,
                                                    zoom_app__Zoom_Event__c,
                                                    zoom_app__Zoom_Event__r.zoom_app__Zoom_Meeting_Id__c 
                                               FROM Event 
                                              WHERE Id IN :eventIdSet 
                                                //AND StartDateTime >= LAST_N_DAYS:7
                                                AND Id NOT IN :participationEventIdSet
                                            ORDER BY Subject ];
            System.debug('eventWrapperInstance.eventList after========'+eventWrapperInstance.eventList);
            System.debug('eventWrapperInstance.eventList after size========'+eventWrapperInstance.eventList.size());
        } catch(Exception e) {
            System.debug('=========Exception=========' + e);
            System.debug('=========Exception Message=========' + e.getMessage());
        }
        return eventWrapperInstance;
    }
        
    // create a wrapper class with @AuraEnabled Properties    
    public class EventWrapper {
        @AuraEnabled public List<Event> eventList {get;set;}
        @AuraEnabled public String keyPrefixOfWorkingGroup {get;set;}
        @AuraEnabled public Participation__c participationRecord {get;set;}
        @AuraEnabled public Boolean isParticipationCreated {get;set;}
        @AuraEnabled public Event participationRelatedEvent {get;set;}
    }
}