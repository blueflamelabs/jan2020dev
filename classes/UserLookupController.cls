public with sharing class UserLookupController {
     
    @AuraEnabled(cacheable=true)
    public static List<RecordsData> fetchRecords( String objectName, String filterField, String searchString ) {
        List<RecordsData> recordsDataList = new List<RecordsData>();
       try {
            String query = 'SELECT Id, ' + filterField+
                ' FROM '+objectName+
                ' WHERE ContactId != null AND IsActive = true AND '+filterField+' LIKE ' + '\'' + String.escapeSingleQuotes(searchString.trim()) + '%\'' + ' LIMIT 50000';
            
            for(SObject s : Database.query(query)){
                RecordsData recordsData = new RecordsData();
                recordsData.value = String.valueOf(s.get('id'));
                recordsData.label = String.valueOf(s.get(filterField));
                recordsDataList.add(recordsData);
            } 
        } catch (Exception err) {
            if ( String.isNotBlank( err.getMessage() ) && err.getMessage().contains( 'error:' ) ) {
                throw new AuraHandledException(err.getMessage().split('error:')[1].split(':')[0] + '.');
            } else {
                throw new AuraHandledException(err.getMessage());
            }
        }
        return recordsDataList;
    }
    /*
    @AuraEnabled
    public static List<RecordsData> fetchCurrentUserRecord(String AccountIds){
        List<RecordsData> recordsDataList = new List<RecordsData>();
        Account acc = [Select Id,OwnerId,Owner.Name From Account where Id =: AccountIds ];
        user u = [Select Id, Name,Title from user where id  = : acc.OwnerId]; 
                RecordsData recordsData = new RecordsData();
                recordsData.value = u.Id;
                recordsData.label = u.Name;
                recordsDataList.add(recordsData);
        system.debug('recordsDataList ' +recordsDataList);
        return recordsDataList; 
    }
   @AuraEnabled
    public static List<RecordsData> fetchCurrentUserCRERecord(){
        List<RecordsData> recordsDataList = new List<RecordsData>();
       // Account acc = [Select Id,OwnerId,Owner.Name From Account where Id =: AccountIds ];
        user u = [Select Id, Name,Title from user where id  = : userInfo.getUserId()]; 
                RecordsData recordsData = new RecordsData();
                recordsData.value = u.Id;
                recordsData.label = u.Name;
                recordsDataList.add(recordsData);
        system.debug('recordsDataList ' +recordsDataList);
        return recordsDataList; 
    }
*/
    public class RecordsData{
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String value {get;set;}
    }
}