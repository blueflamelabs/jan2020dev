/********************************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                 ModifiedDate  ModifiedBy  Description
*     1.0        281119         VennScience_BFL_Amruta                              Used to send the Onboarding Email alert with Invoice as attachement to related Leads.                                                     
*********************************************************************************************************************************************************************/ 
public class SendInvoiceEmailsToLeadQueueable implements Queueable, Database.AllowsCallouts {
    // Variable Declarations
    public Set<Id> setAssIdWithDefaultSenderAdd = new Set<Id>();
    public Id defaultSendAddOrgWideId;
    public List<Invoice__c> listInvoice = new List<Invoice__c>();
    public Map<Id,Id> mapAccIdVSFinalOrgWideAddId = new Map<Id,Id>();

    // Parameterized constructor
    public SendInvoiceEmailsToLeadQueueable(Set<Id> setAssIdWithDefaultSenderAdd, Id defaultSendAddOrgWideId, 
    List<Invoice__c> listInvoice, Map<Id,Id> mapAccIdVSFinalOrgWideAddId) {
        this.setAssIdWithDefaultSenderAdd = setAssIdWithDefaultSenderAdd;
        this.defaultSendAddOrgWideId = defaultSendAddOrgWideId;
        this.listInvoice = listInvoice;
        this.mapAccIdVSFinalOrgWideAddId = mapAccIdVSFinalOrgWideAddId;
    }
    // Interface method
    public void execute(QueueableContext context) {
        System.debug('Inside sendOnBoardingInvoiceEmails');
        // Variable Declarations
        Set<Id> setSuccesfulAssId = new Set<Id>();
        // 101219 - T-000934 - VennScience_BFL_Amruta - Used to store the invoice record id
        Set<Id> setSuccesfulInvoiceId = new Set<Id>();
        String getInvoiceURLToSend = '';
        String getBaseURL1 = 'https://composer.congamerge.com/composer8/index.html'+
                               '?sessionId='+UserInfo.getSessionId()+
                               '&serverUrl='+EncodingUtil.urlEncode(Url.getSalesforceBaseUrl().toExternalForm()+
                               '/services/Soap/u/29.0/'+UserInfo.getOrganizationId(), 'UTF-8');

        // Iterate over Invoice records
        for(Invoice__c invoiceRecord : listInvoice) {
            getInvoiceURLToSend = getBaseURL1;
            getInvoiceURLToSend = getInvoiceURLToSend+
                                  '&Id='+invoiceRecord.Id+
                                  '&TemplateId=a0n5C000000K9lb'+
                                  '&APIMode=12'+
                                  '&QueryId=[InvoiceLineItem]a0f5C000000jQy4QAE,[Association]a0f5C000000jQy5QAE,[Account]a0f5C000000jQy9QAE'+
                                  '&DefaultPDF=1'+
                                  '&CongaEmailTemplateId=a0e5C000000OKO7QAO'+
                                  '&EmailAdditionalTo='+invoiceRecord.Lead__c;
            System.debug('invoiceRecord.Additional_Contact__c'+invoiceRecord.Additional_Contact__c);
            // Set the Additional Contact Id in EmailTo parameter
            if(invoiceRecord.Additional_Contact__c != null && String.isNotBlank(invoiceRecord.Additional_Contact__r.Email_Address__c)) {
                System.debug('invoiceRecord.Additional_Contact__c'+invoiceRecord.Additional_Contact__r.Email_Address__c);
                getInvoiceURLToSend = getInvoiceURLToSend+''+
                                 ','+invoiceRecord.Additional_Contact__r.Email_Address__c;
            } // End of inner if
            // Set the Association's Sender Email Address in EmailFrom parameter
            if(!mapAccIdVSFinalOrgWideAddId.isEmpty() && mapAccIdVSFinalOrgWideAddId.containsKey(invoiceRecord.Association__c)) {
                getInvoiceURLToSend = getInvoiceURLToSend+''+
                                 '&EmailFromID='+mapAccIdVSFinalOrgWideAddId.get(invoiceRecord.Association__c);
            } // End of inner if
            System.debug('Invoice Send URL that needs to be used==='+getInvoiceURLToSend);
            // Make callout to Conga in order to send the On Boarding emails to Lead with related Invoice as attachment
            if(String.isNotBlank(getInvoiceURLToSend)) {
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint(getInvoiceURLToSend);
                req.setMethod('GET');
                req.setTimeout(60000);
                
                // Send the request, and return a response
                HttpResponse res = http.send(req);
            
                System.debug(res);
                System.debug('Callout response:::::'+res.getStatus() + ' => ' + res.getBody());
                System.debug('get error======'+res.getBody());
                if(res.getStatusCode() == 200) {
                    System.debug('Callout made successfully:::::'+res.getStatus() + ' => ' + res.getBody());
                    System.debug('get body======'+res.getBody());
                    // Populate set of Association Id for which emails have been sent
                    setSuccesfulAssId.add(invoiceRecord.Association__c);
                    // 101219 - T-000934 - VennScience_BFL_Amruta - Populate set of Invoice Id 
                    setSuccesfulInvoiceId.add(invoiceRecord.Id);
                } 
            } // End of inner if
        } // End of for
        // Call method in order to send error alert to Admin users for those Association whose Sender Email Address
        // was blank/not present in org-wide default address/unverified in org-wide default address
        AutomatedRenewalsCongaQueueable queueableInstance = new AutomatedRenewalsCongaQueueable();
        queueableInstance.sendErrorAlertToAdmins(setAssIdWithDefaultSenderAdd, setSuccesfulAssId, defaultSendAddOrgWideId, true);
        // 101219 - T-000934 - VennScience_BFL_Amruta - Call method in order to update the Is_Onboarding_Email_Sent__c checkbox
        // on Invoice
        updateInvoice(setSuccesfulInvoiceId);
    }
    /**
    * Method Name : updateInvoice
    * Parameters  : param1: Set<Id>
    * Description : Used to update the Is_Onboarding_Email_Sent__c checkbox on Invoice to true once the on boarding
    *               email is sent.
    * Created By  : VennScience_BFL_Amruta                             
    * Created On  : 10122019         
    **/
    public static void updateInvoice(Set<Id> setInvoiceId) {
        // Variable Declarations
        List<Invoice__c> listInvoiceToUpdate = new List<Invoice__c>();

        System.debug('Inside updateInvoice');
        System.debug('setInvoiceId======='+setInvoiceId);

        // Iterate over set of Invoice Id
        for(Id invoiceId : setInvoiceId) {
            Invoice__c invoiceRecordToUpdate = new Invoice__c(Id = invoiceId);
            invoiceRecordToUpdate.Is_Onboarding_Email_Sent__c = true;
            listInvoiceToUpdate.add(invoiceRecordToUpdate);
        } // End of for

        // Update Invoice record
        try {
            update listInvoiceToUpdate;
            System.debug('Invoice record has been updated');
        } catch(Exception e) {
            System.debug('Error occurred while updating Invoice record:'+e.getMessage());
        }
    }
}