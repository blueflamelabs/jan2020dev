/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy               ModifiedDate   ModifiedBy  Description
*     1.0        241019         VennScience_BFL_Amruta                             This is the handler class for WorkingGroupShare apex trigger.                               
**********************************************************************************************************************************************************/
public class WorkingGroupTriggerHandler {
    // Variable Declarations
    public static Boolean isRecursiveCall = false;
    public static final String MEMBERSTATUS = 'Active';
    
    /**
    * Method Name : filterWorkingGroupRecords
    * Parameters  : param1: List<Working_Group__c>, Map<Id,Working_Group__c>
    * Description : Used to filter the Working Group records as per the required criteria.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 241019
    **/
    public static void filterWorkingGroupRecords(List<Working_Group__c> listNewWorkingGrp, Map<Id,Working_Group__c> mapOldWorkingGrp) {
        System.debug('Inside filterWorkingGroupRecords');   
        // Variable Declarations
        Set<Working_Group__c> listFilteredWorkingGrp = new Set<Working_Group__c>();
        List<Working_Group__c> listOldWorkingGrp = new List<Working_Group__c>();
        Boolean isRecordUpdated = false;

        // 021219 to update related chatter group's access level
        Map<Id, Working_Group__c> mapChatterGorupIdVsWorkingGroup = new Map<Id, Working_Group__c>();

        // Iterate over Trigger.New list
        for(Working_Group__c workingGrpRec : listNewWorkingGrp) {
            // Check if Working group has been updated
            if(!mapOldWorkingGrp.isEmpty() && mapOldWorkingGrp.containsKey(workingGrpRec.Id)) {
                /*
                // Check if chatter group id has been removed
                if(mapOldWorkingGrp.get(workingGrpRec.Id).Chatter_Group__c != workingGrpRec.Chatter_Group__c &&
                   mapOldWorkingGrp.get(workingGrpRec.Id).Chatter_Group__c != null && 
                   workingGrpRec.Chatter_Group__c == null) {
                    isRecordUpdated = true;
                    listOldWorkingGrp.add(mapOldWorkingGrp.get(workingGrpRec.Id));
                } // End of if
                */
                // Check if Create Chatter Group checkbox is updated and is set to true
                if(!mapOldWorkingGrp.get(workingGrpRec.Id).Create_Chatter_Group__c && 
                     workingGrpRec.Create_Chatter_Group__c && String.isBlank(workingGrpRec.Chatter_Group__c)) {
                    listFilteredWorkingGrp.add(workingGrpRec);
                } // End of inner if
                if(mapOldWorkingGrp.get(workingGrpRec.Id).Association__c != workingGrpRec.Association__c) {
                   isRecordUpdated = true;
                   // Check if Association is updated
                   listFilteredWorkingGrp.add(workingGrpRec);
                   listOldWorkingGrp.add(mapOldWorkingGrp.get(workingGrpRec.Id));
                } // End of inner if

                // 021219 if working group type is updated then update access level of related chatter group
                if(mapOldWorkingGrp.get(workingGrpRec.Id).Chatter_Group__c != Null &&
                    String.isNotBlank(workingGrpRec.Type__c) &&
                    mapOldWorkingGrp.get(workingGrpRec.Id).Type__c != workingGrpRec.Type__c
                ) {
                    mapChatterGorupIdVsWorkingGroup.put(workingGrpRec.Chatter_Group__c, workingGrpRec);
                }
            } else if(mapOldWorkingGrp.isEmpty() && workingGrpRec.Association__c != null) {
                // Add all records if it is an after insert event
                listFilteredWorkingGrp.add(workingGrpRec);
            } // End of if-else block
        } // End of for
        
        System.debug('listFilteredWorkingGrp==========='+listFilteredWorkingGrp);
        System.debug('listOldWorkingGrp==========='+listOldWorkingGrp);
        System.debug('mapChatterGorupIdVsWorkingGroup==========='+mapChatterGorupIdVsWorkingGroup); // 021219
        // Call method to createChatterGroups
        if(!listFilteredWorkingGrp.isEmpty()) {
            if(isRecordUpdated) {
                deleteChatterGroup(listOldWorkingGrp);    
            } // End of inner if
            createCommunityChatterGroup(listFilteredWorkingGrp);
        } // End of outer if

        // 021219 Call method to update related chatter group's access level
        if (mapChatterGorupIdVsWorkingGroup.size() > 0) {
            updateChatterGroupAccess(mapChatterGorupIdVsWorkingGroup);
        }
    }

    /**
    * Method Name : updateChatterGroupAccess
    * Parameters  : param1: Map<Id, Id>
    * Description : Used to update Working Group's related chatter group access level
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 021219
    **/
    public static void updateChatterGroupAccess(Map<Id, Working_Group__c> mapChatterGorupIdVsWorkingGroup) { 
        List<CollaborationGroup> collaborationGroupToUpdate = new List<CollaborationGroup>();

        for(CollaborationGroup exitingCollaborationGroup :  [SELECT Id,
                                                                    Name,
                                                                    CollaborationType
                                                               FROM CollaborationGroup
                                                              WHERE Id IN :mapChatterGorupIdVsWorkingGroup.keySet()
        ]) {
            Working_Group__c workingGrpRec = mapChatterGorupIdVsWorkingGroup.get(exitingCollaborationGroup.id);
            Boolean isUpdate = false;
            if(String.isNotBlank(workingGrpRec.Type__c)) {
                System.debug('workingGrpRec.Type__c========='+workingGrpRec.Type__c);
                if(workingGrpRec.Type__c.equalsIgnoreCase('Public') && 
                (!exitingCollaborationGroup.CollaborationType.equalsIgnoreCase('Public'))
                ) {
                    System.debug('Set type to Public');
                    isUpdate = true ;
                    exitingCollaborationGroup.CollaborationType = 'Public'; // Public chatter group
                } else if(workingGrpRec.Type__c.equalsIgnoreCase('Private') && 
                (!exitingCollaborationGroup.CollaborationType.equalsIgnoreCase('Private'))
                ) {
                    System.debug('Set type to Private');
                    isUpdate = true ;
                    exitingCollaborationGroup.CollaborationType = 'Private'; // Private chatter group
                } else if(workingGrpRec.Type__c.equalsIgnoreCase('Unlisted') && 
                (!exitingCollaborationGroup.CollaborationType.equalsIgnoreCase('Unlisted'))
                ) {
                    System.debug('Set type to Unlisted');
                    isUpdate = true ;
                    exitingCollaborationGroup.CollaborationType = 'Unlisted'; // Unlisted chatter group
                } // End of inner if-else block
                if(isUpdate) {
                    collaborationGroupToUpdate.add(exitingCollaborationGroup);
                }
            }
        }

        System.debug('====collaborationGroupToUpdate======= ' + collaborationGroupToUpdate);
        if(!collaborationGroupToUpdate.isEmpty()) {
            try {
                update collaborationGroupToUpdate;
                System.debug('====SUCCESSFULLY UPDATED collaborationGroupToUpdate======= ' + collaborationGroupToUpdate);
            } catch(Exception e) {
                System.debug('====ERROR In UPDATE OF collaborationGroupToUpdate======= ' + collaborationGroupToUpdate);
                System.debug('====ERROR In UPDATE OF e======= ' + e);
            }
        }
    }
    /**
    * Method Name : createCommunityChatterGroup
    * Parameters  : param1: Set<Working_Group__c>
    * Description : Used to create community chatter group when Working Group record is created.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 241019
    **/
    public static void createCommunityChatterGroup(Set<Working_Group__c> listWorkingGroup) {
        // Variable Declarations
        List<CollaborationGroup> listCollaborationGroup = new List<CollaborationGroup>();
        List<CollaborationGroup> listInsertedCollaborationGroup = new List<CollaborationGroup>();
        List<Association__c> listAssociation = new List<Association__c>();
        List<Network> listCommunities = new List<Network>();
        List<Working_Group__c> listWorkingGrpToUpdate = new List<Working_Group__c>();
        List<CollaborationGroupMember> listMembersToInsert = new List<CollaborationGroupMember>();
        List<CollaborationGroupMember> listCollaborationGrpMem = new List<CollaborationGroupMember>();
        // 291119 - T-000938 - VennScience_BFL_Amruta - Used to store the newly inserted chatter group id
        Set<Id> setChatterGroupId = new Set<Id>();
        Map<Id,Id> mapAssIdVSCommunityId = new Map<Id,Id>();
        Map<Id,Id> mapWorkingGrpIdVSAssId = new Map<Id,Id>();
        Map<Id,Id> mapAssIdVSOwnerId = new Map<Id,Id>();
        Map<String,Id> mapGroupNameVSWorkingGrpId = new Map<String,Id>();
        Map<Id,Id> mapWorkingGrpIdVSGroupId = new Map<Id,Id>();
        // 291119 - T-000938 - VennScience_BFL_Amruta - Used to store Working Group Id VS its related chatter grp email
        Map<Id,String> mapWorkingGrpIdVSGroupEmail = new Map<Id,String>();
        Map<Id,Set<Id>> mapAssIdVSWorkingGrpSet = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> mapGrpIdVsMemberIdSet = new Map<Id,Set<Id>>();
        
        //System.debug('Inside createCommunityChatterGroup');
        
        // Iterate over Working Group list
        for(Working_Group__c workingGrpRecord : listWorkingGroup) {
            mapWorkingGrpIdVSAssId.put(workingGrpRecord.Id, workingGrpRecord.Association__c);
            // Populate map of Association Id VS Set of child Working Groups
            if(!mapAssIdVSWorkingGrpSet.containsKey(workingGrpRecord.Association__c)) {
                mapAssIdVSWorkingGrpSet.put(workingGrpRecord.Association__c, new Set<Id>{workingGrpRecord.Id});
            } else {
                Set<Id> setWorkingGrpPreviousId = new Set<Id>();
                setWorkingGrpPreviousId = mapAssIdVSWorkingGrpSet.get(workingGrpRecord.Association__c);
                setWorkingGrpPreviousId.add(workingGrpRecord.Id);
                mapAssIdVSWorkingGrpSet.put(workingGrpRecord.Association__c, setWorkingGrpPreviousId);
            } // End of if-else block
        } // End of for
        //System.debug('mapAssIdVSWorkingGrpSet======='+mapAssIdVSWorkingGrpSet);
        
        // Fetch Association record to get the Community URL
        listAssociation = [SELECT Id,
                                  OwnerId,
                                  Community_URL__c
                             FROM Association__c
                            WHERE Id IN :mapWorkingGrpIdVSAssId.values()];
        //System.debug('listAssociation====='+listAssociation);
        // Fetch Communities
        listCommunities = [SELECT Id,
                                  URLPathPrefix
                             FROM Network];
        
        //System.debug('listCommunities======='+listCommunities);
        // Iterate over Association and community list to map Association's Community URL with corresponding community
        for(Association__c assRecord : listAssociation) {
            // Populate map of Association Id VS Owner Id
            mapAssIdVSOwnerId.put(assRecord.Id, assRecord.OwnerId);
            // Fetch community URL from Association
            for(Network communityRecord : listCommunities) {
                String baseURL = '';
                // Fetch current Community's Base URL
                baseURL = Network.getLoginUrl(communityRecord.Id);
                //System.debug('Community base url======='+baseURL);
                //System.debug('String.valueOf(assRecord.Community_URL__c).trim()======='+String.valueOf(assRecord.Community_URL__c).trim());
                // Check if current community record's URLPathPrefix matches with Association's community path prefix
                if(String.isNotBlank(String.valueOf(assRecord.Community_URL__c)) && String.isNotBlank(baseURL) && 
                   baseURL.contains(String.valueOf(assRecord.Community_URL__c).trim())) {
                    //System.debug('URL matched');
                    mapAssIdVSCommunityId.put(assRecord.Id, communityRecord.Id);
                } // End of if 
            } // End of Network for   
        } // End of Association for
        //System.debug('mapAssIdVSCommunityId======='+mapAssIdVSCommunityId);
        
        // Iterate over Working Group records to create the Chatter Group
        for(Working_Group__c workingGrpRec : listWorkingGroup) {
            // Create Community Chatter Group
            CollaborationGroup communityChatterGroup = new CollaborationGroup();
            communityChatterGroup.Name = workingGrpRec.Working_Group_Abbreviation__c +' Group'; // Define group name here
            // 101219 - T-000736 - VennScience_BFL_Amruta - Disable Auto Archiving
            communityChatterGroup.IsAutoArchiveDisabled = true;

            

            // 291119 - T-000939 - VennScience_BFL_Amruta - Set the chatter group's type based on the value
            // store on the type field of Working Group(Public, Private, Unlisted)
            if(String.isNotBlank(workingGrpRec.Type__c)) {
                System.debug('workingGrpRec.Type__c========='+workingGrpRec.Type__c);
                if(workingGrpRec.Type__c.equalsIgnoreCase('Public')) {
                    System.debug('Set type to Public');
                    communityChatterGroup.CollaborationType = 'Public'; // Public chatter group
                } else if(workingGrpRec.Type__c.equalsIgnoreCase('Private')) {
                    System.debug('Set type to Private');
                    communityChatterGroup.CollaborationType = 'Private'; // Private chatter group
                } else if(workingGrpRec.Type__c.equalsIgnoreCase('Unlisted')) {
                    System.debug('Set type to Unlisted');
                    communityChatterGroup.CollaborationType = 'Unlisted'; // Unlisted chatter group
                } // End of inner if-else block
            } else {
                // 291119 - T-000939 - VennScience_BFL_Amruta - Set the chatter group's type to Public when
                // Working Group's Type field is blank
                communityChatterGroup.CollaborationType = 'Public'; // Public chatter group
            } // End of if-else block to set the Chatter group's Type

            // Set the community Id
            if(!mapAssIdVSCommunityId.isEmpty() && mapAssIdVSCommunityId.containsKey(workingGrpRec.Association__c)) {
                communityChatterGroup.NetworkId = mapAssIdVSCommunityId.get(workingGrpRec.Association__c);    
            } // End of if
            // 121119 - T-000882 - VennScience_BFL_Amruta - Set Association's owner as chatter group's owner 
            if(!mapAssIdVSOwnerId.isEmpty() && mapAssIdVSOwnerId.containsKey(workingGrpRec.Association__c)) {
                communityChatterGroup.OwnerId = mapAssIdVSOwnerId.get(workingGrpRec.Association__c);    
            } // End of if
            listCollaborationGroup.add(communityChatterGroup);
            // Populate mapGroupNameVSWorkingGrpId map
            mapGroupNameVSWorkingGrpId.put(communityChatterGroup.Name, workingGrpRec.Id);
        } // End of Working Group for
        //System.debug('listCollaborationGroup========'+listCollaborationGroup);
        //System.debug('mapAssIdVSOwnerId========'+mapAssIdVSOwnerId);

         // Insert Chatter Group
        if(!listCollaborationGroup.isEmpty()) {
            try {
                insert listCollaborationGroup;
                
                //System.debug('Chatter grp record inserted successfully');
            } catch(Exception e) {
                System.debug('Error Occurred while inserting chatter group records========'+e.getMessage());
            } // End of try-catch block
        } // End of if
        
        // 291119 - T-000938 - VennScience_BFL_Amruta - Iterate over inserted Collaboration Group list
        for(CollaborationGroup groupRecord : listCollaborationGroup) {
            // 291119 - T-000938 - VennScience_BFL_Amruta - Populate the newly inserted chatter group id set
            setChatterGroupId.add(groupRecord.Id);
        } // End of for
        //System.debug('setChatterGroupId======'+setChatterGroupId);

        // 291119 - T-000938 - VennScience_BFL_Amruta - Fetch the chatter group records so as to get the group email
        listInsertedCollaborationGroup = [SELECT Id,
                                                 Name,
                                                 GroupEmail
                                            FROM CollaborationGroup
                                           WHERE Id IN :setChatterGroupId];
        //System.debug('listInsertedCollaborationGroup=========='+listInsertedCollaborationGroup);

        // Iterate over inserted Collaboration Group list
        for(CollaborationGroup groupRecord : listInsertedCollaborationGroup) {
            if(!mapGroupNameVSWorkingGrpId.isEmpty() && mapGroupNameVSWorkingGrpId.containsKey(groupRecord.Name)) {
                mapWorkingGrpIdVSGroupId.put(mapGroupNameVSWorkingGrpId.get(groupRecord.Name), groupRecord.Id);
                // 291119 - T-000938 - VennScience_BFL_Amruta - Populate map of Working grp id VS its related 
                // chatter group email
                mapWorkingGrpIdVSGroupEmail.put(mapGroupNameVSWorkingGrpId.get(groupRecord.Name), groupRecord.GroupEmail);
            } // End of if
        } // End of for
        //System.debug('mapWorkingGrpIdVSGroupEmail========='+mapWorkingGrpIdVSGroupEmail);
        
        // Fetch existing Collaboration Group Members for Chatter Group
        listCollaborationGrpMem = [SELECT CollaborationGroupId,
                                          MemberId
                                     FROM CollaborationGroupMember
                                    WHERE CollaborationGroupId IN :mapWorkingGrpIdVSGroupId.values()];
        //System.debug('listCollaborationGrpMem======='+listCollaborationGrpMem);
        // Iterate over collaboration group list
        for(CollaborationGroupMember collGrpMem : listCollaborationGrpMem) {
            if(!mapGrpIdVsMemberIdSet.containsKey(collGrpMem.CollaborationGroupId)) {
                mapGrpIdVsMemberIdSet.put(collGrpMem.CollaborationGroupId, new Set<Id>{collGrpMem.MemberId});    
            } else {
                Set<Id> setPreviousMemId = new Set<Id>();
                setPreviousMemId = mapGrpIdVsMemberIdSet.get(collGrpMem.CollaborationGroupId);
                setPreviousMemId.add(collGrpMem.MemberId);
                mapGrpIdVsMemberIdSet.put(collGrpMem.CollaborationGroupId, setPreviousMemId);
            } // End of if-else block    
        } // End of for
        //System.debug('mapGrpIdVsMemberIdSet=========='+mapGrpIdVsMemberIdSet);

        // Iterate over Working group list to update the Chatter group Id on Working Group and add the Admin user
        for(Working_Group__c workingGrpRec : listWorkingGroup) {
            Working_Group__c workingGrpToBeUpdated = new Working_Group__c(Id = workingGrpRec.Id);
            if(!mapWorkingGrpIdVSGroupId.isEmpty() && mapWorkingGrpIdVSGroupId.containsKey(workingGrpRec.Id)) {
                workingGrpToBeUpdated.Chatter_Group__c = mapWorkingGrpIdVSGroupId.get(workingGrpRec.Id);
                // 291119 - T-000938 - VennScience_BFL_Amruta - Set the value of chatter Group Feed Email Address
                if(!mapWorkingGrpIdVSGroupEmail.isEmpty() && mapWorkingGrpIdVSGroupEmail.containsKey(workingGrpRec.Id)) {
                    workingGrpToBeUpdated.Chatter_Group_Feed_Email_Address__c = mapWorkingGrpIdVSGroupEmail.get(workingGrpRec.Id);
                } // End of if
                listWorkingGrpToUpdate.add(workingGrpToBeUpdated);

                // Set Association's Owner as owner of Working group's Chatter Group
                if(!mapAssIdVSOwnerId.isEmpty() && mapAssIdVSOwnerId.containsKey(workingGrpRec.Association__c) &&
                   !mapGrpIdVsMemberIdSet.isEmpty() && mapGrpIdVsMemberIdSet.containsKey(mapWorkingGrpIdVSGroupId.get(workingGrpRec.Id)) &&
                   !mapGrpIdVsMemberIdSet.get(mapWorkingGrpIdVSGroupId.get(workingGrpRec.Id)).contains(mapAssIdVSOwnerId.get(workingGrpRec.Association__c))) {
                    //System.debug('Inside Association if');
                    CollaborationGroupMember adminMember = new CollaborationGroupMember();
                    adminMember.memberid = mapAssIdVSOwnerId.get(workingGrpRec.Association__c);
                    adminMember.CollaborationGroupId = mapWorkingGrpIdVSGroupId.get(workingGrpRec.Id);
                    adminMember.CollaborationRole = 'Admin';
                    listMembersToInsert.add(adminMember);   
                } else if(!mapAssIdVSOwnerId.isEmpty() && mapAssIdVSOwnerId.containsKey(workingGrpRec.Association__c) &&
                          (mapGrpIdVsMemberIdSet.isEmpty() || 
                          !mapGrpIdVsMemberIdSet.isEmpty() && !mapGrpIdVsMemberIdSet.containsKey(mapWorkingGrpIdVSGroupId.get(workingGrpRec.Id)))) {
                    //System.debug('Inside Association else');
                    CollaborationGroupMember adminMember = new CollaborationGroupMember();
                    adminMember.memberid = mapAssIdVSOwnerId.get(workingGrpRec.Association__c);
                    adminMember.CollaborationGroupId = mapWorkingGrpIdVSGroupId.get(workingGrpRec.Id);
                    adminMember.CollaborationRole = 'Admin';
                    listMembersToInsert.add(adminMember);            
                } // End of Set Association owner if-else block
            } // End of if
            //System.debug('workingGrpRec.Chatter_Group__c======='+workingGrpRec.Chatter_Group__c);
            
        } // End of for
        //System.debug('listWorkingGrpToUpdate======='+listWorkingGrpToUpdate);
        //System.debug('listMembersToInsert======='+listMembersToInsert);
        
         // Update Chatter Group Id and Group Email on Working Group record
        if(!listWorkingGrpToUpdate.isEmpty()) {
            try {
                isRecursiveCall = true;
                update listWorkingGrpToUpdate;
            } catch(Exception e) {
                System.debug('Error ocurred while updating Working Group records:'+e.getMessage());
            } // End of try-catch block
        } // End of if
        // Insert Admin Group Member
        if(!listMembersToInsert.isEmpty()) {
            try {
                insert listMembersToInsert;
            } catch(Exception e) {
                System.debug('Error ocurred while inserting admin member records:'+e.getMessage());
            } // End of try-catch block
        } // End of if
        // Add Group Members and Association Team to Working Group's Chatter Group
        if(!listCollaborationGroup.isEmpty()) {
            syncGroupMemberWithChatterGroup(mapWorkingGrpIdVSGroupId, mapGrpIdVsMemberIdSet);
            syncAssociationTeamWithChatterGroup(mapWorkingGrpIdVSGroupId, mapAssIdVSWorkingGrpSet, mapGrpIdVsMemberIdSet);
        } // End of if 
    }
    /**
    * Method Name : syncGroupMemberWithChatterGroup
    * Parameters  : param1: Map<Id,Id>, param1: Map<Id,Set<Id>>
    * Description : Used to add the child Group Members in parent Working Group's Chatter Group.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 241019
    **/
    public static void syncGroupMemberWithChatterGroup(Map<Id,Id> mapWorkingGrpIdVSGrpId, Map<Id,Set<Id>> mapGroupIdVsMemberIdSet) {
        // Variable Declarations
        List<Group_Member__c> listGroupMembers = new List<Group_Member__c>();
        List<CollaborationGroupMember> listMembersToInsert = new List<CollaborationGroupMember>();
        List<User> listCommUser = new List<User>();
        Map<Id,Set<Id>> mapContactIdVsGroupIdSet = new Map<Id,Set<Id>>();
        Map<Id,Id> mapContactIdVSUserId = new Map<Id,Id>();
        
        //System.debug('Inside syncGroupMemberWithChatterGroup');
        listGroupMembers = [SELECT Id,
                                   Working_Group__c,
                                   Working_Group__r.Chatter_Group__c,
                                   Grou_Member__c
                              FROM Group_Member__c
                             WHERE Working_Group__c IN :mapWorkingGrpIdVSGrpId.keySet()
                               AND Working_Group__r.Chatter_Group__c != null
                               AND Status__c = :MEMBERSTATUS];
        // Iterate over Group Members records
        for(Group_Member__c grpMemberRecord : listGroupMembers) {
            if(!mapContactIdVsGroupIdSet.containsKey(grpMemberRecord.Grou_Member__c)) {
                mapContactIdVsGroupIdSet.put(grpMemberRecord.Grou_Member__c, new Set<Id>{grpMemberRecord.Working_Group__r.Chatter_Group__c});       
             } else {
                 Set<Id> setPreviousId = new Set<Id>();
                 setPreviousId = mapContactIdVsGroupIdSet.get(grpMemberRecord.Grou_Member__c);
                 setPreviousId.add(grpMemberRecord.Working_Group__r.Chatter_Group__c);
                 mapContactIdVsGroupIdSet.put(grpMemberRecord.Grou_Member__c, setPreviousId);
             }// End of else-if    
        } // End of for
        
        // Fetch users corresponding to Group Member's contact
        listCommUser = [SELECT Id,
                               ContactId
                          FROM User
                         WHERE ContactId IN :mapContactIdVsGroupIdSet.keySet()
                           AND IsActive = true];
        //System.debug('listCommUser=========='+listCommUser);
        // Iterate over users list
        for(User userRecord : listCommUser) {
            // Populate map of Contact Id VS User Id
            mapContactIdVSUserId.put(userRecord.ContactId, userRecord.Id);
        } // End of for
        
        // Add Group Members to Working Group's Chatter Group
        for(Id contactId : mapContactIdVsGroupIdSet.keySet()) {
            for(Id grpId : mapContactIdVsGroupIdSet.get(contactId)) {
                // Check if current contact's user is not already a part of Chatter Group
                if(!mapGroupIdVsMemberIdSet.isEmpty() && mapGroupIdVsMemberIdSet.containsKey(grpId) &&
                   mapContactIdVSUserId.containsKey(contactId) &&
                   !mapGroupIdVsMemberIdSet.get(grpId).contains(mapContactIdVSUserId.get(contactId))) {
                   CollaborationGroupMember groupMember = new CollaborationGroupMember();
                   groupMember.MemberId = mapContactIdVSUserId.get(contactId); // Provide userId here
                   groupMember.CollaborationGroupId = grpId; //Id of chatter group 
                   listMembersToInsert.add(groupMember);   
                } // End of if
            } // End of inner for
        } // End of outer for
        //System.debug('listMembersToInsert=========='+listMembersToInsert);
        
        // Insert Chatter Group Members
        if(!listMembersToInsert.isEmpty()) {
            try {
                insert listMembersToInsert;
            } catch(Exception e) {
                System.debug('Error occurred while inserting Chatter Group Members:'+e.getMessage());
            } // End of try-catch block
        } // End of if
    }
    /**
    * Method Name : syncAssociationTeamWithChatterGroup
    * Parameters  : param1: Map<Id,Id>, param2: Map<Id,Set<Id>>, param3: Map<Id,Set<Id>>
    * Description : Used to add the Association Teams to related Working Group's Chatter Group.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 241019
    **/
    public static void syncAssociationTeamWithChatterGroup(Map<Id,Id> mapWorkingGrpIdVSGrpId, Map<Id,Set<Id>> mapAssIdVSWorkingGroupSet,
                                                           Map<Id,Set<Id>> mapGroupIdVsMemberIdSet) {
        // Variable Declarations
        List<Association_Team__c> listAssociationTeam = new List<Association_Team__c>();
        List<CollaborationGroupMember> listMembersToInsert = new List<CollaborationGroupMember>();
        Map<Id,Set<Id>> mapAssIdVSAssTeamIdSet = new Map<Id,Set<Id>>();
        Map<Id,Id> mapAssTeamIdVSUserId = new Map<Id,Id>();
        Map<Id,Set<Id>> mapAssTeamIdVSWorkingGroupId = new Map<Id,Set<Id>>();
        
        //System.debug('Inside syncAssociationTeamWithChatterGroup');
        // Fetch child Association Team for Association
        listAssociationTeam = [SELECT Id,
                                      Association__c,
                                      User__c
                                 FROM Association_Team__c
                                WHERE Association__c IN :mapAssIdVSWorkingGroupSet.keySet()];
        //System.debug('listAssociationTeam======='+listAssociationTeam);
        // Iterate over Association Team records
        for(Association_Team__c assTeamRecord : listAssociationTeam) {
            // Populate map of Association Team Id VS User Id
            mapAssTeamIdVSUserId.put(assTeamRecord.Id, assTeamRecord.User__c);
            // Populate map of Association Id VS Set of Association's Team Id
            if(!mapAssIdVSAssTeamIdSet.containsKey(assTeamRecord.Association__c)) {
                mapAssIdVSAssTeamIdSet.put(assTeamRecord.Association__c, new Set<Id>{assTeamRecord.Id});    
            } else {
                Set<Id> setPreviousTeamId = new Set<Id>();
                setPreviousTeamId = mapAssIdVSAssTeamIdSet.get(assTeamRecord.Association__c);
                setPreviousTeamId.add(assTeamRecord.Id);
                mapAssIdVSAssTeamIdSet.put(assTeamRecord.Association__c, setPreviousTeamId);
            } // End of if-else block               
        } // End of for
        
        // Iterate over mapAssIdVSAssTeamIdSet keyset
        for(Id assId : mapAssIdVSAssTeamIdSet.keySet()) {
            for(Id assTeamId : mapAssIdVSAssTeamIdSet.get(assId)) {
                // Populate map of Association Team Id VS Set of Working Group Id
                if(!mapAssIdVSWorkingGroupSet.isEmpty() && mapAssIdVSWorkingGroupSet.containsKey(assId)) {
                    mapAssTeamIdVSWorkingGroupId.put(assTeamId, mapAssIdVSWorkingGroupSet.get(assId));
                } // End of if
            } // End of inner for 
        } // End of outer for
        //System.debug('mapAssTeamIdVSWorkingGroupId========'+mapAssTeamIdVSWorkingGroupId);
        
        // Iterate over map of Association Team Id VS Set of Working Group Id
        for(Id assTeamId : mapAssTeamIdVSWorkingGroupId.keySet()) {
            for(Id workingGrpId : mapAssTeamIdVSWorkingGroupId.get(assTeamId)) {
                Id chatterGroupId = null;
                Id userId = null;
                if(!mapAssTeamIdVSUserId.isEmpty() && mapAssTeamIdVSUserId.containsKey(assTeamId)) {
                    userId = mapAssTeamIdVSUserId.get(assTeamId);    
                }
                if(!mapWorkingGrpIdVSGrpId.isEmpty() && mapWorkingGrpIdVSGrpId.containsKey(workingGrpId)) {
                    chatterGroupId = mapWorkingGrpIdVSGrpId.get(workingGrpId);
                }
                // Check if current member is not already a part of Working Group's chatter group
                if(chatterGroupId != null && userId != null && 
                   !mapGroupIdVsMemberIdSet.isEmpty() && mapGroupIdVsMemberIdSet.containsKey(chatterGroupId) && 
                   !mapGroupIdVsMemberIdSet.get(chatterGroupId).contains(userId)) {
                    CollaborationGroupMember groupMember = new CollaborationGroupMember();
                    groupMember.MemberId = userId; //Provide userId here
                    groupMember.CollaborationGroupId = chatterGroupId; // Provide groupId here
                    // Add group member to list
                    listMembersToInsert.add(groupMember);
                } // End of if    
            } // End of inner for 
        } // End of outer for
        //System.debug('listMembersToInsert======='+listMembersToInsert);
        if(!listMembersToInsert.isEmpty()) {
             try {
                insert listMembersToInsert;
                //System.debug('Association Teams are added as chatter grp members');
            } catch(Exception e) {
                System.debug('Error occurred while inserting Group Member:'+e.getMessage());
            } // End of try-catch block    
        } // End of if
    }
    /**
    * Method Name : deleteChatterGroup
    * Parameters  : param1: Set<Id>
    * Description : Used to add the Association Teams to related Working Group's Chatter Group.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 241019
    **/
    public static void deleteChatterGroup(List<Working_Group__c> listDeletedWorkingGrp) {
        // Variable Declarations
        List<CollaborationGroup> listCollGrpToBeDeleted = new List<CollaborationGroup>();
        Set<Id> setChatterGrpToBeDeleted = new Set<Id>();
        
        // Iterate over Working Group list
        for(Working_Group__c workingGrpRecord : listDeletedWorkingGrp) {
            // Populate Set of chtter group Id
            setChatterGrpToBeDeleted.add(workingGrpRecord.Chatter_Group__c);    
        } // End of for     
        // Fetch Collaboration Group
        listCollGrpToBeDeleted = [SELECT Id
                                    FROM CollaborationGroup
                                   WHERE Id IN :setChatterGrpToBeDeleted];
        // Delete the Chatter group records
        if(!listCollGrpToBeDeleted.isEmpty()) {
            try {
                delete listCollGrpToBeDeleted;
            } catch(Exception e) {
                System.debug('Error occurred while deleting chatter group records:'+e.getMessage());
            } // End of try-catch block
        } // End of if
    }
}