@isTest
public class AccountTriggerHandlerTest {
    
      @testSetup static void setupTest() {
           Id PerContactId;
           //UserRole usr = new UserRole(DeveloperName = 'TestCustomRole', Name = 'Test new Role');
           UserRole usr = new UserRole(DeveloperName = 'MyCustomRole', Name = 'CEO');
           insert usr;
           System.debug('UserRole for community -> '+usr);
           
           Profile profileRecord = [SELECT Id 
                                      FROM Profile 
                                     WHERE Name = 'System Administrator'];  
           User adminUserRecord = new User(firstname= 'Samuel',
                                           lastname='janan',
                                           UserRoleId =usr.Id,
                                           Alias='Samuel',
                                           email = 'samueljan@gamil.com',
                                           username= 'samueljan@gamil.com', 
                                           ProfileId = profileRecord.Id, 
                                           emailencodingkey='UTF-8',
                                           languagelocalekey='en_US',
                                           localesidkey='en_US',
                                           timezonesidkey='America/Los_Angeles');
            insert adminUserRecord;
            System.runAs(adminUserRecord) {
            Account newPersonAcc = TestDataFactory.getPersonAccount();
            insert newPersonAcc;
            System.debug('===newPersonAcc====' +newPersonAcc);
                
            List<Account> accountList = [SELECT Id,
                                                PersonContactId
                                           FROM Account
                                          WHERE Id =: newPersonAcc.Id];
            //System.debug('accountList===='+accountList);
            
            for(Account acc : accountList) {  
                System.debug('personeContactId===='+acc.PersonContactId);
                PerContactId = acc.PersonContactId; 
            }
       
            Id partnerCommunityProfileId = [select Id,Name 
                                              FROM profile 
                                             WHERE Name ='Customer Community Plus Login User'].Id;
            //UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];    
            User communityUser = new User(alias = 'test123', 
                                         email='test_partner@noemail.com',
                                         emailencodingkey='UTF-8',
                                         //UserRole = usr,
                                         lastname ='Test Community User', 
                                         languagelocalekey ='en_US',
                                         localesidkey ='en_US', 
                                         ProfileId = partnerCommunityProfileId,
                                         country = 'United States',
                                         IsActive = true,
                                         ContactId = PerContactId, 
                                         timezonesidkey = 'America/Los_Angeles', 
                                         username ='test990@test.com');   
           insert communityUser;
           }
        }
        @isTest
            public static void updateAccountPersonEmail() {
            Id PerContactId;

            List<Account> accountList = [SELECT Id,
                                                firstName,
                                                LastName,
                                                PersonEmail,
                                                PersonMobilePhone,
                                                PersonTitle,
                                                Phone,
                                                PersonContactId
                                           FROM Account
                                          WHERE LastName = 'Member'];
            
            System.debug('accountList====' +accountList);
            for(Account acc : accountList) {  
                System.debug('personeContactId===='+acc.PersonContactId);
                PerContactId = acc.PersonContactId; 
            }    
            
            List<Account> updatedAccList = new List<Account>();
            for(Account accountObj : accountList) {  
                    System.debug('personeContactId===='+accountObj.firstName); 
                    accountObj.PersonEmail ='testperson11@gmail.com';
                    accountObj.Email_Visibility__pc ='Organization Only';
                    accountObj.Title_Visibility__pc ='Organization Only';
                    accountObj.Mobile_Visibility__pc ='Organization Only';
                    accountObj.Phone_Visibility__pc ='Organization Only';
                    // 121219 - T-000876 - VennScience_BFL_Amruta - Commented below code as Photo Visibility field has been replaced
                    //accountObj.Photo_Visibility_Update__pc ='Organization Only';
                
                    updatedAccList.add(accountObj);
            }
            if(updatedAccList!=NULL) {
                update updatedAccList;
                System.debug('====Account updatedAccList====='+updatedAccList);
                System.debug('====Account updatedAccList====='+updatedAccList[0].PersonEmail);
                
                List<User> userList = [SELECT Id,
                                                LastName,
                                                Phone,
                                                Email,
                                                Title,
                                                MobilePhone,
                                                ContactId
                                           FROM User
                                          WHERE ContactId =: PerContactId 
                                            AND LastName ='Test Community User'];
                System.debug('====Account userList===='+userList[0].Email);
                
                //System.assertEquals(updatedAccList[0].PersonEmail,userList[0].Email);
            }     
        }
        @isTest
            public static void updateAccountPersonMobilePhone() {
    
            Id PerContactId;

            List<Account> accountList = [SELECT Id,
                                                firstName,
                                                LastName,
                                                PersonEmail,
                                                PersonMobilePhone,
                                                PersonTitle,
                                                Phone,
                                                PersonContactId
                                           FROM Account
                                          WHERE LastName = 'Member'];
            
            System.debug('accountList====' +accountList);
            for(Account acc : accountList) {  
                System.debug('personeContactId===='+acc.PersonContactId);
                PerContactId = acc.PersonContactId; 
            }    
            
            List<Account> updatedAccList = new List<Account>();
            for(Account accountObj : accountList) {  
                    System.debug('personeContactId===='+accountObj.firstName); 
                    //accountObj.PersonEmail ='testperson33@gmail.com';
                    accountObj.PersonMobilePhone ='9876543211';
                    accountObj.PersonTitle ='test123';
                    accountObj.Phone ='9873333210';
                    accountObj.Email_Visibility__pc ='Public';
                    accountObj.Title_Visibility__pc ='Public';
                    accountObj.Mobile_Visibility__pc ='Public';
                    accountObj.Phone_Visibility__pc ='Public';
                    accountObj.Photo_Visibility_Update__pc ='Public';
                
                updatedAccList.add(accountObj);
                
            }
            if(updatedAccList!=NULL) {
                update updatedAccList;
                System.debug('====Account updatedAccList====='+updatedAccList);
                System.debug('====Account updatedAccList====='+updatedAccList[0].PersonMobilePhone);
                
                List<User> userList = [SELECT Id,
                                                LastName,
                                                Phone,
                                                Email,
                                                Title,
                                                MobilePhone,
                                                ContactId
                                           FROM User
                                          WHERE ContactId =: PerContactId 
                                            AND LastName ='Test Community User'];
                System.debug('====Account userList===='+userList[0].Phone);
              
                 //System.assertEquals(updatedAccList[0].PersonMobilePhone,userList[0].MobilePhone);
                 //System.assertEquals(true,userList[0].UserPreferencesShowTitleToGuestUsers);
              }  
       }
        @isTest
            public static void updateAccountPersonTitle() {
    
            List<Account> accountList = [SELECT Id,
                                                firstName,
                                                LastName,
                                                PersonEmail,
                                                PersonMobilePhone,
                                                PersonTitle,
                                                Phone
                                           FROM Account
                                          WHERE LastName = 'Member'];
            
            System.debug('accountList====' +accountList);
            
            List<Account> updatedAccList = new List<Account>();
            for(Account accountObj : accountList) {  
                    System.debug('personeContactId===='+accountObj.firstName); 
                    //accountObj.PersonEmail ='testing@gmail.com';
                    //accountObj.PersonMobilePhone ='9988873332';
                    accountObj.PersonTitle ='person Testing';
                    //accountObj.Phone ='9879999210';
                    accountObj.Email_Visibility__pc ='Association Only';
                    accountObj.Title_Visibility__pc ='Association Only';
                    accountObj.Mobile_Visibility__pc ='Association Only';
                    accountObj.Phone_Visibility__pc ='Association Only';
                    accountObj.Photo_Visibility__pc ='Association Only';
                
                updatedAccList.add(accountObj);
           }
            if(updatedAccList!=NULL) {
                update updatedAccList;
                
                List<Account> accountList1 = [SELECT Id,
                                                LastName,
                                                PersonEmail,
                                                PersonMobilePhone,
                                                PersonTitle,
                                                Email_Visibility__pc,
                                                Title_Visibility__pc,
                                                Mobile_Visibility__pc,
                                                Phone_Visibility__pc,
                                                Photo_Visibility__pc
                                           FROM Account
                                          WHERE LastName = 'Member'];
                
                List<User> userList = [SELECT Id,
                                                LastName,
                                                UserPreferencesShowTitleToGuestUsers,
                                                UserPreferencesShowTitleToExternalUsers,
                                                Email,
                                                Title,
                                                MobilePhone
                                           FROM User
                                          WHERE LastName = 'Test Community User'];
                 
                 //System.assertEquals(accountList1[0].PersonTitle,userList[0].Title);
                 
                 //System.assertEquals(true,userList[0].UserPreferencesShowTitleToGuestUsers);
              }  
                 
       } 
        @isTest
            public static void updateAccountPhone() {
    
            List<Account> accountList = [SELECT Id,
                                                firstName,
                                                LastName,
                                                PersonEmail,
                                                PersonMobilePhone,
                                                PersonTitle,
                                                Phone
                                           FROM Account
                                          WHERE LastName = 'Member'];
            
            System.debug('accountList====' +accountList);
            
            List<Account> updatedAccList = new List<Account>();
            for(Account accountObj : accountList) {  
                    System.debug('personeContactId===='+accountObj.firstName); 
                    accountObj.Phone ='7892345611';
                    accountObj.Email_Visibility__pc ='Association Only';
                    accountObj.Title_Visibility__pc ='Association Only';
                    accountObj.Mobile_Visibility__pc ='Association Only';
                    accountObj.Phone_Visibility__pc ='Association Only';
                    accountObj.Photo_Visibility__pc ='Association Only';
                
                updatedAccList.add(accountObj);
           }
            if(updatedAccList!=NULL) {
                update updatedAccList;
                System.debug('====Account updatedAccList====='+updatedAccList[0].Phone);
                
                List<Account> accountList1 = [SELECT Id,
                                                firstName,
                                                LastName,
                                                Phone,
                                                PersonEmail,
                                                PersonMobilePhone,
                                                PersonTitle,
                                                Email_Visibility__pc,
                                                Title_Visibility__pc,
                                                Mobile_Visibility__pc,
                                                Phone_Visibility__pc,
                                                Photo_Visibility__pc
                                           FROM Account
                                          WHERE LastName = 'Member'];
                
                List<User> userList = [SELECT Id,
                                                LastName,
                                                UserPreferencesShowTitleToGuestUsers,
                                                UserPreferencesShowTitleToExternalUsers,
                                                Phone,
                                                Email,
                                                Title,
                                                MobilePhone
                                           FROM User
                                          WHERE LastName = 'Test Community User'];
              
            //System.assertEquals(accountList1[0].Phone,userList[0].Phone);
         }  
       }
      /*@isTest
            public static void updateAccountPhotoUrl() {
    
            List<Account> accountList = [SELECT Id,
                                                firstName,
                                                LastName,
                                                PersonEmail,
                                                PersonMobilePhone,
                                                PersonTitle,
                                                Phone,
                                                PhotoUrl
                                           FROM Account
                                          WHERE LastName = 'Member'];
            
            System.debug('accountList====' +accountList);
            
            List<Account> updatedAccList = new List<Account>();
            for(Account accountObj : accountList) {  
                    System.debug('personeContactId===='+accountObj.firstName); 
                    //accountObj.PhotoUrl ='';
                    accountObj.Email_Visibility__pc ='Association Only';
                    accountObj.Title_Visibility__pc ='Association Only';
                    accountObj.Mobile_Visibility__pc ='Association Only';
                    accountObj.Phone_Visibility__pc ='Association Only';
                    accountObj.Photo_Visibility_Update__pc ='Association Only';
                
                updatedAccList.add(accountObj);
           }
            if(updatedAccList!=NULL) {
                update updatedAccList;
                
                List<Account> accountList1 = [SELECT Id,
                                                firstName,
                                                LastName,
                                                Phone,
                                                PersonEmail,
                                                PersonMobilePhone,
                                                PersonTitle,
                                                Email_Visibility__pc,
                                                Title_Visibility__pc,
                                                Mobile_Visibility__pc,
                                                Phone_Visibility__pc,
                                                Photo_Visibility_Update__pc
                                           FROM Account
                                          WHERE LastName = 'Member'];
                
                List<User> userList = [SELECT Id,
                                                LastName,
                                                UserPreferencesShowTitleToGuestUsers,
                                                UserPreferencesShowTitleToExternalUsers,
                                                Phone,
                                                Email,
                                                Title,
                                                MobilePhone
                                           FROM User
                                          WHERE LastName = 'Test Community User'];
                              
                 //System.assertEquals(accountList1[0].PhotoUrl,userList.PhotoUrl);
                 //System.assertEquals(true,userList[0].UserPreferencesShowTitleToGuestUsers);
              }  
        }  */  
 }