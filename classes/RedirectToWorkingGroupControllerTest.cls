/*********************************************************************************************************************************************************
*     Version    CreateDate/Modified   CreatedBy                  Description
*     1.0        111219                VennScience_BFL_Amruta     This is the test class for RedirectToWorkingGroupController apex class.
*
**********************************************************************************************************************************************************/
@isTest
public class RedirectToWorkingGroupControllerTest {
	/**
    * Method Name : getRelatedWorkingGroupPositiveTest
    * Parameters  : none
    * Description : This method is used to test the positive scenario of getRelatedWorkingGroup method
    **/
    @isTest
    public static void getRelatedWorkingGroupPositiveTest() {
        // Variable Declarations
        Working_Group__c workingGroupRecord = new Working_Group__c();
        Working_Group__c getWorkingGroupRecord = new Working_Group__c();
        Association__c associationRecord = new Association__c();
        String getWorkingGrpId = '';
        
        // Create Association record
        associationRecord = TestDataFactory.createAssociationRecords();
        insert associationRecord;
        
        // Create Working Group
        workingGroupRecord.Name = 'Test Redirect Positive';
        workingGroupRecord.Working_Group_Abbreviation__c = 'TestRedirect01';
        if(associationRecord != null && associationRecord.Id != null) {
        	workingGroupRecord.Association__c = associationRecord.Id;   
        }
        workingGroupRecord.Type__c = 'Public';
        insert workingGroupRecord;
        System.debug('workingGroupRecord======'+workingGroupRecord);
        getWorkingGroupRecord = [SELECT Id,
                                		Chatter_Group__c
                                   FROM Working_Group__c
                                  WHERE Id = :workingGroupRecord.Id];
        System.debug('getWorkingGroupRecord======'+getWorkingGroupRecord);
        // Fetch Collaboration Group record related to Working Group
        if(getWorkingGroupRecord.Chatter_Group__c != null) {
            getWorkingGrpId = RedirectToWorkingGroupController.getRelatedWorkingGroup(getWorkingGroupRecord.Chatter_Group__c);
        } // End of if
        System.debug('getWorkingGrpId====='+getWorkingGrpId);
        if(workingGroupRecord.Id != null) {
            // Assert : Check if the returned Id is same as that of Working Group's Id
        	System.assertEquals(workingGroupRecord.Id, getWorkingGrpId, 'Working Group Id does not matches');
        } // End of if
    }    
    /**
    * Method Name : getRelatedWorkingGroupNegativeTest
    * Parameters  : none
    * Description : This method is used to test the negative scenario of getRelatedWorkingGroup method
    **/
    @isTest
    public static void getRelatedWorkingGroupNegativeTest() {
        // Variable Declarations
        String getWorkingGrpId = '';
        String errorString = 'Please navigate to Working Groups tab to access the group details.';
        Id workingGrpId;
        
		getWorkingGrpId = RedirectToWorkingGroupController.getRelatedWorkingGroup(workingGrpId);
        
        System.debug('getWorkingGrpId====='+getWorkingGrpId);
        // Assert : Check if the returned Id is same as that of Working Group's Id
        System.assertEquals(errorString, getWorkingGrpId, 'Expected result is not blank');
    }    
}