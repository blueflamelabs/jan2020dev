/*********************************************************************************************************************************************************
*     Version    CreateDate/Modified     CreatedBy                    Description
*       1.0         261119               VennScience_BFL_Monali       This class is built to Display current user's chatter group setting.

**********************************************************************************************************************************************************/
public class ChatterGroupNotifyController {
    
    /**
   * Method Name : getCurrentMemberCategoryUser
   * Description : Used to check the current user's field value and return boolean.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 231019
   **/

    @AuraEnabled
    public static Boolean getCurrentUser() {
        Boolean IsMemberCategory = false;
        User currentUser = [SELECT  Id, 
                                    Name,
                                    FirstName, 
                                    LastName,
                                    IsActive,
                                    ContactId
                            FROM User 
                            WHERE Id =: UserInfo.getUserId()];
        // If current user hase member category = Primary contach or Alternate Contact.
        if(currentUser.ContactId != null) {
            IsMemberCategory = true;
        }else {
            IsMemberCategory = false;
        }//End of if else.
        return IsMemberCategory;
    }
    
    @AuraEnabled
    public static User getCurrentUservalue() {
        //Boolean IsMemberCategory = false;
        User currentUser = [SELECT  Id, 
                                    Name,
                                    FirstName, 
                                    LastName,
                                    IsActive,
                                    ContactId,
                                    DefaultGroupNotificationFrequency
        					FROM User 
                            WHERE Id =: UserInfo.getUserId()];
        return currentUser;
    }
    
    /**
   * Method Name : getPicklistvalues
   * Parameters  : param1: String objectName, String field_apiname
   * Description : Used to get the picklistvalues.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 160719
   **/
    
    @AuraEnabled
    public static List<PickListWrapper> getDefaultGroupNotificationFreqValues() {
        String objectName = 'User'; 
        String field_apiname = 'DefaultGroupNotificationFrequency';
        
        List<PickListWrapper> optionlist = new List<PickListWrapper>();
        
        Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = globalDescribe.get(objectName.toLowerCase()).getDescribe().fields.getMap(); 
        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();
        //optionlist.add(new PickListWrapper('--None--','--None--'));
        for (Schema.PickListEntry pickValue : picklistValues) {
            optionlist.add(new PickListWrapper(pickValue.getLabel(), pickValue.getValue()));
        }
        return optionlist;
    }
    
    @AuraEnabled //get Account Industry Picklist Values
    public static Map<String, String> getGroupNotificationFrequency(){
        Map<String, String> options = new Map<String, String>();
        //get Account Industry Field Describe
        Schema.DescribeFieldResult fieldResult = User.DefaultGroupNotificationFrequency.getDescribe();
        //get Account Industry Picklist Values
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            //Put Picklist Value & Label in Map
            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }
    
     /**
  * Method Name : getCompanyInformation
  * Parameters  : none
  * Description : Used to get Company Information.
  * Created By  : VennScience_BFL_Monali 
  * Created On  : 231019
  **/
  /* @AuraEnabled
   public static String getDefaultGroup(){
      Organization orgDetails = [SELECT Id, DefaultGroupNotificationFrequency FROM Organization WHERE Id = :UserInfo.getOrganizationId()];
      return orgDetails.DefaultGroupNotificationFrequency;
   }*/
    
   /**
   * Method Name : getCurrentMemberCategoryUser
   * Description : Used to check the current user's field value and return boolean.
   * Created By  : VennScience_BFL_Monali 
   * Created On  : 231019
   **/

    @AuraEnabled
    public static List<CollaborationGroupMember> getCurrentUserRelatedGroups() {
        
        List<CollaborationGroupMember> listOfGroupMemeber = new List<CollaborationGroupMember>();
        //List<CollaborationGroup> listOfGroup = new List<CollaborationGroup>();
        //Set<Id> setOfgroupId = new Set<Id>();
        try{
        User currentUser = [SELECT  Id, 
                                    Name,
                                    FirstName, 
                                    LastName,
                                    IsActive,
                                    ContactId
                            FROM User 
                            WHERE Id =: UserInfo.getUserId()];
            listOfGroupMemeber = [SELECT Id, 
                                  		 CollaborationGroupId, 
                                  		 MemberId, 
                                  		 CollaborationGroup.Name, 
                                  		 Member.Name, 
                                  		NotificationFrequency 
                                  FROM CollaborationGroupMember 
                                  WHERE MemberId = :currentUser.Id];
            System.debug('listOfGroupMemeber==='+listOfGroupMemeber);
            System.debug('listOfGroupMemeber size==='+listOfGroupMemeber.size());
           
        }catch(Exception ex) {
            System.debug('-----'+ex.getMessage());
        }
        return listOfGroupMemeber;
    }
   /**
   * Method Name : getCurrentUserRelatedWorkingGroups
   * Description : Used to check the current user's related Working Group.
   * Created By  : VennScience_BFL_Amruta
   * Created On  : 111219
   **/

    @AuraEnabled
    public static Map<Id,String> getCurrentUserRelatedWorkingGroups() {
        // Variable Declarations
        List<CollaborationGroupMember> listOfGroupMemeber = new List<CollaborationGroupMember>();
        List<Working_Group__c> listWorkingGroup = new List<Working_Group__c>();
        Set<Id> setChatterGrpId = new Set<Id>();
        Map<Id, String> mapChatterGrpIdVSWorkingGrpName = new Map<Id, String>();
        
        System.debug('Inside getCurrentUserRelatedWorkingGroups');
        try{
            User currentUser = [SELECT  Id, 
                                        Name,
                                        FirstName, 
                                        LastName,
                                        IsActive,
                                        ContactId
                                FROM User 
                                WHERE Id =: UserInfo.getUserId()];
            listOfGroupMemeber = [SELECT Id, 
                                  		 CollaborationGroupId, 
                                  		 MemberId, 
                                  		 CollaborationGroup.Name, 
                                  		 Member.Name, 
                                  		NotificationFrequency 
                                  FROM CollaborationGroupMember 
                                  WHERE MemberId = :currentUser.Id];
            System.debug('listOfGroupMemeber==='+listOfGroupMemeber);
            System.debug('listOfGroupMemeber size==='+listOfGroupMemeber.size());
            // Iterate over list of Group Member
            for(CollaborationGroupMember collGrpMemberRecord : listOfGroupMemeber) {
                setChatterGrpId.add(collGrpMemberRecord.CollaborationGroupId);
            } // End of for
            System.debug('setChatterGrpId========'+setChatterGrpId);
            // Fetch related Working Group
            listWorkingGroup = [SELECT Id,
                               		   Name,
                                	   Chatter_Group__c
                                  FROM Working_Group__c
                                 WHERE Chatter_Group__c IN :setChatterGrpId];
            System.debug('listWorkingGroup========='+listWorkingGroup);
            // Iterate over working gropup list
            for(Working_Group__c workingGrpRecord : listWorkingGroup) {
                mapChatterGrpIdVSWorkingGrpName.put(workingGrpRecord.Chatter_Group__c, workingGrpRecord.Name);
            } // End of for
            System.debug('mapChatterGrpIdVSWorkingGrpName====='+mapChatterGrpIdVSWorkingGrpName);
        } catch(Exception ex) {
            System.debug('-----'+ex.getMessage());
        }
        return mapChatterGrpIdVSWorkingGrpName;  
    }
    
    @AuraEnabled
    public static String saveFrequencyNotification(String frequencyValue) {
        //System.debug('frequencyValue  --'+frequencyValue);
        String susessMsg = '';
        User currentUser = new User();
        try{
            currentUser = [SELECT  Id, 
                           Name,
                           FirstName, 
                           LastName,
                           IsActive,
                           ContactId,
                           DefaultGroupNotificationFrequency
                           FROM User 
                           WHERE Id =: UserInfo.getUserId()];
            currentUser.DefaultGroupNotificationFrequency = frequencyValue;
            
            update currentUser;
            susessMsg = 'Record update successfully!';
        } catch(Exception ex) {
            System.debug('currentUser  --'+ex.getMessage());
        }
        System.debug('currentUser  --'+currentUser);
        return susessMsg;
    }
    @AuraEnabled
    public static String updateNotificationFrequency(String selectedId, String changedValue) {
        System.debug('selectedId --'+selectedId);
        System.debug('changedValue --'+changedValue);
        //Variable declaration.
        String updateMessage = '';
        CollaborationGroupMember groupMemberRecord = new CollaborationGroupMember(); 
        try{
            
            groupMemberRecord = [SELECT Id, 
                                     CollaborationGroupId, 
                                     MemberId, 
                                     CollaborationGroup.Name, 
                                     Member.Name, 
                                     NotificationFrequency 
                                 FROM CollaborationGroupMember 
                                 WHERE Id = :selectedId];
            groupMemberRecord.NotificationFrequency = changedValue;
            System.debug('groupMemberRecord  ======'+groupMemberRecord);
            update groupMemberRecord;
            updateMessage = 'Record Successfully Updated!';
        }catch(Exception ex) {
            System.debug('ex  ======'+ex.getMessage());
        }
        return updateMessage;
    }
    
    public class PickListWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        
        public PickListWrapper(String label, String val) {
            this.label = label;
            this.value = val;
        }
    }

}