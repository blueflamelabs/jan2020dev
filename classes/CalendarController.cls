public with sharing class CalendarController {

    @AuraEnabled(cacheable=true)
    public static List<Event> getAssociationEventsList(Id recordId) {
        System.debug('Inside getAssociationEventsList=====');
        System.debug('recordId======'+recordId);
        List<Event> listAssociationEvent = new List<Event>();
        Set<Id> setAssoVsWorkGrpId = new Set<Id>();
        setAssoVsWorkGrpId.add(recordId); 
        for(Working_Group__c WorkGrpRecord : [SELECT Id,
                                                     Association__c 
                                                FROM Working_Group__c
                                               WHERE Association__c = :recordId]){
        
        setAssoVsWorkGrpId.add(WorkGrpRecord.Id);
        }
        System.debug('WorkGrps--List======'+setAssoVsWorkGrpId);

        List<Event> listEventRecords = [SELECT Id,
                                               subject,
                                               whatId 
                                          FROM Event 
                                         WHERE whatId IN :setAssoVsWorkGrpId];

        System.debug('get listEventRecords ======='+listEventRecords);
        for(Event EventRecord : listEventRecords) { 
            System.debug('EventRecord========='+EventRecord); 
            listAssociationEvent.add(EventRecord);
            System.debug('EventRecord-Subject(Both Obj)========='+EventRecord.subject);                                  
        } //End for 

        System.debug('eventList========='+listAssociationEvent);       
        return listAssociationEvent;
    }

    @AuraEnabled(cacheable=true)
    public static List<Event> getWorkingGroupEventsList(Id recordId) {
        System.debug('Inside getWorkingGroupEventsList');
        System.debug('recordId======'+recordId);

        List<Event> listWorkingGroupEvent = new List<Event>();
        for(Event EventRecord : [SELECT Id,
                                        subject,
                                        whatId 
                                   FROM Event 
                                  WHERE whatId = :recordId]) {
        
        listWorkingGroupEvent.add(EventRecord);
        System.debug('EventRecord========='+EventRecord);
        System.debug('eventList========='+listWorkingGroupEvent);                                 
         
        } //End for        
        return listWorkingGroupEvent;
    
    }

    @AuraEnabled(cacheable=true)
    public static List<Event> getAllEventsList(String getRecordId) {       
    System.debug('Inside getAllEventsList=========');
    List<Event> listEvents = new List<Event>();

    Schema.DescribeSObjectResult result = Association__c.sObjectType.getDescribe();
    String AssociationkeyPrifix = result.getKeyPrefix();
    System.debug('Association-keyPrifix========='+AssociationkeyPrifix); 

    Schema.DescribeSObjectResult result1 = Working_Group__c.sObjectType.getDescribe();
    String WorkGrpkeyPrifix = result1.getKeyPrefix();
    System.debug('WorkGrp-keyPrifix========='+WorkGrpkeyPrifix);
   
        if(getRecordId != NULL) {
            if(getRecordId.startsWith(AssociationkeyPrifix)) {
                listEvents = getAssociationEventsList(getRecordId);
            }
            else if(getRecordId.startsWith(WorkGrpkeyPrifix)) {
                listEvents = getWorkingGroupEventsList(getRecordId);
            }
        }
        return listEvents; 
    }
}