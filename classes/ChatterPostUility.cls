public without sharing class ChatterPostUility {
    
    //@AuraEnabled
   /* public static List<feedItemWrapper> getFeedItemRecordsList(Id parentId) {
        
         //Variable declaration.
        List<feedItemWrapper> feedWrapperList = new List<feedItemWrapper>();
        List<FeedAttachment> feedAttList = new List<FeedAttachment>();
        Set<Id> feedAttacmentContentSet = new Set<Id>();
        Map<Id,feedItemWrapper> mapOfFeedIdVsWrapper = new Map<Id,feedItemWrapper>();
        Map<Id,List<FeedAttachment>> mapOfFeedIdVsattachment = new Map<Id,List<FeedAttachment>>();
        //List<FeedItem> feeditemList = new List<FeedItem>();
        Map<Id,User> userMap = new Map<Id,User>([Select id,Name,SmallPhotoUrl, FullPhotoUrl
                                                 FROM User]);
        //Iterate over the FeedItem List.
        for(FeedItem feeditemRecord : [SELECT Id, 
                                       body,
                                       ParentId,
                                       CreatedById,
                                       CreatedBy.Name,
                                       CreatedDate,commentcount,LikeCount,
                                       (SELECT Id, 
                                        CommentBody, 
                                        FeedItemId,
                                        InsertedBy.Name
                                        FROM FeedComments), 
                                       (SELECT Id, 
                                        FeedItemId, 
                                        InsertedById, 
                                        FeedEntityId 
                                        FROM FeedLikes),
                                       (SELECT Id, 
                                        Title,
                                        RecordId,
                                        Type, 
                                        Value 
                                        FROM FeedAttachments) 
                                       FROM FeedItem
                                       WHERE parentId = :parentId])
        {
            feedItemWrapper feedWrap = new feedItemWrapper();
            feedWrap.userRecord = userMap.get(feeditemRecord.CreatedById);
            feedWrap.feedItemWrapList.add(feeditemRecord);
            feedWrap.isChildCommentPost = false;
            //System.debug('feedWrap.feedItemWrapList  == '+feedWrap.feedItemWrapList);
            //Check FeedComments list not empty
            if(feeditemRecord.FeedComments.size() > 0) {
                for(FeedComment feedCommt : feeditemRecord.FeedComments) {
                    
                    feedWrap.feedCommentWrapList.add(feedCommt);
                }
                
                //System.debug('feedWrap.feedCommentWrapList == '+feedWrap.feedCommentWrapList);
            }
            //Check FeedLikes list not empty
            if(feeditemRecord.FeedLikes.size() > 0) {
                feedWrap.feedLikeWrapList.addAll(feeditemRecord.FeedLikes);
                //System.debug('feedWrap.feedLikeWrapList == '+feedWrap.feedLikeWrapList);
            }
            //Check FeedAttachments list not empty
            if(feeditemRecord.FeedAttachments.size() > 0 ) {
                feedWrap.feedAttachmentWrapList.addAll(feeditemRecord.FeedAttachments);
                //System.debug('feedWrap.feedAttachmentWrapList == '+feedWrap.feedAttachmentWrapList);
            }
            mapOfFeedIdVsWrapper.put(feeditemRecord.Id, feedWrap);
            System.debug('mapOfFeedIdVsWrapper map == '+mapOfFeedIdVsWrapper);
            // Add feed wrapper list.
            //feedWrapperList.add(feedWrap);
            //System.debug('feedWrapperList == '+feedWrapperList);
        }//End of for.
        
        for(Id feedId : mapOfFeedIdVsWrapper.keySet()) {
            //System.debug('feedId == '+feedId);
            for(FeedAttachment feedAttacRec : mapOfFeedIdVsWrapper.get(feedId).feedAttachmentWrapList) {
                System.debug('feedAttacRec attachment== '+feedAttacRec);
                feedAttacmentContentSet.add(feedAttacRec.RecordId);
            } //End of inner for.
        }// End of outer for.
        //System.debug('feedAttacmentContentSet feedattachment== '+feedAttacmentContentSet);
        feedAttList = [SELECT Id,
                       Title,
                       RecordId,
                       Type,FeedEntityId, 
                       Value 
                       FROM FeedAttachment
                       WHERE FeedEntityId IN :mapOfFeedIdVsWrapper.keySet()];
        for(FeedAttachment attach : feedAttList) {
            if(!mapOfFeedIdVsattachment.containsKey(attach.FeedEntityId)){
                mapOfFeedIdVsattachment.put(attach.FeedEntityId, new List<FeedAttachment>{attach});
                //System.debug('mapOf atach == '+mapOfFeedIdVsattachment);
            }else {
                List<FeedAttachment> attchFile = new List<FeedAttachment>();
                attchFile.add(attach);
                attchFile = mapOfFeedIdVsattachment.get(attach.FeedEntityId);
                //System.debug('attchFile == '+attchFile);
                mapOfFeedIdVsattachment.put(attach.FeedEntityId, attchFile);
            }
        }
        //System.debug('mapOfFeedIdVsattachment == '+mapOfFeedIdVsattachment);    
        List<ContentVersion> listofContentVersion = [SELECT Id, 
                                                     Title,
                                                     VersionData,
                                                     ContentDocumentId
                                                     FROM ContentVersion
                                                     WHERE Id IN :feedAttacmentContentSet];
        //System.debug('listofContentVersion 1111== '+listofContentVersion);
        Map<Id, List<ContentVersion>> mapOfFeedIdVsContentverList = new Map<Id, List<ContentVersion>>();
        for(ContentVersion contentVersion : listofContentVersion) {
            for(FeedAttachment attach : feedAttList) {
                if(contentVersion.Id == attach.RecordId) {
                    if(!mapOfFeedIdVsContentverList.containsKey(attach.FeedEntityId)) {
                        mapOfFeedIdVsContentverList.put(attach.FeedEntityId, new List<ContentVersion>{contentVersion});
                    }else {
                        mapOfFeedIdVsContentverList.get(attach.FeedEntityId).add(contentVersion);
                    }
                }
                
            }
        }
        //System.debug('mapOfFeedIdVsContentverList == '+mapOfFeedIdVsContentverList);
        for(Id feedId : mapOfFeedIdVsWrapper.keySet()) {
            feedItemWrapper feedWrap = new feedItemWrapper();
            feedWrap = mapOfFeedIdVsWrapper.get(feedId);
            //check feed related attachments.
            if(mapOfFeedIdVsContentverList.containsKey(feedId)) {
                for(ContentVersion contentRec : mapOfFeedIdVsContentverList.get(feedId)) {
                    //system.debug('if related feed');
                    feedWrap.contentVersionList.add(contentRec); //= listofContentVersion;
                    //System.debug('feedWrap.contentVersionList== '+feedWrap.contentVersionList);
                    //System.debug('feedWrap== '+feedWrap); 
                }//End of inner if.
            }
            feedWrapperList.add(feedWrap);
        }//End of for.
        System.debug('feedWrapperList 222== '+feedWrapperList);
        //Return wrapper list.
        return feedWrapperList;
    }
    
    public class feedItemWrapper{
        @AuraEnabled
        public List<FeedItem> feedItemWrapList = new List<FeedItem>();
        @AuraEnabled
        public Boolean isMoreComments = false;
        @AuraEnabled
        public Boolean isDisplayComments = false;
        @AuraEnabled
        public List<FeedComment> feedCommentWrapList = new List<FeedComment>();
        @AuraEnabled
        public List<FeedLike> feedLikeWrapList = new List<FeedLike>();
        @AuraEnabled
        public List<FeedAttachment> feedAttachmentWrapList = new List<FeedAttachment>();
        @AuraEnabled
        public List<ContentVersion> contentVersionList = new List<ContentVersion>();
        @AuraEnabled
        public User userRecord = new User();
        @AuraEnabled
        public Boolean isChildCommentPost;
        @AuraEnabled
        public DateTime displayPostTime = System.today();
    }*/

}