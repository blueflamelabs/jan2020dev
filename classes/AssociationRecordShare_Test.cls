@isTest
public class AssociationRecordShare_Test{
        public static testMethod void AssociationRecordShareTest(){
            
            Association__c ass = new Association__c();
            ass.Name = 'test';
            insert ass;
            
           
            Working_Group__c workgroup = new Working_Group__c();
            workgroup.Association__c = ass.id;
            insert workgroup;
            
            Association_Question__c assque = new Association_Question__c();
            assque.Association__c = ass.id;
            insert assque;
            
                   
           Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId();
           Account acc = new Account();
           acc.RecordTypeId = AccRecordTypeId;
           acc.Name = 'Test Parent' ;
           acc.Company_Association_Membership__c =ass.id ;
           acc.Association__c = ass.id;
           insert acc;
           
             
            User userRecord = new User(Alias = 'new123',  
                                              EmailEncodingKey='UTF-8',
                                              LastName='Testing',
                                              LanguageLocaleKey='en_US', 
                                              LocaleSidKey='en_US', 
                                              ProfileId = UserInfo.getProfileId(),
                                              Email = 'test1.liveston@asdf.com',
                                              Username = 'test.liveston@asdf.com', 
                                              TimeZoneSidKey='Asia/Dubai'
                                              );
            insert userRecord ;
            
            User userRecordUpdate = new User(Alias = 'new1231',  
                                              EmailEncodingKey='UTF-8',
                                              LastName='Testings',
                                              LanguageLocaleKey='en_US', 
                                              LocaleSidKey='en_US', 
                                              ProfileId = UserInfo.getProfileId(),
                                              Email = 'test1.livestons@asdf.com',
                                              Username = 'test.livestons@asdf.com', 
                                              TimeZoneSidKey='Asia/Dubai'
                                              );
            insert userRecordUpdate ;
           
            Association_Team__c assteam = new Association_Team__c();
            assteam.User__c = userRecord.id;
            assteam.Association__c = ass.id;
            insert assteam;
            assteam.User__c = userRecordUpdate.id;
            update assteam;
            delete assteam;
            
            Lead objLead = new Lead();
            objLead.LastName = 'Te';
            objLead.Association__c = ass.id;
            insert objLead;
            
            Additional_Contacts__c objAddcon = new Additional_Contacts__c();
            objAddcon.Last_Name__c = 'test';
            objAddcon.Lead__c = objLead.id;
            objAddcon.Name = 'te';
            insert objAddcon;
            
            Member_Answer__c memAns = new Member_Answer__c();
            memAns.Lead__c = objLead.id;
           // memAns.Member__c =con.id; 
            memAns.Association_Question__c = assque.id;
            memAns.Answer__c = 'Test';
            insert memAns;
            
            Invoice__c invoice = new Invoice__c();
            invoice.Lead__c = objLead.id;
            invoice.Additional_Contact__c = objAddcon.id;
            invoice.Association__c = ass.id;
            insert invoice;
           
    }  
    
     public static testMethod void AssociationRecordShareTest1(){
            
            Association__c ass = new Association__c();
            ass.Name = 'test';
            insert ass;
                    
           Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
           Account acc = new Account();
           acc.RecordTypeId = AccRecordTypeId;
           acc.LastName = 'Test Parent' ;
           acc.Company_Association_Membership__c =ass.id ;
           acc.Association__c = ass.id;
           insert acc;
           
             
            User userRecord = new User(Alias = 'new123',  
                                              EmailEncodingKey='UTF-8',
                                              LastName='Testing',
                                              LanguageLocaleKey='en_US', 
                                              LocaleSidKey='en_US', 
                                              ProfileId = UserInfo.getProfileId(),
                                              Email = 'test1.liveston@asdf.com',
                                              Username = 'test.liveston@asdf.com', 
                                              TimeZoneSidKey='Asia/Dubai'
                                              );
            insert userRecord ;
            
            User userRecordUpdate = new User(Alias = 'new1231',  
                                              EmailEncodingKey='UTF-8',
                                              LastName='Testings',
                                              LanguageLocaleKey='en_US', 
                                              LocaleSidKey='en_US', 
                                              ProfileId = UserInfo.getProfileId(),
                                              Email = 'test1.livestons@asdf.com',
                                              Username = 'test.livestons@asdf.com', 
                                              TimeZoneSidKey='Asia/Dubai'
                                              );
            insert userRecordUpdate ;
            
            
            Association_Team__c assteam = new Association_Team__c();
            assteam.User__c = userRecord.id;
            assteam.Association__c = ass.id;
            insert assteam;
            assteam.User__c = userRecordUpdate.id;
            update assteam;
            
            Working_Group__c workgroup = new Working_Group__c();
            workgroup.Association__c = ass.id;
            insert workgroup;
            
            Association_Question__c assque = new Association_Question__c();
            assque.Association__c = ass.id;
            insert assque;
            
            Lead objLead = new Lead();
            objLead.LastName = 'Te';
            objLead.Association__c = ass.id;
            insert objLead;
            
            Additional_Contacts__c objAddcon = new Additional_Contacts__c();
            objAddcon.Last_Name__c = 'te';
            objAddcon.Lead__c = objLead.id;
            objAddcon.Name = 'te';
            insert objAddcon;
            
            Member_Answer__c memAns = new Member_Answer__c();
            memAns.Lead__c = objLead.id;
           // memAns.Member__c =con.id; 
            memAns.Association_Question__c = assque.id;
            memAns.Answer__c = 'Test';
            insert memAns;
            
            Invoice__c invoice = new Invoice__c();
            invoice.Lead__c = objLead.id;
            invoice.Additional_Contact__c = objAddcon.id;
            invoice.Association__c = ass.id;
            insert invoice;
            
            User userRecordUpdateforLead = new User(Alias = 'new12314',  
                                              EmailEncodingKey='UTF-8',
                                              LastName='Testings',
                                              LanguageLocaleKey='en_US', 
                                              LocaleSidKey='en_US', 
                                              ProfileId = UserInfo.getProfileId(),
                                              Email = 'test21.livestons@asdf.com',
                                              Username = 'tes23t.livestons@asdf.com', 
                                              TimeZoneSidKey='Asia/Dubai'
                                              );
            insert userRecordUpdateforLead ;
            assteam.User__c = userRecordUpdateforLead.id;
            update assteam;
           
    }  
        
        
}