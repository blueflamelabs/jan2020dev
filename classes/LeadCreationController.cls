public class LeadCreationController {
    @AuraEnabled
    Public static Lead saveLeadRecord(Lead rec){
        upsert rec;
        return rec;
    }
}