public class AssociationQuestionController {
    
    @AuraEnabled
    public static List<MasterQuestWrapper> getMasterQuestionRec() {
        List<MasterQuestWrapper> questWrapperList = new List<MasterQuestWrapper>();
        for(Master_Question__c mastObj : [SELECT Id,
                                          Master_Question__c,
                                          Category__c,
                                          Field_Data_Type__c,
                                          LastModifiedDate,
                                          (SELECT Id,Name,Value__c,Override_Sort__c FROM Question_Answer_Values__r Order by Override_Sort__c,Value__c ASC nulls last
                                          )
                                          FROM Master_Question__c 
                                          WHERE Status__c = 'Published']){
                                              list<Master_Question_Answer_Values__c> listOfMasterQuestionAns = new list<Master_Question_Answer_Values__c>();
                                              MasterQuestWrapper MasterQuestWrapper = new MasterQuestWrapper();
                                              map<string,Master_Question_Answer_Values__c> mapOfNullOrderValue = new map<string,Master_Question_Answer_Values__c>();
                                              for(Master_Question_Answer_Values__c  Question_Answer: mastObj.Question_Answer_Values__r){
                                                  if(Question_Answer.Override_Sort__c != null){
                                                      listOfMasterQuestionAns.add(Question_Answer);
                                                  }else{
                                                      mapOfNullOrderValue.put(Question_Answer.value__c,Question_Answer);
                                                  }
                                              }
                                              list<string> listOfValues = new list<string>();
                                              listOfValues.addall(mapOfNullOrderValue.keyset());
                                              listOfValues.sort();
                                              for(string str : listOfValues){
                                                  listOfMasterQuestionAns.add(mapOfNullOrderValue.get(str));
                                              }
                                              MasterQuestWrapper.masterQuest = mastObj;
                                              MasterQuestWrapper.masterQuestAnsValue = listOfMasterQuestionAns;
                                              questWrapperList.add(MasterQuestWrapper);
                                          }
        return questWrapperList;
    }
    
    @AuraEnabled
    public static List<String> getPicklistvalues() {
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Master_Question__c.Category__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        options.add('All');
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
    }
    
    @AuraEnabled
    public static List<MasterQuestAnsWrapper> getAssociationQuestionRec(String associationId) {
        List<MasterQuestAnsWrapper> relatedQuestWrapperList = new List<MasterQuestAnsWrapper>();
        Set<Id> setOfAssociationMember = new Set<Id>();
        
        for(Association_Question__c associationQObj : [SELECT Id,Association__c,
                                                       Displayed_Question__c,
                                                       Required__c,
                                                       View_in_Member_Directory__c,
                                                       Master_Question__c,
                                                       Active__c,
                                                       (SELECT Id,
                                                        Value__c
                                                        FROM Association_Question_Answer_Values__r),
                                                       (SELECT Id, 
                                                        Association_Question__c
                                                        FROM Member_Answers__r)
                                                       FROM Association_Question__c
                                                       WHERE Association__c =: associationId ]) {
                                                           MasterQuestAnsWrapper masterQuestAns = new MasterQuestAnsWrapper();
                                                           if(!associationQObj.Member_Answers__r.isEmpty()){
                                                               System.debug('--Memeber Answers--');
                                                               masterQuestAns.checkEye = true;
                                                           }else {
                                                               masterQuestAns.checkEye = false;
                                                               masterQuestAns.isDeleteIcon = true;
                                                           }
                                                           list<MasterQuestionAnswerValueWrapper> associationQuestAnsValueList = new list<MasterQuestionAnswerValueWrapper>();
                                                           
                                                           for(Association_Question_Answer_Value__c assocationQuesTionAns : associationQObj.Association_Question_Answer_Values__r ){
                                                               MasterQuestionAnswerValueWrapper associationQuestAnsValue = new MasterQuestionAnswerValueWrapper();
                                                               associationQuestAnsValue.value = assocationQuesTionAns.value__c;
                                                               associationQuestAnsValue.isSelected = true;
                                                               associationQuestAnsValue.associationQuestAnsValue = assocationQuesTionAns;
                                                               associationQuestAnsValueList.add(associationQuestAnsValue);
                                                           }
                                                           masterQuestAns.masterQuestAns = associationQObj;
                                                           masterQuestAns.associationQuestAnsValueList = associationQuestAnsValueList;
                                                           relatedQuestWrapperList.add(masterQuestAns);
                                                       }
        return relatedQuestWrapperList;
    }
    @AuraEnabled
    public static string saveMasterAns(string jsonString,string deleteJsonString){
        list<MasterQuestAnsWrapper> MasterQuestAnsWrapperList = (list<MasterQuestAnsWrapper>)JSON.deserializeStrict(jsonString,list<MasterQuestAnsWrapper>.class);
        system.debug(MasterQuestAnsWrapperList);
        list<Association_Question__c> AssociationQuestionList = new list<Association_Question__c>();
        for(MasterQuestAnsWrapper MasterQuestAns : MasterQuestAnsWrapperList){
            AssociationQuestionList.add(MasterQuestAns.masterQuestAns);
        }
        upsert AssociationQuestionList;
        list<Association_Question_Answer_Value__c> AssociationQuestionAnsListInsert = new list<Association_Question_Answer_Value__c>();
        list<Association_Question_Answer_Value__c> AssociationQuestionAnsListDel = new list<Association_Question_Answer_Value__c>();
        integer i = 0;
        for(MasterQuestAnsWrapper MasterQuestAns : MasterQuestAnsWrapperList){
            for(MasterQuestionAnswerValueWrapper MasterQuestAnsWrp : MasterQuestAns.associationQuestAnsValueList){
                if(MasterQuestAnsWrp.isSelected){
                    if(MasterQuestAnsWrp.associationQuestAnsValue.Association_Question__c == null)
                        MasterQuestAnsWrp.associationQuestAnsValue.Association_Question__c  = AssociationQuestionList[i].Id;
                    MasterQuestAnsWrp.associationQuestAnsValue.name = MasterQuestAnsWrp.value;
                    AssociationQuestionAnsListInsert.add(MasterQuestAnsWrp.associationQuestAnsValue);
                }else if(MasterQuestAnsWrp.associationQuestAnsValue.Id != null){
                    AssociationQuestionAnsListDel.add(MasterQuestAnsWrp.associationQuestAnsValue); 
                }
            }
            i++;
        }
        upsert AssociationQuestionAnsListInsert;
        delete AssociationQuestionAnsListDel;
        list<MasterQuestAnsWrapper> MasterQuestAnsWrapperListDel = (list<MasterQuestAnsWrapper>)JSON.deserializeStrict(deleteJsonString,list<MasterQuestAnsWrapper>.class);
        list<Association_Question__c> AssociationQuestionListDel = new list<Association_Question__c>();
        for(MasterQuestAnsWrapper MasterQuestAns : MasterQuestAnsWrapperListDel){
            AssociationQuestionListDel.add(MasterQuestAns.masterQuestAns);
        }
        delete AssociationQuestionListDel;
        return null;
    }
    public class MasterQuestAnsWrapper{
        @AuraEnabled
        public List<String> dataTypeValue;
        @AuraEnabled
        public Association_Question__c masterQuestAns;
        @AuraEnabled
        public Boolean checkAccordion = false;
        @AuraEnabled
        public Boolean checkEye = false;
        @AuraEnabled
        public Boolean isDeleteIcon = false;
        @AuraEnabled
        public Boolean isDelete = false;
        @AuraEnabled
        public list<MasterQuestionAnswerValueWrapper> associationQuestAnsValueList;        
        @AuraEnabled
        public Boolean isAssAnsValue = false;
        
    }
    public class MasterQuestionAnswerValueWrapper{
        @AuraEnabled
        public Association_Question_Answer_Value__c  associationQuestAnsValue;
        @AuraEnabled
        public string value;
        @AuraEnabled
        public boolean isSelected = false;
    }
    public class MasterQuestWrapper{
        
        @AuraEnabled
        public Master_Question__c masterQuest;
        @AuraEnabled
        public list<Master_Question_Answer_Values__c> masterQuestAnsValue;
        @AuraEnabled
        public boolean checkAccordion = false;
        
    }
}