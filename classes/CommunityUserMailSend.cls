/*
    Created By - Manas
    Created Date - 8/11/2019
    Description - Class use for Mail send to Community User
*/
public class CommunityUserMailSend {
      /**
        * Method Name : MailSendToUser
        * Parameters  : param1: List<User>
        * Description : Used to filter the user records.
        * Created By  : Manas 
        * Created On  : 8/11/2019
     **/
    public static void MailSendToUser(List<User> objUserList){
    UserTriggerHandler.isUserTriggerRunned = true;
        System.debug('######### '+objUserList);
        Map<Id,String> objConIdMailId = new Map<Id,String>();
        for(User objUser : objUserList){
            if(String.isNotBlank(objUser.ContactId)){
                objConIdMailId.put(objUser.ContactId,objUser.Email);
            }
        }
        if(objConIdMailId.size() > 0){
            System.debug('############# objConIdMailId %%%%%%%% '+objConIdMailId);
            Set<Id> objAssId = new Set<Id>();
            Set<Id> objMemId = new Set<Id>();
            Map<String,List<Contact>> objAssIdConId = new Map<String,List<Contact>>();
            List<Contact> objConList = [Select Id,Name,Account.Association__c,Account.Member_Type__c,Account.Association__r.Renewal_Sender_Email_Address__c from Contact where Id IN : objConIdMailId.keySet() and Account.Association__c != null];
            for(Contact objCon : objConList){
                objAssId.add(objCon.Account.Association__c);
                objMemId.add(objCon.Account.Member_Type__c);
                if(!objAssIdConId.keySet().Contains(objCon.Account.Association__c+''+objCon.Account.Member_Type__c)){
                    objAssIdConId.put(objCon.Account.Association__c+''+objCon.Account.Member_Type__c, new List<Contact>{objCon});    
                }else{
                    List<Contact> objConLists = objAssIdConId.get(objCon.Account.Association__c+''+objCon.Account.Member_Type__c);
                    objConLists.add(objCon);
                    objAssIdConId.put(objCon.Account.Association__c+''+objCon.Account.Member_Type__c, objConLists); 
                }
            }
            if(objAssIdConId.size() > 0){
                Map<String,String> objMapEmailtoFromEmail = new Map<String,String>();
                Map<String,String> objMapEmailTemname = new Map<String,String>();
                Set<Id> objCongaEmailId = new Set<Id>();
                List<Welcome_Emails__c> objWelComeEmailList = [Select Id,Name,Member_Type__c,Email_Template_Names__c,Association__c,Association__r.Renewal_Sender_Email_Address__c 
                                                               from Welcome_Emails__c where Association__c IN : objAssId and Member_Type__c IN : objMemId and Email_Template_Names__c != null];
                for(Welcome_Emails__c objWel : objWelComeEmailList){
                    List<Contact> objConAssList = objAssIdConId.get(objWel.Association__c+''+objWel.Member_Type__c);
                    for(Contact objCon : objConAssList){
                        String Email = objConIdMailId.get(objCon.Id);
                        objMapEmailTemname.put(objCon.Id, objWel.Email_Template_Names__c);
                        objCongaEmailId.add(objWel.Email_Template_Names__c);
                        objMapEmailtoFromEmail.put(objCon.Id, objCon.Account.Association__r.Renewal_Sender_Email_Address__c);
                    }
                }
                if(objMapEmailTemname.size() > 0){
                    Map<String,APXTConga4__Conga_Email_Template__c> objMapCongaEmailTempate = new Map<String,APXTConga4__Conga_Email_Template__c>([Select Id,Conga_Email_Template_Name__c,APXTConga4__Description__c,APXTConga4__HTMLBody__c,
                                                                                                                                                   APXTConga4__Name__c,APXTConga4__Subject__c,APXTConga4__Template_Group__c,
                                                                                                                                                   APXTConga4__TextBody__c,APXTConga4__Is_Body_Attachment__c from APXTConga4__Conga_Email_Template__c where Id IN: objCongaEmailId]);
                   try{
                        SendMail(objMapEmailTemname,objMapCongaEmailTempate,objMapEmailtoFromEmail,false);                    
                    }catch(Exception ex){
                        String ErrorMessaage = ex.getMessage();
                        if(ErrorMessaage.Contains('UNVERIFIED_SENDER_ADDRESS, Organization-Wide Email Address has not be verified for use')){
                            SendMail(objMapEmailTemname,objMapCongaEmailTempate,objMapEmailtoFromEmail,true);    
                        }
                    }
                }
            }
        }
    }
    public static void SendMail(Map<String,String> objMapEmailTemname,Map<String,APXTConga4__Conga_Email_Template__c> objMapCongaEmailTempate,Map<String,String> objMapEmailtoFromEmail,Boolean isDefaultSenderMail){
        Map<String,OrgWideEmailAddress> objMapEmailIdORW = new Map<String,OrgWideEmailAddress>();
        List<OrgWideEmailAddress> owaList = [select id, DisplayName, Address from OrgWideEmailAddress];
        for(OrgWideEmailAddress objORW : owaList){
            objMapEmailIdORW.put(objORW.Address,objORW);
        }
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        List<User> objUserList = [Select Id,Email,ContactId from user where ContactId IN : objMapEmailTemname.keySet()];
        for(User objUser : objUserList){
            if(objMapCongaEmailTempate.keySet().Contains(objMapEmailTemname.get(objUser.ContactId))){
                APXTConga4__Conga_Email_Template__c objEmailTemplate = objMapCongaEmailTempate.get(objMapEmailTemname.get(objUser.ContactId));
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setSubject(objEmailTemplate.APXTConga4__Subject__c);
                mail.setHtmlBody(objEmailTemplate.APXTConga4__HTMLBody__c);
                mail.setToAddresses(new String[]{objUser.Email});
                if(objMapEmailIdORW.size() > 0 && objMapEmailtoFromEmail.keySet().Contains(objUser.ContactId) && isDefaultSenderMail == false){
                    String FromEmail = objMapEmailtoFromEmail.get(objUser.ContactId);
                    if(objMapEmailIdORW.keySet().Contains(FromEmail)){
                        mail.setOrgWideEmailAddressId(objMapEmailIdORW.get(FromEmail).Id);
                    }else{
                        mail.setOrgWideEmailAddressId(objMapEmailIdORW.get('vforceadmin@virtualinc.com').Id);
                    }
                }else{
                    mail.setOrgWideEmailAddressId(objMapEmailIdORW.get('vforceadmin@virtualinc.com').Id);
                }
                mail.settargetObjectId(objUser.id);
                mail.setSaveAsActivity(false);
                allmsg.add(mail);
            }
        }
        if(allmsg.size() > 0){
            Messaging.sendEmail(allmsg);
        }
    }
}