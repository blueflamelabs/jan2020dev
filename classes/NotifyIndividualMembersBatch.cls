/********************************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                 ModifiedDate  ModifiedBy              Description
*       1.2      230719         VennScience_BFL_Amruta    130819        VennScience_BFL_Amruta  This Batch is built to send email notification to Individual Members 
                               
*********************************************************************************************************************************************************************/   
global class NotifyIndividualMembersBatch implements Database.Batchable<Sobject> {
    // Variable Declarations
    public static final String MEMBERCATEGORY = 'Primary Contact';
    public Id individualMemId;
    
    // Parameterised Constructor
    public NotifyIndividualMembersBatch(Id recordId) {
        individualMemId = recordId;
    }
    // Constructor
    public NotifyIndividualMembersBatch() {
    
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        Individual_Members_Email_Alerts_Setting__c objCustomSetting = new Individual_Members_Email_Alerts_Setting__c();
        objCustomSetting = NotifyIndividualMembersBatchHandler.getCustomSettingValues();
        if(objCustomSetting == null || String.isBlank(objCustomSetting.Account_Record_Type_Name__c) ||
           String.isBlank(objCustomSetting.Account_Member_Category__c)) {
           return Database.getQueryLocator('SELECT Id FROM Account LIMIT 0');
        }
        String query = '';
        String accRecordTypeName = objCustomSetting.Account_Record_Type_Name__c;
        String accMemberCategory = objCustomSetting.Account_Member_Category__c;
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
                                   .get(accRecordTypeName)
                                   .getRecordTypeId();
        //System.debug('devRecordTypeId==='+devRecordTypeId);
        // T-00510 - V1.2 - 130819 - VennScience_BFL_Amruta - Added condition to run for specific record when batch is executed from
        // detail page button on Individual Member
        if(individualMemId != null) {
            query = 'SELECT Id,'
                    +'Name,'
                    +'First_Renewal_Alert_Email_Template__c,'
                    +'First_Renewal_Alert_Send_Days__c,'
                    +'Second_Renewal_Alert_Email_Template__c,'
                    +'Second_Renewal_Alert_Send_Days__c,'
                    +'Third_Renewal_Alert_Email_Template__c,'
                    +'Third_Renewal_Alert_Send_Days__c,'
                    +'Second_Expiration_Alert_Email_Template__c,'
                    +'Second_Expiration_Alert_Send_Days__c,'
                    +'Final_Termination_Notice_Email_Template__c,'
                    +'Final_Termination_Notice_Send_Days__c,'
                    +'First_Expiration_Alert_Email_Template__c,'
                    +'First_Expiration_Alert_Send_Days__c,'
                    +'Email_Sender_Address__c,'
                    +'(SELECT Id,Name,Next_Renewal_Date__c,Expiration_Date__c,PersonEmail,Association__c,PersonContactId,Association_Account__c,Association__r.OwnerId FROM Accounts__r'
                    +' WHERE RecordTypeId = :devRecordTypeId' 
                    +' AND Member_Category__pc = :accMemberCategory'
                    +' AND Do_Not_Send_Automated_Emails__c = false'
                    +' AND Id = :individualMemId)'
                    +'FROM Association__c';
        } else {
            // T-00510 - V1.1 - 010819 - VennScience_BFL_Amruta - Added fields in Association query
            query = 'SELECT Id,'
                    +'Name,'
                    +'First_Renewal_Alert_Email_Template__c,'
                    +'First_Renewal_Alert_Send_Days__c,'
                    +'Second_Renewal_Alert_Email_Template__c,'
                    +'Second_Renewal_Alert_Send_Days__c,'
                    +'Third_Renewal_Alert_Email_Template__c,'
                    +'Third_Renewal_Alert_Send_Days__c,'
                    +'Second_Expiration_Alert_Email_Template__c,'
                    +'Second_Expiration_Alert_Send_Days__c,'
                    +'Final_Termination_Notice_Email_Template__c,'
                    +'Final_Termination_Notice_Send_Days__c,'
                    +'First_Expiration_Alert_Email_Template__c,'
                    +'First_Expiration_Alert_Send_Days__c,'
                    +'Email_Sender_Address__c,'
                    +'(SELECT Id,Name,Next_Renewal_Date__c,Expiration_Date__c,PersonEmail,Association__c,PersonContactId,Association_Account__c,Association__r.OwnerId FROM Accounts__r'
                    +' WHERE RecordTypeId = :devRecordTypeId' 
                    +' AND Member_Category__pc = :accMemberCategory'
                    +' AND Do_Not_Send_Automated_Emails__c = false)'
                    +'FROM Association__c';
        }
        //System.debug('query========'+query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Association__c> listAssociationRecords) { 
        
        // 250719 - T-00408 - VennScience_BFL_Amruta - Call handler 
        NotifyIndividualMembersBatchHandler.sendEmailAlertsToMembers(listAssociationRecords);  
    }
    global void finish(Database.BatchableContext bc) {   
    }   
}