@isTest
public class LeadCreationControllerTest {
    
    public static testMethod void createLeadTest(){
        Association__c a = new Association__c(Name='test');
        insert a;
        Lead l = new Lead(LastName='test',Company='testcompany', Association__c=a.Id);
        Lead savedRecord = LeadCreationController.saveLeadRecord(l);
        system.assertEquals(1, [Select Id FROM Lead WHERE Id =: savedRecord.Id].size());
    }
    
}