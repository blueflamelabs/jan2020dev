public class AssociationRecordShareCTRL{
    public static void recordShare(List<Association_Team__c> objAssTeamList){
         Set<Id> objListAssociationId = new Set<Id>();
         Map<Id,Id> objMapAssIdUserId = new Map<Id,Id>();
         for(Association_Team__c objAssTeam : objAssTeamList){
             objListAssociationId.add(objAssTeam.Association__c);
             objMapAssIdUserId.put(objAssTeam.Association__c,objAssTeam.User__c);
         }
         if(objListAssociationId.size() > 0){
             List<Association__Share> objListAssociationShare = new List<Association__Share>();
             List<Association__c> objAssList = [Select id from Association__c where Id IN : objListAssociationId];
             for(Association__c objAss : objAssList){
                   Association__Share objAssociationShare = new Association__Share();
                   objAssociationShare.ParentId = objAss.id;
                   objAssociationShare.AccessLevel = 'edit';
                   objAssociationShare.UserOrGroupId = objMapAssIdUserId.get(objAss.id);
                   objAssociationShare.RowCause = Schema.Association__Share.RowCause.Association_Team__c;
                   objListAssociationShare.add(objAssociationShare);
             }
             if(objListAssociationShare.size() > 0 ){
                 insert objListAssociationShare;
             }
             
             List<Working_Group__Share> objListWorkingGroupShare = new List<Working_Group__Share>();
             List<Working_Group__c> objgroupList = [Select id,Association__c from Working_Group__c where Association__c IN : objListAssociationId];
             for(Working_Group__c objWorkingGroup : objgroupList){
                   Working_Group__Share objWorkingGroupShare = new Working_Group__Share();
                   objWorkingGroupShare.ParentId = objWorkingGroup.id;
                   objWorkingGroupShare.AccessLevel = 'edit';
                   objWorkingGroupShare.UserOrGroupId = objMapAssIdUserId.get(objWorkingGroup.Association__c);
                   objWorkingGroupShare.RowCause = Schema.Working_Group__Share.RowCause.Association_Team__c;
                   objListWorkingGroupShare.add(objWorkingGroupShare);
             }
             if(objListWorkingGroupShare.size() > 0 ){
                 insert objListWorkingGroupShare;
             }
             
             List<Association_Question__Share> objListQuestionShare = new List<Association_Question__Share>();
             List<Association_Question__c> objQuestionList = [Select id,Association__c from Association_Question__c where Association__c IN : objListAssociationId];
             for(Association_Question__c objQuestion : objQuestionList){
                   Association_Question__Share objAssQuShare = new Association_Question__Share();
                   objAssQuShare.ParentId = objQuestion.id;
                   objAssQuShare.AccessLevel = 'edit';
                   objAssQuShare.UserOrGroupId = objMapAssIdUserId.get(objQuestion.Association__c);
                   objAssQuShare.RowCause = Schema.Association_Question__Share.RowCause.Association_Team__c;
                   objListQuestionShare.add(objAssQuShare);
             }
             if(objListQuestionShare.size() > 0 ){
                 insert objListQuestionShare;
             }
            
            Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId();
            List<AccountShare> objListAccountCompanyShare = new List<AccountShare>();
            List<Account> objAccCompanyList = [Select id,RecordType.Name,Company_Association_Membership__c,Association__c from Account where 
                                                 Company_Association_Membership__c IN : objListAssociationId and RecordTypeId =:AccRecordTypeId];
             for(Account objAcc : objAccCompanyList){
                   AccountShare objAccShare = new AccountShare();
                   objAccShare.AccountId = objAcc.id;
                   objAccShare.AccountAccessLevel = 'edit';
                   objAccShare.OpportunityAccessLevel = 'Read';
                   objAccShare.UserOrGroupId = objMapAssIdUserId.get(objAcc.Company_Association_Membership__c);
                   //objAccShare.RowCause = Schema.AccountShare.RowCause.Team;
                   objListAccountCompanyShare.add(objAccShare);
             }
             if(objListAccountCompanyShare.size() > 0 ){
                 try{
                     DataBase.insert(objListAccountCompanyShare,false);
                 }catch(Exception ex){
                     System.debug(ex.getMessage());
                 }
             } 
             
             Id AccRecordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
             List<AccountShare> objListAccountAssociationShare = new List<AccountShare>();
             List<Account> objAccAssociationList = [Select id,Company_Association_Membership__c,Association__c from Account where 
                                                     Association__c IN : objListAssociationId and RecordTypeId =:AccRecordTypeIdAcc];
             for(Account objAcc : objAccAssociationList){
                   AccountShare objAccShare = new AccountShare();
                   objAccShare.AccountId = objAcc.id;
                   objAccShare.AccountAccessLevel = 'edit';
                   objAccShare.OpportunityAccessLevel = 'edit';
                   objAccShare.UserOrGroupId = objMapAssIdUserId.get(objAcc.Association__c);
                   //objAccShare.RowCause = Schema.AccountShare.RowCause.Team;
                   objListAccountAssociationShare.add(objAccShare);
             }
             if(objListAccountAssociationShare.size() > 0 ){
                 try{
                     DataBase.insert(objListAccountAssociationShare,false);
                 }catch(Exception ex){
                     System.debug(ex.getMessage());
                 }
             } 
             
             Map<Id,Lead> objLeadMap = New Map<Id,Lead>([Select Id,Association__c from Lead where Association__c IN : objListAssociationId]);
             List<Additional_Contacts__c> objConList = [Select Id,Lead__r.Association__c From Additional_Contacts__c where Lead__c IN : objLeadMap.KeySet() and Lead__r.Association__c != null];
             List<Additional_Contacts__Share> objListAddContactAssociationShare = new List<Additional_Contacts__Share>();
             for(Additional_Contacts__c objCon : objConList){
                   Additional_Contacts__Share objAddContactShare = new Additional_Contacts__Share();
                   objAddContactShare.ParentId = objCon.id;
                   objAddContactShare.AccessLevel = 'edit';
                   objAddContactShare.UserOrGroupId = objMapAssIdUserId.get(objCon.Lead__r.Association__c);
                   objAddContactShare.RowCause = Schema.Additional_Contacts__Share.RowCause.Association_Team__c;
                   objListAddContactAssociationShare.add(objAddContactShare);
             }
             if(objListAddContactAssociationShare.size() > 0){
                 insert objListAddContactAssociationShare;
             }
             
             Map<Id,Association_Question__c> objQuestionMap = new Map<Id,Association_Question__c>([Select id,Association__c from Association_Question__c where Association__c IN : objListAssociationId]);
             List<Member_Answer__c> objMemAns = [Select  Id,Association_Question__r.Association__c from Member_Answer__c where Association_Question__c IN : objQuestionMap.keySet() and Association_Question__r.Association__c != null];
             List<Member_Answer__Share> objListAddMemberAssociationShare = new List<Member_Answer__Share>();
             for(Member_Answer__c objMem : objMemAns){
                   Member_Answer__Share objAddMemShare = new Member_Answer__Share();
                   objAddMemShare.ParentId = objMem.id;
                   objAddMemShare.AccessLevel = 'edit';
                   objAddMemShare.UserOrGroupId = objMapAssIdUserId.get(objMem.Association_Question__r.Association__c);
                   objAddMemShare.RowCause = Schema.Member_Answer__Share.RowCause.Association_Team__c;
                   objListAddMemberAssociationShare.add(objAddMemShare);
             }
             if(objListAddMemberAssociationShare.size() > 0){
                 insert objListAddMemberAssociationShare;
             }
             
             List<Invoice__c> objListInvoice = [Select id,Association__c from Invoice__c where Association__c IN : objListAssociationId];
             List<Invoice__Share> objListInvoiceAssociationShare = new List<Invoice__Share>();
             for(Invoice__c objInvoice : objListInvoice){
                   Invoice__Share objInShare = new Invoice__Share();
                   objInShare.ParentId = objInvoice.id;
                   objInShare.AccessLevel = 'edit';
                   objInShare.UserOrGroupId = objMapAssIdUserId.get(objInvoice.Association__c);
                   objInShare.RowCause = Schema.Invoice__Share.RowCause.Association_Team__c;
                   objListInvoiceAssociationShare.add(objInShare);
             }
             if(objListInvoiceAssociationShare.size() > 0){
                 insert objListInvoiceAssociationShare;
             }
             
            List<Lead> objLeadList = [Select Id,Association__c from Lead where Association__c IN : objListAssociationId];
            List<LeadShare> objListLeadAssociationShare = new List<LeadShare>();
            for(Lead objLead : objLeadList){
               LeadShare objLeShare = new LeadShare();
               objLeShare.LeadId = objLead.Id;
               objLeShare.LeadAccessLevel = 'edit';
               objLeShare.UserOrGroupId = objMapAssIdUserId.get(objLead.Association__c);
               objListLeadAssociationShare.add(objLeShare);
            }
            if(objListLeadAssociationShare.size() > 0){
                System.debug('###@@@ objListLeadAssociationShare '+objListLeadAssociationShare);
                //insert objListLeadAssociationShare;
                Database.insert(objListLeadAssociationShare,false);
            }
         }
     }
     
     Public Static void AddLeadRecord(List<Lead> objLeadList){
         Set<Id> objAssId = new Set<Id>();
         for(Lead objLead : objLeadList){
             objAssId.add(objLead.Association__c);
         }
         if(objAssId.size() > 0){
             Set<Id> objUserId = new Set<Id>();
             List<Association_Team__c> objAssTeam = [Select id,User__c from Association_Team__c where Association__c IN : objAssId];
             for(Association_Team__c objTeam : objAssTeam){
                 objUserId.add(objTeam.User__c);
             }
             if(objUserId.size() > 0){
                 List<LeadShare> objListLeadAssociationShare = new List<LeadShare>();
                 for(Lead objLead : objLeadList){
                     for(Id objUser : objUserId){
                       LeadShare objInShare = new LeadShare();
                       objInShare.LeadId = objLead.id;
                       objInShare.LeadAccessLevel = 'edit';
                       objInShare.UserOrGroupId = objUser;
                       objListLeadAssociationShare.add(objInShare);
                      }
                 }
                 if(objListLeadAssociationShare.size() > 0){
                     Database.insert(objListLeadAssociationShare,false);
                     //insert objListLeadAssociationShare;
                 }
             }
         }
     }
     
     Public Static void AddContactRecord(List<Additional_Contacts__c> objListAddContact){
         Set<Id> objLeadId = new Set<Id>();
         for(Additional_Contacts__c objAdCon : objListAddContact){
             objLeadId.add(objAdCon.Lead__c);
         }
         if(objLeadId.size() > 0){
             Set<Id> objAssId = new Set<Id>();
             List<Lead> objLeadList = [Select id,Association__c from Lead where Id IN : objLeadId and Association__c != null];
             for(Lead objLead : objLeadList){
                 objAssId.add(objLead.Association__c);
             }
             if(objAssId.size() > 0){
                 Set<Id> objUserId = new Set<Id>();
                 List<Association_Team__c> objAssTeam = [Select id,User__c from Association_Team__c where Association__c IN : objAssId];
                 for(Association_Team__c objTeam : objAssTeam){
                     objUserId.add(objTeam.User__c);
                 }
                 if(objUserId.size() > 0){
                     List<Additional_Contacts__Share> objListAddContactAssociationShare = new List<Additional_Contacts__Share>();
                     for(Additional_Contacts__c objCon : objListAddContact){
                         for(Id objUser : objUserId){
                           Additional_Contacts__Share objAddContactShare = new Additional_Contacts__Share();
                           objAddContactShare.ParentId = objCon.id;
                           objAddContactShare.AccessLevel = 'edit';
                           objAddContactShare.UserOrGroupId = objUser;
                           objAddContactShare.RowCause = Schema.Additional_Contacts__Share.RowCause.Association_Team__c;
                           objListAddContactAssociationShare.add(objAddContactShare);
                         }  
                     }
                     if(objListAddContactAssociationShare.size() > 0){
                         insert objListAddContactAssociationShare;
                     }
                 } 
             }
         }
     }
     
     Public Static void AddMemberAnsRecord(List<Member_Answer__c> objListMemberAns){
         Set<Id> objAssQuId = new Set<Id>();
         for(Member_Answer__c objAdCon : objListMemberAns){
             objAssQuId.add(objAdCon.Association_Question__c);
         }
         if(objAssQuId.size() > 0){
             List<Association_Question__c> objAssQuList = [Select id,Association__c from Association_Question__c where Id IN : objAssQuId and Association__c != null];
             Set<Id> objAssId = new Set<Id>();
             for(Association_Question__c objAssQu : objAssQuList){
                 objAssId.add(objAssQu.Association__c);
             }
             if(objAssId.size() > 0 ){
                 Set<Id> objUserId = new Set<Id>();
                 List<Association_Team__c> objAssTeam = [Select id,User__c from Association_Team__c where Association__c IN : objAssId];
                 for(Association_Team__c objTeam : objAssTeam){
                     objUserId.add(objTeam.User__c);
                 }
                 if(objUserId.size() > 0){
                     List<Member_Answer__Share> objListAddMemberAssociationShare = new List<Member_Answer__Share>();
                     for(Member_Answer__c objMem : objListMemberAns){
                         for(Id objUser : objUserId){
                           Member_Answer__Share objAddMemShare = new Member_Answer__Share();
                           objAddMemShare.ParentId = objMem.id;
                           objAddMemShare.AccessLevel = 'edit';
                           objAddMemShare.UserOrGroupId = objUser;
                           objAddMemShare.RowCause = Schema.Member_Answer__Share.RowCause.Association_Team__c;
                           objListAddMemberAssociationShare.add(objAddMemShare);
                         }
                     }
                     if(objListAddMemberAssociationShare.size() > 0){
                         insert objListAddMemberAssociationShare;
                     }
                 }
             }
         }
     }
     
     Public Static void InvoiceRecord(List<Invoice__c> objListInvoice){
         Set<Id> objAssId = new Set<Id>();
         for(Invoice__c objInvo : objListInvoice){
             objAssId.add(objInvo.Association__c);
         }
         Set<Id> objUserId = new Set<Id>();
         List<Association_Team__c> objAssTeam = [Select id,User__c from Association_Team__c where Association__c IN : objAssId];
         for(Association_Team__c objTeam : objAssTeam){
             objUserId.add(objTeam.User__c);
         }
         if(objUserId.Size() > 0){
             List<Invoice__Share> objListInvoiceAssociationShare = new List<Invoice__Share>();
             for(Invoice__c objInvoice : objListInvoice){
                 for(Id objUser : objUserId){
                   Invoice__Share objInShare = new Invoice__Share();
                   objInShare.ParentId = objInvoice.id;
                   objInShare.AccessLevel = 'edit';
                   objInShare.UserOrGroupId = objUser;
                   objInShare.RowCause = Schema.Invoice__Share.RowCause.Association_Team__c;
                   objListInvoiceAssociationShare.add(objInShare);
                  }
             }
             if(objListInvoiceAssociationShare.size() > 0){
                 insert objListInvoiceAssociationShare;
             }
         }
     }
     
     
     
     
     Public Static void WorkingGroupRecord(List<Working_Group__c> objListWorkGroup){
         Set<Id> objAssId = new Set<Id>();
         for(Working_Group__c objWorGroup : objListWorkGroup){
             objAssId.add(objWorGroup.Association__c);
         }
         Set<Id> objUserId = new Set<Id>();
         List<Association_Team__c> objAssTeam = [Select id,User__c from Association_Team__c where Association__c IN : objAssId];
         for(Association_Team__c objTeam : objAssTeam){
             objUserId.add(objTeam.User__c);
         }
         if(objUserId.Size() > 0){
             List<Working_Group__Share> objListWorkingGroupShare = new List<Working_Group__Share>();
             for(Working_Group__c objWorGroup : objListWorkGroup){
                 for(Id objUser : objUserId){
                     Working_Group__Share objWorkingGroupShare = new Working_Group__Share();
                     objWorkingGroupShare.ParentId = objWorGroup.id;
                     objWorkingGroupShare.AccessLevel = 'edit';
                     objWorkingGroupShare.UserOrGroupId = objUser; 
                     objWorkingGroupShare.RowCause = Schema.Working_Group__Share.RowCause.Association_Team__c;
                     objListWorkingGroupShare.add(objWorkingGroupShare);
                 }
             }
             if(objListWorkingGroupShare.size() > 0){
                 insert objListWorkingGroupShare;
             }
         }
     }
     
     Public Static void AssQuestionRecord(List<Association_Question__c> objAssQuList){
         Set<Id> objAssId = new Set<Id>();
         for(Association_Question__c objQues : objAssQuList){
             objAssId.add(objQues.Association__c);
         }
         Set<Id> objUserId = new Set<Id>();
         List<Association_Team__c> objAssTeam = [Select id,User__c from Association_Team__c where Association__c IN : objAssId];
         for(Association_Team__c objTeam : objAssTeam){
             objUserId.add(objTeam.User__c);
         }
         if(objUserId.size() > 0){
             List<Association_Question__Share> objListQuestionShare = new List<Association_Question__Share>();
             for(Association_Question__c objQues : objAssQuList){
                 for(Id objuser : objUserId){
                   Association_Question__Share objAssQuShare = new Association_Question__Share();
                   objAssQuShare.ParentId = objQues.id;
                   objAssQuShare.AccessLevel = 'edit';
                   objAssQuShare.UserOrGroupId = objuser;
                   objAssQuShare.RowCause = Schema.Association_Question__Share.RowCause.Association_Team__c;
                   objListQuestionShare.add(objAssQuShare);
                 }
             }
             if(objListQuestionShare.size() > 0){
                 insert objListQuestionShare;
             }
             
         }
     }
    public Static void AccoutRecordShare(List<Account> objListAcc){
        Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId();
        Id AccRecordTypeIdAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
        Set<Id> objAssId = new Set<Id>();
        Set<Id> objAccounId = new Set<Id>();
        for(Account objAcc : objListAcc){
            if(objAcc.RecordTypeId == AccRecordTypeId || objAcc.RecordTypeId == AccRecordTypeIdAcc){
                objAssId.add(objAcc.Company_Association_Membership__c);
                objAssId.add(objAcc.Association__c);
                objAccounId.add(objAcc.id);
            }
        }
        if(objAssId.size() > 0){
             Set<Id> objUserId = new Set<Id>();
             List<Association_Team__c> objAssTeam = [Select id,User__c from Association_Team__c where Association__c IN : objAssId];
             for(Association_Team__c objTeam : objAssTeam){
                 objUserId.add(objTeam.User__c);
             }
             if(objUserId.size() > 0){
                 List<AccountShare> objListAccountAssociationShare = new List<AccountShare>();
                 for(Id objAccID : objAccounId){
                     for(Id objuser : objUserId){
                       AccountShare objAccShare = new AccountShare();
                       objAccShare.AccountId = objAccID;
                       objAccShare.AccountAccessLevel = 'edit';
                       objAccShare.OpportunityAccessLevel = 'edit';
                       objAccShare.UserOrGroupId = objuser;
                       //objAccShare.RowCause = Schema.AccountShare.RowCause.Team;
                       objListAccountAssociationShare.add(objAccShare);
                     }
                 }
                 if(objListAccountAssociationShare.size() > 0){
                     try{
                         DataBase.insert(objListAccountAssociationShare,false);
                     }catch(Exception ex){
                         System.debug(ex.getMessage());
                     }
                 }
             }
        }
        
    } 
    public Static void RecordDelete(List<Association_Team__c> objListAssTeam){
        Set<Id> objUserId = new Set<Id>();
        for(Association_Team__c objAssTeam : objListAssTeam){
            objUserId.add(objAssTeam.User__c);
        }
        List<Association__Share> oldAssSharing = [SELECT Id FROM Association__Share WHERE RowCause = :Schema.Association__Share.RowCause.Association_Team__c AND UserOrGroupId = :objUserId];
        delete oldAssSharing;
        List<Working_Group__Share> oldwoGroupSharing = [SELECT Id FROM Working_Group__Share WHERE RowCause = :Schema.Working_Group__Share.RowCause.Association_Team__c AND UserOrGroupId = :objUserId];
        delete oldwoGroupSharing;
        List<Association_Question__Share> oldAssQuestionSharing = [SELECT Id FROM Association_Question__Share WHERE RowCause = :Schema.Association_Question__Share.RowCause.Association_Team__c AND UserOrGroupId = :objUserId];
        delete oldAssQuestionSharing;
        List<AccountShare> oldAccSharing = [SELECT Id FROM AccountShare WHERE RowCause = :Schema.AccountShare.RowCause.Manual AND UserOrGroupId = :objUserId];
        delete oldAccSharing;
        List<Additional_Contacts__Share> oldAssConSharing = [SELECT Id FROM Additional_Contacts__Share WHERE RowCause = :Schema.Additional_Contacts__Share.RowCause.Association_Team__c AND UserOrGroupId = :objUserId];
        delete oldAssConSharing;
        List<Member_Answer__Share> oldAssMemSharing = [SELECT Id FROM Member_Answer__Share WHERE RowCause = :Schema.Member_Answer__Share.RowCause.Association_Team__c AND UserOrGroupId = :objUserId];
        delete oldAssMemSharing; 
        List<Invoice__Share> oldAssInvSharing = [SELECT Id FROM Invoice__Share WHERE RowCause = :Schema.Invoice__Share.RowCause.Association_Team__c AND UserOrGroupId = :objUserId];
        delete oldAssInvSharing;    
        List<LeadShare> oldLeadSharing = [SELECT Id FROM LeadShare WHERE RowCause = :Schema.LeadShare.RowCause.Manual AND UserOrGroupId = :objUserId];
        delete oldLeadSharing;   
        
    } 
}