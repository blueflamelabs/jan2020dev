/*
    Test Class Use for -: Trigger - AccountRecordShare,Class - FormAssemblyIdUpdateAccount
    Ticket Number - T-000102
    Created Date -: 9/30/2019
*/
@isTest
public class FormAssemblyIdUpdateAccount_Test{
    public static testMethod void FormAssemblyIdUpdate(){
       Association__c assRecord = new Association__c();
       assRecord.Name = 'test';
       insert assRecord;
       
       Association__c assRecd = new Association__c();
       assRecd.Name = 'test';
       insert assRecd;
        
       Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Corporate Member').getRecordTypeId();
       Account acc = new Account();
       acc.RecordTypeId = AccRecordTypeId;
       acc.Name = 'Test Parent' ;
       acc.Company_Association_Membership__c =assRecord.id;
       insert acc;
       acc.Company_Association_Membership__c =assRecd.id;
       update acc;
       
       Id AccRecordTypeIdMem = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Individual Member').getRecordTypeId();
       Account accMember = new Account();
       accMember.RecordTypeId = AccRecordTypeIdMem;
       accMember.LastName = 'Test Parent' ;
       accMember.Association__c = assRecord.id;
       insert accMember;
       
    }
}