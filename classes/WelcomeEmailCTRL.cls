/*
    Created By - Manas
    Created Date - 26/11/2019
    Description - handle the Duplicate Welcome Email as par the Association and Member Type
*/
public class WelcomeEmailCTRL{
 public static void DuplicateWelcomEmail(List<Welcome_Emails__c> objMapWelEmail){
        Set<Id> objAssId = new Set<Id>();
        Set<Id> objMemId = new Set<Id>();
        for(Welcome_Emails__c objWel : objMapWelEmail){
            objAssId.add(objWel.Association__c);
            objMemId.add(objWel.Member_Type__c);
        }
        Map<String,Welcome_Emails__c> objMapAsMemToWelcome = new Map<String,Welcome_Emails__c>();
        List<Welcome_Emails__c> objWelcomeList = [Select Id,Name,Association__c,Member_Type__c from Welcome_Emails__c where Association__c IN : objAssId 
                                                    and Member_Type__c IN : objMemId];
        for(Welcome_Emails__c objWelEmail : objWelcomeList){
            objMapAsMemToWelcome.put(objWelEmail.Association__c+''+objWelEmail.Member_Type__c,objWelEmail);
        }
        for(Welcome_Emails__c objWel : objMapWelEmail){
            if(objMapAsMemToWelcome.keyset().Contains(objWel.Association__c+''+objWel.Member_Type__c)){
                objWel.addError('Duplicate : Same type of record available Association and Member Type');
            }
        }
    }
}