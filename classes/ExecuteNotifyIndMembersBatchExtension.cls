/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy                  ModifiedDate  ModifiedBy              Description
*     1.0        120819         VennScience_BFL_Amruta                                           This is the extension class for ExecuteNotifyIndividualMembersBatch visualforce page.
*     
*     1.1                                                  190919        VennScience_BFL_Amruta  Pass parent Association Id of Account to batch class
*     1.2                                                  18102019      VennScience_BFL_Amruta  Updated the newly created batch name which is to be called
**********************************************************************************************************************************************************/
public class ExecuteNotifyIndMembersBatchExtension {

    Account getAccRecord;
    Account fetchAccRecord;
    public ExecuteNotifyIndMembersBatchExtension(ApexPages.StandardController controller) {
        getAccRecord = new Account();
        fetchAccRecord = new Account();
        getAccRecord = (Account)controller.getRecord();
    }
    public PageReference executeBatch() {
        PageReference pg;
        if(getAccRecord.Id != null) {
            // T-00686 - 190919 - VennScience_BFL_Amruta - Fetch parent Association for current Account and pass the same to batch class
            fetchAccRecord  = [SELECT Id,
                                      Association__c
                                 FROM Account
                                WHERE Id = :getAccRecord.Id];
            // T-000091 - 18102019 - VennScience_BFL_Amruta - Modified the batch name which is to be called
            if(fetchAccRecord.Association__c != null) {
                // Database.executeBatch(new NotifyIndividualMembersBatch(getAccRecord.Id,fetchAccRecord.Association__c));
                Database.executeBatch(new AutomatedRenewalsCongaBatch(fetchAccRecord.Id,fetchAccRecord.Association__c));
            } // End of if
            pg = new PageReference('/'+getAccRecord.Id);
            pg.setRedirect(true);
            //return pg;  
        }
        return pg;
    }
}