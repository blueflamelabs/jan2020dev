/*********************************************************************************************************************************************************
*     Version    CreateDate/Modified     CreatedBy                    Description
*       1.0         011019               VennScience_BFL_Monali       This class is built to upload files for all users. 
*     
**********************************************************************************************************************************************************/
public without sharing class FilesController {
    Public Class FileWrapper {
        @AuraEnabled
        public String title;
        @AuraEnabled
        public String parentId;
        @AuraEnabled
        public String source;
        @AuraEnabled
        public String fileId;
        @AuraEnabled
        public String ownerId;
        @AuraEnabled
        public String ownerName;
        @AuraEnabled
        public DateTime lastModified;
        @AuraEnabled
        public Integer contentSize;
        /*@AuraEnabled
        public String caseMoniterId;
        @AuraEnabled
        public Boolean fileReviewd;*/
        @AuraEnabled
        public String fileType;
        @AuraEnabled
        public String filePrivacy;
        
    }
        
     /**
    * Method Name : getFiles
    * Parameters  : 
    * Description : This method is used to get related files.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 011019
    **/
    @AuraEnabled
    public static FilesControllerWrapper getFiles(String parentId) {
        system.debug(parentId);
        List<ContentDocumentLink> files =  [SELECT ContentDocumentId, 
                                            	   LinkedEntityId  
                                            FROM ContentDocumentLink 
                                            WHERE LinkedEntityId = :parentId];
        
        List<String> fileIds = new List<String>();
        
        for(ContentDocumentLink cdl : files){
            fileIds.add(cdl.ContentDocumentId);
        }
        
        List<ContentVersion> files2 = [SELECT Id, 
                                       		  Title, 
                                       		  FileType, 
                                       		  FileExtension, 
                                       		  Description,
                                      		  OwnerId,
                                       		  Owner.Name,
                                       		  LastModifiedDate, 
                                       		  ContentSize, 
                                       		  ContentDocumentId, 
                                       		  IsLatest, 
                                       		  ContentModifiedDate 
                                       FROM ContentVersion 
                                       WHERE IsLatest = True 
                                       AND ContentDocumentId in :fileIds 
                                       ORDER BY ContentModifiedDate DESC];
        
        FilesControllerWrapper fwrap = new FilesControllerWrapper();
        fwrap.isCommunityUser = (getPathPrefix() != '' ? true : false);
        fwrap.files = files2;

        return fwrap;
    }
    
     /**
    * Method Name : saveFiles
    * Parameters  : List<ContentVersion> , Id
    * Description : This method is used to update the files.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 011019
    **/
    @AuraEnabled
    public static void saveFiles(List<ContentVersion> files, Id caseId) {
        update files;      
    } 
    
    /**
    * Method Name : getPathPrefix
    * Parameters  : 
    * Description : This method is used to get the community url.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 011019
    **/
    @AuraEnabled
    public static String getPathPrefix () {
        Id net_id = Network.getNetworkId();
        if (net_id  != null) {
            return [SELECT Id, 
                           UrlPathPrefix
                      FROM Network
                     WHERE Id = :net_id].UrlPathPrefix;
        }
        return '';
    }
    
   	/**
    * Method Name : getAllFiles
    * Parameters  : List<String>
    * Description : This method is used to get the all fields of uploaded file.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 011019
    **/
    @AuraEnabled
    public static List<ContentVersion> getAllFiles(List<String> fileIds) {
        return [SELECT Id, 
                	   Title,
                	   Name__c,
                	   Folder__c,
                	   FileType,
                	   FileExtension, 
                	   Description,
                	   ContentSize,
                	   OwnerId,Owner.Name,
                	   LastModifiedDate,
                	   ContentDocumentId, 
                	   IsLatest, 
                	   ContentModifiedDate, 
                	   SharingPrivacy
                FROM ContentVersion 
                WHERE IsLatest = True 
                AND ContentDocumentId in :fileIds 
                ORDER BY ContentModifiedDate DESC];

    }
        
        /**
    * Method Name : getAllUploadedFiles
    * Parameters  : List<String>
    * Description : This method is used to get the all fields of uploaded file.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 261219
    **/
    @AuraEnabled
    public static List<ContentVersion> getAllUploadedFiles(String parentId, List<String> fileIds) {
        List<ContentVersion> listContentVersion = [SELECT Id, 
                                                           Title,
                                                           Name__c,
                                                           FileType,
                                                           FileExtension, 
                                                           Description,
                                                           ContentSize,
                                                           OwnerId,Owner.Name,
                                                           LastModifiedDate,
                                                           ContentDocumentId, 
                                                           IsLatest, 
                                                           ContentModifiedDate, 
                                                           SharingPrivacy
                                                    FROM ContentVersion 
                                                    WHERE IsLatest = True 
                                                    AND ContentDocumentId in :fileIds 
                                                    ORDER BY ContentModifiedDate DESC];
        return listContentVersion;

    }
    
    /**
    * Method Name : getAllFolder
    * Parameters  : String
    * Description : This method is used to get the all folders.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 261219
    **/
    @AuraEnabled
    public static List<Folder__c> getAllFolder() {
        List<Folder__c> folderList = [SELECT Id,
                                             Name,
                                      		 Parent_Object_Id__c
                                      	FROM Folder__c];
                                       //WHERE Parent_Object_Id__c = :parentId];
        return folderList;

    }
    
    
    /**
    * Method Name : getFilesClone
    * Parameters  : String
    * Description : This method is used to get the files for community user and internal users.
    * Created By  : VennScience_BFL_Monali
    * Created On  : 011019
    **/
    @AuraEnabled
    public static FilesControllerWrapper getFilesClone(String parentId) {
        System.debug('parentId========='+parentId);
        system.debug('======getFilesClone==========');
        List<ContentDocumentLink> ContentDocumentLinkList = [SELECT ContentDocumentId, 
                                           		  LinkedEntityId,
                                                             Visibility
                                           FROM ContentDocumentLink 
                                           WHERE LinkedEntityId = :parentId];
        
        List<String> fileIds = new List<String>();
        system.debug('======ContentDocumentLinkList==========' + ContentDocumentLinkList);
        for(ContentDocumentLink ContentDocumentLinkRec : ContentDocumentLinkList ) {
            system.debug('======ContentDocumentLinkRec.Visibility==========' + ContentDocumentLinkRec.Visibility);
        }
        for(ContentDocumentLink cdl : ContentDocumentLinkList){
            fileIds.add(cdl.ContentDocumentId);
        }
        System.debug('fileIds========='+fileIds);
        Boolean isInternalUser = true;
        isInternalUser = (getPathPrefix() == '' ? true : false);
        System.debug('isInternalUser========='+isInternalUser);
       
        List<ContentVersion> files2 = new List<ContentVersion>();
        FilesControllerWrapper fwrap = new FilesControllerWrapper();
        if(isInternalUser) {
            fwrap.isCommunityUser = false;
            // Check if user is internal user then display all files
            files2 = [SELECT Id, 
                             Title, 
                             FileType, 
                             FileExtension, 
                             Description,
                             OwnerId,
                             Owner.Name,
                             LastModifiedDate, 
                      		 LastModifiedById,
                      		 LastModifiedBy.Name,
                             ContentSize,
                             ContentDocumentId, 
                             IsLatest,toLabel(SharingPrivacy), 
                             ContentModifiedDate 
                      FROM ContentVersion 
                      WHERE IsLatest = True 
                      AND ContentDocumentId IN :fileIds //Id in :caseMoniterMap.keySet() 
                    ORDER BY ContentModifiedDate DESC];
        } else {
            fwrap.isCommunityUser = true;
            // Check if logged in user is community user then display only those files whose sharing option is set
            // to visible to all
            files2 = [SELECT Id, 
                             Title, 
                             FileType, 
                             FileExtension, 
                             Description,
                             OwnerId,
                             Owner.Name,
                             LastModifiedDate, 
                      		 LastModifiedById,
                      		 LastModifiedBy.Name,
                             ContentSize,
                             ContentDocumentId, 
                             IsLatest, toLabel(SharingPrivacy),
                             ContentModifiedDate 
                      FROM ContentVersion 
                      WHERE IsLatest = True AND ContentDocumentId IN :fileIds
                      AND (SharingPrivacy != 'P' OR OwnerId = :UserInfo.getUserId())
                    ORDER BY ContentModifiedDate DESC];
        } // End of if
        
        //fwrap.isCommunityUser = (getPathPrefix() != '' ? true : false);
        System.debug('isCommunityUser========='+fwrap.isCommunityUser);
        fwrap.files = files2;
        return fwrap;
    }
    
    /**
    * Method Name : getPublisherUser
    * Description : Used to get the current user access.
    * Created By  : VennScience_BFL_Monali 
    * Created On  : 271119
    **/
    @AuraEnabled
    public static Boolean getPublisherUser (String parentId) {
        // Variable declaration.
        Boolean isUserPublishAccess = false;
        
        Map<String, Schema.SObjectType> workingGroup = Schema.getGlobalDescribe() ; 
        system.debug('==>m is==>'+workingGroup); 
        Schema.SObjectType workingObject = workingGroup.get('Working_Group__c') ; 
        system.debug('==>Sobject Type is ==>'+workingObject); 
        Schema.DescribeSObjectResult workingGroupPrefix = workingObject.getDescribe() ; 
        String keyPrefix = workingGroupPrefix.getKeyPrefix();
        system.debug('==>keyPrefix Type is ==>'+keyPrefix);
        
        if(parentId.startsWith(keyPrefix)) {
            // Fetch working group record.
            Working_Group__c workingGroupRecord = [SELECT Id,
                                                   Chatter_Group__c
                                                   FROM Working_Group__c
                                                   WHERE Id = :parentId];
            System.debug('workingGroupRecord === '+workingGroupRecord);
            // Fetch current logged in user record.
            User currentUser = [SELECT  Id, 
                                Name,
                                FirstName, 
                                LastName,
                                IsActive,
                                ContactId
                                FROM User 
                                WHERE Id =: UserInfo.getUserId()];
            System.debug('currentUser === '+currentUser);
            // Fetch group member record.
            List<Group_Member__c> groupMemberRecordList = [SELECT Id,
                                                           Grou_Member__c,
                                                           Working_Group__c,
                                                           Working_Group_Role__c
                                                           FROM Group_Member__c
                                                           WHERE Working_Group__c = :parentId
                                                           AND Working_Group__r.Chatter_Group__c != null];
            System.debug('groupMemberRecordList === '+groupMemberRecordList);
            for(Group_Member__c groupMemberRecord : groupMemberRecordList) {
                if(currentUser.ContactId != null && groupMemberRecord.Grou_Member__c == currentUser.ContactId 
                   && groupMemberRecord.Working_Group_Role__c != 'Limited Member') {
                       isUserPublishAccess = true;
                   }
                else if(currentUser.ContactId != null && String.isEmpty(groupMemberRecord.Grou_Member__c) 
                        && groupMemberRecord.Working_Group_Role__c == 'Limited Member') {
                            isUserPublishAccess = false;
                        }
                else if(String.isEmpty(currentUser.ContactId)) {
                    isUserPublishAccess = true;
                }
                else if(String.isEmpty(currentUser.ContactId) && !String.isEmpty(groupMemberRecord.Grou_Member__c)) {
                    isUserPublishAccess = false;
                }
            }
            System.debug('isUserPublishAccess === '+isUserPublishAccess);
        }else {
            isUserPublishAccess = true;
        }
        return isUserPublishAccess;
    }
    
    public Class FilesControllerWrapper {
        @AuraEnabled
        public Boolean isCommunityUser;
        @AuraEnabled
        public List<ContentVersion> files;
    } 
}