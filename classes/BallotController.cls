public class BallotController {
    @AuraEnabled
    public static List<Ballot__c> getBallots(String selectVal ) {
        String CommunityUserId = UserInfo.getUserId();
        List<User> objUserList = [Select Id,ContactId from User where Id =:CommunityUserId and ContactId != null];
        String UserId='';
        if(objUserList.size() > 0)
            UserId = objUserList[0].ContactId;
        datetime dt = System.Now();
        List<Ballot__c> Ballots;
        if(selectVal=='Next 10 Days'){
            datetime dtNextTen=dt.addDays(10);
            Ballots=[SELECT Id, Name, Company__c, Vote__c, Status__c, Vote_Title__c, Vote__r.Working_Group__r.Name,Vote__r.Association__r.Name, Vote__r.Vote_Start__c, Vote__r.Vote_End__c, Cast_Vote__c	 FROM Ballot__c 
                     where Working_Group_Member__r.Grou_Member__c =:UserId and 
                     Vote__r.Status__c = 'Active' and 
                     Vote__r.Vote_Start__c >: dt and  Vote__r.Vote_Start__c <:dtNextTen and
                     Status__c = 'Pending'];
            
            
        } 
        return Ballots;
    }
    @AuraEnabled
    public static List<Ballot__c> filterRecord(String selectVal) {
        String CommunityUserId = UserInfo.getUserId();
        List<User> objUserList = [Select Id,ContactId from User where Id =:CommunityUserId and ContactId != null];
        String UserId='';
        if(objUserList.size() > 0)
            UserId = objUserList[0].ContactId;
        datetime dt = System.Now();
        List<Ballot__c> ballots;
        if(selectVal=='Next 10 Days'){
            datetime dtNextTen=dt.addDays(10);
            ballots = [SELECT Id, Name, Company__c, Vote__c, Status__c, Vote_Title__c, Vote__r.Working_Group__r.Name,Vote__r.Association__r.Name, Vote__r.Vote_Start__c, Vote__r.Vote_End__c, Cast_Vote__c	 FROM Ballot__c 
                       where Working_Group_Member__r.Grou_Member__c =:UserId and 
                       Vote__r.Status__c = 'Active' and 
                       Vote__r.Vote_Start__c >: dt and  Vote__r.Vote_Start__c <:dtNextTen and
                       Status__c = 'Pending'];
        }else if(selectVal=='Next 30 Days'){
            datetime dtNextThrty=dt.addDays(30);
            ballots = [SELECT Id, Name, Company__c, Vote__c, Status__c, Vote_Title__c, Vote__r.Working_Group__r.Name,Vote__r.Association__r.Name, Vote__r.Vote_Start__c, Vote__r.Vote_End__c, Cast_Vote__c	 FROM Ballot__c 
                       where Working_Group_Member__r.Grou_Member__c =:UserId and 
                       Vote__r.Status__c = 'Active' and 
                       Vote__r.Vote_Start__c >: dt and  Vote__r.Vote_Start__c <:dtNextThrty and
                       Status__c = 'Pending'];  
            
        }else if(selectVal=='Next 60 Days'){
            datetime dtNextSixty=dt.addDays(60);
            ballots = [SELECT Id, Name, Company__c, Vote__c, Status__c, Vote_Title__c, Vote__r.Working_Group__r.Name,Vote__r.Association__r.Name, Vote__r.Vote_Start__c, Vote__r.Vote_End__c, Cast_Vote__c	 FROM Ballot__c 
                       where Working_Group_Member__r.Grou_Member__c =:UserId and 
                       Vote__r.Status__c = 'Active' and 
                       Vote__r.Vote_Start__c >: dt and  Vote__r.Vote_Start__c <: dtNextSixty and
                       Status__c = 'Pending'];  
            
        }                           
        return Ballots; 
    }
}