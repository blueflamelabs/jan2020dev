/**
    * Method Name : sendRejectiontNotificationToGroupChair
    * Parameters  : param1: List<Group_Member__c>
    * Description : Used to send new member Request Rejection Notification To Working Group's Chair.
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 171219
    **/
public class GroupMemberNotificationHandler {
    
    public static void sendRejectiontNotificationToGroupChair(List<Group_Member__c> newGrpMemList) {
        Set<Id> setGrpMemId = new Set<Id>();
        Set<Id> setContactId = new Set<Id>();
        Map<Id,String> mapContactIdVSUserEmail = new Map<Id,String>();
        List<Messaging.SingleEmailMessage> mailList =  new List<Messaging.SingleEmailMessage>();
        
        for(Group_Member__c grpMemRecord : newGrpMemList) {
            System.debug('Working_Group Id===' + grpMemRecord.Working_Group__c);
            if(String.isNotBlank(grpMemRecord.Rejection_Reason__c)) {
                 setGrpMemId.add(grpMemRecord.Id);
                 setContactId.add(grpMemRecord.Grou_Member__c);
            }
        }
        for(User userRecords : [SELECT Id,
                                       ContactId,
                                	   Email
                                  FROM User 
                                 WHERE ContactId IN : setContactId]) {
           
        	mapContactIdVSUserEmail.put(userRecords.ContactId,userRecords.Email);                                                                           
        }
        
        for(Group_Member__c GrpMemberRecord : [SELECT Id,
                                                      Grou_Member__c,
                                                      Working_Group__r.Association__r.Renewal_Sender_Email_Address__c 
                                                 FROM Group_Member__c 
                                                WHERE Working_Group__r.Association__c != NULL
                                                  AND Id IN : setGrpMemId]) {
            
            
            List<String> sendTo = new List<String>();
            //sendTo.add();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if(!mapContactIdVSUserEmail.isEmpty() && mapContactIdVSUserEmail.containsKey(GrpMemberRecord.Grou_Member__c)) {
            	sendTo.add(mapContactIdVSUserEmail.get(GrpMemberRecord.Grou_Member__c));
                mail.setToAddresses(sendTo);    
            } // End of inner if
            mail.setSubject(mailSubject());
            mail.setHtmlBody(mailBody());
            // Add into master mail list
            mailList.add(mail);
        } // End of for
        //Send emails
        System.debug('mailList======='+mailList);
        if(!mailList.isEmpty()) {
            System.debug('====Inside send email====');
            // Added try catch to handle send email related exceptions
            try {
                Messaging.sendEmail(mailList);
                System.debug('Group Request Rejection notification email sent');
            } catch(Exception e) {
                System.debug('Error occurred while sending Group Request Rejection notificatio email:'+e.getMessage());
            } // End of try-catch block
        } // End of if
    }
    /**
    * Method Name : mailSubject
    * Parameters  : 
    * Description : This method is used to built email Subject for group request rejection notification email
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 171219
    **/
    public static String mailSubject(){
        String subject = 'Group Request Rejection Notification';
        return subject;
    }
    /**
    * Method Name : mailBody
    * Parameters  : param1: Set<String>
    * Description : This method is used to built email Body for group request notification email
    * Created By  : VennScience_BFL_Amruta 
    * Created On  : 171219
    **/
    public static String mailBody() {
        
        String body = '';
        
        body = 'Hi ';
        System.debug('body=========='+body);
        return body;
    }
}